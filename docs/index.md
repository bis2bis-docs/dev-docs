Hello welcome!

This is a magento 1 documentation, converted from the original documentation made available by the Magento team in the repository: [devdocs](https://github.com/magento/devdocs-m1)

For corrections and collaboration, request a merge in the repository: [Magento 1 Docs](https://gitlab.com/macoaureb2b/dev-docs)


Welcome to the home page for Magento 1.x documentation for installation,
configuration, developers, patches, and more. Here you\'ll find articles
that were formerly located on
`http://magentocommerce.com/knowledge-base`.

For information you don\'t find here---including Release Notes---see
[the User Guide page](https://magento.com/resources/technical).

### [Table of Contents](#table-of-contents)

-   [Magento 1.x system
    requirements](../system-requirements.html)
-   Release documentation, including installation and upgrade
    -   [Magento Community Edition (CE) 1.8 and Enterprise Edition (EE) 1.13 Documentation Home](ce18-ee113-home.html)
    -   [Magento Community Edition (CE) 1.9 and Enterprise Edition (EE) 1.14 Documentation Home](ce19-ee114-home.html)
-   Other guides
    -   [Using Redis With Magento Community Edition (CE) or Enterprise Edition (EE)](ce18-ee113/using_redis.html)
    -   [How to Extend the Magento REST API to Use Coupon Auto Generation](other/ht_extend_magento_rest_api.html)
    -   [Magento for Developers (8-part series)](magefordev/mage-for-dev-1.html)
-   Magento 1.x API
    -   [REST API](api/rest-api-index.html)
    -   [SOAP API](api/soap-api-index.html)
-   Other documentation
 - [How to Apply and Revert Patches](other/ht_install-patches.html)
 - [Discover credit card validation issue: Magento EE
    1.9.1.1---1.13.1.0 and CE 1.4.2.0---1.8.1.0](other/discover-card-validation.html)
    - [Patches for Magento Enterprise Edition (EE) Versions 1.10--1.12](other/ee_connect_patches.html)
    - [Error Using Payflow with Magento Enterprise Edition (EE) 1.12.0.2](other/payflow.html)
    - [Getting the PHP 5.4 patch for Magento Enterprise Edition (EE)
    and Community Edition (CE)](other/php5.4_patch.html)
    - [Resolving a Remote Code Execution
    Exploit](other/remote-code-exploit.html)
    - [How to Install and Configure the Solr Search Engine With Magento Enterprise Edition (EE)](other/ht_magento-solr.html)
    - [Information About Enterprise Edition (EE) Patches for Apache Solr](other/solr-ee-patches.html)
    
