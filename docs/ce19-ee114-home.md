# Magento Community Edition (CE) 1.9 and Enterprise Edition (EE) 1.14 Documentation Home
---

### [Table of Contents](#table-of-contents)

-   [Welcome](#welcome)
-   Release Notes
    -   [Magento Community Edition (CE) Release Notes (1.9 and
        later)](/ce19-ee114/ce1.9_release-notes.html)
    -   [Magento Enterprise Edition (EE) Release Notes (1.14 and
        later)](/ce19-ee114/ee1.14_release-notes.html)
-   Developer guides
    -   [Responsive Email Developer\'s Guide for Magento EE 1.14.1 and
        Magento CE 1.9.1](/ce19-ee114/RWD_responsive_emails.html)
    -   [Magento Community Edition (CE) 1.9 and Magento Enterprise
        Edition (EE) 1.14 Responsive Web Design Developer\'s
        Guide](/ce19-ee114/RWD_dev-guide.html)
    -   [Creating an iOS App for Magento
        MobileConnect](/ce19-ee114/ios-instrux.html)
-   Installation
    -   [Installing and Upgrading to Magento Community Edition (CE) and
        Magento Enterprise Edition (EE)](/install/installing.html)
    -   [Before You Install Magento: Recommended File System Ownership
        and Privileges](/install/installer-privileges_before.html)
    -   [After You Install Magento: Recommended File System Ownership
        and Privileges](/install/installer-privileges_after.html)
    -   [Installing Sample Data for Magento Community Edition
        (CE)](/ce18-ee113/ht_magento-ce-sample.data.html)
-   Upgrade: [Upgrading to and Verifying Magento Community Edition (CE)
    and Enterprise Edition (EE)---Part
    1](/install/installing_upgrade_landing.html)
    

## Welcome

Welcome to the documentation home page for the Community Edition (CE)
1.9 and Magento Enterprise Edition (EE) 1.14 and releases! Let\'s start
out by telling you a little bit about them.

Your ability to create captivating online experiences just got even
better. Magento Enterprise Edition 1.14.1, now available for download
from My Account, features powerful new merchandising tools and
responsive design capabilities that will help you engage customers and
stand out from the competition. You'll also enjoy better performance,
faster technical issue resolution, and deeper insights to drive your
business forward.

The new releases helps you:

-   _EE only_. Merchandise product categories like never before with a
    new drag-and-drop tool and the ability to set up rules to
    automatically assign products to categories.
-   Boost conversion with swatches that present richer product
    information to shoppers on category, search result, and product
    detail pages.
-   Optimize sales on any device with an enhanced responsive design
    reference theme that now also includes responsive emails.
-   Get a faster, more secure site with support for MySQL 5.6 and PHP
    5.5.
-   _EE only_. Diagnose technical issues faster using a new support tool
    that provides comprehensive information about a Magento
    installation.
-   _EE only_. Empower larger teams by enabling many administrators to
    make product updates at the same time.
-   Access deeper shopper insights now that Magento supports Google
    Universal Analytics.
-   Have an even better Magento experience with more than 70 other
    product improvements.
    
