---
sidebar: auto
---



# Ubuntu: Installing Required Prerequisite Software for Magento CE 1.8 Magento EE 1.13 (or Later)

---

### [Contents](#contents)

-   [Overview](#overview)
-   [Updating System Software](#update-system)
-   [Apache](#apache)
-   [nginx](nginx.html)
-   [PHP](#php)
-   [MySQL](#mysql)
-   [Next Steps](#next)
    

## Overview

This article discusses how to install required prerequisite software for
Ubuntu. You must complete these tasks before you install Magento CE 1.8
or later or Magento EE 1.13 or later.

Before you continue, make sure you familiarize yourself with the
installation process discussed in
[Prerequisites](installing_install.html#prereq).

![note](~@assets/icon-note.png) **Note**:
You must install system software on Ubuntu as a user with `root`
privileges.


## [Updating System Software ](#update-system)

It\'s a good practice to update your repositories and system software,
if necessary.

Log in to your Magento server as a user with `root` privileges and enter
the commands shown in this section.

Update repositories:

    apt-get update

Optionally upgrade software. This might require a system reboot.

    apt-get upgrade

## Apache

This section discusses how to install Apache. For more details, you can
consult a reference like the [Ubuntu
site](https://help.ubuntu.com/lts/serverguide/httpd.html).

Magento requires Apache use server rewrites. You must also specify the
type of directives that can be used in `.htaccess`, which Magento uses
to specify rewrite rules.

Installing and configuring Apache is basically a three-step process:
install the software, enable rewrites, and specify `.htaccess`
directives.

## [Installing Apache ](#apache-install)

Install Apache 2 if you haven\'t already done so:

    apt-get -y install apache2

## [Enabling Apache Rewrites ](#apache-rewrites)

Ubuntu 12 (which natively supports Apache 2.2) is different from Ubuntu
14 (which natively supports Apache 2.4).

It\'s very important you choose a value for `AllowOverride` that is
suited to your deployment. You can use `AllowOverride All` in
development but it might not be desirable in production.

For more information, see one of the following references:

-   [Apache
    2.2](http://httpd.apache.org/docs/2.2/mod/core.html#allowoverride)
-   [Apache
    2.4](http://httpd.apache.org/docs/current/mod/core.html#allowoverride)

### Enabling Apache Rewrites for Apache 2.2 {#apache-rewrites2.2}

Use this section to enable Apache rewrites and specify `.htaccess` if
you use Apache 2.2, which is supported by the default Ubuntu 12
repository.

1.  Open the following file for editing.

        vim /etc/apache2/sites-available/default

2.  Locate the following block.

        <Directory /var/www/>
            Options Indexes FollowSymLinks MultiViews
            AllowOverride None
            Order allow,deny
            allow from all
        </Directory>

3.  Change the value of `AllowOverride` to `[value from Apache site]`.

        <Directory /var/www/>
            Options Indexes FollowSymLinks MultiViews
            AllowOverride All
            Order allow,deny
            allow from all
        </Directory>

4.  Save the file and exit the text editor.

5.  Configure Apache to use the `mod_rewrite` module.

        cd /etc/apache2/mods-enabled
        ln -s .mods-available/rewrite.load

6.  Restart Apache.

        service apache2 restart

### Enabling Apache Rewrites for Apache 2.4 {#apache-rewrites2.4}

Use this section to enable Apache rewrites and specify `.htaccess` if
you use Apache 2.4, which is supported by the default Ubuntu 14
repository.

1.  Enter the following command:

        a2enmod rewrite

2.  Specify the type of directives that can be used in `.htaccess`.
    For guidelines, see the [Apache 2.4
    documentation](http://httpd.apache.org/docs/current/mod/mod_rewrite.html).
    Note that in Apache 2.4, the server\'s default site configuration
    file is `/etc/apache2/sites-available/000-default.conf`
    For example, you can add the following to the bottom of
    `000-default.conf`:

        <Directory "/var/www">
        AllowOverride [value from Apache site]
        </Directory>

    **Note**: You must change the value of `AllowOverride` in the
    directive for the directory to which you expect to install the
    Magento software. For example, to install in the web server docroot,
    edit the directive in `<Directory /var/www>`.

3.  Restart Apache:
    `service apache2 restart`

## PHP

Magento CE and EE support the following PHP versions:

-   Magento CE 1.6.0.0--1.8.1.0 and Magento EE 1.11.0.0--1.13.1.0
    support PHP 5.3 natively. They can be used with PHP 5.4 if you apply
    the [PHP 5.4
    patch](other/php5.4_patch.html).
-   Magento CE 1.9.0.x and EE 1.14.0.x support PHP 5.4 natively. They
    are backward-compatible with PHP 5.3
-   Magento CE 1.9.1 and EE 1.14.1 support PHP 5.5 natively. They are
    backward-compatible with PHP 5.4

We recommend you use the most recent PHP version supported by your
version of Magento. For example, you should use PHP 5.5 with CE 1.9.1 or
EE 1.14.1.

For more information about PHP, see:

-   [PHP 5.4 changelog](http://php.net/ChangeLog-5.php#5.4.0)
-   [PHP 5.5 changelog](http://php.net/ChangeLog-5.php#5.5.0)

Check with a system administrator or reference for your version of
Ubuntu to see what PHP versions are available.

If you\'re installing Magento CE 1.9.1 or Magento EE 1.14.1, you can use
PHP 5.5; otherwise, we recommend PHP 5.4. For certain versions of CE and
EE, a [patch](other/php5.4_patch.html)
is required to use PHP 5.4.

Enter the following command to see what version of PHP is currently
running:

    php -v

See one of the following sections for more information:

-   [Upgrading PHP](#php-upgrade)
-   [Installing PHP](#php-install-install)
-   [Installing Required PHP Extensions](#php-extensions)
-   [Increasing the PHP memory Limit](#php-memory)

## [Upgrading PHP ](#php-upgrade)

![important](~@assets/icon-important.png) **Important**:
The following sections discuss _suggested_ PHP upgrade paths. Because
you\'re choosing a non-Ubuntu repository to upgrade PHP, make your
choice carefully. Not all repositories work equally well. We don\'t
recommend any particular repository. Consult a system administrator or
Ubuntu reference for more information.


To upgrade your version of PHP, see one of the following:

-   Upgrade to PHP 5.5: [PHP
    site](http://php.net/manual/en/migration55.php) and
    [dev-metal](http://www.dev-metal.com/how-to-setup-latest-version-of-php-5-5-on-ubuntu-12-04-lts/).
-   Upgrade to PHP 5.4:
    [phpave](http://phpave.com/upgrade-php-5-3-php-5-5-ubuntu-12-04-lts/).

## [Installing PHP ](#php-install-install)

See one of the following sections:

-   [Installing PHP 5.5 on Ubuntu 12](#php55-installub12)
-   [Installing PHP 5.4 on Ubuntu 12](#php54-installub12)
-   [Installing PHP 5.5 on Ubuntu 14](#php55-installub14)

### [Installing PHP 5.5 on Ubuntu 12 ](#php55-installub12)

Use the following commands from
[dev-metal](http://www.dev-metal.com/how-to-setup-latest-version-of-php-5-5-on-ubuntu-12-04-lts/):

    add-apt-repository ppa:ondrej/php5
    apt-get update
    sudo apt-get install php5

### [Installing PHP 5.4 on Ubuntu 12 ](#php54-installub12)

Use the following commands from
[askubuntu](http://askubuntu.com/questions/109404/how-do-i-install-latest-php-in-supported-ubuntu-versions-like-5-4-x-in-ubuntu-1):

    add-apt-repository ppa:ondrej/php5-oldstable
    apt-get update
    apt-get upgrade
    apt-get install php5

### [Installing PHP 5.5 on Ubuntu 14 ](#php55-installub14)

Enter the following command:

    apt-get -y install php5

## [Installing Required PHP Extensions ](#php-extensions)

Enter the following command to install PHP extensions required by
Magento:

    apt-get -y install php5 php5-mhash php5-mcrypt php5-curl php5-cli php5-mysql php5-gd libapache2-mod-php5

## [Increasing the PHP memory Limit ](#php-memory)

Increase `memory_limit` in `php.ini` to at least 512MB:

1.  Open `/etc/php5/apache2/php.ini` in a text editor.

2.  Change `memory_limit` to:

        memory_limit = 512M

3.  Save your changes and exit the text editor.

## MySQL

Magento CE and EE support the following MySQL versions:

-   Magento CE 1.9.1 and Magento EE 1.14.1 support MySQL versions 5.0.2
    through 5.6.x.
    To install MySQL 5.6, see [Installing MySQL 5.6](#mysql56).
-   Magento CE versions 1.8.0.0--1.9.0.x support MySQL versions
    4.1.20--5.5.x.
    To install MySQL version 5.5, see the next section.

## [Installing MySQL 5.5 ](#mysql55)

Install the MySQL database:

    apt-get -y install mysql-client mysql-server

## [Installing MySQL 5.6 ](#mysql56)

Only Magento CE 1.9.1 and EE 1.14.1 support MySQL 5.6. To install MySQL
5.6 on Ubuntu 14, see [Installing MySQL 5.6 on Ubuntu
14](#mysql56ubu14). To install MySQL 5.6 on Ubuntu 12, see the next
section.

### [Installing MySQL 5.6 on Ubuntu 12](#mysql56ubu12)

To install MySQL 5.6 on Ubuntu 12, enter the following commands in the
order shown:

    apt-add-repository ppa:ondrej/mysql-5.6
    apt-get -y update
    apt-get -y install mysql-server

Test the installation by entering the following command:

    mysql -u root -p

Messages similar to the following display:

    Welcome to the MySQL monitor.  Commands end with ; or \g.
    Your MySQL connection id is 43
    Server version: 5.6.21-1+deb.sury.org~precise+1 (Ubuntu)

    Copyright (c) 2000, 2014, Oracle and/or its affiliates. All rights reserved.

    Oracle is a registered trademark of Oracle Corporation and/or its
    affiliates. Other names may be trademarks of their respective
    owners.

    Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

    mysql>

### [Installing MySQL 5.6 on Ubuntu 14](#mysql56ubu14)

To install MySQL 5.6 on Ubuntu 14, enter the following command:

    apt-get -y install mysql-server-5.6 mysql-client-5.6

Test the installation by entering the following command:

    Welcome to the MySQL monitor.  Commands end with ; or \g.
    Your MySQL connection id is 45
    Server version: 5.6.19-0ubuntu0.14.04.1 (Ubuntu)

    Copyright (c) 2000, 2014, Oracle and/or its affiliates. All rights reserved.

    Oracle is a registered trademark of Oracle Corporation and/or its
    affiliates. Other names may be trademarks of their respective
    owners.

    Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

    mysql>

## [Next Steps ](#next)

After installing the required prerequisite software, continue your
installation with [Preparing Your Server for Magento CE or
EE](installing_install.html#prepare).

