---
sidebar: auto
---



# CentOS: Installing Required Prerequisite Software for Magento CE 1.8 Magento EE 1.13 (or Later)

--- 

### [Contents](#contents)

-   [Overview](#overview)
-   [Updating System Software](#update-system)
-   [Apache](#apache)
-   [nginx](nginx.html)
-   [PHP](#php)
-   [MySQL](#mysql)
-   [Next Steps](#next)
    

## Overview

This article discusses how to install required prerequisite software for
CentOS. You must complete these tasks before you install Magento CE 1.8
or later or Magento EE 1.13 or later.

Before you continue, make sure you familiarize yourself with the
installation process discussed in
[Prerequisites](installing_install.html#prereq).

 
![note](~@assets/icon-note.png) **Note**:
You must install system software on CentOS as a user with `root`
privileges.


## [Updating System Software ](#update-system)

It\'s a good practice to update your repositories and optionally update
system software.

Update repositories:

    yum -y update

Optionally upgrade software. This might cause a system reboot.

    yum -y upgrade

## Apache

Magento requires Apache use server rewrites. You must also specify the
type of directives that can be used in `.htaccess`, which Magento uses
to specify rewrite rules.

Installing and configuring Apache is basically a three-step process:
install the software, enable rewrites, and specify `.htaccess`
directives.

## [Installing Apache ](#apache-install)

Install Apache 2 if you haven\'t already done so.

    yum -y install httpd

## [Enabling Apache Rewrites ](#apache-rewrites)

1.  Open `httpd.conf` for editing.

        vim /etc/httpd/conf/httpd.conf

2.  Locate the block that starts with:

        <Directory /var/www/html>

3.  In that block, change the value of `AllowOverride` to `All`.

4.  Save your changes to `httpd.conf` and exit the text editor.

5.  Restart Apache.

        service httpd restart

## PHP

Magento CE and EE support the following PHP versions:

-   Magento CE 1.6.0.0--1.8.1.0 and Magento EE 1.11.0.0--1.13.1.0
    support PHP 5.3 natively. They can be used with PHP 5.4 if you apply
    the [PHP 5.4
    patch](other/php5.4_patch.html).
-   Magento CE 1.9.0.x and EE 1.14.0.x support PHP 5.4 natively. They
    are backward-compatible with PHP 5.3
-   Magento CE 1.9.1 and EE 1.14.1 support PHP 5.5 natively. They are
    backward-compatible with PHP 5.4

We recommend you use the most recent PHP version supported by your
version of Magento. For example, you should use PHP 5.5 with CE 1.9.1 or
EE 1.14.1.

For more information about PHP, see:

-   [PHP 5.4 changelog](http://php.net/ChangeLog-5.php#5.4.0)
-   [PHP 5.5 changelog](http://php.net/ChangeLog-5.php#5.5.0)

Check with a system administrator or reference for your version of
CentOS to see what PHP versions are available.

If you\'re installing Magento CE 1.9.1 or Magento EE 1.14.1, you can use
PHP 5.5; otherwise, we recommend PHP 5.4. For certain versions of CE and
EE, a [patch](other/php5.4_patch.html)
is required to use PHP 5.4.

Enter the following command to see what version of PHP is currently
running:

    php -v

See one of the following sections for more information:

-   [Upgrading PHP](#php-upgrade)
-   [Installing Required PHP Extensions](#php-extensions)
-   [Increasing the PHP memory Limit](#php-memory)

## [Upgrading PHP ](#php-upgrade)

CentOS 6.x repositories have PHP 5.3. This section assumes you use
either PHP 5.4 or 5.5. Make sure you understand which version of Magento
CE or EE supports the PHP version to which you upgrade.

Before you start, verify you have PHP 5.3 installed:

    php -v

If you already have the desired PHP version installed, you don\'t have
to do anything.

If PHP is not installed, install PHP 5.3 using the following command:

    yum -y install php php-xml

Continue with one of the following sections.

 
![important](~@assets/icon-important.png) **Important**:
The following sections discuss _suggested_ PHP upgrade paths. Because
you\'re choosing a non-CentOS repository to upgrade PHP, make your
choice carefully. Not all repositories work equally well. We don\'t
recommend any particular repository. Consult a system administrator or
CentOS reference for more information.


### [Upgrading to PHP 5.5 ](#php-upgrade55)

There is more than one way to upgrade CentOS 6.5 to PHP 5.5; the
following is a suggestion only. Consult a reference for additional
options.

Enter the following commands in the order shown.

    cd /tmp
    rpm -Uvh https://mirror.webtatic.com/yum/el6/latest.rpm
    yum -y remove php-common-5.3.3-40.el6_6.x86_64
    yum -y install php55w php55w-opcache

Skip the next section and continue with [Installing Required PHP
Extensions](#php-extensions).

### [Upgrading to PHP 5.4 ](#php-upgrade54)

Enter the following commands to upgrade to PHP 5.4:

    cd /tmp
    rpm -Uvh http://download.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm
    rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-6.rpm
    yum --enablerepo=remi install httpd php php-common

The following resources are also available:

-   kb.parallels
-   [stackoverflow](http://stackoverflow.com/questions/21502656/upgrading-php-on-centos-6-5-final)
-   [remi repository](http://rpms.famillecollet.com/)

## [Installing Required PHP Extensions ](#php-extensions)

The names of the PHP extensions you must install depend on the
repository from which you installed PHP. Use one of the following
commands:

PHP 5.5:

    yum -y install php55w-xml php55w-mcrypt php55w-gd php55w-devel php55w-mysql php55w-mbstring

PHP 5.4:

    yum -y install --enablerepo=remi php-mcrypt gd gd-devel php-gd php-mysql php54w-mbstring

## [Increasing the PHP Memory Limit ](#php-memory)

Increase `memory_limit` in `php.ini` to at least 512MB.

1.  Open `/etc/php.ini` in a text editor.

2.  Change `memory_limit` to:

        memory_limit = 512M

3.  Save your changes and exit the text editor.

## MySQL

This section discusses how to install and configure MySQL 5.6. CentOS
6.x repositories have MySQL 5.1; to install a different version of
MySQL, see the [MySQL
documentation](http://dev.mysql.com/doc/index.html).

 
![note](~@assets/icon-note.png) **Note**:
Use the tasks that follow only on a _new MySQL database_. Some of the
tasks require you to delete users and should not be performed on a
database that has already been set up.


The following procedure is based on [Install MySQL Server 5.6 in CentOS
6.x and Red Hat 6.x
Linux](http://sharadchhetri.com/2013/12/26/install-mysql-server-5-6-in-centos-6-x-and-red-hat-6-x-linux/).

1.  Install the MySQL database:

        cd /tmp
        wget http://repo.mysql.com/mysql-community-release-el6-5.noarch.rpm
        rpm -ivh mysql-community-release-el6-5.noarch.rpm
        yum -y install mysql-server

2.  Start MySQL.

        service mysqld start

3.  Set a password for the `root` user and set other security-related
    options. Enter the following command and follow the prompts on your
    screen to complete the configuration.

        mysql_secure_installation

## [Next Steps ](#next)

After installing the required prerequisite software, continue your
installation with [Preparing Your Server for Magento CE or
EE](installing_install.html#prepare).

