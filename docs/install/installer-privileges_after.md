---
sidebar: auto
---



 
# After You Install Magento: Recommended File System Ownership and Privileges

---


### [Table of Contents](#table-of-contents)

-   [Overview](#overview)
-   [Terminology](#terms)
-   [Setting Privileges and Ownership After You Install
    Magento](#privs-after)
-   [Securing Magento Extensions](#extensions)
-   [Applying Magento Support Patches](#privs-patches)
-   [For More Information](#more-info)
    

## Overview

In a continuing effort to improve security and ease of use, Magento is
updating its recommendations for file system permissions and ownership
for the following Magento editions:

-   Magento Enterprise Edition (EE) versions 1.9 and later
-   Magento Community Edition (CE) versions 1.4 and later

This article discusses recommended permission and ownership schemes to
apply after you install Magento.

The guidelines discussed in this article apply to:

-   New installations of or upgrades to the previously listed Magento
    versions only.
    The instructions might not work with older versions. If you have
    already installed Magento, you can optionally change your file
    system permissions and ownership as discussed in this article.
-   [Supported operating
    systems](../system-requirements.html)
    only

This article discusses the following permission and ownership schemes:

-   [Post-installation](#privs-after) for both a hosted Magento server
    and for a dedicated Magento server (in other words, you have `root`
    access on the server).
-   [Securing extensions](#extensions), recommended ownership and
    privilege settings to apply after you install Magento extensions.
    Typically, Magento extensions install with 777 (world-writable)
    privileges, which is undesirable from a security point of view.

![important](~@assets/icon-important.png) **Important**:
This topic contains _suggestions_ based on our experience. They are not
_requirements_ because we don\'t know the details of your deployment.
Consult your hosting provider, a qualified security specialist, or an
experienced system administrator for advice about specific security
settings.


## [Terminology ](#terms)

This article uses the following terminology:

Hosted system
: A Magento server located on a hosting provider. A _hosted_ system
typically does not enable you to elevate to `root`. The web server
typically runs as an ordinary user. Magento assumes you log in as
this user to start and stop the web server and that you already own
all the files and directories in the Magento installation directory.
You can use `chmod` to change permissions on files and directories.

Dedicated system
: A Magento server you control and operate. Unlike a hosted system,
you can elevate to `root` and, as `root`, you can use the `chown`
and `chmod` commands to set ownership and privileges in the Magento
installation directory.

## [Setting Privileges and Ownership After You Install Magento ](#privs-after)

If you have installed Magento, you can set file system privileges and
ownership as follows:

-   For a dedicated Magento server, you set ownership of files and
    directory as the web server user. You set privileges as 500
    (directories) and 400 (files).
-   For a hosted Magento server on which the web server runs as the
    logged-in username, you set privileges as 500 (directories) and 400
    (files).

![note](~@assets/icon-note.png) **Note**:
In both hosted and dedicated systems, you set the privileges for the
`media/` and `var/` directories at 700/600 because they must be
writable.


Following is an explanation of the privileges:

-   500 permissions for directories (`dr-x------`) gives the web server
    user read and execute privileges to prevent the accidental deletion
    or modification of files in the directory. Other users have no
    access to Magento directories.
-   400 permissions for files (`-r--------`) prevent any user (even the
    web server user) from overwriting files.
    This prevents attacks that depend on overwriting existing files with
    malicious content.
-   700 permissions (`drwx------`) for the `media/` and `var/`
    directories give full control (that is, read/write/execute) to the
    owner and no permissions to anyone else.
-   600 permissions (`-rw-------`) for files in the `media/` and `var/`
    directories enable the web server user to write to them and to
    overwrite them.

![note](~@assets/icon-note.png) **Note**:
On a dedicated system, all commands discussed in this article must be
entered as a user with `root` privileges. On a hosted system, commands
must be entered as the web server user.


To set up ownership and permissions on a dedicated Magento server:

1.  _Dedicated Magento server only_. As a user with `root` privileges,
    find the web server user:

    -   Apache:

        -   Ubuntu: `grep User /etc/apache2/apache2.conf`
        -   CentOS: `grep User /etc/httpd/conf/httpd.conf`

            ![note](~@assets/icon-note.png) **Note**:
            The preceding paths are samples only. The paths to these
            `.conf` files on your system might be different. You can use
            the command `whereis nginx` to find the location of the
            configuration files.
            

        Typically, the Apache web server user on CentOS is `apache` and
        the Apache web server user on Ubuntu is `www-data`.

    -   nginx: Open the nginx configuration file, typically
        `/etc/nginx/nginx.conf`. The `user` directive specifies the
        username. It might run as the Apache user if Apache is installed
        on the same system.

2.  Change to the Magento installation directory.
    On CentOS, this is typically `/var/www/html/magento`. On Ubuntu, it
    is typically `/var/www/magento`.

3.  _Dedicated Magento server only_. As a user with `root` privileges,
    enter the following command to set ownership of the Magento
    installation directory and all its subdirectories:

        chown -R web-server-user-name .

    For example, on Ubuntu where Apache usually runs as `www-data`,
    enter

        chown -R www-data .

4.  Enter the following commands to set permissions:

        find . -type f -exec chmod 400 {} +
        find . -type d -exec chmod 500 {} +
        find var/ -type f -exec chmod 600 {} +
        find media/ -type f -exec chmod 600 {} +
        find var/ -type d -exec chmod 700 {} +
        find media/ -type d -exec chmod 700 {} +
        chmod 700 includes
        chmod 600 includes/config.php

## [Securing Magento Extensions ](#extensions)

If you set permissions and ownership as discussed in this article, you
must change permissions temporarily to be able to use the Magento
Connect Manager in the Admin Panel. (**System** \> **Magento Connect**
\> **Magento Connect Manager**). You can still install extensions
manually, however; that is beyond the scope of this article.

You can confirm the issue when you access Magento Connect Manager in the
Admin Panel. The following error displays on the **Extensions** tab
page:

    Warning: Your Magento folder does not have sufficient write permissions.

To use Magento Connect Manager, you must:

1.  Temporarily set 700/600 permissions on your Magento installation
    directory and subdirectories.
2.  Install the extension.
    Magento Connect Manager typically installs extensions with 777
    (world-writable) permissions.
3.  Set permissions back to their recommended values.

In addition, if you have a dedicated Magento server, you should check
ownership of files and directories and reset them if necessary. Often,
Magento Connect Manager installs extensions with user and group
ownership both set to the web server user.

## [Temporarily Resetting Permissions on Your Magento Installation Directory ](#extensions-before)

To temporarily set file and directory permissions so you can use Magento
Connect Manager:

1.  Change to the Magento installation directory.
    On CentOS, this is typically `/var/www/html/magento`. On Ubuntu, it
    is typically `/var/www/magento`.

2.  Enter the following commands:

        find . -type d -exec chmod 700 {} +
        find . -type f -exec chmod 600 {} +

3.  Install your extension using the Magento Connect Manager.

## [Restoring the Recommended Permissions ](#extensions-after)

Enter the commands discussed in this section to return permissions and
ownership to their recommended values after you have installed
extensions.

To restore Magento installation directory permissions:

1.  Change to the Magento installation directory.
    On CentOS, this is typically `/var/www/html/magento`. On Ubuntu, it
    is typically `/var/www/magento`.

2.  _Dedicated Magento server only_. As a user with `root` privileges,
    enter the following command to set ownership of the Magento
    installation directory and all its subdirectories:

        chown -R web-server-user-name .

    For example, on Ubuntu where Apache usually runs as `www-data`,
    enter

        chown -R www-data .

3.  Enter the following commands to set permissions:

        find . -type f -exec chmod 400 {} +
        find . -type d -exec chmod 500 {} +
        find var/ -type f -exec chmod 600 {} +
        find media/ -type f -exec chmod 600 {} +
        find var/ -type d -exec chmod 700 {} +
        find media/ -type d -exec chmod 700 {} +

## [Applying Magento Support Patches ](#privs-patches)

Magento Support typically provides a shell script to patch various
Magento issues. When you run the shell script, file and directory
permissions are typically not changed; however, the files provided with
the patch are owned by the user who applied the patch. If you have a
dedicated Magento server, this is typically `root`; therefore, after
applying the patch, you must change file ownership.

If you are required to apply a patch provided by Magento Support, use
the following process:

1.  Get the patch from Magento Support.
2.  Follow the instructions provided with the patch.
    Typically, you run a shell script as either a user with `root`
    privileges or as the owner of the Magento installation directory.
3.  If you ran the patch as the owner of the Magento installation
    directory, you\'re done. File permissions aren\'t usually changed;
    however, you should check and reapply file and directory privileges
    if necessary.
4.  If you ran the patch as a user with `root` privileges, use the
    following steps to reset file ownership:
5.  _Dedicated Magento server only_. Find the web server user:

    -   Apache:

        -   Ubuntu: `grep User /etc/apache2/apache2.conf`
        -   CentOS: `grep User /etc/httpd/conf/httpd.conf`

        Typically, the Apache web server user on CentOS is `apache` and
        the Apache web server user on Ubuntu is `www-data`.

    -   nginx: Open the nginx configuration file, typically
        `/etc/nginx/nginx.conf`. The `user` directive specifies the
        username. It might run as the Apache user if Apache is installed
        on the same system.

6.  As a user with `root` privileges, enter the following command from
    the Magento installation directory:
    `chown -R web-server-user-name .` For example, on Ubuntu where
    Apache usually runs as `www-data`, enter `chown -R www-data .`

## [For More Information ](#more-info)

For more information about UNIX permissions, see the following
resources:

-   [UNIX File
    Permissions](http://www.statslab.cam.ac.uk/~eva/unixinfo/perms.html)
-   [Unix Permissions Calculator](http://permissions-calculator.org/)
-   [Article on
    unix.stackexchange](http://unix.stackexchange.com/questions/39710/how-to-get-permission-number-by-string-rw-r-r)
    
