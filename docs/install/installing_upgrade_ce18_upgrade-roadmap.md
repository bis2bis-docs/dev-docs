---
sidebar: auto
---



# Upgrade Roadmap for Magento Community Edition (CE) 1.8 or 1.9

 
Magento recommends you upgrade your installation using the following
guidelines in a _development or test environment_, separate from your
existing production environment:

1.  Install Magento in a different directory:
    -   _Recommended_. Set up a new system (that is, another host) on
        which to install Magento.
        The system should be identical to, if not better than, your
        current system. The new system must meet the [Magento system
        requirements](../system-requirements.html).
    -   Install Magento in a new, empty root installation directory on
        the same server.
2.  In your current production environment:
    1.  Back up your Magento database.
    2.  Archive the file system.
        This includes the `media` directory and subdirectories; all
        extensions and customizations; and all custom themes.
3.  In the development or test environment:
    1.  Create a new, empty database instance.
    2.  Import the production database tables into the development
        database instance.
    3.  Copy your production `media` directory, extensions, themes, and
        other customizations to the development system.
    4.  Copy `local.xml` to `[your Magento install dir]/app/etc` and
        edit it if necessary to reference the production database
        instance.
    5.  In a web browser, go to your development system base URL.
    6.  Wait for upgrade scripts to run.
    7.  Verify the development system is now identical to the production
        system.
        If not, fix issues, retest, and upgrade again.
4.  _Test the development system thoroughly_, including:
5.  Verify all extensions, themes, and customizations work.
6.  Place orders using all webstores and all payment methods.

When you\'re ready to start your upgrade, see [Upgrading to and
Verifying Magento Community Edition 1.8 and Enterprise Edition 1.13 -
Part
2](installing_upgrade_details.html).

