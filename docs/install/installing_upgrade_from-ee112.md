---
sidebar: auto
---



 
EE 1.14 Upgrade: Running the EE 1.12 and Earlier URL Redirect Script
---

This article is part of the Magento Enterprise Edition (EE) upgrade
documentation. If you\'re looking for something else, [click
here](http://www.magentocommerce.com/resources/magento-user-guide) to go
back to the Magento Knowledge Base.

This article applies to the following Magento EE upgrades:

-   From EE 1.12.x or earlier to EE 1.13.0.2
-   From EE 1.12.x or earlier to EE 1.13.1.0
-   From EE 1.12.x or earlier to EE 1.14.0.0

If your upgrade is not in the preceding list, go back to [Upgrading to
and Verifying Magento Community Edition (CE) 1.8 and Enterprise Edition
(EE)
1.13](installing_upgrade_details.html).

This article discusses how to run the URL redirect script that creates
HTTP 301 (Moved Permanently) redirects for any duplicate product URL
keys. This enables **Previous** links in your web store to work, for
example.

To create redirects for your URLs, perform the following tasks in the
order shown:

1.  [Verifying that cron is
    Disabled](#upgrade-manual-post-upgrade-11302-cron-verify)
2.  [Changing Indexer Settings to Update When
    Scheduled](#upgrade-manual-post-upgrade-11302-indexer)
3.  [Running the URL Redirect
    Script](#upgrade-manual-post-upgrade-11302-run)
4.  [Running a Full Reindex from the Command
    Line](#upgrade-manual-post-upgrade-11302-full-reindex)
5.  [Viewing the List of
    Redirects](#upgrade-manual-post-upgrade-11302-view-redirs)
6.  [Verifying That URLs Work
    Properly](#upgrade-manual-post-upgrade-11302-verify)
7.  [Completing Your Upgrade](#complete-upgrade)

## [Verifying that cron is Disabled ](#upgrade-manual-post-upgrade-11302-cron-verify)

It\'s very important to stop cron jobs from running until after the
upgrade script completes. To do so, enter one of the following commands
as a user with `root` privileges:

-   Ubuntu: `service crond stop`
-   CentOS: `service cron stop`

## [Changing Indexer Settings to Update When Scheduled ](#upgrade-manual-post-upgrade-11302-indexer)

To change indexer settings:

1.  Click **System** \> **Configuration**.
2.  In the left navigation bar, in the ADVANCED group, click **Index
    Management**.
3.  In the right pane, expand **Index Options**.
4.  Click **Update when scheduled** from the list for every indexer.
    The following figure shows an example.
    ![](~@assets/upgrade_index-management_when-scheduled.png){width="600px"
    height="258px"}
5.  In the upper right corner, click **Save Config**.
     
    ![note](~@assets/icon-note.png) **Note**:
    Ignore any messages requesting you to reindex or flush the cache;
    you\'ll do that later.
    
6.  Log out of the Admin Panel.
7.  If the script ran without errors, continue with [Running the URL
    Redirect Script](#upgrade-manual-post-upgrade-11302-run).

## [Running the URL Redirect Script ](#upgrade-manual-post-upgrade-11302-run)

This section discusses how to run the URL redirect script to create
permanent redirects for any URLs that changed in EE 1.13.1.0.

 
![note](~@assets/icon-note.png) **Note**:
The time the script takes to run and the amount of memory it uses is
directly proportional to the size of your database. If your database has
100,000 SKUs or more, Magento _strongly recommends_ you monitor thread
execution as discussed in this section. For large databases, the script
can require several hours to complete.


To run the script:

1.  Change to your Magento installation directory.

2.  As the user who owns the files (typically, either `root` or the web
    server user), enter the following command:

        php -f shell/url_migration_to_1_13.php - thread-count

    where
    _thread_count_ is the number of threads to use. Magento recommends
    setting this to the number of CPU cores in your system minus 1 to a
    maximum of 15. For example, if your system has 8 CPU cores, enter

        php -f shell/url_migration_to_1_13.php 7

     
    ![note](~@assets/icon-note.png) **Note**:
    Each thread can use up to 512MB of RAM.
    

3.  Wait while the script runs.
    Messages like the following display to indicate progress:

        [INFO]: Initialization...
        [INFO]: Renaming conflicting entities...
        100% [#################################################################################]
        [INFO]: Creating redirects from previous version...
        100% [#################################################################################]
        ETA HH:MM:SS

4.  If you have a large database, open another command window and run
    the following command:

        watch -n1 "ps ax | grep umt | grep -v grep"

    This command helps you to determine if each thread behaves
    correctly. After each process finishes, a new process should start
    immediately. There should be no delays or \"hangs\" between
    processes.

5.  If the script runs successfully, continue with [Running a Full
    Reindex from the Command
    Line](#upgrade-manual-post-upgrade-11302-full-reindex).

If errors display, check
`[your Magento install dir]/shell/migration.log` for exceptions and see
the following table.

**Troubleshooting**

Symptom


Suggested action

`ERROR: Scope of attribute "url_key" is set to Global. This may cause DB inconsistency. Aborting.`

This error can be caused by either a product or category `url_key`
attribute being set for Global scope. To determine which type of
`url_key` attribute is causing the error:

1.  Log in as an administrator to the Admin Panel of the production
    system (in other words, the system you\'re upgrading _from_).
2.  Click **Catalog** \> **Attributes** \> **Manage Attributes**.
3.  Click the **url_key** row.
4.  Examine the value of the **Scope** list. A sample follows.
    ![](~@assets/index-management_product-url-key-attrib.png)
5.  If the value in the **Scope** list is **Global**:
    1.  Select another value from the list. You can select any value
        _other than_ **Global**.
    2.  Click **Save Attribute**.
    3.  Log out of the Admin Panel.
    4.  Re-export the database as discussed in [Backing Up the
        Database](installing_upgrade_details.html#prereq-tasks-prod-backup-db).
    5.  In the development system, drop and re-create the database as
        discussed in [Pre-Upgrade Tasks for Your Development
        Environment](installing_upgrade_details.html#prereq-tasks-dev).
    6.  Import the database you just exported as discussed in [Updating
        the Magento
        Database](installing_upgrade_details.html#upgrade-manual-db).
    7.  Try running the upgrade script again.
6.  If the value of the **Scope** list is _not_ **Global**, the issue is
    likely that the category `url_key` attribute is set to global. To
    confirm this:

    1.  Log out of the Admin Panel.

    2.  In the development database instance, use either the MySQL
        command line or phpmyadmin to get the value of `is_global` from
        the `catalog_eav_attribute` table. If the value is `1`, your
        catalog `url_key` attribute is set to Global.
        Following is an example of finding the value using the MySQL
        command line:

            select attribute_id, is_global from catalog_eav_attribute /
              where attribute_id in (select attribute_id from eav_attribute /
              where attribute_code = 'url_key');

        
        **Note:** The command must be entered on a single line. It is
        shown here on multiple lines because of space limitations. When
        you enter the command, omit the `/` characters.

    3.  If any returned value is `1`, you must contact [Magento
        Support](http://support.magentocommerce.com).

Other errors

Try running the script again. If errors persist, contact [Magento
Support](http://support.magentocommerce.com).

## [Running a Full Reindex from the Command Line ](#upgrade-manual-post-upgrade-11302-full-reindex)

After successfully running the URL redirect script, enter the following
command from the Magento root directory:

    php -f shell/indexer.php --reindexall

This command runs a full reindex and it might take a long time,
depending on the size of your database.

After the reindex completes, enter the following command from your
Magento installation directory:

    rm -rf var/cache var/full_page_cache var/locks

## [Viewing the List of Redirects ](#upgrade-manual-post-upgrade-11302-view-redirs)

As a result of running the URL redirect script, a list of redirects
created for duplicate URL keys displays in the Admin Panel. To view
these redirects:

1.  Log in to the Admin Panel as an administrator.

2.  Click **Catalog** \> **URL Redirects**.

3.  To optionally sort results by description, click the **Description**
    column.
    All redirects created by the URL redirect script display the
    following description:

        1.12.0.2-1.13.x migration redirect

    The following figure shows an example.
    ![](~@assets/upgrade_url-redirects.png){width="600px"
    height="159px"}

## [Verifying That URLs Work Properly ](#upgrade-manual-post-upgrade-11302-verify)

Assuming all your post-upgrade issues are fixed, go to your web store
and navigate the category tree. Click some products and make sure they
display properly. Verify that all **Previous** and **Back** links work
properly.

## [Completing Your Upgrade ](#complete-upgrade)

Complete your upgrade:

1.  [Setting Up the Magento Cron
    Job](installing_install.html#install-cron)
2.  [Clearing Magento var/
    Subdirectories](installing_upgrade_details.html#upgrade-manual-var)
3.  Complete the remainder of the tasks on that page in the order shown
