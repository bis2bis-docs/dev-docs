---
sidebar: auto
---



# Upgrading to and Verifying Magento Community Edition and Enterprise Edition---Part 2

---

### [Table of Contents](#table-of-contents)

-   [Overview](#overview)
-   [Upgrade Roadmaps](#upgrade-ee-versions)
-   [Getting Ready For Your Upgrade](#prereq-tasks)
-   [Upgrading the File System, Solr, and Database](#upgrade-manual)
-   [Running the URL Redirect Script _EE 1.13.0.0 and 1.13.0.1
    only_](#upgrade-manual-post-upgrade-ee11300)
-   [Completing the Magento Upgrade](#upgrade-manual-run)
-   [Testing the Magento Upgrade](#upgrade-manual-test)
-   [Setting Magento File System Permissions and Ownership After
    Upgrade](#install-privs-after)
-   [Troubleshooting](#upgrade-troubleshoot)
    

## Overview

This article discusses how you upgrade to:

-   Magento Community Edition (CE) 1.8 or 1.9.x
-   Magento Enterprise Edition (EE) 1.13.0.2, 1.13.1, or 1.14.x

![important](~@assets/icon-important.png) **Important**:
Magento recommends CE 1.9.1.0 or later or EE 1.14.1.0 or later for _all
CE and EE installations and upgrades_ to get the latest fixes, features,
and security updates.


Because of changes to URL rewrites, the following upgrades are more
complex than other upgrades.

-   Upgrading from EE 1.12 or earlier to EE 1.14.x
-   Upgrading from EE 1.13.0.0 or EE 1.13.0.1 to EE 1.14.x

![note](~@assets/icon-note.png) **Note**:
If you\'re upgrading from EE 1.13.0.2 to EE 1.13.1.0 or later, your
upgrade doesn\'t involve changes to URL rewrites and you can skip many
of the steps discussed in the other upgrades. Continue with [Getting
Ready For Your Upgrade](#prereq-tasks).


## [Upgrade Roadmaps ](#upgrade-ee-versions)

-   High-level roadmaps for upgrading to EE 1.13.0.2 or later are
    discussed in [Understanding the Upgrade to EE 1.13.0.2 or EE
    1.14](installing_upgrade_landing.html#ee1300-11301-upgrade-to-ee11302-start).
-   To upgrade from EE 1.13.0.2 to EE 1.13.1.0 or later (including EE
    1.14.x), see [Getting Ready For Your Upgrade](#prereq-tasks).
-   To upgrade to CE 1.8 or 1.9, see [Upgrade Roadmap for CE 1.8 or
    1.9](installing_upgrade_ce18_upgrade-roadmap.html)

## [Getting Ready For Your Upgrade ](#prereq-tasks)

This section discusses how to get ready for your upgrade by backing up
the database and customizations on the file system. The steps that
follow do not affect your current production system. You can continue
serving customers with no downtime.

To get ready for your upgrade:

1.  Get the Magento installation archive as discussed in [Getting
    Magento CE or
    EE](installing.html#get).
2.  Install Magento in a different directory:
3.  _Recommended_. Set up a new system (that is, another host) on which
    to install Magento.
    The system should be identical to, if not better than, your current
    system. The new system must meet the [Magento system
    requirements](../system-requirements.html).
4.  Install Magento in a new, empty root installation directory on the
    same server.
5.  Magento _strongly_ recommends you observe the following guidelines
    when you set up the Magento database in your development
    environment:
    -   Magento for the first time uses [MySQL database
        triggers](http://dev.mysql.com/doc/refman/5.0/en/triggers.html)
        to improve database access during reindexing. Magento does not
        support any custom triggers in the Magento database because
        custom triggers can introduce incompatibilities with future
        Magento versions.
    -   Familiarize yourself with [these potential MySQL trigger
        limitations](http://dev.mysql.com/doc/mysql-reslimits-excerpt/5.1/en/stored-program-restrictions.html)
        before you continue.
    -   If you use MySQL database replication, Magento _does not_
        support MySQL statement-based replication. Make sure you use
        only [row-based
        replication](http://dev.mysql.com/doc/refman/5.1/en/replication-formats.html).
    -   Review the MySQL recommendations for [MySQL password
        security](http://dev.mysql.com/doc/refman/5.0/en/password-security-user.html).
6.  Complete the tasks discussed in the following sections in the order
    shown:
    1.  [Pre-Upgrade Tasks For Your Production
        Environment](#prereq-tasks-prod)
    2.  [Pre-Upgrade Tasks For Your Development
        Environment](#prereq-tasks-dev)

## [Pre-Upgrade Tasks For Your Production Environment ](#prereq-tasks-prod)

Complete the tasks discussed in the following sections in the order
shown:

1.  [Enabling Exception Logging](#prereq-tasks-prod-logging)
2.  [Disabling cron jobs _(EE 1.13.0.2 or
    later)_](#prereq-tasks-prod-disable-cron-11302)
3.  [Setting All Indexers for Update on Save](#prereq-tasks-prod-index)
4.  [Flushing the Cache](#prereq-tasks-prod-clear-cache)
5.  [Backing Up the Database](#prereq-tasks-prod-backup-db)
6.  [Archiving Customizations and
    Extensions](#prereq-tasks-prod-archive)
7.  [Restoring Configuration
    Settings](#prereq-tasks-prod-restore-settings)

### [Enabling Exception Logging](#prereq-tasks-prod-logging)

Before you start your upgrade, you should enable exception logging so it
will be enabled on the development system. Without exception logging, it
will be more difficult to diagnose errors during your upgrade. You can
disable exception logging after you\'ve exported the Magento database.

To enable exception logging:

1.  Log in to the Admin Panel as an administrator.
2.  Click **System** \> **Configuration**.
3.  In the ADVANCED group, click **Developer**.
    This enables you to enable exception logging so you\'ll see
    exceptions in the development environment after the upgrade.
4.  In the right pane, click **Log Settings** to expand it.
    The following figure shows an example.
    (~@assets/upgrade_log.png){width="600px"
    height="338px"}
5.  From the **Enabled** list, click **Yes**.
6.  Optionally change the names of the log files.
7.  Click **Save Config**.

### [Disabling cron Jobs ](#prereq-tasks-prod-disable-cron-11302)

Before upgrading to EE 1.13.0.2 or later, disable all running cron jobs.
A simple way to stop cron jobs follows; consult an appropriate reference
for other options.

As a user with `root` privileges, enter one of the following commands:

-   Ubuntu: `service crond stop`
-   CentOS: `service cron stop`

### [Setting All Indexers for Update on Save](#prereq-tasks-prod-index)

_This section applies to upgrading Magento EE only_. Skip this section
if you\'re using Magento CE.

Because of the changes to indexing in Magento EE 1.13, you must set all
indexers to update on save before you upgrade; otherwise, unpredictable
performance will result. You can revert indexer settings after you\'ve
exported the Magento database.

To set indexers to update on save:

1.  Click **System** \> **Index Management**.
2.  On the top left corner of the row, click **Select All** to select
    all indexers.
3.  From the **Options** list, click **Change indexing mode**.
    The **Index Mode** list displays.
4.  From the **Index Mode** list, click **Update on Save**.
    The following figure shows an example.
    ![](~@assets/upgrade_index-management.png){width="600px"
    height="156px"}
5.  At the top right corner, click **Submit**.
6.  Make sure all indexers are now set for Update on Save as the
    following figure shows.
    ![](~@assets/upgrade_index-management2.png){width="600px"
    height="156px"}

### [Flushing the Cache](#prereq-tasks-prod-clear-cache)

Clear the Magento cache as follows:

1.  Log in to the Admin Panel as an administrator.
2.  Click **System** \> **Cache Management**.
3.  Click **Flush Magento Cache**.

### [Backing Up the Database](#prereq-tasks-prod-backup-db)

Back up your database using `mysqldump` or another tool. `mysqldump`
syntax follows:

    mysqldump -u root magento-database-name > export-file-name.sql

### [Archiving Customizations and Extensions](#prereq-tasks-prod-archive)

To archive your custom themes and extensions:

1.  Archive the Magento `media` directory and all subdirectories. For
    example,

        cd [your Magento install dir]
        tar -czf media.tgz media

2.  Archive any custom themes in the following directories:

        [your Magento install dir]/app/design/frontend
        [your Magento install dir]/skin/frontend

3.  Archive any customizations or extensions in the following
    directories:

        [your Magento install dir]/app/code/local
        [your Magento install dir]/app/code/community

4.  Transfer the archives you just created to the development
    environment.

5.  Copy `[your Magento install dir]/app/etc/local.xml` to your
    development environment.

### [Restoring Configuration Settings](#prereq-tasks-prod-restore-settings)

After you\'ve exported the Magento database and extensions, you can
revert the configuration changes you made as discussed in these
sections:

-   [Enabling Exception Logging](#prereq-tasks-prod-logging)
-   [Setting All Indexers for Update on Save _(EE
    only)_](#prereq-tasks-prod-index)

## [Pre-Upgrade Tasks For Your Development Environment ](#prereq-tasks-dev)

In your development environment:

1.  If necessary, create a Magento installation master directory;
    Magento installs in a `magento` subdirectory of that directory.
    The Magento installation archive creates a `magento` subdirectory so
    if you want your Magento installation directory to be
    `/var/www/magento`, you don\'t need to do anything.

    ![tip](~@assets/icon-tip.png) **Tip**:
    The name and path to the Magento installation directory in your
    development system does not have to be the same as on your
    production system, although it might make some tasks simpler.
    

2.  Create a new database instance. For example, you can create a new
    MySQL database instance named `magento` using the following
    commands:

        mysql -u root -p

    Enter the remaining commands at the `mysql>` prompt

        create database magento;
        GRANT USAGE ON *.* TO magento@localhost IDENTIFIED BY 'magento';
        GRANT ALL ON magento.* TO magento@localhost;

    For MySQL versions later than 5.0.2 but earlier than 5.1.6, the
    following command is required:

        GRANT SUPER ON *.* TO 'magento'@'localhost';

    Exit the MySQL command shell:

        exit

    Verify the database exists using the following command:

        mysql -u magento -p magento

    If an error displays, repeat the commands. If the command succeeded,
    enter `exit` to return to the command prompt.

3.  If you\'re using [Security Enhanced Linux
    (SELinux)](http://selinuxproject.org/page/Main_Page) _and_ Apache
    must initiate a connection to another host, you must run the
    commands discussed in this step.

    1.  To determine if SELinux is enabled, use the following command:
        `getenforce`
        `Enforcing` displays to confirm that SELinux is running.

    2.  Enter one of the following commands:
        CentOS:

            setsebool -P httpd_can_network_connect=1

        Ubuntu

            setsebool -P apache2_can_network_connect=1

### [Optional: Upgrading to PHP 5.4 ](#prereq-tasks-dev-php)

PHP 5.3 is currently the latest PHP version available in the default
repositories for Ubuntu and CentOS. PHP 5.3 works with CE 1.8, CE 1.9,
EE 1.13, and EE 1.14.

We recommend PHP 5.4 for all of the preceding CE and EE versions because
of the [new features and changes](http://php.net/ChangeLog-5.php#5.4.0)
in that release.

CE 1.8 and EE 1.13 both require a [PHP 5.4
patch](other/php5.4_patch.html). The
patch is listed as **PHP 5.4 Compatibility** in the EE support portal.

## Upgrading to PHP 5.4---Ubuntu

Use the instructions on
[phpave](http://phpave.com/upgrade-php-5-3-php-5-5-ubuntu-12-04-lts/).

## Upgrading to PHP 5.4---CentOS

Enter the following commands in the order shown as a user with `root`
privileges:

    cd /tmp
    rpm -Uvh http://download.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm
    rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-6.rpm
    yum --enablerepo=remi install httpd php php-common

The following resources are also available:

-   kb.parallels
-   [stackoverflow](http://stackoverflow.com/questions/21502656/upgrading-php-on-centos-6-5-final)
-   [remi repository](http://rpms.famillecollet.com/)

## [Upgrading the File System, Solr, and Database ](#upgrade-manual)

Upgrade your Magento installation by completing all of the following
tasks in the order shown:

1.  [Upgrade Requirements](#upgrade-manual-prereq)
2.  [Upgrading Assets on the Magento File
    System](#upgrade-manual-file-sys)
3.  [Upgrading the Magento Database](#upgrade-manual-db)
4.  [Upgrading Solr (_EE 1.14.0.0 only_)](#upgrade-manual-solr)
5.  [Finishing the File System](#upgrade-manual-cleanup)

## [Upgrade Requirements ](#upgrade-manual-prereq)

Before attempting your Magento upgrade, complete all the tasks discussed
in:

-   [Getting Ready for Your Upgrade](#prereq-tasks)
-   [Getting Magento CE or
    EE](installing.html#get)

## [Upgrading Assets on the Magento File System ](#upgrade-manual-file-sys)

This section discusses how to extract the Magento archive in your
development system and manually copy over your customizations, themes,
and extensions.

![important](~@assets/icon-important.png) **Important**:
To avoid errors after upgrading, you _must_ set up a parallel system.
_Do not_ upgrade your existing system or post-upgrade errors are likely
to occur.


1.  Make sure the CE or EE installation archive is located in the
    directory above where you want Magento to be installed.
    For example, to install Magento to `/var/www/magento`, copy the
    archive to `/var/www`.
2.  Extract the archive. For example, `tar -zxf archive-name`
    This installs Magento CE or EE on the file system on your
    development system. Next, you\'ll overwrite some of these assets
    with customizations from your production system.
3.  Extract the production system\'s `media` archive, overwriting the
    installation archive. You created the `media` archive as discussed
    in [Getting Ready For Your Upgrade](#prereq-tasks).

    ![tip](~@assets/icon-tip.png) **Tip**:
    Copy the media archive to the same level in the directory structure
    to simplify the process. For example, if you created the media
    archive on your production system in your Magento install directory,
    the archive when extracted creates the `media` directory and all
    subdirectories. If you copy the media archive to the Magento
    installation directory on your development system, extracting it
    automatically replaces existing `media` subdirectory contents.
    

4.  If necessary, move the `media` directory and subdirectories to
    `media` directory in your new Magento installation, overwriting the
    existing contents.
5.  Extract the custom theme package archive you created as discussed in
    [Getting Ready For Your Upgrade](#prereq-tasks).
6.  If necessary, move the theme packages to the
    `[your Magento install dir]/app/design/frontend` and
    `[your Magento install dir]/skin/frontend` directories, as
    appropriate, overwriting the existing contents.
7.  Extract the customization and extension archives you created as
    discussed in [Getting Ready For Your Upgrade](#prereq-tasks).
8.  If necessary, move the extensions and customizations to the
    `[your Magento install dir]/app/code/local` and
    `[your Magento install dir]/app/code/community` directories, as
    appropriate, overwriting the existing contents.

## [Updating the Magento Database ](#upgrade-manual-db)

This section discusses how to:

-   Import the production database data into the development database
    instance.
-   In the development database instance, change the values of the paths
    `web/unsecure/base_url` and `web/secure/base_url` in the
    `core_config_data` table to the development server\'s IP address or
    hostname.

Import the production database data using your database manager tool.

![important](~@assets/icon-important.png) **Important**:The
database instance into which you import _must be empty_.


`mysql` syntax follows:

    mysql -u root -p database-name < database-export-filename.sql

For example, if the database name is `magento` and the database export
file name is `mangento-db-export.sql`, enter

    mysql -u root -p magento < mangento-db-export.sql

The following sections discuss how to change the base URL paths from
`http://prod.example.com` to `http://dev.example.com` using SQL commands
and phpmyadmin:

-   [Updating the Base URL the Database Using SQL
    Commands](#upgrade-manual-db-sql)
-   [Updating the Base URL the Database Using
    phpmyadmin](#upgrade-manual-db-phpmyadmin)

### [Updating the Base URL the Database Using SQL Commands](#upgrade-manual-db-sql)

This section discusses how to update the secure and unsecure URLs of
your webstores and store views in the Magento database. The following
example assumes you have two URLs, which every Magento installation has
by default. Depending on your setup, you could have more (for additional
store views, Content Delivery Networks (CDNs) and so on.

Use the following procedure to update _all_ matching URLs in the
database.

To update the database with the development system\'s base URLs using
SQL commands:

1.  Log in to the database server as any user.

2.  Enter the following commands in the order shown:

        mysql -u root -p

    Enter the remaining commands at the mysql\> prompt.

        use magento-db-name;
        SELECT * FROM core_config_data WHERE path LIKE '%base_url%';

3.  In the resulting output, make note of the rows that include
    `web/unsecure/base_url` and `web/secure/base_url`.
    In particular, you must know the value of the `config_id` for these
    rows.

4.  Carefully examine the _entire URL_ to avoid mistakes.

5.  Enter the following command to update the values of
    `web/unsecure/base_url` and `web/secure/base_url`:

        UPDATE core_config_data SET value='base-url' WHERE config_id=id1 or config_id=id2;

    Example 1---`config_ids` are the same: if your base URL is
    `http://dev.example.com/`, the `config_id` of the row containing
    `web/unsecure/base_url` is 354 and the `config_id` of the row
    containing `web/secure/base_url` is 355, enter:

        UPDATE core_config_data SET value='http://dev.example.com/' WHERE config_id=354 or config_id=355;

    Example 2---`config_ids` are different: Same as preceding example
    except that `web/secure/base_url` uses `https://`

        UPDATE core_config_data SET value='http://dev.example.com/' WHERE config_id=354;
        UPDATE core_config_data SET value='https://dev.example.com/' WHERE config_id=355;

6.  Verify the values are set correctly.

        SELECT * FROM core_config_data WHERE path LIKE '%base_url%';

7.  For other URLs, be careful to perform the replacement correctly.
    For example, store view base link URLs resemble the following:

        http://prod.example.com/view1/

    Make sure to update the URL like the following:

        UPDATE core_config_data SET value='http://dev.example.com/view1/' WHERE config_id=377;

8.  When you\'re done, run the following command again:

        SELECT * FROM core_config_data WHERE path LIKE '%base_url%';

9.  If the values are set correctly, enter `exit` at the `mysql>` prompt
    and continue with [Finishing the File
    System](#upgrade-manual-cleanup).
    If not, repeat the tasks discussed in this section. _Do not
    continue_ until the database is updated.

### [Updating the Base URL the Database Using phpmyadmin](#upgrade-manual-db-phpmyadmin)

This section discusses how to use phpmyadmin to change the values of the
paths `web/unsecure/base_url` and `web/secure/base_url` in the
`core_config_data` table to the development server\'s IP address or
hostname.

1.  In a web browser\'s location or address field, enter
    `dev-web-server-host-or-ip/phpmyadmin` For example, if your web
    server address is `http://dev.example.com`, enter
    `http://dev.example.com/phpmyadmin`
2.  Log in as the MySQL `root` user.
3.  In the left navigation bar, click the name of your Magento database.
4.  In the left navigation bar, click the **core_config_data** table.
5.  At the top of the page, click the **Search** tab.
6.  In the Search tab page, from the **Operator** list, click **LIKE**.
7.  Enter `%base_url%` in the **Value** field.
    The following figure shows an example.
    (~@assets/install_myphpadmin_search.png){width="600px"
    height="279px"}
8.  Click **Go**.
    The following figure shows sample results.
    (~@assets/install_myphpadmin_results.png){width="600px"
    height="139px"}
9.  Click
    ![](~@assets/install_myphpadmin_edit-icon.png)
    (Edit) in the row that includes the path `web/unsecure/base_url`.
10. Change the value to the hostname or IP address of your development
    server.
    The following figure shows an example.
    (~@assets/install_myphpadmin_edit.png){width="600px"
    height="333px"}
11. Click **Go**.
12. Repeat these tasks for the path that includes `web/secure/base_url`.
13. Exit phpmyadmin.

## [Upgrading Solr (EE 1.14.0.0 only) ](#upgrade-manual-solr)

_This section applies to upgrading to EE 1.14.0.0 using the Solr search
engine only_. If you\'re upgrading to a different version---or if
you\'re not using Solr---skip this section and continue with [Finishing
the File System](#upgrade-manual-cleanup).

Because of changes to the Solr schema, you must copy two files from
Magento to your Solr installation. Failure to do so might prevent
products from displaying on your web store. After copying the files, you
must also reindex the catalog search index.

To update Solr:

1.  If the Solr search engine is running, stop it.

2.  Copy the following files from the Magento 1.14.0.0 installation to
    the equivalent directory on your Solr installation:

        [your Magento install dir]/lib/Apache/Solr/conf/schema.xml
        [your Magento install dir]/lib/Apache/Solr/conf/solrconfig.xml

    For example, if Magento is installed in `/var/www/html/magento` and
    the example Solr configuration is installed in the
    `/etc/solr/apache-solr-3.6.2/example/solr/conf` directory on the
    same host, enter:

        cp /var/www/html/magento/lib/Apache/Solr/conf/solrconfig.xml /etc/solr/apache-solr-3.6.2/example/solr/conf
        cp /var/www/html/magento/lib/Apache/Solr/conf/schema.xml /etc/solr/apache-solr-3.6.2/example/solr/conf

3.  Restart Solr.

4.  After you copy the files, you must update the catalog search index.
    To update the catalog search index, open a command prompt window.

5.  Change to the `shell` subdirectory of your Magento installation
    directory.
    For example, on CentOS:

        cd /var/www/html/magento/shell

6.  Enter the following command:

        php indexer.php --reindex catalogsearch_fulltext

![note](~@assets/icon-note.png) **Note**:
The preceding example showed paths used by the Solr example
configuration. Solr ships with a sample configuration that Magento
suggests you use for development only. Do not use the Solr example
configuration in production.


## [Finishing the File System ](#upgrade-manual-cleanup)

This section discusses how to edit copy `local.xml` and set file system
permissions and ownership on the production system.

### [Copying local.xml](#upgrade-manual-cleanup-localxml)

1.  Copy your production system\'s `local.xml` to
    `[your Magento install dir]/app/etc`.

2.  Open `local.xml` in a text editor.

3.  If necessary, change the database connection information in the
    `default_setup` element, as follows:

        <default_setup>
             <connection>
                <host><![CDATA[change-if-necessary]></host>
                <username><![CDATA[change-if-necessary]></username>
                <password><![CDATA[change-if-necessary]></password>
                <dbname><![CDATA[change-if-necessary]></dbname>
                <initStatements><![CDATA[SET NAMES utf8]></initStatements>
                <model><![CDATA[mysql4]></model>
                <type><![CDATA[pdo_mysql]></type>
                <pdoType><![CDATA[]></pdoType>
                <active>1</active>
             </connection>
        </default_setup>

4.  Save your changes to `local.xml` and exit the text editor.

### [Setting File and Directory Ownership and Privileges](#upgrade-manual-cleanup-perms)

Magento recommends the following ownership and privilege settings for
files and directories in the Magento installation directory:

-   The Magento installation directory and all subdirectories are owned
    by the web server user.
    This enables the web server to change files in these subdirectories
    but other users cannot access them (except a higher-level user such
    as `root`).
-   All directories have 700 permissions (`drwx------`).
    700 permissions give full control (that is, read/write/execute) to
    the owner and no permissions to anyone else.
-   All files have 600 permissions (`-rw-------`).
    600 permissions mean the owner can read and write but other users
    have no permissions.

**Note**: The way you set permissions and ownership depends on whether
Magento is running on a dedicated or hosted system:

-   _Hosted_: A Magento server located on a hosting provider. A _hosted_
    system typically does not enable you to elevate to `root`. The web
    server typically runs as an ordinary user. Magento assumes you log
    in as this user to start and stop the web server and that you
    already own all the files and directories in the Magento
    installation directory. You can use `chmod` to change permissions on
    files and directories.
-   _Dedicated_: A Magento server you control and operate. Unlike a
    hosted system, you can elevate to `root` and, as `root`, you can use
    the `chown` and `chmod` commands to set ownership and privileges in
    the Magento installation directory.

To set privileges and ownership:

1.  Log in to your Magento server.

2.  Change to your Magento installation directory:

        #Ubuntu example
        cd /var/www/magento

        #CentOS example
        cd /var/www/html/magento

3.  _Dedicated Magento server only_. Enter the following command to set
    ownership of the Magento installation directory and all its
    subdirectories:

        chown -R web-server-user-name .

    For example, on Ubuntu where Apache usually runs as `www-data`,
    enter

        chown -R www-data .

4.  Enter the following commands to set directory permissions to 700 and
    file permissions to 600:

        find . -type d -exec chmod 700 {} +
        find . -type f -exec chmod 600 {} +

## [Running the URL Redirect Script (EE 1.13.0.0 and 1.13.0.1 only) ](#upgrade-manual-post-upgrade-ee11300)

If you\'re upgrading to EE 1.14 from EE 1.13.0.0 or 1.13.0.1, see [EE
1.13.1.0 Upgrade: Running the EE 1.13.0.0 or EE 1.13.0.1 URL Redirect
Script](installing_upgrade_from-ee11300-or-01.html)
now. When you\'re done, return here to continue your upgrade.

## [Completing the Magento Upgrade ](#upgrade-manual-run)

To complete the upgrade, go to your Magento base URL in a web browser.
The first time you go to your Magento base URL, server-side scripts run
to update the database. Depending on the amount of data in your
database, this process can take a long time.

![important](~@assets/icon-important.png) **Important**:
_Completing the upgrade requires downtime on your production system_. If
you continue to take orders on your production system, there is no way
to reconcile all of the new orders in the replacement development
system. You can reduce the amount of downtime to a minimum by thoroughly
testing your development system before shutting down production.


If you\'re upgrading from a version earlier than CE 1.4 or EE 1.7,
Magento _strongly recommends_ the two-step upgrade approach discussed in
[Upgrade
Path](installing_upgrade_landing.html#upgrade-path).
In addition, you should expect the upgrade and testing process to take a
long time and to expect more downtime for your production system.

Complete the following tasks in the order shown:

1.  [Running the Upgrade Scripts and Fixing
    Exceptions](#upgrade-manual-scripts)
2.  [Running the URL Redirect Script _(EE 1.12.x and earlier
    only)_](#upgrade-manual-post-upgrade-11302)
3.  [Post-Upgrade Tasks _(EE 1.13.0.1
    Only)_](#upgrade-manual-post-upgrade-11300)
4.  To apply the [security
    patch](https://magento.com/security/patches/supee-8788) released in
    October, 2016, see [How to Apply the SUPEE-8788
    Patch](guides/m1x/other/ht_install-patches.html#apply-8788).
5.  [Setting Up the Magento Cron Job](#upgrade-manual-cron)
6.  [Clearing Magento var/ Subdirectories](#upgrade-manual-var)
7.  [Verifying the Status of Indexers](#upgrade-manual-index)
8.  [Testing the Magento Upgrade](#upgrade-manual-test)
9.  [Taking Your Old Production System Offline](#upgrade-downtime)

## [Running the Upgrade Scripts and Fixing Exceptions ](#upgrade-manual-scripts)

The first step in your upgrade is to run server-side upgrade scripts.
Depending on the nature of your customizations and extensions, and how
many customers and products are in your database, these scripts can take
a long time to run and can result in exceptions. You must resolve all
exceptions before continuing to the next step in the process.

This step in the upgrade process is iterative; that is, you\'ll probably
run through it more than once.

![note](~@assets/icon-note.png) **Note**:
Your production system remains up during this step in the upgrade
process. You can continue taking and shipping orders and also making
changes in the Admin Panel.


To run the upgrade scripts:

1.  In a web browser address or location field, enter your Magento
    installation\'s base URL.

    ![note](~@assets/icon-note.png) **Note**:
    If you upgraded Magento on the same server, you might need to update
    your Apache web server\'s virtual host configuration to reference
    the new Magento installation directory. For more information about
    configuring virtual hosts, see the [Apache Virtual Host
    documentation](http://httpd.apache.org/docs/2.2/vhosts).
    

    (You updated the Magento database with the development system base
    URL as discussed in [Updating the Magento
    Database](#upgrade-manual-db).)

2.  Wait while upgrade scripts run on the server.
3.  If fatal errors or exceptions display in the browser, they are most
    likely related to customizations. To resolve fatal errors, you must:

    1.  Analyze the errors to determine the root cause.
        You can find errors in the web server\'s error log or in
        `[your Magento install dir]/var/log`.

    2.  Fix the cause in your extension code.

    3.  In your development environment, drop the Magento database.

    4.  Re-create the development database and import production data
        into it as discussed in [Updating the Magento
        Database](#upgrade-manual-db).

    5.  Use the following command to delete the contents of the Magento
        `var/` subdirectories:

            rm -rf [your Magento install dir]/var/cache [your Magento install dir]/var/full_page_cache 
                [your Magento install dir]/var/locks [your Magento install dir]/var/session

        ![note](~@assets/icon-note.png) **Note**:
        Some of these directories might be empty; this is normal.
        

    6.  Clean up the file system as discussed in [Finishing the File
        System](#upgrade-manual-cleanup).

    7.  Repeat step 1 in this section.

    8.  Repeat the tasks discussed in this section until no more fatal
        errors display in the browser.

## [Post-Upgrade Tasks (EE 1.13.0.1 Only) ](#upgrade-manual-post-upgrade-11300)

_This section applies to upgrading from 1.13.0.1 only._ If you\'re
upgrading CE or if you\'re upgrading from a different EE version,
continue with [Setting Up the Magento Cron Job](#upgrade-manual-cron).

After successfully running the Magento upgrade and fixing errors, enter
the following command from the Magento root directory:

    php -f shell/indexer.php -- --reindexall

This command runs a full reindex and it might take a long time,
depending on the size of your database.

After the reindex completes, enter the following command from your
Magento installation directory:

    rm -rf var/cache var/full_page_cache var/locks

Continue with [Setting Up Magento Cron Jobs](#upgrade-manual-cron).

## [Running the URL Redirect Script (EE 1.12.x and Earlier Only) ](#upgrade-manual-post-upgrade-11302)

If you\'re upgrading to EE 1.14 from EE 1.12 or earlier, see [EE 1.14
Upgrade: Running the EE 1.12 and Earlier URL Redirect
Script](installing_upgrade_from-ee112.html)
now. When you\'re done, return here to continue your upgrade.

## [Setting Up the Magento Cron Job ](#upgrade-manual-cron)

You should now set up your Magento cron job as discussed
[here](installing_install.html#install-cron).

## [Clearing Magento var/ Subdirectories ](#upgrade-manual-var)

Run the following command to clean the Magento `var/` subdirectories:

    rm -rf [your Magento install dir]/var/cache [your Magento install dir]/var/full_page_cache [your Magento install dir]/var/locks

![note](~@assets/icon-note.png) **Note**:
Some of these directories might be empty; this is normal.


## [Verifying the Status of Indexers ](#upgrade-manual-index)

After the cron job runs, all indexers should be in a Ready status.
Before testing your upgrade, verify all indexers are Ready; otherwise,
your testing will be inconclusive.

To verify the status of indexers:

1.  Log in to the Admin Panel as an administrator.
2.  Click **System** \> **Index Management**.
    Verify that all indexers have a status of Ready, as the following
    figure shows.
    ![](~@assets/upgrade_index-management_after.png){width="600px"
    height="154px"}
3.  If indexers are _not_ Ready, use the following guidelines:
    Indexer status

## [Testing the Magento Upgrade ](#upgrade-manual-test)

After you have upgraded with no fatal errors or exceptions, Magento
strongly recommends you thoroughly test the upgrade in your development
environment as follows:

1.  Test all extensions and customizations thoroughly.
    Make sure the UIs perform as expected, make sure the extensions
    behave properly, and so on.
2.  Run some orders, making sure prices are calculated correctly and
    that orders go through the configured payment mechanisms.
3.  If you encounter issues, [get
    help](installing.html#help).
4.  Before making the development system live, back up everything in
    your production and development environments again.
    The way you make your development system live is up to you; a simple
    way is to update your DNS server to point to the development IP
    address.

## [Taking Your Old Production System Offline ](#upgrade-downtime)

The last step in upgrading is to take your production system offline and
switching to your development system---which then becomes the production
system from that point on.

![important](~@assets/icon-important.png) **Important**:
Before continuing, make sure all errors have been resolved and that you
have thoroughly tested the upgrade as discussed in the preceding
section.


To switch from your production to development system:

1.  Put your production system in maintenance mode so it cannot accept
    orders or other changes.
    To do so, create an empty file named
    `[your Magento install dir]/maintenance.flag`.
2.  Test it by going to your Magento web store\'s front page.
    The following message displays if you set up maintenance mode
    correctly:
    `The server is temporarily unable to service your request due to maintenance downtime or capacity problems. Please try again later.`
3.  If necessary, update the development database again to get any final
    transactions processed by your current production system.
    For details, see [Updating the Magento
    Database](#upgrade-manual-db).
4.  Put your production system in maintenance mode so it cannot process
    orders.
5.  Make your development system live.
    The way you do this is up to you; typically, you can configure your
    DNS server to send requests for your web store domain to the
    development system\'s IP address. Be aware that propagating the
    change to other DNS servers can take some time.
6.  Verify your former development system is functioning as your new
    Magento web store in production.
7.  Shut down the old production system.

Congratulations! You successfully upgraded! Review our [welcome
page](ce19-ee114-home.html) to see the
improvements you\'re getting.

## [Setting Magento File System Permissions and Ownership After Upgrade ](#install-privs-after)

To secure your Magento installation after the upgrade, see [After You
Install Magento: Recommended File System Ownership and
Privileges](installer-privileges_after.html).

## [Troubleshooting ](#upgrade-troubleshoot)

There is a known issue after upgrading to EE 1.13.1 that affects you
_only_ if you do _not_ follow the recommended procedure to upgrade to a
new environment as discussed in [Getting Ready For Your
Upgrade](#prereq-tasks).

**Symptom**: After completing the upgrade, when you log in to the Admin
Panel and click **System** \> **Configuration**, a fatal error similar
to the following displays in your browser:

    Class 'Mage_Googlecheckout_Helper_Data' not found in /var/www/html/magento/app/Mage.php on line 547

**Solution**:

1.  Close the Admin Panel browser window.

2.  As a user with `root` privileges, delete all files _except_
    `config.xml` from the following directory:

        [your Magento install dir]/app/code/core/Mage/GoogleCheckout/etc

3.  When you log back in to the Admin Panel, everything works as
    expected.

In the event of any other issue during the upgrade, see [Getting Help
With Your Installation or
Upgrade](installing.html#help).
