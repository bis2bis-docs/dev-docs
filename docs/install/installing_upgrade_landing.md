---
sidebar: auto
---



# Upgrading to and Verifying Magento Community Edition (CE) and Enterprise Edition (EE), Part 1

---
 

### [Table of Contents](#table-of-contents)

-   [Overview](#overview)
-   [Upgrade Path](#upgrade-path)
-   [Understanding the Upgrade to EE 1.13.0.2 or
    Later](#ee1300-11301-upgrade-to-ee11302-start)
    

## Overview

This article discusses how to upgrade to:

-   Magento Community Edition (CE) 1.8 or 1.9
-   Magento Enterprise Edition (EE) 1.13 or 1.14

## Upgrade Path

 
![important](~@assets/icon-important.png) **Important**:
Magento recommends CE 1.9.1.0 or later or EE 1.14.1.0 or later for _all
CE and EE installations and upgrades_ to get the latest fixes, features,
and security updates.


The following table provides basic information about how you perform
your upgrade. More detailed information is discussed later in this
article.

Edition and version Upgrade path

---

CE 1.4 or earlier Your current CE version \> CE 1.7 \> CE 1.9.0.0
CE 1.5 or later Your current CE version \> CE 1.9.0.0
EE 1.7 or earlier Your current EE version \> EE 1.12 \> EE 1.14.0.0
EE 1.8 or later Your current EE version \> EE 1.14.0.0

## [Magento EE Upgrade Path ](#upgrade-path-ee)

Because of changes to URL rewrites, the upgrade to Magento EE 1.13.0.2
or later is more complex than other upgrades. (This includes upgrading
to Magento EE 1.14.)

Follow are the specific versions affected:

-   Upgrading from EE 1.12 or earlier to EE 1.13.0.2 or later (including
    to EE 1.14)
-   Upgrading from EE 1.13.0.1 to EE 1.13.0.2 or later (including to EE
    1.14)

For more information about these upgrades, see [Understanding the
Upgrade to EE 1.13.0.2 or EE
1.14](#ee1300-11301-upgrade-to-ee11302-start).

 
![note](~@assets/icon-note.png) **Note**:
If you\'re upgrading from EE 1.13.0.2 to EE 1.13.1.0 or later, your
upgrade doesn\'t involve changes to URL rewrites and you can skip some
of the steps discussed in the other upgrades. Continue with [Upgrading
to and Verifying Magento Community Edition (CE) and Enterprise Edition
(EE)---Part
2](installing_upgrade_details.html).


## [Upgrading to Magento CE 1.9 ](#upgrade-path-ce)

Unless you\'re upgrading from an [older CE
version](#upgrade-path-older), skip the remainder of this article and
continue with [Upgrade Roadmap for Community Edition
(CE)](installing_upgrade_ce18_upgrade-roadmap.html).

## [Upgrading From Older Versions of Magento CE or EE ](#upgrade-path-older)

Provided you complete all tasks discussed in this article exactly as
discussed, you can upgrade to Magento CE 1.9 or EE 1.14 from CE 1.5 or
later or EE 1.8 or later.

**Important**: _Do not_ upgrade directly to this release if you\'re
currently running CE 1.4 or earlier or EE 1.7 or earlier. A safer
approach would be:

1.  Upgrade to CE 1.7 or EE 1.12
2.  Thoroughly test CE 1.7 or EE 1.12 and fix any issues
3.  Back up the system
4.  Upgrade to CE 1.9 or EE 1.14

 
![note](~@assets/icon-note.png) **Note**:

-   Use caution when upgrading from versions older than CE 1.7 or EE
    1.12 because core code on which your extensions are based and
    database schema have likely changed.
-   To apply the [security patch
    (October 2016)](https://magento.com/security/patches/supee-8788) as
    part of your upgrade, see [How to Apply the SUPEE-8788
    Patch](other/ht_install-patches.html#apply-8788).
    

To upgrade to CE 1.8, skip the remainder of this article and continue
with [Upgrading to and Verifying Magento Community Edition 1.8 and
Enterprise Edition 1.13---Part
2](installing_upgrade_details.html).

To upgrade from EE 1.13.0.2 to EE 1.13.1.0, skip the remainder of this
article and continue with [Upgrading to and Verifying Magento Community
Edition 1.8 and Enterprise Edition 1.13---Part
2](installing_upgrade_details.html).

## [Understanding the Upgrade to EE 1.13.0.2 or EE 1.14 ](#ee1300-11301-upgrade-to-ee11302-start)

The following sections apply _only_ to the following Magento EE
upgrades:

-   Upgrading from EE 1.12 or earlier to EE 1.13.0.2 or later
-   Upgrading from EE 1.13.0.1 to EE 1.13.0.2 or later

 
![note](~@assets/icon-note.png) **Note**:
If you\'re upgrading from EE 1.13.0.2 to EE 1.13.1.0 or later, your
upgrade doesn\'t involve changes to URL rewrites and you can skip some
of the steps discussed in the other upgrades. Continue with [Upgrading
to and Verifying Magento Community Edition (CE) and Enterprise Edition
(EE)---Part
2](installing_upgrade_details.html).


## [Why This Upgrade is Different ](#overview-ee11302-upgrade-url-keys)

This section discusses why the upgrade to EE 1.13.0.2 or later requires
you to perform different tasks than previous upgrades.

One of the results of the change to the way URL keys are handled is that
URLs for the same products or categories might need to change from the
URLs in the version you\'re upgrading from. Following are scenarios that
might cause URL keys to change:

-   All product keys must be unique. Any duplicate product URL keys are
    changed. For example, if two products had the URL key `nike-shoes`,
    one of them is changed to a URL key like `nike-shoes-1` by the URL
    redirect scripts. This also resolves any conflicts (for example, if
    you already had another product with key `nike-shoes-1`).
-   Categories at the same level in the hierarchy in the same store view
    must have unique URL keys.
    For example, if you have a `shoes` category with URL key `shoes` in
    English store view, `schuhe` in the German store view and `schuhe`
    in the Swiss store view, the upgrade scripts change one of the
    category\'s URL keys to something like `schuhe-1`.

To ensure customers and search engines will be directed to the correct
page, Magento provides a script that creates HTTP 301 (Moved
Permanently) redirects for URLs that are changed.

In other words, after the upgrade, all pre-upgrade URLs work but they
might be different than they were before you upgraded.

The way you upgrade depends on what EE version you\'re starting from.
The following roadmaps provide a high-level overview of the upgrade
process:

-   [Upgrading From EE 1.12 or Earlier](#ee11302-upgrade-112)
-   [Upgrading From EE 1.13.0.1](#ee11302-upgrade-113)

## [Upgrading From EE 1.12 or Earlier ](#ee11302-upgrade-112)

Following is a high-level roadmap to upgrade to EE 1.13.0.2 or later
from EE 1.12 or earlier. This upgrade involves tasks not required for
other Magento upgrades; these new tasks are indicated in the roadmap.

1.  Install Magento in a different directory:
2.  _Recommended_. Set up a new system (that is, another host) on which
    to install Magento.
    The system should be identical to, if not better than, your current
    system. The new system must meet the [Magento system
    requirements](../system-requirements.html).
3.  Install Magento in a new, empty root installation directory on the
    same server.
4.  In your current production environment:
    1.  Back up your Magento database.
    2.  Archive the file system.
        This includes the `media` directory and subdirectories; all
        extensions and customizations; and all custom themes.
5.  In the development or test environment:

    1.  Create a new, empty database instance.

    2.  Import the production database tables into the development
        database instance.

    3.  Copy your production `media` directory, extensions, themes, and
        other customizations to the development system.

    4.  Copy `local.xml` to `[your Magento install dir]/app/etc` and
        edit it if necessary to reference the production database
        instance.

    5.  Stop all cron jobs.

    6.  In a web browser, go to your new base URL.

    7.  Wait for upgrade scripts to run.

    8.  Set all indexers to update when scheduled (**System** \>
        **Configuration** \> ADVANCED \> **Index Management**, **Update
        when scheduled**.

    9.  Run the URL redirect script to create 301 redirects:

            cd [your Magento install dir]
            php -f shell/url_migration_to_1_13.php - thread-count

        _thread-count_ is the number of CPU cores in your Magento
        server, minus 1, to a maximum of 15.

    10. Run a full reindex from the command line:

            [your Magento install dir]/php -f shell/indexer.php -- --reindexall

    11. Clear the following directories from the command line:

            [your Magento install dir]/var/cache /var/full_page_cache /var/locks

    12. Enable cron and make sure the Magento cron job is set up.

    13. Verify the upgraded system is now identical to the production
        system.
        If not, fix issues, retest, and upgrade again.

6.  _Test the upgrade thoroughly_, including:
7.  Verify all extensions, themes, and customizations work.
8.  Place orders using all webstores and all payment methods.

Step-by-step instructions for your upgrade start
[here](installing_upgrade_details.html).

## [Upgrading From EE 1.13.0.1 ](#ee11302-upgrade-113)

Following is a high-level roadmap to upgrade to EE 1.13.0.2 or later
from EE 1.13.0.1. This upgrade involves tasks not required for other
Magento upgrades; these new tasks are indicated in the roadmap.

1.  Install Magento in a different directory:
2.  _Recommended_. Set up a new system (that is, another host) on which
    to install Magento.
    The system should be identical to, if not better than, your current
    system. The new system must meet the [Magento system
    requirements](../system-requirements.html).
3.  Install Magento in a new, empty root installation directory on the
    same server.
4.  In your current production environment:
    1.  Back up your Magento database.
    2.  Archive the file system.
        This includes the `media` directory and subdirectories; all
        extensions and customizations; and all custom themes.
5.  In the development or test environment:

    1.  Create a new, empty database instance.

    2.  Import the production database tables into the development
        database instance.

    3.  Copy your production `media` directory, extensions, themes, and
        other customizations to the development system.

    4.  Copy `local.xml` to `[your Magento install dir]/app/etc` and
        edit it if necessary to reference the production database
        instance.

    5.  Stop all cron jobs.

    6.  Run the URL redirect script to create 301 redirects:

            [your Magento install dir]/php -f shell/url_migration_from_1_13_0_0_to_1_13_0_2.php

    7.  In a web browser, go to your new base URL.

    8.  Wait for upgrade scripts to run.

    9.  Run a full reindex from the command line:

            [your Magento install dir]/php -f shell/indexer.php -- --reindexall

    10. Clear the following directories from the command line:

            [your Magento install dir]/var/cache /var/full_page_cache /var/locks

    11. Enable cron and make sure the Magento cron job is set up.

    12. Verify the upgraded system is now identical to the production
        system.
        If not, fix issues, retest, and upgrade again.

6.  _Test the upgrade thoroughly_, including:
7.  Verify all extensions, themes, and customizations work.
8.  Place orders using all webstores and all payment methods.

**Important**:

-   Only after upgrade is tested and all errors fixed should you
    consider using it as your production system.
-   _Back up_ your system frequently during the upgrade process so you
    can roll back to a previous state in the event of errors or issues.
-   To avoid unnecessary downtime and potential issues, _never_ upgrade
    Magento in a live environment!

Step-by-step instructions for your upgrade start
[here](installing_upgrade_details.html).

