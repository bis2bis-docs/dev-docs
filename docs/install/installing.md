---
sidebar: auto
---



 
Installing and Upgrading to Magento Community Edition (CE) and Magento Enterprise Edition (EE)
---========

 

### [Table of Contents](#table-of-contents)

-   [Overview](#overview)
-   Getting Help With Your Installation or Upgrade
-   [Getting Magento Software](#get)
-   [Installing Magento CE or
    EE](installing_install.html)
-   [Upgrading to Magento CE or
    EE](installing_upgrade_landing.html)
    

## Overview

We\'re happy you chose to install or upgrade your Magento software.
We\'re going to help you to install or to upgrade to the following
versions:

-   Magento Community Edition (CE) 1.8 or 1.9
-   Magento Enterprise Edition (EE) 1.13. or 1.14

These releases include a number of improvements:

-   [Magento Community Edition (CE) Release
    Notes](http://www.magentocommerce.com/knowledge-base/entry/ce19-later-release-notes)
-   [Magento Enterprise Edition (EE) Release
    Notes](http://www.magentocommerce.com/knowledge-base/entry/ee114-later-release-notes)
-   [Magento CE and EE documentation home
    page](ce19-ee114-home.html), which
    also has information about new features

 
![important](~@assets/icon-important.png) **Important**:
Use CE 1.9.1 or EE 1.14.1 or later for _all new EE installations and
upgrades_ to get the latest fixes, features, and security updates.


The sections that follow get you started on your installation or
upgrade. Detailed, step-by-step procedures are discussed in the
following articles:

-   [Installing Magento CE or Magento
    EE](installing_install.html)
-   [Upgrading to Magento CE or
    EE](installing_upgrade_landing.html)

## [Getting Help With Your Installation or Upgrade ](#help)

In the event you have a large, distributed system or you need additional
help, consult the following resources.



## [Getting Magento CE or EE ](#get)

The following table discusses where to get the upgrade image and
optional sample data.

+-----------------------------------+-----------------------------------+
| Magento edition | Upgrade image location |
+===================================+===================================+
| Magento CE | [www.mage |
| | ntocommerce.com/download](http:// |
| | www.magentocommerce.com/download) |
+-----------------------------------+-----------------------------------+
| Magento EE | Use the following steps: |
| | |
| | 1. Go to |
| | [www.mag |
| | ento.com](http://www.magento.com) |
| | 2. In the top horizontal |
| | navigation bar, click **My |
| | Account**. |
| | 3. Log in with your Magento |
| | username and password. |
| | 4. In the left navigation bar, |
| | click **Downloads**. |
| | 5. In the right pane, click |
| | **Magento Enterprise |
| | Edition** \> **Release** for |
| | the software or **Sample |
| | Data** for the optional |
| | sample data. |
| | 6. Follow the instructions on |
| | your screen to complete the |
| | Magento EE download. |
| | 7. Transfer the installation |
| | archive to your development |
| | system. |
+-----------------------------------+-----------------------------------+

## [Installing Magento CE or EE ](#install)

See [Installing Magento CE or
EE](installing_install.html).

## [Upgrading to Magento CE or EE ](#upgrade-manual)

See [Upgrading to Magento CE or
EE](installing_upgrade_landing.html).

