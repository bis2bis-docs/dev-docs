---
sidebar: auto
---



 
# Installing and Verifying Magento Community Edition (CE) and Enterprise Edition (EE)

---


### [Contents](#contents)

-   [Overview](#overview)
-   [Prerequisites](#prereq)
-   [Preparing Your Server for Magento CE or EE](#prepare)
-   [Installing Magento CE or EE](#install-magento)
-   [Verifying that Magento CE or Magento EE Installed
    Successfully](#verify)
-   [Setting Up Cronjobs](#install-cron)
-   [Setting Magento File System Permissions and Ownership After
    Installation](#install-privs-after)
-   [Troubleshooting](#install-troubleshoot)
    

## Overview

This article discusses how to install:

-   Magento Community Edition (CE) 1.8.x or 1.9.x
-   Magento Enterprise Edition (EE) 1.13.x or 1.14.x

To install Magento CE or Magento EE, use the following high-level
guidelines:

1.  Set up a system that meets the [Magento system
    requirements](../system-requirements.html).
2.  Install prerequisite software (web server, PHP, and required PHP
    extensions).
3.  Set up a MySQL Magento database instance.
4.  Run the Magento installer to complete the installation.
5.  Verify that Magento installed correctly.

 
![important](~@assets/icon-important.png) **Important**:
Perform _all installations_ to Magento the latest version of CE 1.9 or
Magento EE 1.14 to get the latest fixes, features, and security updates.


## [Prerequisites ](#prereq)

Before starting your installation:

-   Get the Magento installation archive as discussed in [Getting
    Magento CE or
    EE](installing.html#get).
-   Set up one or more hosts that meet the [Magento system
    requirements](../system-requirements.html).
-   If you are setting up more than one web node with load balancing,
    set up and test that part of your system _before_ you install
    Magento.
-   If your database server and web server are located on different
    hosts, [get
    help](installing.html#help)
    before proceeding.
-   Make sure you can back up your entire system at various points
    during the installation so you can roll back in the event of issues.

Magento _strongly_ recommends you observe the following guidelines when
you set up your Magento database:

-   Magento for the first time uses [MySQL database
    triggers](http://dev.mysql.com/doc/refman/5.0/en/triggers.html) to
    improve database access during reindexing. Magento does not support
    any custom triggers in the Magento database because custom triggers
    can introduce incompatibilities with future Magento versions.
-   Familiarize yourself with [these potential MySQL trigger
    limitations](http://dev.mysql.com/doc/mysql-reslimits-excerpt/5.1/en/stored-program-restrictions.html)
    before you continue.
-   If you use MySQL database replication, be aware that Magento _does
    not_ support MySQL statement-based replication. Make sure you use
    only [row-based
    replication](http://dev.mysql.com/doc/refman/5.1/en/replication-formats.html).

More information, including software prerequisites, can be found in the
next section.

## [Preparing Your Server for Magento CE or EE ](#prepare)

The following sections discuss how to download and install prerequisite
software and install Magento CE or EE on a system running:

-   Ubuntu 10 or later, or CentOS 6
-   Apache 2.x
-   nginx 1.7.x
-   PHP 5.4 and required extensions
-   MySQL database

**Notes**:

-   Discussing how to configure Secure Sockets Layer (SSL) is beyond the
    scope of this guide. You can consult some general resources such as
    [Apache](http://httpd.apache.org/docs/2.4/ssl/),
    [zytrax.com](http://www.zytrax.com/tech/survival/ssl.html), or the
    [OpenSSL documentation page](https://www.openssl.org/docs).
-   Discussing how to [install and configure
    nginx](http://wiki.nginx.org/Install) is beyond the scope of this
    article.
-   Review the MySQL developer site\'s recommendations for [MySQL
    password
    security](http://dev.mysql.com/doc/refman/5.0/en/password-security-user.html).
-   Before you continue, make sure the system on which you install
    Magento meets the [Magento system
    requirements](../system-requirements.html).

Complete the tasks discussed in the following sections in the order
shown:

1.  Installing and configuring required prerequisite Software for
    [Ubuntu](installing_req-prereq_ubuntu.html)
    or
    [CentOS](installing_req-prereq_centos.html)
2.  [SELinux Prerequisite](#install-prereq-selinux)
3.  [Opening Ports In Your Firewall](#install-iptables)
4.  [Installing Optional Software](#install-optional)
5.  [Creating a Magento Database Instance](#install-create-db)
6.  [Extracting the Magento Installation
    Archive](#install-extract-archive)
7.  [Installing Optional Sample Data](#install-sample)

 
![note](~@assets/icon-note.png) **Note**:
Unless otherwise noted, all tasks discussed in this article must be
performed by a user with `root` privileges.


## [SELinux Prerequisite ](#install-prereq-selinux)

[Security Enhanced Linux
(SELinux)](http://selinuxproject.org/page/Main_Page) enables CentOS and
Ubuntu administrators greater access control over their servers. If
you\'re using SELinux _and_ Apache must initiate a connection to another
host, you must run the commands discussed in this section.

If Apache and the database server are on the same host, you can skip
this section and continue with [Opening Ports In Your
Firewall](#install-iptables).

To enable Apache to initiate a connection to another host with SELinux
enabled:

1.  To determine if SELinux is enabled, use the following command:

        getenforce

    `Enforcing` displays to confirm that SELinux is running.

2.  Enter one of the following commands:

    -   CentOS

            setsebool -P httpd_can_network_connect=1

    -   Ubuntu

            setsebool -P apache2_can_network_connect=1

## [Opening Ports In Your Firewall ](#install-iptables)

Depending on your security requirements, you might find it necessary to
open port 80 and other ports in your firewall. Because of the sensitive
nature of networking security, Magento strongly recommends you consult
with your IT department before proceeding. Following are some suggested
references:

-   Ubuntu: [Ubuntu documentation
    page](https://help.ubuntu.com/community/IptablesHowTo).
-   CentOS: [CentOS
    how-to](http://wiki.centos.org/HowTos/Network/IPTables).

## [Installing Optional Software ](#install-optional)

This section discusses how to create or install the following:

-   Install and configure Network Time Protocol (NTP) which enables you
    to synchronize the system clock with pool servers located around the
    world. NTP is particularly useful for synchronizing the clocks of
    multiple hosts.
-   Create a
    [`phpinfo.php`](http://php.net/manual/en/function.phpinfo.php) file
-   Install and configure the `phpmyadmin` database administration
    utility.

These optional utilities might assist you with your installation;
however, they are not required to install or use Magento.

To skip installing these utilities, go directly to [Creating a Magento
Database Instance](#install-create-db).

See the following sections:

-   [Installing and Configuring Network Time Protocol
    (NTP)](#install-optional-ntp)
-   [Creating phpinfo.php](#install-optional-phpinfo)
-   [Installing phpmyadmin](#install-optional-phpmyadmin)

### [Installing and Configuring Network Time Protocol (NTP) ](#install-optional-ntp)

[NTP](http://www.ntp.org) enables servers to synchronize their system
clocks using [globally available pool
servers](http://www.pool.ntp.org/en). Magento recommends you use NTP
servers you trust, whether they are dedicated hardware solutions your
internal network or external, public servers.

If you are deploying Magento on multiple hosts, NTP is a simple way to
guarantee their clocks are all synchronized, no matter what time zone
the servers are in.

To install and configure NTP:

1.  _CentOS only._

    1.  Enter the following command to find the appropriate NTP
        software: `yum search ntp`
    2.  Select a package to install. For example, `ntp.x86_64`.
    3.  Install the package. `yum -y install ntp.x86_64`

2.  _Ubuntu only_. Enter the following command to install NTP:
    `apt-get install ntp`

3.  Select the NTP pool servers you wish to use.
    Selecting pool servers is up to you. If you use NTP pool servers,
    ntp.org recommends you use [pool
    servers](http://www.pool.ntp.org/en) that are close to your
    servers\' time zone as discussed on the [NTP pool project help
    page](http://www.pool.ntp.org/en/use.html). If you have a private
    NTP server that is available to all hosts in your Magento
    deployment, you can use that server instead.

4.  Open `/etc/ntp.conf` in a text editor.

5.  Look for lines similar to the following:

        server 0.centos.pool.ntp.org
        server 1.centos.pool.ntp.org
        server 2.centos.pool.ntp.org

6.  Replace those lines or add additional lines that specify your NTP
    pool server or other NTP servers. It\'s a good idea to specify more
    than one.
    An example of using three United States-based NTP servers follows:

        server 0.us.pool.ntp.org
        server 1.us.pool.ntp.org
        server 2.us.pool.ntp.org

7.  Save your changes to `/etc/ntp.conf` and exit the text editor.

8.  _CentOS only_. Enter the following command so that NTP starts when
    the server starts.

        chkconfig ntpd on

9.  Restart the service.

    -   CentOS

            service ntpd restart

    -   Ubuntu

            service ntp restart

10. Enter the `date` command to check the server\'s date.
    If the date is incorrect, make sure the NTP client port (typically,
    UDP 123) is open in your firewall. Try the
    `ntpdate pool-server-host-name` command. If it fails, search for the
    error it returns.
    If all else fails, try restarting the server.

### [Creating phpinfo.php](#install-optional-phpinfo)

[`phpinfo.php`](http://php.net/manual/en/function.phpinfo.php) displays
a large amount of information about PHP and its extensions. Add the
following code anywhere in your web server\'s docroot:

    <?php
    // Show all information, defaults to INFO_ALL
    phpinfo();

For more information, see the [phpinfo manual
page](http://php.net/manual/en/function.phpinfo.php).

To view the results, enter the following URL in your browser\'s location
or address field:

    http://web-server-ip-or-host/phpinfo.php

If a 404 (Not Found) error displays, check the following:

-   Start the web server if necessary.
-   Make sure your firewall allows traffic on port 80.
    [Help for Ubuntu](https://help.ubuntu.com/community/UFW)
    [Help for CentOS](http://wiki.centos.org/HowTos/Network/IPTables)

### [Installing phpmyadmin](#install-optional-phpmyadmin)

`phpmyadmin` is an easy-to-use, free database administration utility.
You can use it to check and manipulate the contents of your database.
You must log in to `phpmyadmin` as the MySQL database administrative
user.

For more information about `phpmyadmin`, see the [phpmyadmin home
page](http://www.phpmyadmin.net/home_page/index.php).

For more detailed information about installation, see the [phpmyadmin
installation
documentation](http://docs.phpmyadmin.net/en/latest/setup.html#quick-install).

To install phpmyadmin on CentOS:

1.  Download the epel RPM for the version of CentOS you\'re using. A
    sample follows.

        cd /tmp

        wget http://download.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm

        rpm -ivh epel-release-6-8.noarch.rpm

2.  Install `phpmyadmin` as follows:

        yum -y install phpmyadmin

3.  Authorize access to phpmyadmin from your machine\'s IP address.

    1.  Open the following file for editing:

            vim /etc/httpd/conf.d/phpMyAdmin.conf

    2.  Replace the following IP address with your IP address

            #Require ip 127.0.0.1

        For example,

            Require ip 192.51.100.101

    3.  Replace the following IP with your IP address

            #Allow from 127.0.0.1

        For example,

            Allow from 192.51.100.101

4.  Save your changes to `/etc/httpd/conf.d/phpMyAdmin.conf` and exit
    the text editor.

5.  Restart Apache.

        service httpd restart

6.  To use phpmyadmin, enter the following command in your browser\'s
    address or location field:

        http://host-or-ip-address/phpmyadmin

7.  When prompted, log in using your MySQL database `root` or
    administrative user\'s username and password.

To install phpmyadmin on Ubuntu:

1.  Use the following command: `apt-get install phpmyadmin`
2.  Follow the prompts on your screen to complete the installation.
3.  To use phpmyadmin, enter the following URL in your browser\'s
    address or location field: `http://host-or-ip-address/phpmyadmin`
4.  When prompted, log in using your MySQL database `root` or
    administrative user\'s username and password.

## [Creating a Magento Database Instance ](#install-create-db)

This section discusses how to create a new database instance for
Magento. Although a new database instance is recommended, you can
optionally install Magento into an existing database instance. If you
choose to do that, skip this section and continue with [Installing
Optional Sample Data](#install-sample).

 
![note](~@assets/icon-note.png) **Note**:
Before you continue, review the information about MySQL discussed in
[Prerequisites](#prereq).


To create a new database instance:

1.  Log in to your database server as any user.

2.  Enter the following commands in the order shown to create a database
    instance named `magento`:

        mysql -u root -p
        #Enter the remaining commands at the mysql> prompt.

        create database magento;
        GRANT ALL ON magento.* TO magento@localhost IDENTIFIED BY 'magento';

3.  For MySQL versions between 5.0.2 and 5.1.6, you must enter this
    command:

        GRANT SUPER ON *.* TO 'magento'@'localhost';

4.  After you\'re done, enter `exit`

5.  Test the database instance.

        mysql -u magento -p

    Messages similar to the following display to confirm you
    successfully created the database instance. If errors display,
    repeat the preceding commands.

        Welcome to the MySQL monitor.  Commands end with ; or \g.
        Your MySQL connection id is 20
        Server version: 5.1.67 Source distribution

        Copyright (c) 2000, 2012, Oracle and/or its affiliates. All rights reserved.

        Oracle is a registered trademark of Oracle Corporation and/or its
        affiliates. Other names may be trademarks of their respective
        owners.

        Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

## [Extracting the Magento Installation Archive ](#install-extract-archive)

Extract the Magento installation archive on your web server\'s docroot.

The path on Ubuntu is typically `/var/www`

The path on CentOS is typically `/var/www/html`

Examples follow:

-   Ubuntu

        cd /var/www
        cp /path/magento-install-archive .
        tar -zxf magento-install-archive-name

-   CentOS

        cd /var/www/html
        cp /path/magento-install-archive-name .
        tar -zxf magento-install-archive

To optionally install Magento sample data, continue with the next
section.

To install Magento without sample data, continue with [Setting File and
Directory Ownership and Privileges](#install-privs).

## [Installing Optional Sample Data ](#install-sample)

Magento provides sample data you can optionally install to give you an
idea of what products look like in the Admin Panel and in your web
store. You can use this sample data to learn how Magento works and to
experiment with custom themes.

You must install sample data on the file system and in the database
_before_ you install Magento.

 
![note](~@assets/icon-note.png) **Note**:
_Only if your database is shared between more than one Magento
installation_. Database table names used by the sample data are not
configurable. To use sample data with a new Magento installation, you
must manually add a prefix to all sample data tables and use the same
prefix when you install Magento.


To skip sample data installation, continue with [Setting File and
Directory Ownership and Privileges](#install-privs).

To install Magento sample data:

1.  Download the sample data from one of the locations discussed in
    [Getting Magento CE or
    EE](installing.html#get).

2.  If necessary, transfer the sample data to your Magento server.

3.  On your Magento server, extract the sample data archive to a
    directory other than your Magento installation directory.

4.  Make note of the path to the sample data\'s `media` subdirectory.

5.  Change to the `[your Magento install dir]/media` directory.

        #Ubuntu example
        cd /var/www/magento/media

        #CentOS example
        cd /var/www/html/magento/media

6.  As a user with privileges to write to the Magento installation
    directories (typically the web server user), copy the sample data\'s
    `media` directory and subdirectories to your Magento installation
    directory.
    For example, to copy Magento sample data from `/home/username` to
    `/var/www/magento`, enter

        cp -R /home/username/media/* .

7.  _Magento CE 1.9 and Magento EE 1.14 only_. You must also copy the
    sample data\'s `skin` directory to `[your Magento install dir]/skin`
    as follows:
    For example, to copy Magento skin files from `/home/username/skin`
    to `/var/www/magento/skin`, enter

        cd [your Magento install dir]/skin
        cp -R /home/username/skin/* .

8.  Import the CE or EE sample data into your MySQL database as follows:

        mysql -u root -p magento-db-instance-name < path-to-sample-data-extract-dir/sample-data-filename.sql

    EE 1.14 example

        mysql -u root -p magento < /home/username/magento_sample_data_for_1.14.0.0.sql

## [Setting File and Directory Ownership and Privileges ](#install-privs)

Magento recommends the following ownership and privilege settings for
files and directories in the Magento installation directory:

-   The Magento installation directory and all subdirectories are owned
    by the web server user.
    This enables the web server to change files in these subdirectories
    but other users cannot access them (except a higher-level user such
    as `root`).
-   All directories have 700 permissions (`drwx------`).
    700 permissions give full control (that is, read/write/execute) to
    the owner and no permissions to anyone else.
-   All files have 600 permissions (`-rw-------`).
    600 permissions mean the owner can read and write but other users
    have no permissions.

**Note**: The way you set permissions and ownership depends on whether
Magento is running on a dedicated or hosted system:

-   _Hosted_: A Magento server located on a hosting provider. A _hosted_
    system typically does not enable you to elevate to `root`. The web
    server typically runs as an ordinary user. Magento assumes you log
    in as this user to start and stop the web server and that you
    already own all the files and directories in the Magento
    installation directory. You can use `chmod` to change permissions on
    files and directories.
-   _Dedicated_: A Magento server you control and operate. Unlike a
    hosted system, you can elevate to `root` and, as `root`, you can use
    the `chown` and `chmod` commands to set ownership and privileges in
    the Magento installation directory.

To set privileges and ownership:

1.  Log in to your Magento server.

2.  Change to your Magento installation directory:

        #Ubuntu example
        cd /var/www/magento

        #CentOS example
        cd /var/www/html/magento

3.  _Dedicated Magento server only_. Enter the following command to set
    ownership of the Magento installation directory and all its
    subdirectories:

        chown -R web-server-user-name .

    For example, on Ubuntu where Apache usually runs as `www-data`,
    enter

        chown -R www-data .

4.  Enter the following commands to set directory permissions to 700 and
    file permissions to 600:

        find . -type d -exec chmod 700 {} +
        find . -type f -exec chmod 600 {} +

## [Installing Magento CE or EE ](#install-magento)

This section discusses how to run the Magento installer, using Magento
EE as an example. The Magento CE installer has exactly the same options;
only the appearance is different.

 
![important](~@assets/icon-important.png) **Important**:
The procedure that follows assumes that your web server and database
server are on the same host. If they are installed on different hosts,
additional tasks are required. [Get
help](installing.html#help)
before you continue your installation.


To install Magento CE or EE:

Complete all of the tasks discussed earlier in this article.

Enter the following URL in your web browser\'s address or location
field:

    web-server-ip-or-host:port/magento-path/magento

For example, if your web server is `http://www.example.com`, listens on
port 80, and Magento is installed in the web server docroot\'s `magento`
subdirectory, enter

    http://www.example.com/magento

The following page displays.
![](~@assets/install_first-page.png) 

Select the checkbox that indicates you agree to the terms and
conditions, and click **Continue**.

On the Localization page, enter the following information.

Option Meaning

---

Locale From the list, click the option that best matches the locale in which your Magento server resides.
Time Zone From the list, click the option that best matches the time zone in which your Magento server resides.
Default currency From the list, click the default currency to use on your Magento web store.


The following figure shows an example of configuring Magento for US
English in the US Central time zone and using the US Dollar as the
default currency.
(~@assets/install_localization.png) 

Click **Continue**.
The Configuration page displays. Each of its three sections is discussed
in the following step.

In the Configuration page, enter the following information:

1.  In the Database Configuration section, enter the following
    information.
    +-----------------------------------+-----------------------------------+
    | Option | Meaning |
    +===================================+===================================+
    | Database Type | From the list, click **MySQL**. |
    +-----------------------------------+-----------------------------------+
    | Host | Enter the database server\'s |
    | | fully qualified hostname or IP |
    | | address. Use the default setting |
    | | of **localhost** if your database |
    | | server is on the same host as |
    | | your web server. |
    +-----------------------------------+-----------------------------------+
    | Database Name | Enter the name of the Magento |
    | | database instance in which you |
    | | want to install the Magento |
    | | database tables. |
    +-----------------------------------+-----------------------------------+
    | User Name | Enter the username of the Magento |
    | | database instance owner. |
    +-----------------------------------+-----------------------------------+
    | User Password | Enter the Magento database |
    | | owner\'s password. |
    +-----------------------------------+-----------------------------------+
    | Tables Prefix | _(Optional.)_ Use only if you\'re |
    | | installing the Magento database |
    | | tables in a database instance |
    | | that has Magento tables in it |
    | | already. In that case, enter a |
    | | prefix to identify the Magento |
    | | tables for this installation. |
    | | |
    | | Some customers have more than one |
    | | Magento instance running on a |
    | | server with all tables in the |
    | | same database. This option |
    | | enables those customers to share |
    | | the database server with more |
    | | than one Magento installation. |
    +-----------------------------------+-----------------------------------+

    
    A sample follows.
    (~@assets/install_configuration_database.png){width="600px"
    height="274px"}

2.  In the Web Access Options section, enter the following information.

    ***

    Option Meaning

    ***

    Base URL Enter the base URL to use to access the Magento Admin Panel
    and your Magento web store.

    Admin Path Enter the path to access the Admin Panel. This path is
    appended to **Base URL**.
     For example, if **Base URL** is `http://www.example.com` and
    **Admin Path** is `admin`, the Admin Panel\'s URL is
    `http://www.example.com/admin`---provided you configured
    your web server for server rewrites.

    Enable Charts Select the checkbox to display charts on the Admin Panel.

    Skip Base URL Validation Before the Clearing this checkbox validates your server\'s base URL by
    Next Step performing an HTTP GET. Clear this checkbox unless your web
    server\'s base URL is not verifiable; for example, in a
    development or test environment.

    Use Web Server (Apache) Rewrites Select this checkbox to enable the use of the Apache
    [`mod_rewrite`](http://httpd.apache.org/docs/2.2/rewrite/)
    module. You can select this checkbox only if you configured
    Apache to use server rewrites.

    Use Secure URLs (SSL) Select this checkbox only if your web server supports SSL.

    ***

    
    A sample follows.
    ![](~@assets/install_web-access-options.png){width="600px"
    height="344px"}

3.  In the Session Storage Options section, click one of the following
    options:
    -   **File** to store user session data on the file system in the
        `[your Magento install dir]/var/session` directory.
        File-based session storage is appropriate unless the Magento
        file system access is slow or you have a clustered database.
    -   **Database** to store user session data in the database.
        Choose database storage if you have a clustered database;
        otherwise, there might not be much benefit over file-based
        storage.
4.  Click **Continue**.
     
    ![important](~@assets/icon-important.png) **Important**:
    If errors display, you must resolve them before continuing.
    
5.  In the Create Admin Account page, enter the following information.

    ***

    Option Meaning

    ***

    First Name Enter the first name of the user you want to
    identify as the Magento web store owner.

    Last Name Enter the user\'s last name.

    Email Enter the user\'s email address.

    Username Enter the Magento Admin Panel administrator\'s
    username. (You can create additional Magento
    administrators later.)

    Password Enter the user\'s password.

    Confirm Password Enter the user\'s password again for
    verification.

    Encryption Key If you have one, enter a key to encrypt sensitive
    data in the Magento database. If you don\'t have
    one, Magento generates one for you.
     The encryption key is stored in
    `[your Magento install dir]/app/etc/local.xml`.

    ***

    
    A sample follows.
    ![](~@assets/install_create-admin-account.png){width="600px"
    height="511px"}

6.  Click **Continue**.
    The following page displays to indicate a successful installation.
    (~@assets/install_confirm_mock.png){width="600px"
    height="308px"}

## [Verifying that Magento CE or Magento EE Installed Successfully ](#verify)

To make sure Magento installed properly, you should log in to the Admin
Panel to verify the Magento version.

You can also import products into Magento or perform other tasks that
verify you can write to the database.

1.  In a web browser\'s location or address field, enter the URL to the
    Admin Panel. An example follows:

        http://www.example.com/magento/admin

    (The Admin Panel URL is a combination of the **Base URL** and
    **Admin Path** fields you entered when you installed Magento.

2.  Log in to the Admin Panel as an administrator.

3.  Scroll to the bottom of the page; the version should display as 1.14
    (Magento EE) or 1.9.0.0 (Magento CE).
    The following figure shows an example.
    ![](~@assets/install_verify-version_mock.png){width="600px"
    height="36px"}

Congratulations! You successfully installed Magento!

## [Setting Up Cronjobs ](#install-cron)

Several Magento features require at least one cronjob, which schedules
activities to occur in the future. A partial list of these activities
follows:

-   Catalog price rules
-   Newsletters
-   Generating Google sitemaps
-   Customer Alerts/Notifications (product price change, product back in
    stock)
-   Reindexing _(Magento EE 1.13 only)_
-   Private sales _(Magento EE only)_
-   Automatic updating of currency rates
-   _Magento EE 1.14.1 and later, Magento CE 1.9.1 and later_ All
    Magento e-mails (including order confirmation and transactional)

 
![note](~@assets/icon-note.png) **Note**:
Magento depends on proper cronjob configuration for many important
system functions, including indexing. Failure to set it up properly
means Magento won\'t function as expected.


Crontab files define tasks (cronjobs) that are performed at scheduled
dates and times. Each user on a system has its own crontab file, and the
cron daemon runs each cronjob as the user who owns the crontab. For
Magento, this user is the web server.

### Recommendations for cronjobs

Magento recommends splitting your cronjob on two tasks in your crontab
for best performance and completing your cronjobs without issues:

-   Indexer Cronjob (`malways` mode) can take longer to execute and
    complete, skipping other cronjobs (`mdefault` mode) scheduled during
    the time that the indexer runs.
-   Cronjobs in `mdefault` scope may take longer to execute and
    complete, blocking others scheduled during the time it is still
    running.

To split your `malways` and `mdefault` cronjob modes, use the following:

    * * * * * /path/to/magento/cron.sh cron.php -mdefault
    * * * * * /path/to/magento/cron.sh cron.php -malways

Determine which of your cronjobs may be long-running and move them into
separate crontabs. For example, imports, exports, and indexes can run
long, blocking other cronjobs scheduled in that crontab.

Magento recommends running cron every minute for EE and every five
minutes for CE.

### Create cronjobs

First, determine your web server\'s user. SSH into the server and enter
the following command:

    ps -o "user group command" -C httpd,apache2

In CentOS, the Apache user is typically `apache`. In Ubuntu, it\'s
typically `www-data`. Enter this user in the commands for creating the
crontabs and cronjobs.

We recommend splitting the cronjob between two tasks to prevent the
indexer blocking other `mdefault` jobs or an `mdefault` job blocking the
indexer. These instructions include these commands.

To create a cronjob as the user that runs Apache:

1.  Create or edit a crontab for the Apache user. This command opens a
    vim text editor with the username of the Apache user. You might need
    to choose a text editor first.

        crontab -u apache-user-name -e

2.  To split cronjobs for better performance, enter the following:

        * * * * * /bin/sh /path/to/magento/cron.sh cron.php -mdefault
        * * * * * /bin/sh /path/to/magento/cron.sh cron.php -malways

    
    For example, for CentOS,

        * * * * * /bin/sh /var/www/html/magento/cron.sh cron.php -mdefault
        * * * * * /bin/sh /var/www/html/magento/cron.sh cron.php -malways

3.  If you do not want to split the cronjob, enter the following:

        * * * * * /bin/sh /path/to/magento/cron.sh

    
    For example, for CentOS,

        * * * * * /bin/sh /var/www/html/magento/cron.sh

4.  Save your changes to the crontab and exit the editor. For vim, press
    `:wp`.

To check if crons are running, you should see the following log files in
the Magento directory about 10 minutes after the scheduled cron runs:

    cron.php.log
    cron.sh.log

## [Setting Magento File System Permissions and Ownership After Installation ](#install-privs-after)

To secure your Magento installation after the installation, see [After
You Install Magento: Recommended File System Ownership and
Privileges](installer-privileges_after.html).

## [Troubleshooting ](#install-troubleshoot)

This section discusses some suggestions for issues you might encounter
when installing Magento.

---

Issue Suggested solution

---

During the installation, errors like the following Set privileges and ownership on the Magento installation directory and
display:\ subdirectories as discussed in [Setting File and Directory Ownership
`Path "/var/www/html/magento/app/etc" must be writable.` and Privileges](#install-privs).

You never advance past the installer\'s Configuration Try selecting the **Skip Base URL Validation Before the Next Step**
page. checkbox. If problems persist, [get
help](installing.html#help).

---


