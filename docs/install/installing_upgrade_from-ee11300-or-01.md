---
sidebar: auto
---



# EE 1.14 Upgrade: Running the EE 1.13.0.0 or EE 1.13.0.1 URL Redirect Script

---

This article is part of the Magento Enterprise Edition (EE) upgrade
documentation. If you\'re looking for something else, [click
here](http://www.magentocommerce.com/knowledge-base) to go back to the
Magento Knowledge Base.

This article applies to the following Magento EE upgrades:

-   From EE 1.13.0.1 to EE 1.13.0.2
-   From EE 1.13.0.1 to EE 1.13.1.0
-   From EE 1.13.0.1 to EE 1.14.0.0

If your upgrade is not in the preceding list, skip this article and
continue with [Completing the Magento
Upgrade](installing_upgrade_details.html#upgrade-manual-run).

This article discusses how to run the URL redirect script that creates
HTTP 301 (Moved Permanently) redirects for any duplicate product URL
keys. This enables **Previous** links in your web store to work, for
example.

For more information about URL changes, see the [EE 1.13.0.2 Release
Notes](http://www.magentocommerce.com/knowledge-base/entry/ee113-later-release-notes#ee113-11302-seo).
(Release Notes for the latest release are
[here](http://www.magentocommerce.com/knowledge-base/entry/ee113-later-release-notes#ee1131-11310).)

To create redirects for your URLs, perform the following tasks in the
order shown:

1.  [Verifying that cron is
    Disabled](#upgrade-manual-post-upgrade-ee11300-disable-cron)
2.  [Running the Script](#upgrade-manual-post-upgrade-ee11300-script)

## [Verifying that cron is Disabled ](#upgrade-manual-post-upgrade-ee11300-disable-cron)

It\'s very important to stop cron jobs from running until after the
upgrade script completes. To do so, enter one of the following commands
as a user with `root` privileges:

-   Ubuntu: `service crond stop`
-   CentOS: `service cron stop`

## [Running the Script ](#upgrade-manual-post-upgrade-ee11300-script)

This section discusses how to run the URL redirect script to create
permanent redirects for any URLs that changed in EE 1.13.1.0.

**Note:** The time the script takes to run and the amount of memory it
uses is directly proportional to the size of your database.

To run the script:

1.  Change to your Magento installation directory.

2.  As the user who owns the files (typically, either `root` or the web
    server user), enter the following command:
    `php -f shell/url_migration_from_1_13_0_0_to_1_13_0_2.php`

3.  Wait while the script runs.
    Messages like the following display to indicate progress:

        [INFO]: Initialization...
        [INFO]: Start url rewrites processing from 1.13.0.0 to 1.13.0.2 ...
        [INFO]: Start root category "Default Category" processing ...
        [INFO]: Start root category "test" processing ...
        [INFO]: Executed in time

4.  If the script succeeded, continue with [Completing the Magento
    Upgrade](installing_upgrade_details.html#upgrade-manual-run).

**Troubleshooting**

If errors display, check
`[your Magento install dir]/shell/migration.log` for exceptions.
Following are errors that might display when you run the migration
script:

-   `Application is not installed yet, please complete install wizard first`
-   `PHP Fatal error: Uncaught exception 'PDOException' with message 'SQLSTATE[28000] [1045] Access denied for user 'name'@'hostname' (using password: YES)' in /var/www/magento/lib/Zend/Db/Adapter/Pdo/Abstract.php`

If one of these displays, make sure you copied
`[your Magento install dir]/app/etc/local.xml` from your development
system to the production system. Edit it if necessary to reference the
production database instance. After you copy and edit `local.xml`, run
the URL redirect script again.

Continue your upgrade with [Completing the Magento
Upgrade](installing_upgrade_details.html#upgrade-manual-run).

