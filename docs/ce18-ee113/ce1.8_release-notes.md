---
sidebar: auto
---



# Magento Community Edition (CE) Release Notes (1.8 and later)

![note](~@assets/icon-note.png) **Note**:
We\'d like to make you aware that a security patch for older versions of
[Magento Community Edition](http://www.magentocommerce.com/download) has
been posted (in the Magento Community Edition Patches section). This
patch resolves a remote code execution vulnerability that enables an
attacker with Magento administrator privileges to delete files and
directories on a Magento installation. This vulnerability was discovered
through our quarterly penetration testing process and has not been
reported by merchants.

This issue was fixed in Magento Community Edition 1.8.0.0; no patch is
necessary for versions 1.8.0.0 and later. Patches are available for
Magento Community Edition 1.4.0.0 through 1.7.0.2. We encourage all
affected merchants to apply the patch in their next regularly scheduled
maintenance cycle.

Magento takes security very seriously and will continue to focus on
identifying potential issues and hardening our defenses.


### Table of Contents

These Release Notes contain the following information:

[Patches for Multiple Magento CE Versions](#patches)

[Magento Community Edition (CE) 1.8.1.0 Release Notes](#ee1131-11310)

[Magento CE 1.8.0.0 Release Notes](#ce-1800-release-notes)


## [Patches for Multiple Magento CE Versions ](#patches)

Magento has the following patches available for multiple Magento CE
versions.

-   [General Magento Connect Patches](#ce18-patches-connect-general)
-   [Magento Install Page Displays After SOAP v2 Index Page
    Refresh](#ce18-patches-soap)
-   [PHP 5.4 Patch Available](#php54)
-   [How to get Patches for Magento CE](#ce18-patches-how-to-get)

![note](~@assets/icon-note.png) **Note**:
Some of the patches discussed in this section have `EE_1.14.0.1` in the
name. These patches were all tested against CE 1.8.x as well.


## [General Magento Connect Patches ](#ce18-patches-connect-general)

_Patch name_: SUPEE-3941

-   When you install a community-created translation package, the
    translation provided by the package overwrites any existing
    translations for the same items. This enables you to more easily
    install packages with translations.
-   To improve security, Magento Connect now uses HTTPS by default to
    download extensions, rather than FTP.
-   Extension developers can now create an extensions with a dash
    character in the name. Merchants can install those extensions
    without issues.
-   Magento administrators who attempt to install an extension with
    insufficient file system privileges are now informed. Typically, the
    Magento Admin Panel runs as the web server user. If this user has
    insufficient privileges to the _your Magento install
    dir_`/app/code/community` directory structure, the Magento
    administrator sees an error message in the Magento Connect Manager.
    To set file system permissions appropriately, see [After You Install
    Magento: Recommended File System Ownership and
    Privileges](install/installer-privileges_after.html#extensions).

## [Magento Install Page Displays After SOAP v2 Index Page Refresh ](#ce18-patches-soap)

_Patch name_: SUPEE-3762. Refreshing the SOAP v2 index page
(`http://your-magento-host-name/index.php/api/v2_soap/index/`) results
in all administrators and customers viewing the Magento installation
page.

## [Discover Card Validation Patch Available ](#discover-fix)

Magento has fixed an issue that prevented some Discover credit cards
from validating properly. The issue was that certain Discover credit
card number ranges were not recognized as being valid. As a result of
the fix, all Discover cards should validate properly.

The issue affects Magento CE versions 1.4.2.0--1.8.1.0.

To get a fix for the issue, see [Discover credit card validation issue:
Magento EE 1.9.1.1--1.13.1.0 and CE
1.4.2.0--1.8.1.0](other/discover-card-validation.html).

![important](~@assets/icon-important.png) **Important**:
This is _not_ a security threat. No data has been compromised or
misused. It affects only the ability to validate certain credit card
number ranges as valid Discover card numbers.


## [PHP 5.4 Patch Available ](#php54)

You can use PHP 5.4 with Magento CE versions 1.6.0.0--1.8.1.0.

To get the patch, see [Getting the PHP 5.4 patch for Magento Enterprise
Edition (EE) and Community Edition
(CE)](other/php5.4_patch.html).

For more information about PHP 5.4, see [the PHP migration
page](http://www.php.net/manual/en/migration54.changes.php) and [the PHP
changelog](http://php.net/ChangeLog-5.php#5.4.0).

## [How to Get Patches For Magento CE ](#ce18-patches-how-to-get)

This section discusses how to get patches referenced in these Release
Notes.

To get patches for Magento CE:

1.  Log in to
    [www.magentocommerce.com/download](http://www.magentocommerce.com/download).
2.  In the left pane, click **Downloads**.
3.  Scroll down to the Magento Community Edition Patches section.
4.  Follow the prompts on your screen to download a patch for your
    version of CE.
5.  Apply the patch as discussed in [How to Apply and Revert Magento
    Patches](other/ht_install-patches.html).

## [Magento Community Edition (CE) 1.8.1.0 Release Notes ](#ee1131-11310)

See the following sections for information about changes in this
release:

-   [Highlights](#ee1131-113100-hilites)
-   [Security Enhancements](#ee1131-113100-security)
-   [Potential Issue After Upgrading to CE
    1.8.1.0](#11810-changes-upgrade-issue)
-   [Changes in This Release](#11310-changes)
-   [Tax Calculation Fixes](#tax-11310)
-   [Fixes in Magento CE 1.8.1.0](#ee113-11310-fixes)

## [Highlights ](#ee1131-113100-hilites)

Magento CE 1.8.1.0 helps advance overall product quality and ease
operations by providing significant tax calculation improvements, a wide
range of bug fixes, and several security enhancements.

### Tax Calculation Improvements

CE 1.8.1.0 resolves Value Added Tax (VAT) and Fixed Product Tax (FPT)
issues so that Magento administrators can create invoices and credit
memos to give merchants merchants access to accurate and consistent tax
calculations and displays. We\'ve also addressed:

-   VAT tax calculation issues for cross-border trade
-   Tax rounding issues when multiple taxes are applied
-   VAT and FPT calculation issues for bundled products
-   Support for the Waste Electrical and Electronic Equipment (W.E.E.E.)
    recycling tax in the EU

### Functional Improvements

CE 1.8.1.0 includes bug fixes across important feature areas, including
the shopping cart, checkout, content management system, and product
import and export function. Many of these updates came from a hackathon
held with Magento community developers, which demonstrates the vitality
of our development community and their powerful ability to help us
advance the platform.

### Security Enhancements

CE 1.8.1.0 includes several security enhancements that were identified
through our rigorous security assessment process. Magento complements
its own comprehensive internal testing with quarterly penetration
testing by expert consultants and actively works with the development
community to identify security issues in order to harden the platform
against potential threats.

## [Security Enhancements ](#ee1131-113100-security)

Magento addressed the following security issues:

-   Improved the password hashing algorithm.
    Magento thanks Bjorn Kraus for contributing to this fix.
-   Resolved issues that could have resulted in Cross-Site Request
    Forgery (CSRF) in the web store.
-   Resolved potential issues when issuing Return Materials
    Authorizations (RMAs).
    Magento thanks Ivan Chepurnyi for contributing to this fix.
-   Resolved a session fixation issue when registering a user with the
    web store.
-   Resolved issues with the expiration of file-based user sessions.
-   Resolved issues that could have resulted in [Remote File Inclusion
    (RFI)](https://en.wikipedia.org/wiki/File_inclusion_vulnerability)
    vulnerabilities.
-   Addressed vulnerabilities in OAuth code.
-   Closed a potential loophole that enables another user to possibly
    access personal information when viewing billing agreements.
    Magento thanks Darryl Adie and Ampersand Commerce for contributing
    to this fix.
-   Fixed the security settings for the `frontend` cookie to protect
    user sessions.

## [Issue After Upgrading to CE 1.8.1 ](#11810-changes-upgrade-issue)

There is a known Issue After Upgrading to CE 1.8.1 that affects you
_only_ if you do _not_ follow the recommended procedure to upgrade to a
new environment as discussed in [Getting Ready For Your
Upgrade](install/installing_upgrade_details.html#prereq-tasks).

**Symptom**: After completing the upgrade, when you log in to the Admin
Panel and click **System** \> **Configuration**, a fatal error similar
to the following displays in your browser:

    Class 'Mage_Googlecheckout_Helper_Data' not found in /var/www/html/magento/app/Mage.php on line 547

**Solution**:

1.  Close the Admin Panel browser window.

2.  As a user with `root` privileges, delete all files _except_
    `config.xml` from the following directory:

        magento-install-dir/app/code/core/Mage/GoogleCheckout/etc

3.  When you log back in to the Admin Panel, everything works as
    expected.
    If you\'re still encountering errors, see [Getting Help With Your
    Installation or
    Upgrade](install/installing.html#help).

## [Changes in This Release ](#11310-changes)

See the following sections for a discussion of changes in this release:

-   A tax configuration option for Fixed Product Tax (FPT) has changed.
    This option is in the Admin Panel at **System** \>
    **Configuration** \> SALES \> **Tax** \> Fixed Product Taxes, option
    **FPT Tax Configuration**. This option replaces the **Apply Tax to
    FPT** option in earlier CE releases.
    This option specifies how FPT is calculated as follows:
    -   **Not Taxed**: Click this option if your taxing jurisdiction
        does not tax FPT. (For example, the state of California does not
        tax FPT.)
    -   **Taxed**: Click this option if your taxing jurisdiction does
        tax FPT. (For example, Canada taxes FPT.)
    -   **Loaded and Displayed with Tax**: Click this option if FPT is
        added to the order total before applying tax (for example, in EU
        countries).
-   You can now specify a 0% tax rate. (In the Admin Panel, click
    **Sales** \> **Tax** \> **Manage Tax Zones & Rates.**).
    For more information, see the [Magento User
    Guide](http://www.magentocommerce.com/resources/magento-user-guide).
-   Magento changed its recommended setting for **System** \>
    **Configuration** \> SALES \> **Tax** \> **Calculation Settings**,
    option **Apply Discount On Prices** as follows:
    -   US and Canadian merchants: Set the value of **Apply Discount On
        Prices** to **Excluding Tax**.
    -   EU merchants: Set the value of **Apply Discount On Prices** to
        **Including Tax**.
-   Magento strongly recommends all merchants set **Apply Customer Tax**
    to **After Discount**, regardless of all other tax-related settings.
    This avoids issues with calculating the total product price.
-   When you specify a tax rate, the **State** list is now available
    whenever you choose a country that has states.
-   You can now specify the asterisk (`*`) wildcard character for the
    value of **State** when you set up a new tax rate. This enables you
    to apply the same tax rate to all states or provinces in a
    particular country.
-   Stores now display in the Admin Panel in **System** \> **Manage
    Stores** as a three-column hierarchy, with the website in the left
    column, all stores associated with the website in the center column,
    and all store views associated with the store in the right column.
    This makes it easier for you to browse your stores and understand
    which websites, store views, and stores are associated with each
    other. The updated Manage Stores page also displays the root
    category for each store and the code for each website and store
    view.
    Magento thanks Fabrizio Branca for contributing to this fix. For
    more information, see [Fabrizio\'s blog
    post](http://www.fabrizio-branca.de/magento-website-store-groups-store-views.html).
-   For the DHL (Deprecated) shipping method to work, you must change
    the gateway URL as follows:

    1.  Log in to the Admin Panel as an administrator.

    2.  Click **System** \> **Configuration** \> SALES \> **Shipping
        Methods**.

    3.  In the right pane, expand **DHL (Deprecated)**.

    4.  Change the value of the **Gateway URL** field to the following:

            http://xmlapi.dhl-usa.com/ApiLanding.asp

    5.  In the upper right corner, click **Save Config**.

## [Tax Calculation Fixes ](#tax-11310)

Tax calculation issues can be divided into the following sections:

-   [General Tax Notes](#tax-11310-general)
-   [Rounding Issues](#tax-11310-round)
-   [Display Issues](#tax-11310-display)
-   [Bundled Products Issues](#tax-11310-bundle)
-   [Fixed Product Tax (FPT) Issues](#tax-11310-fpt)

### [General Tax Notes ](#tax-11310-general)

The following general fixes were made to Magento tax configuration and
calculations:

-   Canadian customers now receive an e-mail with the correct totals for
    invoices and credit memos that include Provincial Sales Tax (PST)
    and Goods and Services Tax (GST).
-   Resolved issues with incorrect prices and incorrect tax amounts when
    a custom price is used together with the configuration setting
    **System** \> **Configuration** \> SALES \> **Tax** \> **Calculation
    Settings**, option **Apply Tax On** set to **Original price only**.
-   The tax amount is calculated correctly when:
    -   The customer is in a different taxing jurisdiction than the web
        store
    -   The configuration option **System** \> **Configuration** \>
        SALES \> **Tax** \> **Calculation Settings**, option **Catalog
        Prices** is set to **Including Tax**
    -   The configuration option **System** \> **Configuration** \>
        SALES \> **Tax** \> **Calculation Settings**, option **Tax
        Calculation Method Based On** is set to **Unit Price**
-   The row total including tax displayed in the shopping cart is
    calculated correctly when:
    -   The configuration option **System** \> **Configuration** \>
        SALES \> **Tax** \> **Calculation Settings**, option **Catalog
        Prices** is set to **Excluding Tax**
    -   The configuration option **System** \> **Configuration** \>
        SALES \> **Tax** \> **Calculation Settings**, option **Tax
        Calculation Method Based On** is set to **Row Total**
-   The row subtotal displays the correct amount when reordering a
    product that includes a discount coupon.
-   Multiple tax rates for a product display correctly in the Admin
    Panel when creating an invoice or credit memo.
-   Resolved calculation errors when tax and currency conversion are
    applied. As a result, the price the customer views on a catalog or
    product page is the same as the price displayed in the shopping
    cart.
-   A customer can now place an order when two tax rules are applied to
    a product, even if the tax rules specify the same tax rate.
-   Resolved issues with calculating taxes on orders that are shipped to
    different countries that have different tax rates.
-   Product prices, including taxes, display on category and product
    pages the same for a guest customer as for a logged-in customer. (A
    guest customer is a customer who does not log in to your web store;
    this customer belongs to the `NOT LOGGED IN` customer group.)

### [Rounding Issues ](#tax-11310-round)

The following tax rounding issues were resolved:

-   Resolved a rounding issue in the tax detail display in the shopping
    cart when more than one tax rule is used.
-   Resolved an issue reported on
    [stackoverflow](http://stackoverflow.com/questions/13529580/magento-tax-rounding-issue)
    where a calculation error resulted from the following
    configuration:
    **System** \> **Configuration** \> SALES \> **Tax** \> **Calculation
    Settings**, option **Tax Calculation Method Based On** set to
    **Total**
    **System** \> **Configuration** \> SALES \> **Tax** \> **Calculation
    Settings**, option **Catalog Prices** set to **Including Tax**
-   As a result of allowing a 0% tax rate, rounding errors related to
    different VAT jurisdictions have been resolved. For example, if a
    product originates in a country with VAT but is shipped to another
    country that has no VAT, correct prices display in the shopping
    cart.
-   Row totals display correctly in the shopping cart when:
    -   A shopping cart discount is applied
    -   The following configuration options are set in **System** \>
        **Configuration** \> SALES \> **Tax** \> **Calculation
        Settings**:
        -   **Catalog Prices** is set to **Including Tax**
        -   **Tax Calculation Method Based On** is set to **Excluding
            Tax**
        -   **Apply Customer Tax** is set to **After Discount**
        -   **Apply Discount On Prices** is set to **Including Taxes**

### [Display Issues ](#tax-11310-display)

The following issues relate to the incorrect display of tax information
in the Admin Panel or in your Magento web store:

-   Shipping prices including tax display properly in the shopping cart.
-   A special price now displays correctly on the product view page.
-   Values displayed in PDFs for invoices and credit memos no longer
    overlap each other.
-   Orders, invoices, and credit memos for downloadable and virtual
    products display the correct row total when viewed in the Admin
    Panel.
-   Orders display the FPT in the Admin Panel when the full tax summary
    is specified.
-   Fixed-price bundled products that include FPT now display only one
    price for both From and To values, regardless of how you configured
    the products.

### [Bundled Products Issues ](#tax-11310-bundle)

-   The price of a dynamic bundled product is calculated correctly after
    being customized by the customer.
-   The price of a dynamic bundled product with tiered pricing is
    calculated correctly after being customized by the customer.
-   Resolved issues with calculating the value displayed for the price
    including tax for bundled products.
-   The price excluding tax of a bundle product to which a discount is
    applied is the same:
    -   When viewed on the customization page
    -   after adding the bundled product to the shopping cart.
-   A dynamically-priced bundled product\'s prices displayed for Unit
    Price in the shopping cart (that is, the price including tax and the
    price excluding tax) are now correct. Before the fix, the prices
    were equal, which was incorrect.
-   The price of a bundled product displayed on the product view page
    and in the shopping cart are the same.
-   The grand total including tax and the subtotal including tax
    displayed in the shopping cart are now identical when you specify
    that catalog prices include tax and the shopping cart is set to
    display prices with and without taxes for a dynamic bundled product
    that consists of two simple products.

### [Fixed Product Tax (FPT) Issues ](#tax-11310-fpt)

-   Resolved issues in calculating FPT on a credit memo.
-   With both discounts and FPT enabled (and FPT is taxable), the
    subtotal including tax displayed in the shopping cart is correct.
-   FPT calculation for bundled products that have FPT applied to them
    now are now correct for all FPT configuration settings.
-   The invoice total is calculated correctly for an order that has both
    FPT and a shopping cart discount.
-   The FPT amount is now included in the Subtotal (Incl.Tax) row for
    partial invoice.
-   Resolved an issue that resulted in FPT being applied twice to the
    grand total in the shopping cart.

## [Fixes in Magento CE 1.8.1.0 ](#ee113-11310-fixes)

Fixes in this release can be divided into the following categories:

-   [Shopping Cart and Checkout Fixes](#ee113-11310-cart)
-   [Import and Export Fixes](#ee113-11310-import)
-   [Shipping Fixes](#ee113-11310-fixes-ship)
-   [Other Fixes](#ee113-11310-fixes-other)

### [Shopping Cart and Checkout Fixes ](#ee113-11310-cart)

-

-   Resolved a new customer registration issue that enabled a user to
    register and see another customer\'s dashboard.
-   Resolved issues with breadcrumbs disappearing or displaying
    incorrectly.
-   The following options are available in the Admin Panel in
    **System** \> **Configuration** \> SALES \> **Sales** \> **Gift
    Options**:
    **Allow Gift Wrapping on Order Level** set to **No**
    **Allow Gift Wrapping for Order Items** set to **Yes**
-   Category and subcategory names display correctly. Before the issue
    was resolved, subcategory names that were significantly longer than
    the category name did not display properly.
-   If a customer adds more than one product that requires products to
    be purchased in increments, only the products that meet the
    increment requirements are added. Before the fix, all products were
    added.
-   Scheduled payments work properly.
    Magento thanks Sylvain Raye for contributing to this fix.
-   If a bundled or configurable product is out of stock, it\'s no
    longer available to check out.
    Magento thanks Francesco Marangi for contributing to this fix.
-   Placing an order in the Admin Panel correctly sets the order status
    to Pending.
    Magento thanks GitHub user elframan for contributing to this fix.

### [Import and Export Fixes ](#ee113-11310-import)

-   Scheduled export works properly.
-   You can now export a shipment to CSV after printing its shipping
    label.
    Magento thanks Florinel Chis for contributing to this fix.

### [Shipping Fixes ](#ee113-11310-fixes-ship)

-   You are not required to enter a declared value to ship with FedEx.
-   FedEx shipping labels print properly; addresses are not truncated.
-   Fix for the USPS change to the names of their Priority and Express
    shipping options in their API made on Sunday, July 28, 2013.

### [Other Fixes ](#ee113-11310-fixes-other)

-   Resolved issues that caused spurious errors in the Magento exception
    log:
    `'Zend_Date_Exception' with message 'Invalid year, it must be between -10000 and 10000'`
-
-   Widgets display properly on the CMS.
-
-   The product attribute option **Use Default Value** works properly
    when used in a non-default store view.
-   A category attribute set to store view scope displays in layered
    navigation.
-   A store set for British Pound Sterling currency units now displays
    the correct currency in payment logs.
-   Resolved an issue with the
    `Mage_Catalog_Block_Product_Abstract class` that caused errors to
    display on product view pages in your web store.
-
-   Back-in-stock e-mails contain the correct content.
-   You can now manage product ratings and reviews from the Admin Panel
    as well as from the web store.
    Magento thanks Fabian Blechschmidt Schrank for contributing to this
    fix.
-   Resolved an issue with
    `Mage_Page_Block_Template_Links::addLinkBlock` to enable you to sort
    an array of results by position.
    Magento thanks Benjamin Marks for contributing to this fix.

## [Magento Community Edition (CE) 1.8.0.0 Release Notes ](#ce-1800-release-notes)

See the following sections for information about changes in this
release:

-   [Highlights](#ee113-11300-highlights)
-   [Security Enhancements](#ce18-1800security)
-   [United States Postal Service (USPS) Update](#ce18-1800usps)
-   [Performance Improvements](#ee113-11300-improvements)
-   [Tax Calculation Fixes](#ce18-1800tax)
-   [API Fixes](#ce18-1800api)
-   [Fixes](#ce18-1800fixes)
-   [Thanks](#thanks)

## [Highlights ](#ee113-11300-highlights)

-   Major overhaul of tax calculation formulas, correction of rounding
    errors, and additional assistance with configuration.
-   Optimized cache adapters for single-server systems
-   Upgraded Redis cache adapters for multi-server systems.
    To set up and use Redis with Magento, see [Using Redis with Magento
    Community Edition (CE) and Enterprise Edition
    (EE)](ce18-ee113/using_redis.html).
-   Eliminated many types of database deadlocks.

## [Security Enhancements ](#ce18-1800security)

-   Errors are not displayed in a new Magento installation.
-   Fixed a session fixation vulnerability in the new user registration
    process. Attackers can no longer abuse this flaw to take over new
    user accounts during registration.
-   Resolved a remote code execution vulnerability that enabled an
    attacker to delete files and directories on the Magento
    installation. (The attack required access to the Admin Panel as a
    Magento administrator.)
-   Prevent attacks that use OAuth to leak sensitive information to an
    attacker that knows the consumer key and user token.
-   Resolved an issue that enabled attackers to gain access to billing
    information.
    We thank Darryl Adia (from Ampersand Commerce) for contributing to
    this fix.
-   Resolved issues with the security of OAuth tokens and keys.
-   A remote code execution vulnerability was fixed.
    We thank Bastian Ike for contributing to this fix.
-   The Magento Admin Panel and web stores no longer allow web browsers
    to store usernames or passwords.
-   The Magento web store has additional Cross Site Request Forgery
    (CSRF) protections, meaning an imposter can no longer impersonate a
    newly registered customer and perform actions on the customer\'s
    behalf.
-   The cryptographic methods used to store passwords were improved to
    enhance security.

## [United States Postal Service (USPS) Update ](#ce18-1800usps)

The USPS changed the names of their Priority and Express shipping
options in their API in July 2013. To enable you to continue utilizing
USPS Priority _and_ Express mail methods, CE 1.8 includes a patch that
addresses the issue.

![important](~@assets/icon-important.png) **Important**:
The USPS API patch has an impact on upgrading to CE 1.8 from earlier
versions. If you\'re doing a new CE 1.8 installation, however, you
don\'t need to do anything.


Following are details about the upgrade impact:

-   Print all USPS shipping labels before upgrading; after upgrading,
    you will not be able to print them.
-   Any shopping cart price rules that use the USPS shipping method that
    created before you upgrade must be re created after you upgrade.
    Pre-existing USPS shipping methods do not work with shopping cart
    price rules after the upgrade.

## [Performance Improvements ](#ee113-11300-improvements)

-   Limited the way Magento performs large database lookups.
-   Checkout performance improvements achieved by:
    -   Eliminating unnecessary RSS cache cleanups when RSS
        functionality is disabled
    -   The locale used to send a new order confirmation e-mail now
        first checks to see if the customer\'s locale is the same as the
        store\'s locale before attempting to localize the e-mail.
    -   Improving the overall checkout process performance by loading
        the progress information for the current checkout step only
-   You can load a large number of tax codes (35,000 or so) without
    impacting performance.

## [Tax Calculation Fixes ](#ce18-1800tax)

Tax calculation issues can be divided into the following sections:

-   [General Tax Notes](#ce18-1800tax-general)
-   [Rounding Error Fixes](#ce18-1800tax-round)
-   [Fixed Product Tax (FPT) Fixes](#ce18-1800tax-fpt)
-   [Discount Calculation Fixes](#ce18-1800tax-disc)
-   [Display Fixes](#ce18-1800tax-display)

### [General Tax Notes ](#ce18-1800tax-general)

The following general fixes were made to Magento tax configuration and
calculations:

-   Based on Magento testing and merchant experience, certain tax
    configuration settings have been determined to be susceptible to
    rounding issues and can be confusing to buyers. To help you avoid
    issues with those settings, warning messages display in the Admin
    Panel if you attempt to save such a configuration.
    Administrative users can choose to dismiss the messages and can
    still save the configuration; however, Magento strongly recommends
    you change the configuration in a way recommended by the details
    displayed in the message.
    For details, see the [Magento User
    Guide](http://www.magentocommerce.com/resources/magento-user-guide).
-   Bundle pricing is more consistent as follows:
    -   The calculation formula is:
        `Sub item price = Sub item base price * Applicable tiered price adjustment or discount, then rounded Bundle price = Sum (round(sub item price * qty))`
    -   When non-integer quantities are multiplied by a product price,
        Magento rounds the resulting subtotal is as follows:
        `round(unit price * non-integer quantity)`
-   All product price information on which taxation is based are rounded
    to two digits of precision regardless of how many digits of
    precision have been loaded into the database (for example, \$10.24
    instead of \$10.2385). This situation can occur when certain
    integrations enable third-party applications to send four-digit
    precision prices to Magento.
    Starting with this release those additional digits will have no
    impact on customer facing prices. Forcing two digits of precision
    enables more exact calculations involving Fixed Product Tax (FPT),
    discounts, and taxes---among other concerns.
-   For certain Canadian provinces and localities, calculations and
    methods were updated to support changing legal requirements in
    Canada:
    -   Provincial Sales Tax (PST)
    -   Goods and Services Tax (GST)
    -   Taxe de vente du Québec (TVQ)---also referred to as Quebec Sales
        Tax (QST)

For details, see the [Magento User
Guide](http://www.magentocommerce.com/resources/magento-user-guide).

### [Rounding Error Fixes ](#ce18-1800tax-round)

The following issues relate to one-cent rounding errors in the web store
or shopping cart:

-   Calculating taxes for bundled products with tiered pricing.
-   Calculating the price before customization for bundled products.
-   Calculating the grand total of items added to a cart in a different
    order.
-   Viewing an order when taxes are calculated after a discount using
    either row-based or unit price.
-   Applying a discount to an order with a shipping address different
    than the billing address.
-   Calculating the grand total based on the order in which products are
    added to the shopping cart.
-   Specifying that prices display in the web store excluding tax and
    setting a 20% tax rate (or discount rate) now calculates the grand
    total correctly. It is now possible to have grand totals in amounts
    like 6.99, 9.99, or 99.99---regardless of the currency units used in
    the web store.
-   Adding multiple items to a cart does not affect the accuracy with
    which taxation is calculated.
-   Subtotal (Incl. Tax) is now correct when catalog and shipping prices
    include tax. Both tax and discounts are applied after tax.
-   Prices displayed in the cart and on the catalog page are consistent
    and correct when catalog prices include tax, and when items in the
    catalog are set to display both including and excluding tax.
    (In the Admin Panel, click **System** \> **Configuration** \>
    SALES \> **Tax**. In the right pane, expand **Calculation
    Settings**.)
-   Error in calculating the Grand Total Excl. Tax was resolved. This
    error occurred in a specific configuration: tax is applied to FPT,
    FPT is included in the subtotal, and the customer selects
    non-taxable flat rate shipping.

### [Fixed Product Tax (FPT) Fixes ](#ce18-1800tax-fpt)

The following issues relate to errors in calculating taxes that include
FPT in the web store or shopping cart:

-   Price in the cart displays the correct before-tax price and grand
    total.
-   Subtotals displayed in the cart---both Including Tax and Excluding
    Tax---are now correctly calculated when FPT is applied.
-   Free shipping offers are now processed correctly when FPT is
    applied.
-   FPT taxes are calculated correctly when a discount is applied.

### [Discount Calculation Fixes ](#ce18-1800tax-disc)

The following issues relate to price calculations when coupon codes or
other discounts are applied in the web store or shopping cart:

-   The Row Subtotal displayed in the cart is calculated correctly (that
    is, both Excl. Tax and Incl. Tax are correct).
-   The price for bundled items now displays with tax included if the
    bundle is configured to do so.
-   Taxation is now correctly calculated on a product with a discounted
    price.
-   Taxation on discounts is now calculated correctly when the ship-to
    country is different from the web store\'s default country.

### [Display Fixes ](#ce18-1800tax-display)

The following issues relate to the incorrect display of tax information
in the Admin Panel or in your Magento web store:

-   Row Subtotal displays correctly in the shopping cart when:
    -   FPT is applied.
    -   A discount is applied to a situation where the tax the customer
        pays is different from the tax specified for the web store\'s
        locale (for example, when the shipping origin is different than
        the shipping address).
-   Subtotal including tax on a credit memo is correct when one or more
    items in the memo includes FPT.
-   Item subtotal displays correctly when a discount is applied to a
    purchase that includes FPT.
-   If the administrator sets catalog prices to exclude tax and to
    display product prices in catalog as including tax, the price of the
    product in your web store includes applicable taxes.
    (In the Admin Panel, click **System** \> **Configuration** \>
    SALES \> **Tax**. In the right pane, expand **Calculation
    Settings**.)
-   The amount of tax displayed in the Order Totals section of the
    shopping cart is now correct when free shipping and a shopping cart
    rule discount are applied.

## [API Fixes ](#ce18-1800api)

The following are fixed in the Magento SOAP v2.0 APIs (with exceptions
noted):

-   Requesting a product using a call like the following returns the
    product with the specified numeric SKU value (`8888` in the
    following example):
    `$result = $client->call($sessionId, 'catalog_product.info', '8888', null, null, 'sku');`
-   Order status is changed correctly using
    [`salesOrderAddComment`](http://www.magentocommerce.com/api/soap/sales/salesOrder/sales_order.addComment.html).
-   The
    [`shoppingCartProductMoveToCustomerQuote`](http://www.magentocommerce.com/api/soap/checkout/cartProduct/cart_product.moveToCustomerQuote.html)
    method works properly.
-   You can now use `from`-`to` complex filters to perform \"window\"
    filtration on a single field. For example, you can use `from` and
    `to` on the `created_at` return a list of sales orders using the
    [`salesOrderList`](http://www.magentocommerce.com/api/soap/sales/salesOrder/sales_order.list.html).
-   When you use the SOAP API v.2.0 with [WS-I
    Compliance](http://www.magentocommerce.com/api/soap/wsi_compliance.html)
    enabled to retrieve sales orders information, the server responds
    with the correct
    [Content-Length](http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.13)
    header.
-   The `productGetSpecialPrice` method returns special price
    information for a product, whether or not [WS-I
    Compliance](http://www.magentocommerce.com/api/soap/wsi_compliance.html)
    is enabled.
-   The
    [`shoppingCartPaymentList`](http://www.magentocommerce.com/api/soap/checkout/cartPayment/cart_payment.list.html)
    method returns the list of the available payment methods for the
    shopping cart appropriately. The following error is no longer
    returned:
    `SOAP-ERROR: Encoding: object has no 'code' property in name`
-   The following issues with [WSDL](http://www.w3.org/TR/wsdl) and
    [WS-I
    Compliance](http://www.magentocommerce.com/api/soap/wsi_compliance.html)
    are resolved:
    -   The `productAttributeAddOption` and
        [`catalogProductAttributeUpdate`](http://www.magentocommerce.com/api/soap/catalog/catalogProductAttribute/product_attribute.update.html)
        methods are now supported when WS-I Compliance is enabled.
    -   The WSDL declaration for
        [`salesOrderCreditmemoCreate`](http://www.magentocommerce.com/api/soap/sales/salesOrderCreditMemo/sales_order_creditmemo.create.html)
        is now correct; that is, it matches the code.
    -   You can now add a C\# web reference in Microsoft Visual Studio
        2010 using the Magento WSDL.
        For example, this command no longer fails:
        `C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0A\Bin> wsdl /out:Magento.cs /v http://magentohost/api/v2_soap/?wsdl`
    -   The [WS-I
        Compliance](http://www.magentocommerce.com/api/soap/wsi_compliance.html)
        mode WSDL now includes
        [`catalogProductAttributeInfo`](http://www.magentocommerce.com/api/soap/catalog/catalogProduct/catalogProduct.html).
-   _XML-RPC API_: Using the `product_custom_option.add` operation with
    `multicall` no longer results in redundant options.

## [Fixes ](#ce18-1800fixes)

Fixes in this release can be divided into the following categories:

-   [Web Store and Shopping Cart Fixes](#ce18-1800fixes-webstore)
-   [Promotional Price Rule Fixes](#ce18-1800fixes-price-rules)
-   [Administrative Ordering and Credit Memo
    Fixes](#ce18-1800fixes-adminpanel)
-   [Import Fixes](#ce18-1800fixes-importexport)
-   [Payment Fixes](#ce18-1800fixes-payment)
-   [Other Fixes](#ce18-1800fixes-other)

### [Web Store and Shopping Cart Fixes ](#ce18-1800fixes-webstore)

-   A customer\'s account created date is correct.
-   When a product price is set with website scope and an administrative
    user has access to only one website, the default price is taken from
    that website scope. Also, when saving the product on the website
    scope, the price is updated only in that scope and not in the
    default scope.
-   An error no longer displays on your web store after a customer
    places an order. (The error message was
    `There has been an error processing your request. Please contact us or try again later`).
-   Restricted coupon codes work properly, even if the customer has
    selected the **Remember me** checkbox.
-   Using the Table Rates shipping option, free shipping options work
    properly. (In the Admin Panel, click **System** \>
    **Configuration** \> **SALES** \> **Shipping Methods**. In the right
    pane, expand **Table Rates**.)
-   Issues with shipping table rates have been resolved.
-   Entering a value such as `10,50` (using a comma character and not a
    period) for **Adjustment Fee** now results in the correct amount of
    credit being applied to the transaction.
-   Unit price for bundled products is now calculated correctly.
-   The tiered price of bundled items now displays properly on the web
    store.
-   Composite products can be successfully reordered.
-   You can now use special characters in a product URL key.
-   After a customer visits the sitemap, web stores URLs are no longer
    prepended by `/sitemap/catalog/string`.
-   Welcome messages now display properly in the web store after a
    customer\'s profile information is changed.
-   Recently viewed products now display updates properly.
-   Armed Forces Middle East is now available for State when checking
    out.
-   Searching for a customer\'s orders and returns works properly.
-   Shipping is calculated correctly if you select **Using origin weight
    (few requests)** for **Packages Request Type**. (In the Admin Panel,
    click **System** \> **Configuration** \> SALES \> **Shipping
    Methods** \> **DHL (Deprecated)**).
-   Free shipping is no longer available to a customer during checkout
    if the option was disabled by an administrator. (In the Admin Panel,
    click **System** \> **Configuration** \> **Sales** \> **Shipping
    Method** \> **DHL(Deprecated)**, click one or more options from the
    **Allowed Methods** list, and, from the **Free Shipping with Minimum
    Order Amount** list, click **No**.)
-   A user can navigate your web store while downloading a downloadable
    product.
-   You can now specify weight units in kilograms (kg) using the FedEx
    shipping method.
-   FedEx shipping rates are now consistent with Magento discounted
    rates.
-   Fixed issues with United Parcel Service (UPS) shipping rates.
-   UPS shipping labels have the word `SAMPLE` printed on them only when
    you request a sample label.
-   Changes made to United States Post Office (USPS) APIs and rates have
    been incorporated in Magento.
-   The products in a customer\'s wish list no longer disappear after
    one or more products are edited by an administrator.
-   Administrators can view the contents of a customer\'s shopping cart.
-   When a customer selects a product on your web store, the assigned
    category is selected in the navigation menu.

### [Promotional Price Rule Fixes ](#ce18-1800fixes-price-rules)

The following fixes relate to administering and using shopping cart
price rules and catalog price rules:

-   Shopping cart price rules applied to specific customer groups work
    properly.
-   Catalog price rules are applied properly to customer groups.
-   The scope of a product attribute is now honored by a catalog price
    rule.
-   Discounts specified by a shopping cart price rule are applied
    properly when a particular order is shipped to multiple addresses.
-   A discount specified by a shopping cart price rule that allows for
    more than one use per customer is applied the correct number of
    times if the customer has their orders shipped to more than one
    address.
-   When an administrative user whose role is restricted to only viewing
    catalog price rules, the user cannot add or edit catalog price
    rules.
-   Shopping cart price rules now work properly with bundled products.

### [Administrative Ordering and Credit Memo Fixes ](#ce18-1800fixes-adminpanel)

-   When you create an order using the Admin Panel and you have multiple
    stores, the **State/Province** field updates appropriately for the
    country in which the order is placed.
-   When you create an order using the Admin Panel and you have
    specified a default billing address and a default shipping address,
    the addresses are used correctly.
-   Orders placed by an administrator display in a customer\'s last
    order list.
-   Product comparisons now display properly when an administrator makes
    a change using the Admin Panel (for example, deleting a product from
    a customer\'s comparison list).
-   You can now cancel an order using the Admin Panel.
-   Orders and invoices that include taxable shipping---when created in
    the Admin Panel---now calculate the shipping taxes properly.
-   Products added to a customer\'s wish list by an administrator
    display properly.

### [Import Fixes ](#ce18-1800fixes-importexport)

-   The quantity (`QTY`) of all products imports correctly.
-   The value of Maximum Qty Allowed in Shopping Cart
    (`use_cfg_max_sale_qty`) is correct.
-   The product displays correctly in layered navigation.
-   Importing customer lists with capitalization variations in the
    e-mail address now imports the customer only once (for example,
    `user@example.com` and `User@example.com`).
-   Issues with importing products with **Append Complex Data**
    selecting from a comma-separated value (`.csv`) file have been
    resolved.

### [Payment Fixes ](#ce18-1800fixes-payment)

Resolved issue sending customer e-mail when using Payflow Link.

Security issues with Google Checkout payments have been resolved.

Security issues with Authorize.net payments have been resolved.

Magento conforms to the latest version of the PayPal Instant Payment
Notification (IPN) guidelines.

The contents of a shopping cart are unaffected by canceling a PayPal
payment.

Issues with not being able to continue checkout after switching payment
methods have been resolved.

You can now process partial refunds and invoices for orders that were
placed using Payflow Pro.

Payflow Link and Payments Advance now capture IPN transactions properly.

Special characters (such as e-mail addresses) are now handled properly
by the Magento Payflow API integration.

Resolved errors with orders placed using the Website Payments Pro
payment method.

PayPal Express Checkout payments are handled properly when a shopping
cart price rule is specified.

Any PayPal Name-Value Pair (NVP) payment method no longer automatically
refunds an order when a chargeback is initiated. Magento now allows the
dispute to be resolved before taking the appropriate action.
PayPal NVP payment methods include: PayPal Payments Pro (including
PayPal Payments Pro Hosted), Payments Standard, and all Payflow methods.

PayPal Pro now correctly processes the shipping address for an order.

PayPal Express Checkout and PayPal Pro now handle partial refunds
properly.

Fixed rounding errors that were preventing PayPal Express Checkout
transactions from completing. The error occurred with the following
configuration:

-   tax calculation method based on the total
-   tax calculated based on the shipping address
-   catalog prices exclude tax
-   shipping prices exclude tax
-   customer discount applied after a discount
-   discount applied to prices excluding tax
-   tax applied to a custom price if available
    (In the Admin Panel, click **System** \> **Configuration** \>
    SALES \> **Tax**. In the right pane, expand **Calculation
    Settings**.)

The order status Suspected Fraud is now supported by PayPal Payments Pro
(hosted) when PayPal fraud protection is enabled. Using the Magento
Admin Panel, the merchant can also accept or deny any Suspected Fraud
orders and have that decision applied to the PayPal transaction.

When sending payments in the United Kingdom, PayPal Payments Pro
(hosted) now sends the value for `state` correctly. (Before the fix,
`city` was sent as the value for `state`.)

Using the Ogone payment method, transactions display in the Magento
Admin Panel after you capture them.

When an administrator places an order and uses PSi Gate, then cancels
the order, the PSi Gate gateway displays both the order and the void
transactions.

The following fields related to PayPal\'s Payflow Pro Gateway payment
method are now implemented properly:

-   [`[custref]`](https://cms.paypal.com/cms_content/GB/en_GB/files/developer/PP_PayflowPro_Guide.pdf)
    is the Magento customer\'s ID
-   [`[INVNUM]`](http://www.paypalobjects.com/en_US/ebook/PP_NVPAPI_DeveloperGuide/Appx_fieldreference.html)
    is Magento\'s order number

Fixed spurious
`Gateway error: Void error: V18A4B18E0F9 has been captured` errors when
canceling partially invoiced orders when the Payflow Pro processor was
used to process the payment.

_3-D secure fix that affects UK merchants only_: 3-D Secure for UK
merchants implementing Direct Payment works properly.

Partial captures are now supported for the following PayPal payment
methods: Express Checkout, Payments Pro Payflow Edition, and PayPal
Standard.

Using the PayPal Express Checkout method, a recently added customer can
check out without the error `This customer email already exists`.

### [Other Fixes ](#ce18-1800fixes-other)

-   Issues related to the DHL shipping method for picking up and pricing
    orders on holidays have been resolved as follows:
    -   If the current date is a weekend, Magento chooses next Monday as
        the pick-up date.
    -   If the current date is a holiday, Magento requests from DHL
        information about the next five consecutive days to find a
        workday on which to pick up the order.
    -   If there is no workday in the five consecutive days following a
        holiday, the DHL shipping method is unavailable.
-   The `.htaccess.sample` provided with Magento now includes
    `php_value memory_limit 512M` to be consistent with the [Magento
    system
    requirements](../system-requirements.html).
-   You can now install or upgrade to CE 1.8.0.0 if your Magento
    database had a table prefix (for example, all tables start with
    `mage_` because you specified a tables prefix during installation).
-   MySQL database deadlock issues were resolved.
-   CE 1.8 is now [World Wide Web Consortium (W3C)
    compliant](http://www.w3.org/standards/).
-   When an administrative user whose role is restricted to managing
    products attempts to edit Inventory settings (**Catalog** \>
    **Manage Products**, **Inventory**), only the available options
    display.
-   Related product information updates appropriately in the Admin
    Panel.
-   Issues with editing product inventory settings and category
    attributes using the Google Chrome web browser have been resolved.
-   Rolling back after a backup now works properly. (The Magento backup
    and rollback options are available in the Admin Panel in
    **System** \> **Tools** \> **Backup**.)
-   You can now fetch data for a PayPal Settlement Report using a custom
    Secure FTP (SFTP) server.
-   You can now save a category with the option **Available Product
    Listing Sort By: Best value or Price** enabled.

### Thanks

Magento acknowledges and thanks everyone in the Magento Community who
contributed to this release, including Colin Mollenhour for Redis
modules.


