---
sidebar: auto
---



 
Using Redis With Magento Community Edition (CE) or Enterprise Edition (EE)
---

### [Contents](#top)

-   [Overview](#overview)
-   [Requirements for Using Redis with Magento](#matrix)
-   [Configuring Redis](#config)
-   [Getting Support for Redis](#support)
-   [For More Information](#more-info)
-   [Acknowledgment](#ack)

## Overview

[Redis](http://redis.io/) is an open source, Berkeley Software
Distribution (BSD) licensed, advanced key-value store that can
optionally be used in Magento for backend and session storage. In fact,
you can replace memcached with Redis.

Following are some of the benefits Redis provides for Magento
implementations:

-   Redis supports on-disk save and master/slave replication. This is a
    powerful feature not supported by memcached. Replication enables
    high availability by eliminating a single point of failure.
-   Redis can be used for PHP session storage.
-   Redis provides much better eviction control and its backend is
    written with eviction support in mind.
-   Redis supports multiple databases that use the same server instance
    so you can use different databases for the Magento cache, full page
    cache _(EE only)_, and sessions without starting many processes
    listening on different ports.
-   Redis supports compression libraries [`gzip`](http://www.gzip.org/),
    [`lzf`](http://search.cpan.org/dist/Compress-LZF/LZF.pm), and
    [`snappy`](https://code.google.com/p/snappy/). lzf and snappy are
    much faster than gzip.
-   Limits the number of concurrent lock requests before an [HTTP 503
    (Service
    Unavailable)](http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.5.4)
    error is returned.

## [Requirements for Using Redis with Magento ](#matrix)

The following Magento editions support Redis session and backend
caching:

-   Enterprise Edition (EE) 1.13 and later
-   Community Edition (CE) 1.8

The preceding Magento editions support Redis server version 2.6.9 and
later available from [redis.io](http://redis.io).

In addition, you can optionally use the [Redis extension for
PHP](http://pecl.php.net/package/redis) version 2.2.3 or later if
you\'re using Redis for backend caching; however, Magento works without
this extension.

## [Configuring Redis ](#config)

To use Redis with Magento, you must configure Magento to use Redis and
you must install and configure the Redis server. These tasks are
discussed in the following sections:

-   [Configuring Magento To Use Redis](#config-mage)
-   [Installing and Configuring the Redis Server](#config-redis)

## [Configuring Magento To Use Redis ](#config-mage)

To use Redis with Magento, you only need to install and configure the
Redis server. The integration between Redis and Magento is already
included with Magento CE 1.8 and EE 1.13 and later versions. All you
need to do is configure it.

**Important:** The Cm_RedisSession module in CE 1.8 is disabled by
default. Magento disables the module to avoid unnecessary connection
tries to Redis when you choose to use file, database, or a different
session storage method.

To enable Magento to use Redis, perform the following tasks:

1.  Enable the Cm_RedisSession module.
    1.  Open _magento-install-dir_`/app/etc/modules/Cm_RedisSession.xml`
        in a text editor.
    2.  Change the value of `<active>` to `true`.
    3.  Save your changes to `Cm_RedisSession.xml` and exit the text
        editor.
2.  Modify _magento-install-dir_`/app/etc/local.xml`.
    For configuration information, see the sample provided with Magento
    in _magento-install-dir_`/app/etc/local.xml.additional` and also see
    the [Readme
    (session)](https://github.com/colinmollenhour/Cm_RedisSession/blob/master/README.md)
    and [Readme
    (backend)](https://github.com/colinmollenhour/Cm_Cache_Backend_Redis/blob/master/README.md).
3.  Flush the Magento cache in any of the following ways:
    -   If you have access to the file system as the owner of the files
        in the Magento installation directory, change to that directory
        and enter `rm -rf var/cache`.
    -   Log in to the Admin Panel as an administrator. Click
        **System** \> **Cache Management**, then click **Flush Magento
        Cache** at the top of the page.

## [Installing and Configuring the Redis Server ](#config-redis)

Get [Redis server](http://redis.io/download) version 2.6.9 or later and
configure it according to [their
documentation](http://redis.io/documentation).

You can optionally install the [Redis extension for
PHP](http://pecl.php.net/package/redis) version 2.2.3 or later as well,
but Magento functions without it.

## [Getting Support for Redis ](#support)

**Important:** Colin Mollenhour, the original author of Redis, does
_not_ provide support for Magento implementations. You can get support
for Magento implementations in the following ways:



## [For More Information ](#more-info)

For more information about using Redis with Magento, see:

-   [Magento Expert Consulting Group (ECG)
    article](http://info2.magento.com/rs/magentosoftware/images/MagentoECG-UsingRedisasaCacheBackendinMagento.pdf).
-   [Redis server documentation](http://redis.io/documentation).

## [Acknowledgment ](#ack)

Magento acknowledges the contributions of Colin Mollenhour in providing
the code for the Magento implementation of Redis.

