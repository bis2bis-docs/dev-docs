\-\-- layout: m1x title: Installing Sample Data for Magento Community
Edition (CE) \-\--

## Overview

This page discusses how to get the sample data for:

-   Magento Community Edition (CE) 1.6, 1.7, or 1.8
-   Magento Community Edition (CE) 1.9.0.0
-   Magento Community Edition (CE) 1.9.1.0--1.9.2.0

![important](~@assets/icon-important.png) **Important**:
Magento CE 1.9 and later use different sample data from the other
versions in the preceding list. Make sure you use the correct version of
sample data with your version of Magento CE.


## Install Sample Data Before You Install Magento

Magento CE sample data must be installed _before_ you install Magento
CE.

## Getting the Sample Data

The sample data is provided in different formats for your convenience.
Archives for each version have exactly the same content (they differ
only by compression method).

-   [Magento CE
    1.9.1.0--1.9.2.0](https://www.magentocommerce.com/download#download1759)

## Installing the Sample Data

Detailed instructions for installing sample data for Magento CE 1.6--1.9
can be found on the [Magento Knowledge
Base](install/installing_install.html#install-sample).
