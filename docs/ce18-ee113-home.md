---
sidebar: auto
---




Magento Community Edition (CE) 1.8 and Enterprise Edition (EE) 1.13 Documentation Home

---

### [Table of Contents](#table-of-contents)

-   [Welcome](#welcome)
-   [EE 1.13 Benchmarking
    Guide](ce18-ee113/EE113-benchmark.html)
-   Release Notes
    -   [Magento Community Edition (CE) Release Notes (1.8 and later)](ce18-ee113/ce1.8_release-notes.html)
    -   [Magento Enterprise Edition (EE) Release Notes (1.13 and later)](ce18-ee113/ee1.13_release-notes.html)
-   Installation
    -   [Installing and Upgrading to Magento Community Edition (CE) and Magento Enterprise Edition (EE)](install/installing.html)
    -   [Before You Install Magento: Recommended File System Ownership and Privileges](install/installer-privileges_before.html)
    -   [After You Install Magento: Recommended File System Ownership and Privileges](install/installer-privileges_after.html)
    -   [Installing Sample Data for Magento Community Edition (CE)](ce18-ee113/ht_magento-ce-sample.data.html)
-   Upgrade: [Upgrading to and Verifying Magento Community Edition (CE) and Enterprise Edition (EE) ---Part 1](install/installing_upgrade_landing.html)
    

## Welcome

Welcome to the documentation home page for the Magento Enterprise
Edition (EE) 1.13 and Community Edition (CE) 1.8 releases! Let\'s start
out by telling you a little bit about them.

Magento recognizes that merchants and systems integrators require higher
performance in key areas---such as product search, product browsing, and
checkout. Merchants and integrators are now commonly coming to Magento
with:

-   Large databases (that is, more than one million products)
-   Heavy traffic volume
-   Frequent merchandising updates
-   Extensive marketing and promotion campaigns
-   Many web stores to address their customers\' needs

To address these ever-growing needs, Magento systematically examined
performance and scalability characteristics. Our engineers continually
measured performance on a range of environments and use cases to
identify and remove bottlenecks, and then conducted extensive testing to
confirm improvements.

We characterized performance based on variations in deployment and
business configurations to ensure improvement for a range of merchant
sizes and configurations.

In this release, we focused primarily on improvements to indexing, tax
calculations, caching, and checkout performance.

We\'re excited about the changes you\'ll find, including:

Reindexing (_Enterprise Edition only_):

-   Most indexing processes now run only to update products, categories,
    URL redirects, and so on that have changed---eliminating the need
    for manual full reindexing
-   Your Magento web store is not locked at any point during reindexing
-   Reindexing is now a background process.

Caching (_Enterprise Edition only_):

-   Full page caching now invalidates only pages that are affected by
    product or category changes
-   Improved cache adapter for single-host systems
-   Additional option of using [Redis NoSQL](http://redis.io) for cache
    and session storage in multi-host deployments (recommended for new
    deployments)

EE customers also get [detailed performance and tuning
guidelines](ce18-ee113/EE113-benchmark.html)
geared toward enterprise installations

Sales, Value Added, and Fixed Product Tax calculations (_Magento CE and
EE_):

-   Major improvements in the way sales tax is calculated.

Checkout performance (_Magento CE and EE_) achieved by:

-   Eliminating unnecessary calls to gift wrapping when loading the
    Shipping Method checkout step
-   Eliminating unnecessary RSS cache cleanups when RSS functionality is
    disabled
-   Eliminating unnecessary calls to the translation module when sending
    new order e-mails if the current locale is the same as the locale
    set in Magento
-   Improving the overall checkout process performance by loading the
    information for the current checkout step only
-   Improving the overall checkout process performance by loading the
    progress information for the current checkout step only
    
