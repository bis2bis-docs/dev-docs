---
sidebar: auto
---



 
# Magento for Developers: Part 2---The Magento Config

---


## Other articles in this series:

-   [Part 1---Introduction to
    Magento](mage-for-dev-1.html)
-   [Part 2---The Magento
    Config](mage-for-dev-2.html)
-   [Part 3---Magento Controller
    Dispatch](mage-for-dev-3.html)
-   [Part 4---Magento Layouts, Blocks and
    Templates](mage-for-dev-4.html)
-   [Part 5---Magento Models and ORM
    Basics](mage-for-dev-5.html)
-   [Part 6---Magento Setup
    Resources](mage-for-dev-6.html)
-   [Part 7---Advanced ORM: Entity Attribute
    Value](mage-for-dev-7.html)
-   [Part 8---Varien Data
    Collections](mage-for-dev-8.html)
    

The config is the beating heart of the Magento System. It describes, in
whole, almost any module, model, class, template, etc. than you\'ll need
to access. It\'s a level of abstraction that most PHP developers aren\'t
used to working with, and while it adds development time in the form of
confusion and head scratching, it also allows you an unprecedented
amount of flexibility as far as overriding default system behaviors go.

To start with, we\'re going to create a Magento module that will let us
view the system config in our web browser. Follow along by copying and
pasting the code below, it\'s worth going through on your own as a way
to start getting comfortable with things you\'ll be doing while working
with Magento, as well as learning key terminology.

## In this article\...

-   [Setting up a Module Directory
    Structure](#1 "Setting up a Module Directory Structure")
-   [Creating a Module Config](#2 "Creating a Module Config")
-   [What Am I Looking at?](#3 "What Am I Looking at?")
-   [Why Do I Care?](#4 "Why Do I Care?")

[](#1)

## Setting up a Module Directory Structure

We\'re going to be creating a Magento module. A module is a group of php
and xml files meant to extend the system with new functionality, or
override core system behavior. This may meaning adding additional data
models to track sales information, changing the behavior of existing
classes, or adding entirely new features.

It\'s worth noting that most of the base Magento system is built using
the same module system you\'ll be using. If you look in

    app/code/core/Mage

each folder is a separate module built by the Magento team. Together,
these modules form the community shopping cart system you\'re using.
Your modules should be placed in the following folder

    app/code/local/Packagename

\"Packagename\" should be a unique string to Namespace/Package your
code. It\'s an unofficial convention that this should be the name of
your company. The idea is to pick a string that no one else in the world
could possibly be using.

    app/code/local/Microsoft

We\'ll use \"Magentotutorial\".

So, to add a module to your Magento system, create the following
directory structure

    app/code/local/Magentotutorial/Configviewer/Block
    app/code/local/Magentotutorial/Configviewer/controllers
    app/code/local/Magentotutorial/Configviewer/etc
    app/code/local/Magentotutorial/Configviewer/Helper
    app/code/local/Magentotutorial/Configviewer/Model
    app/code/local/Magentotutorial/Configviewer/sql

You won\'t need all these folder for every module, but setting them all
up now is a smart idea.

Next, there\'s two files you\'ll need to create. The first, config.xml,
goes in the etc folder you just created.

    app/code/local/Magentotutorial/Configviewer/etc/config.xml

The second file should be created at the following location

    app/etc/modules/Magentotutorial_Configviewer.xml

The naming convention for this files is Packagename_Modulename.xml.

The config.xml file should contain the following XML. Don\'t worry too
much about what all this does for now, we\'ll get there eventually

    <config>
        <modules>
            <Magentotutorial_Configviewer>
                <version>0.1.0</version>
            </Magentotutorial_Configviewer>
        </modules>
    </config>

Finally, Magentotutorial_Configviewer.xml should contain the following
xml.

    <config>
        <modules>
            <Magentotutorial_Configviewer>
                <active>true</active>
                <codePool>local</codePool>
            </Magentotutorial_Configviewer>
        </modules>
    </config>

That\'s it. You now have a bare bones module that won\'t do anything,
but that Magento will be aware of. To make sure you\'ve done everything
right, do the following:

1.  Clear your Magento cache
2.  In the Magento Admin, go to **System-\>Configuration-\>Advanced**
3.  In the \"Disable modules output\" panel verify that
    Magentotutorial_Configviewer shows up

Congratulations, you\'ve built your first Magento module!

[](#2)

## Creating a Module Config

Of course, this module doesn\'t do anything yet. When we\'re done, our
module will

1.  Check for the existence of a \"showConfig\" query string variable
2.  If showConfig is present, display our Magento config and halt normal
    execution
3.  Check for the existence of an additional query string variable,
    showConfigFormat that will let us specify text or xml output.

First, we\'re going to add the following \<global\> section to our
config.xml file.

    <config>
        <modules>...</modules>
        <global>
            <events>
                <controller_front_init_routers>
                    <observers>
                        <Magentotutorial_configviewer_model_observer>
                            <type>singleton</type>
                            <class>Magentotutorial_Configviewer_Model_Observer</class>
                            <method>checkForConfigRequest</method>
                        </Magentotutorial_configviewer_model_observer>
                    </observers>
                </controller_front_init_routers>
            </events>
        </global>
    </config>

Then, create a file at

    Magentotutorial/Configviewer/Model/Observer.php

and place the following code inside

    <?php
        class Magentotutorial_Configviewer_Model_Observer {
            const FLAG_SHOW_CONFIG = 'showConfig';
            const FLAG_SHOW_CONFIG_FORMAT = 'showConfigFormat';

            private $request;

            public function checkForConfigRequest($observer) {
                $this->request = $observer->getEvent()->getData('front')->getRequest();
                if($this->request->{self::FLAG_SHOW_CONFIG} === 'true'){
                    $this->setHeader();
                    $this->outputConfig();
                }
            }

            private function setHeader() {
                $format = isset($this->request->{self::FLAG_SHOW_CONFIG_FORMAT}) ?
                $this->request->{self::FLAG_SHOW_CONFIG_FORMAT} : 'xml';
                switch($format){
                    case 'text':
                        header("Content-Type: text/plain");
                        break;
                    default:
                        header("Content-Type: text/xml");
                }
            }

            private function outputConfig() {
                die(Mage::app()->getConfig()->getNode()->asXML());
            }
        }

That\'s it. Clear your Magento cache again and then load any Magento URL
with a `showConfig=true` query string

    http://magento.example.com/?showConfig=true

[](#3)

## What am I looking at?

You should be looking at a giant XML file. This describes the state of
your Magento system. It lists all modules, models, classes, event
listeners or almost anything else you could think of.

For example, consider the config.xml file you created above. If you
search the XML file in your browser for the text
`Configviewer_Model_Observer` you\'ll find your class listed. Every
module\'s config.xml file is parsed by Magento and included in the
global config.

[](#4)

## Why Do I Care?

Right now this may seem esoteric, but this config is key to
understanding Magento. Every module you\'ll be creating will add to this
config, and anytime you need to access a piece of core system
functionality, Magento will be referring back to the config to look
something up.

A quick example: As an MVC developer, you\'ve likely worked with some
kind of helper class, instantiated something like

    $helper_sales = new HelperSales();

One of the things Magento has done is abstract away PHP\'s class
declaration. In Magento, the above code looks something like

    $helper_sales = Mage::helper('sales');

In plain english, the static helper method will:

1.  Look in the \<helpers /\> section of the Config.
2.  Within \<helpers /\>, look for a \<sales /\> section
3.  Within the \<sales /\> section look for a \<class /\> section
4.  Append the part after the slash to the value found in \#3
    (defaulting to `data` in this case)
5.  Instantiate the class found in \#4 (Mage_Sales_Helper_Data)

While this seems like a lot of work (and it is), the key advantage is by
always looking to the config file for class names, we can override core
Magento functionality **without** changing or adding to the core code.
This level of meta programming, not usually found in PHP, allows you to
cleanly extend only the parts of the system you need to.
