---
sidebar: auto
---



 (#top)


# Error Using Payflow with Magento Enterprise Edition (EE) 1.12.0.2

 
Problem

---

A customer applies a coupon code when checking out in a web store that
runs EE 1.12.0.2 and is configured to use the Payflow Pro or Payflow
Express payment methods. The following error displays:

`PayPal gateway rejected the request. Field format error: 10431-Item amount is invalid`

## Solution

To resolve this issue, [contact Magento
Support](http://support.magento.com/) and request the patch for support
issue ID SUPEE-1474.

Then apply the patch as discussed in [How to Apply and Revert Magento
Patches](ht_install-patches.html).

