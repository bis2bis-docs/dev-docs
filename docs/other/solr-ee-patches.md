---
sidebar: auto
---



# Information About Enterprise Edition (EE) Patches for Apache Solr

 
This article lists patches that Magento Support has made available for
Magento EE versions 1.10.1.0 and later. The patches themselves are
available on the Magento EE support portal.

To get the patches:

1.  Log in to `http://magento.com` with your Magento EE credentials.
2.  Click **Downloads** in the left navigation.
3.  In the right pane, expand **Magento Enterprise Edition 1.x**.
4.  Expand **Support Patches and Security Patches** \> **Solr Search
    Engine patches**.

Following is a list of available patches:

-   Magento EE Solr Patch for EE 1.12.0.x (ID: SUPEE-111)
-   [Magento EE Solr Patch for EE 1.11.1.0 (ID: SUPEE-107)](#supee-107)
-   [Magento EE Solr Patch for EE 1.11.0.x (ID: SUPEE-108)](#supee-108)
-   [Magento EE Solr Patch for EE 1.10.1.x (ID: SUPEE-110)](#supee-110)

## [Magento EE Solr Patch for EE 1.12.0.x (ID: SUPEE-111) ](#supee-111)

### Magento EE Version(s) Affected

This fix affects Magento EE 1.12.0.0, 1.12.0.1, and 1.12.0.1

## [Issues Fixed in the Magento EE 1.12.0.x Patch ](#fixed_solr_ee11200)

-   Using layered navigation filtering returns the wrong results. (For
    example, filtering by brand has no effect.)
-   Resolved an issue where search results don\'t display correctly
    after a Magento upgrade.
-   Catalog navigation works properly.
-   Products display as expected in categories if the products have a
    Date attribute with the option **Used for Sorting in Product
    Listing\>** set to **Yes**. There are no exceptions in Magento logs
    after reindexing.
-   Corrected the sort order of products searched by SKU.
-   Search results of products with names and/or SKUs that contains
    numbers, letters, and a hyphen character (-) are as expected.
-   Resolved issues with search results for products in a locale other
    than en_US with numeric SKUs.
-   Resolved issues with Solr not returning product search results.
-   Search results no longer include products that are either Disabled
    or Out of Stock.

## [Installation Instructions ](#install_ee11200)

To install this fix:

1.  Stop Solr if it\'s currently running.

2.  Download the patch `.sh` and transfer it to your Magento
    installation root directory.
    For example, `/var/www/html/magento`.

3.  Enter the following command as a user with sufficient privileges to
    write to Magento files (typically, the web server user or `root`):

        sh PATCH_SUPEE-111_EE_1.12.0.0_v6.sh

    
    **Note**: The patch version---indicated by `_v6` in this
    example---is subject to change. The version you get might be
    different.
    A message such as the following displays to confirm the patch
    installed successfully:
    `Patch was applied/reverted successfully.`

4.  To reapply ownership to the files changed by the patch:

    1.  Find the web server user:
        `ps -o "user group command" -C httpd,apache2`
        The value in the USER column is the web server username.
        Typically, the Apache web server user on CentOS is `apache` and
        the Apache web server user on Ubuntu is `www-data`.
    2.  As the owner of the files in the Magento installation directory
        or as a user with `root` privileges, enter the following command
        from the Magento installation directory:
        `chown -R web-server-user-name .` For example, on Ubuntu where
        Apache usually runs as `www-data`, enter `chown -R www-data .`

5.  Copy the updated configuration files from Magento to your Solr
    configuration directory.

    1.  Back up the files in your current Solr configuration directory.
    2.  Enter the following command:
        `cp magento-install-root/lib/Apache/Solr/conf/* solr-config-dir`
        For example, if Magento is installed in `/var/www/magento` and
        Solr configuration files are located in
        `/etc/apache-solr-3.5.0/mysolrapp/solr/conf`, enter
        `cp /var/www/magento/lib/Apache/Solr/conf/* /etc/apache-solr-3.5.0/mysolrapp/solr/conf`

6.  Start Solr.

7.  Log in to the Admin Panel as an administrator.

8.  Click **System** \> **Cache Management**.

9.  Click **Flush Magento Cache**.

10. Click **System** \> **Configuration**.

11. In the left navigation bar, from the CATALOG group, click
    **Catalog**.

12. In the right pane, expand **Catalog Search**.

13. From the **Indexation Mode** list, click **Final Commit**.
    This is the setting Magento recommends. For a description of the
    other options, see the [Magento EE User
    Guide](http://merch.docs.magento.com/ee/user_guide/Magento_Enterprise_Edition_User_Guide.html).

14. Click **System** \> **Index Management**.

15. Click the **Reindex Data** link in the Catalog Search row.

16. Reindex other indexers if necessary.

### Reverting the Patch

If applying the patch results in errors, [contact Magento
Support](http://support.magentocommerce.com/). If you are instructed to
do so, revert the patch:

1.  Change to your Magento installation directory.

2.  Enter the following command as a user with sufficient privileges to
    write to Magento files (typically, the web server user or `root`):

        sh PATCH_SUPEE-111_EE_1.12.0.0_v6.sh -R

### Troubleshooting

If you get an error when you run the patch, use the following
suggestions:

-   Verify the patch is located in your Magento installation root
    directory.
    Ubuntu example: `/var/www/magento`
    CentOS example: `/var/www/html/magento`
-   Verify you\'re running the patch with sufficient privileges.
    Typically, this means running it as the web server user or as a user
    with `root` privileges.
-   Try running the patch again.
    If problems persist, [contact Magento
    Support](http://support.magentocommerce.com/).

## [Magento EE Solr Patch for EE 1.11.1.0 (ID: SUPEE-107) ](#supee-107)

### Magento EE Version(s) Affected

This fix affects Magento EE 1.11.1.0.

## [Issues Fixed in the Magento EE 1.11.1.0 Patch ](#fixed_solr_ee11110)

-   Search results display correctly if products have attribute values
    set at the store view.
-   You can now enable or disable search engine recommendations.
-   Search results properly detect the configured price navigation step
    calculation.
-   Reindexing performance has been improved.
-   Solr search displays expected results when products use an attribute
    set for **Catalog Input Type for Store Owner** configured for
    **Price**---and this attribute is used for sorting. The following
    error no longer displays after reindexing the catalog search index:
    `SEVERE: org.apache.solr.common.SolrException: can not sort on multivalued field`
-   Search results display in the order consistent with the setting for
    **Catalog** \> **Attributes** \> **Manage Label** \> **Options**.
-   Search results display correctly if you use non-default attributes.
-   A product that uses an attribute name that has a trailing space
    displays in search results.
-   Resolved an issue that prevented products from displaying in the web
    store if Solr is enabled.
-   Products can be located using layered navigation if the products
    have attributes set for **Use in Quick Search** and/or **Use in
    Advanced Search** set to **No**.
-   Advanced search works properly when Solr is enabled.
-   Category permissions work properly when Solr is enabled.
-   Changing pricing, such as configuring a discount for an existing
    product, automatically re-indexes and displays the correct price in
    the web store.
-   Advanced search results display as expected after toggling the value
    of a Yes/No attribute.
-   A product displays in search results after changing the value of the
    product\'s Visibility setting to **Catalog**.
-   Quick search results display as expected when the value of maximum
    query length is null.
-   Searching for the value of a Yes/No attribute in a supported locale
    other than en_US works properly.
-   Resolved issues with incorrect attribute values being assigned to a
    configurable product.
-   Out of stock products display in the web store when **Display Out of
    Stock Products** is set to **Yes**.
-   Products display in advanced search if they have attributes of
    multiple select and drop-down.
-   Quick search works properly in store views with a locale other than
    English.
-   Cron job reindexing uses the specified indexation mode for the
    catalog search index.
-   Simple products that are configured as part of a grouped product
    display in search results for the simple product\'s SKU.
-   A product\'s price attribute displays in layered navigation when
    Solr is enabled.
-   Resolves an issue with attributes displaying in layered navigation
    as _empty_.
-   Catalog navigation works properly.
-   Resolved issues with Solr after upgrading to EE 1.11.1.0
-   Products display in categories after import.

## [Installation Instructions ](#install_ee11110)

To install this fix:

1.  Stop Solr if it\'s currently running.

2.  Download the patch `.sh` and transfer it to your Magento
    installation root directory.
    For example, `/var/www/html/magento`.

3.  Enter the following command as a user with sufficient privileges to
    write to Magento files (typically, the web server user or `root`):

        sh PATCH_SUPEE-107_EE_1.11.1.0_v1.sh

    
    **Note**: The patch version---indicated by `_v6` in this
    example---is subject to change. The version you get might be
    different.
    A message such as the following displays to confirm the patch
    installed successfully:
    `Patch was applied/reverted successfully.`

4.  To reapply ownership to the files changed by the patch:

    1.  Find the web server user:
        `ps -o "user group command" -C httpd,apache2`
        The value in the USER column is the web server username.
        Typically, the Apache web server user on CentOS is `apache` and
        the Apache web server user on Ubuntu is `www-data`.
    2.  As the owner of the files in the Magento installation directory
        or as a user with `root` privileges, enter the following command
        from the Magento installation directory:
        `chown -R web-server-user-name .` For example, on Ubuntu where
        Apache usually runs as `www-data`, enter `chown -R www-data .`

5.  Copy the updated configuration files from Magento to your Solr
    configuration directory.

    1.  Back up the files in your current Solr configuration directory.
    2.  Enter the following command:
        `cp magento-install-root/lib/Apache/Solr/conf/* solr-config-dir`
        For example, if Magento is installed in `/var/www/magento` and
        Solr configuration files are located in
        `/etc/apache-solr-3.5.0/mysolrapp/solr/conf`, enter
        `cp /var/www/magento/lib/Apache/Solr/conf/* /etc/apache-solr-3.5.0/mysolrapp/solr/conf`

6.  Start Solr.

7.  Log in to the Admin Panel as an administrator.

8.  Click **System** \> **Cache Management**.

9.  Click **Flush Magento Cache**.

10. Click **System** \> **Configuration**.

11. In the left navigation bar, from the CATALOG group, click
    **Catalog**.

12. In the right pane, expand **Catalog Search**.

13. From the **Indexation Mode** list, click **Final Commit**.
    This is the setting Magento recommends. For a description of the
    other options, see the [Magento EE User
    Guide](http://merch.docs.magento.com/ee/user_guide/Magento_Enterprise_Edition_User_Guide.html).

14. Click **System** \> **Index Management**.

15. Click the **Reindex Data** link in the Catalog Search row.

16. Reindex other indexers if necessary.

### Reverting the Patch

If applying the patch results in errors, [contact Magento
Support](http://support.magentocommerce.com/). If you are instructed to
do so, revert the patch:

1.  Change to your Magento installation directory.

2.  Enter the following command as a user with sufficient privileges to
    write to Magento files (typically, the web server user or `root`):

        sh PATCH_SUPEE-107_EE_1.11.1.0_v1.sh -R

### Troubleshooting

If you get an error when you run the patch, use the following
suggestions:

-   Verify the patch is located in your Magento installation root
    directory.
    Ubuntu example: `/var/www/magento`
    CentOS example: `/var/www/html/magento`
-   Verify you\'re running the patch with sufficient privileges.
    Typically, this means running it as the web server user or as a user
    with `root` privileges.
-   Try running the patch again.
    If problems persist, [contact Magento
    Support](http://support.magentocommerce.com/).

## [Magento EE Solr Patch for EE 1.11.0.x (ID: SUPEE-108) ](#supee-108)

### Magento EE Version(s) Affected

This fix affects Magento EE 1.11.0.0, 1.11.0.1, and 1.11.0.2.

## [Issues Fixed in the Magento EE 1.11.0.x Patch ](#fixed_solr_ee11102)

-   Solr does not display an anchor category if the layered navigation
    block is absent, such as when this block has been removed from the
    layout. Search results in this case are processed by the database,
    not by Solr.
-   Search results display correctly regardless of the order in which
    content blocks load.
-   Paginated search results display the total number of pages returned
    by a Solr search.
-   Search results display properly for all locales supported by the
    Magento configuration. (A _supported locale_ is one for which you
    have uploaded translations.)
-   Reindexing using a cron job no longer causes PHP fatal errors.
-   You can now enable or disable search engine recommendations.
-   Search results properly detect the configured price navigation step
    calculation.
-   Solr search displays expected results when products use an attribute
    set for **Catalog Input Type for Store Owner** configured for
    **Price**---and this attribute is used for sorting. The following
    error no longer displays after reindexing the catalog search index:
    `SEVERE: org.apache.solr.common.SolrException: can not sort on multivalued field`
-   Reindexing performance has been improved.
-   Search results display in the order consistent with the setting for
    **Catalog** \> **Attributes** \> **Manage Label** \> **Options**.
-   Search results display correctly if you use non-default attributes.
-   Products that use an attribute name that has a trailing space
    display in search results.
-   Resolved an issue that prevented products from displaying in the web
    store if Solr is enabled.
-   Advanced search works properly when Solr is enabled.
-   Category permissions work properly when Solr is enabled.
-   Changing pricing, such as configuring a discount for an existing
    product, automatically re-indexes and displays the correct price in
    the web store.
-   Advanced search results display as expected after toggling the value
    of a Yes/No attribute.
-   A product displays in search results after changing the value of the
    product\'s Visibility setting to **Catalog**.
-   Searching for the value of a Yes/No attribute in a supported locale
    other than en_US works properly.
-   Resolved issues with incorrect attribute values being assigned to a
    configurable product.
-   Out of stock products display in the web store when **Display Out of
    Stock Products** is set to **Yes**.
    (In the Admin Panel, click **System** \> **Configuration** \>
    CATALOG \> **Inventory** \> **Stock Options**.)
-   Products display in advanced search if they have attributes of
    multiple select and drop-down.
-   Quick search works properly in store views with a locale other than
    English.
-   Products can be located using layered navigation if the products
    have attributes set for **Use in Quick Search** and/or **Use in
    Advanced Search** set to **No**.
-   Quick search results display as expected when the value of maximum
    query length is null.
    (In the Admin Panel, click **System** \> **Configuration**
    CATALOG \> **Catalog Search**, value of **Maximum Query Length**.)
-   Cron job reindexing uses the specified indexation mode for the
    catalog search index.
-   Simple products that are configured as part of a grouped product
    display in search results for the simple product\'s SKU.
-   A product\'s price attribute displays in layered navigation when
    Solr is enabled.
    (Attribute is configured with **Catalog Input Type for Store Owner**
    set to **Price** and **Use In Layered Navigation** set to
    **Filterable (with results)**.)
-   Products display as expected in categories if the products have a
    Date attribute with the option **Used for Sorting in Product
    Listing** set to **Yes**. There are no exceptions in Magento logs
    after reindexing.
-   Resolves an issue with attributes displaying in layered navigation
    as _empty_.
-   Resolved an issue where search results don\'t display correctly
    after a Magento upgrade.
-   Catalog navigation works properly.
-   Search results display correctly if products have attribute values
    set at the store view.

## [Installation Instructions ](#install_ee11102)

To install this fix:

1.  Stop Solr if it\'s currently running.

2.  Download the patch `.sh` and transfer it to your Magento
    installation root directory.
    For example, `/var/www/html/magento`.

3.  Enter the following command as a user with sufficient privileges to
    write to Magento files (typically, the web server user or `root`):

        sh PATCH_SUPEE-108_EE_1.11.0.2_v1.sh

    
    **Note**: The patch version---indicated by `_v1` in this
    example---is subject to change. The version you get might be
    different.
    A message such as the following displays to confirm the patch
    installed successfully:
    `Patch was applied/reverted successfully.`

4.  To reapply ownership to the files changed by the patch:

    1.  Find the web server user:
        `ps -o "user group command" -C httpd,apache2`
        The value in the USER column is the web server username.
        Typically, the Apache web server user on CentOS is `apache` and
        the Apache web server user on Ubuntu is `www-data`.
    2.  As the owner of the files in the Magento installation directory
        or as a user with `root` privileges, enter the following command
        from the Magento installation directory:
        `chown -R web-server-user-name .`
        For example, on Ubuntu where Apache usually runs as `www-data`,
        enter
        `chown -R www-data .`

5.  Copy the updated configuration files from Magento to your Solr
    configuration directory.

    1.  Back up the files in your current Solr configuration directory.
    2.  Enter the following command:
        `cp magento-install-root/lib/Apache/Solr/conf/* solr-config-dir`
        For example, if Magento is installed in `/var/www/magento` and
        Solr configuration files are located in
        `/etc/apache-solr-3.5.0/mysolrapp/solr/conf`, enter
        `cp /var/www/magento/lib/Apache/Solr/conf/* /etc/apache-solr-3.5.0/mysolrapp/solr/conf`

6.  Start Solr.

7.  Log in to the Admin Panel as an administrator.

8.  Click **System** \> **Cache Management**.

9.  Click **Flush Magento Cache**.

10. Click **System** \> **Configuration**.

11. In the left navigation bar, from the CATALOG group, click
    **Catalog**.

12. In the right pane, expand **Catalog Search**.

13. From the **Indexation Mode** list, click **Final Commit**.
    This is the setting Magento recommends. For a description of the
    other options, see the [Magento EE User
    Guide](http://merch.docs.magento.com/ee/user_guide/Magento_Enterprise_Edition_User_Guide.html).

14. Click **System** \> **Index Management**.

15. Click the **Reindex Data** link in the Catalog Search row.

16. Reindex other indexers if necessary.

### Reverting the Patch

If applying the patch results in errors, [contact Magento
Support](http://support.magentocommerce.com/). If you are instructed to
do so, revert the patch:

1.  Change to your Magento installation directory.

2.  Enter the following command as a user with sufficient privileges to
    write to Magento files (typically, the web server user or `root`):

        sh PATCH_SUPEE-108_EE_1.11.0.2_v1.sh -R

### Troubleshooting

If you get an error when you run the patch, use the following
suggestions:

-   Verify the patch is located in your Magento installation root
    directory.
    Ubuntu example: `/var/www/magento`
    CentOS example: `/var/www/html/magento`
-   Verify you\'re running the patch with sufficient privileges.
    Typically, this means running it as the web server user or as a user
    with `root` privileges.
-   Try running the patch again.
    If problems persist, [contact Magento
    Support](http://support.magentocommerce.com/).

## [Magento EE Solr Patch for EE 1.10.1.x (ID: SUPEE-110) ](#supee-110)

### Magento EE Version(s) Affected

This fix affects Magento EE 1.10.1.0 and 1.10.1.1.

## [Issues Fixed in the Magento EE 1.10.1.x Patch ](#fixed_solr_ee11010)

-   Solr does not display an anchor category if the layered navigation
    block is absent, such as when this block has been removed from the
    layout. Search results in this case are processed by the database,
    not by Solr.
-   Solr search results display correctly with different settings for an
    attribute set that uses **Use In Layered Navigation** set to
    **Filterable (no results)**.
    In other words, if some products are assigned to a category that do
    _not_ use that attribute set, they display correctly in Solr search
    results if there are products in other categories that _do_ use the
    attribute set.
-   Catalog search reindexing has better performance.
-   Paginated search results display the total number of pages returned
    by a Solr search.
-   Search results display properly for all locales supported by the
    Magento configuration. (A _supported locale_ is one for which you
    have uploaded translations.)
-   Search results properly detect the configured price navigation step
    calculation.
    (In the Admin Panel, click **System** \> **Configuration** \>
    CATALOG \> **Catalog** \> **Layered Navigation**. From the **Price
    Navigation Step Calculation** list, click **Manual**.
-   Search results display correctly if you use non-default attributes.
-   Relevancy and suggestions were improved.
-   Catalog navigation works properly.
-   When the user chooses to display all search results, the correct
    number of results display.
-   Search results include only products that are in stock.

## [Installation Instructions ](#install_11010)

To install this fix:

1.  Download the patch `.sh` and transfer it to your Magento
    installation root directory.
    For example, `/var/www/html/magento`.

2.  Enter the following command as a user with sufficient privileges to
    write to Magento files (typically, the web server user or `root`):

        sh PATCH_SUPEE-110_EE_1.10.1.1_v2.sh

    
    **Note**: The patch version---indicated by `_v2` in this
    example---is subject to change. The version you get might be
    different.
    A message such as the following displays to confirm the patch
    installed successfully:
    `Patch was applied/reverted successfully.`

3.  To reapply ownership to the files changed by the patch:

    1.  Find the web server user:
        `ps -o "user group command" -C httpd,apache2`
        The value in the USER column is the web server username.
        Typically, the Apache web server user on CentOS is `apache` and
        the Apache web server user on Ubuntu is `www-data`.
    2.  As a user with `root` privileges, enter the following command
        from the Magento installation directory:
        `chown -R web-server-user-name .`
        For example, on Ubuntu where Apache usually runs as `www-data`,
        enter
        `chown -R www-data .`

4.  If Solr is running, stop it.

5.  Copy the updated configuration files from Magento to your Solr
    configuration directory.

    1.  Back up the files in your current Solr configuration directory.
    2.  Enter the following command:
        `cp magento-install-root/lib/Apache/Solr/conf/* solr-config-dir`
        For example, if Magento is installed in `/var/www/magento` and
        Solr configuration files are located in
        `/etc/apache-solr-3.5.0/mysolrapp/solr/conf`, enter
        `cp /var/www/magento/lib/Apache/Solr/conf/* /etc/apache-solr-3.5.0/mysolrapp/solr/conf`

6.  Start Solr.

7.  Log in to the Admin Panel as an administrator.

8.  Click **System** \> **Cache Management**.

9.  Click **Flush Magento Cache**.

10. Click **System** \> **Configuration**.

11. In the left navigation bar, from the CATALOG group, click
    **Catalog**.

12. In the right pane, expand **Catalog Search**.

13. From the **Indexation Mode** list, click **Final Commit**.
    This is the setting Magento recommends. For a description of the
    other options, see the [Magento EE User
    Guide](http://merch.docs.magento.com/ee/user_guide/Magento_Enterprise_Edition_User_Guide.html).

14. Click **System** \> **Index Management**.

15. Click the **Reindex Data** link in the Catalog Search row.

16. Reindex other indexers if necessary.

### Reverting the Patch

If applying the patch results in errors, [contact Magento
Support](http://support.magentocommerce.com/). If you are instructed to
do so, revert the patch:

1.  Change to your Magento installation directory.

2.  Enter the following command as a user with sufficient privileges to
    write to Magento files (typically, the web server user or `root`):

        sh PATCH_SUPEE-110_EE_1.10.1.1_v2.sh -R

### Troubleshooting

If you get an error when you run the patch, use the following
suggestions:

-   Verify the patch is located in your Magento installation root
    directory.
    Ubuntu example: `/var/www/magento`
    CentOS example: `/var/www/html/magento`
-   Verify you\'re running the patch with sufficient privileges.
    Typically, this means running it as the web server user or as a user
    with `root` privileges.
-   Try running the patch again.
     If problems persist, [contact Magento
    Support](http://support.magentocommerce.com/).
    
