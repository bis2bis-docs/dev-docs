---
sidebar: auto
---



# Getting the PHP 5.4 patch for Magento Enterprise Edition (EE) and Community Edition (CE)

Magento has a patch that enables you to use PHP 5.4.x with Magento
Enterprise Edition (EE) and Magento Community Edition (CE).

This patch applies to:

-   Magento CE versions 1.6.0.0--1.8.1.0
-   Magento EE versions 1.11.0.0--1.13.1.0

To get the patch for Magento CE:

1. Go to
   [www.magentocommerce.com/download](http://www.magentocommerce.com/download).
2. Click **My Account** in the upper right corner of the page.
3. Log in when prompted.
4. Scroll to the Magento Community Edition Patches section.
5. Follow the prompts on your screen to get the PHP 5.4 support patch.

To get the patch for Magento EE:

1. Log in to [www.magentocommerce.com](http://www.magentocommerce.com).
2. In the left pane, click **Downloads**.
3. In the right pane, click **Magento Enterprise Edition**.
4. Follow the prompts on your screen to download the PHP 5.4
   Compatibility patch.
5. Apply the patch as discussed in [How to Apply and Revert Magento
   Patches](ht_install-patches.html).
