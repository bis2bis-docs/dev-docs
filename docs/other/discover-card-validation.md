---
sidebar: auto
---



# Discover credit card validation issue: Magento EE 1.9.1.1---1.13.1.0 and CE 1.4.2.0---1.8.1.0

Magento has fixed an issue that prevented some Discover credit cards
from validating properly. The issue was that certain Discover credit
card number ranges were not recognized as being valid. As a result of
the fix, all Discover cards should validate properly.

This fix applies to:

-   Magento Enterprise Edition (EE) versions 1.9.1.1--1.13.1.0
-   Magento Community Edition (CE) versions 1.4.2.0--1.8.1.0

![important](~@assets/icon-important.png) **Important**:
This is _not_ a security threat. No data has been compromised or
misused. It affects only the ability to validate certain credit card
number ranges as valid Discover card numbers.


To get the patch for Magento CE, [submit a Magento CE help
request](http://www.magentocommerce.com/bug-tracking).

To get the patch for Magento EE:

1. Log in to [www.magentocommerce.com](http://www.magentocommerce.com).
2. In the left pane, click **Downloads**.
3. In the right pane, click **Magento Enterprise Edition**.
4. Follow the prompts on your screen to download the SUPEE-2725 patch
   for your version of EE.
5. Apply the patch as discussed in [How to Apply and Revert Magento
   Patches](ht_install-patches.html).

![important]<img :src="$withBase('.docs/images/icon-important.png')" /> **Important**:
If you applied an earlier version of this patch, you must first revert
the old patch.

