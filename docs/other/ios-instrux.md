---
sidebar: auto
---



 
Creating an iOS App for Magento MobileConnect
---

### [Contents](#contents)

-   [Introduction](#intro)
-   [Prerequisites](#prereq)
-   [Customizing Your iOS App](#customize)
-   [For More Information](#more-info)
    

## [Introduction ](#intro)

This guide helps you create and configure an iOS app for Magento
MobileConnect. Your app enables customers to view and make purchases for
your store.

We provide you with a sample app in the MagentoMobile extension package
that you can open as an XCode project to do the customization.

## [Prerequisites ](#prereq)

Before you begin:

1.  You must be familiar with iOS app development.
    To come up to speed, you can [enroll in an Apple Developer
    program](https://developer.apple.com/programs/start/standard/).
2.  You must know the App Code for your Magento MobileConnect
    application.
    To view the code, log in to the Admin Panel as an administrator and
    click **MobileConnect** \> **Manage Apps**. You must know the value
    displayed in the App Code column.

## [Creating the iOS App ](#creating)

To create the iOS app:

1.  Open `MagentoShop/MagentoShop.xcodeproj` in Xcode.
2.  Locate `Configuration.plist`.
3.  Change the value of `serverURL` to your store\'s base URL (for
    example, `http://mystore.example.com`).
4.  Change the value of `applicationCode` to the application code
    displayed in the Magento Admin Panel.
    To view the code, log in to the Admin Panel as an administrator and
    click **MobileConnect** \> **Manage Apps**. Enter the value
    displayed in the App Code column.
5.  Build and run your app.

## [Customizing Your iOS App ](#customize)

The following sections discuss how you can customize your Magento
MobileConnect iOS app.

## [Customizing the Launch Image ](#customize-start)

Launch images are located in
`MagentoShop/Resources/Images/LaunchImages.xcassets`.

Refer to Apple\'s [Human Interface
Guidelines](https://developer.apple.com/library/ios/documentation/UserExperience/Conceptual/MobileHIG/LaunchImages.html)
for details.

## [Customizing the App Icon ](#customize-icon)

App icons are located in
`MagentoShop/Resources/Images/ApplicationIcon.xcassets`.

Refer to Apple\'s
[HIG](https://developer.apple.com/library/ios/documentation/UserExperience/Conceptual/MobileHIG/AppIcons.html)
for best practices.

## [Customizing Thumbnail Images ](#customize-thumb)

Thumbnail images display when a category or a product does not have an
image defined for it.

You can find customizable images in
`MagentoShop/Resources/Images/Images.xcassets/thumbnails`.

## [For More Information ](#more-info)

-   [About App
    Distribution](https://developer.apple.com/library/mac/documentation/IDEs/Conceptual/AppDistributionGuide/Introduction/Introduction.html#//apple_ref/doc/uid/TP40012582-CH1-SW1)
-   [Enrolling in Apple Developer
    Programs](https://developer.apple.com/programs/start/standard/)
    
