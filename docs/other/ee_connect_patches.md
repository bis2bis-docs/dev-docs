---
sidebar: auto
---



# Patches for Magento Enterprise Edition (EE) Versions 1.10--1.12

 
We\'d like to draw your attention to new patches that were recently
posted to the Partner Portal and Support Center. These patches deliver
important improvements, such as improving security of the Magento
Connect Manager and making it easier to install community-created
translation packages.

## [General Magento Connect Patches ](#ee113-patches-connect-general)

_Patch name_:

-   PATCH_SUPEE-3941_EE_1.10.1.0_v1-2014-08-12-12-13-15.sh for EE
    1.10.x
-   PATCH_SUPEE-3941_EE_1.11.2.0_v1-2014-08-12-12-11-32.sh for EE
    1.11.x
-   PATCH_SUPEE-3941_EE_1.14.0.1_v1-2014-08-12-12-10-06.sh for EE
    1.12.x

Result of applying this patch:

-   When you install a community-created translation package, the
    translation provided by the package overwrites any existing
    translations for the same items. This enables you to more easily
    install packages with translations.
-   To improve security, Magento Connect now uses HTTPS by default to
    download extensions, rather than FTP.
-   Extension developers can now create an extensions with a dash
    character in the name. Merchants can install those extensions
    without issues.
-   Magento administrators who attempt to install an extension with
    insufficient file system privileges are now informed. Typically, the
    Magento Admin Panel runs as the web server user. If this user has
    insufficient privileges to the _your Magento install
    dir_`/app/code/community` directory structure, the Magento
    administrator sees an error message in the Magento Connect Manager.
    To set file system permissions appropriately, see [After You Install
    Magento: Recommended File System Ownership and
    Privileges](install/installer-privileges_after.html#extensions).

### [How to Get This Patch ](#ee113-patches-how-to-get)

To get this patch:

1.  Log in to [www.magentocommerce.com](http://www.magentocommerce.com).
2.  In the left pane, click **Downloads**.
3.  In the right pane, click **Magento Enterprise Edition**.
4.  Follow the prompts on your screen to download a patch for your
    version of EE.
5.  Apply the patch as discussed in [How to Apply and Revert Magento
    Patches](ht_install-patches.html).

## [Solr Search Results and Indexing ](#ee113-patches-solr-index)

_Patch name_: PATCH*SUPEE-3630_EE_1.12.x_v1.sh for EE 1.12.x. After
applying this patch, your search engine returns search results to users
on the web store while reindexing is underway. We recommend you install
this patch if you\'re using the Solr search engine. \_This patch can
result in slower performance if you\'re using the default MySQL Full
Text search engine*.

To get this patch, [contact Magento
Support](https://www.magentocommerce.com/products/customer/account/login/).

