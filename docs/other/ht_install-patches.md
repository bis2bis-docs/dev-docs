---
sidebar: auto
---



# How to Apply and Revert Magento Patches

 

### [Table of Contents](#contents)

-   [Overview](#overview)
-   [Need More Detail?](#detail)
-   [How to Get Magento Patches](#get)
-   [How to Apply a Magento Patch](#apply)
-   [How to Apply the SUPEE-8788 Patch](#apply-8788)
-   [Listing Patches You Have Installed](#id)
-   [How to Revert a Magento Patch](#revert)
-   [Troubleshooting](#trouble)

## Overview

This article discusses how to apply and revert Magento patches you get
in any of the following ways:

-   Magento Support
-   Magento Enterprise Edition (EE) support portal
-   (Magento partners) from the [partner
    portal](https://partners.magento.com)

If you don\'t already have a patch, [contact Magento
Support](http://support.magentocommerce.com/).

![note](~@assets/icon-note.png) **Note**:
This article assumes your patch file name ends in `.sh`. If your patch
file name ends in `.patch` or something else, [contact Magento
Support](http://support.magentocommerce.com/) before proceeding.


## [Need More Detail? ](#detail)

For more step-by-step details that are provided here, see one of the
following:

-   [Magento CE patch
    instructions](https://info.magento.com/rs/magentocommerce/images/Installing-a-Patch-for-Magento-Community-Edition.pdf)
-   [Magento EE patch
    instructions](https://info.magento.com/rs/magentocommerce/images/Installing-a-Patch-for-Magento-Enterprise-Edition.pdf)

## [How to Get Magento Patches ](#get)

Magento Support provides some patches for Magento CE and EE on
magentocommerce.com. This section discusses how to get those patches.

If Magento Support provided a patch to you, skip this section and
continue with [How to Apply a Magento Patch](#apply).

See one of the following sections for specific information about Magento
CE or EE:

-   [Getting Magento CE Patches](#get-ce)
-   [Getting Magento EE Patches](#get-ee)

## [Getting Magento CE Patches ](#get-ce)

To get patches for Magento CE:

1.  Log in to
    [magentocommerce.com/download](https://www.magentocommerce.com/download).
    (Click **My Account** in the upper right corner of the page.)
    If you don\'t have an account, you can register for one; the account
    is free.
2.  In the Magento Community Edition Patches section, locate the patch
    to install.
3.  From the list next to the patch, choose your CE version.
4.  Click **Download**.
5.  After the patch downloads, continue with [How to Apply a Magento
    Patch](#apply).

## [Getting Magento EE Patches ](#get-ee)

To get patches for Magento EE:

1.  Log in to [magentocommerce.com](https://www.magentocommerce.com).
    (Click **My Account** in the upper right corner of the page.)
2.  Click **Downloads** in the left pane.
3.  Click **Magento Enterprise Edition** in the right pane.
    The following figure shows an example.
    ![](~@assets/EE-spt-portal.png){width="812px"
    height="554px"}
4.  Click **Support Patches**.
5.  Locate the patch to download.
6.  Click **Download** corresponding to the patch for the version of EE
    you\'re using.
7.  After the download completes, continue with the next section.

## [How to Apply a Magento Patch ](#apply)

To apply a Magento patch:

1.  Transfer the patch `.sh` file to your Magento installation root
    directory.

    ![note](~@assets/icon-note.png) **Note**:
    This article assumes your patch file name ends in `.sh`. If your
    patch file name ends in `.patch` or something else, [contact Magento
    Support](http://support.magentocommerce.com/) before proceeding.
    

    For example, `/var/www/html/magento`.

2.  Enter the following commands as a user with sufficient privileges to
    write to Magento files (typically, the web server user or `root`):

        chmod +x <patch-file-name>.sh
        <patch-file-name>.sh

    A message such as the following displays to confirm the patch
    installed successfully:

        Patch was applied/reverted successfully.

3.  To reapply ownership to the files changed by the patch:

    1.  Find the web server user:
        `ps -o "user group command" -C httpd,apache2`
        The value in the USER column is the web server username.
        Typically, the Apache web server user on CentOS is `apache` and
        the Apache web server user on Ubuntu is `www-data`.

    2.  As a user with `root` privileges, enter the following command
        from the Magento installation directory:

            chown -R web-server-user-name .

        For example, on Ubuntu where Apache usually runs as `www-data`,
        enter

            chown -R www-data .

4.  Perform any other tasks as instructed by Magento Support.
    (For example, some patches require you to stop external services,
    such as the Solr search engine.)

## [How to Apply the SUPEE-8788 Patch ](#apply-8788)

We released a [security
patch](https://magento.com/security/patches/supee-8788) in October, 2016
that might cause issues for some users. This section applies to you if
any of the following is true:

-   You haven\'t yet applied the SUPEE-8788 patch and your Magento
    version is earlier than EE 1.14.1.0 or CE 1.9.1.0.
-   You applied version 1 of the SUPEE-8788 patch. (The patch name
    includes `PATCH_SUPEE-8788_<magento version>_v1`.)
-   You previously applied the SUPEE-1533 patch and you want to apply
    the SUPEE-8788 patch.
-   You\'re applying the SUPEE-8788 patch as part of an upgrade from an
    earlier Magento version.

We recommend the following:

-   If you applied SUPEE-8788 version 1, revert that patch, revert
    SUPEE-1533 (version restrictions apply), apply SUPEE-3941 (version
    restrictions apply), then [apply SUPEE-8788 version 2 or
    later](#apply-8788-v1).
-   If you haven\'t applied SUPEE-8788, revert SUPEE-1533 (version
    restrictions apply), apply SUPEE-3941 (version restrictions apply),
    then [apply SUPEE-8788](#apply-8788-new).

## [Replace SUPEE-8788 version 1 with version 2 or later ](#apply-8788-v1)

To replace SUPEE-7877 version 1 with version 2 or later:

1.  Log in to your Magento server.

2.  Open `<your Magento install dir>/app/etc/applied.patches.list` in a
    text editor.
    
    This file lists all currently applied patches.

3.  Determine which patches are already applied. Version 1 of SUPEE-8788
    includes `PATCH_SUPEE-8788_<magento version>_v1` in the name.

4.  If your Magento version is EE 1.14.1.0 or CE 1.9.1.0, _and_ patch
    SUPEE-1533 is applied, [revert](#revert) SUPEE-1533.

5.  If your Magento version is earlier than EE 1.14.1.0 or CE 1.9.1.0,
    _and_ SUPEE-3941 is not applied, [apply SUPEE-3941](#apply).

6.  [Get](#get) version 2 or later of SUPEE-8788.

7.  [Apply](#apply) version 2 or later of SUPEE-8788.

8.  _Magento EE 1.14.2 only_. After applying the SUPEE-8788 patch,
    remove `test_oauth.php` from your Magento base directory.

9.  If you upgraded to Magento CE 1.9.3 or Magento EE 1.14.3 after
    applying the [SUPEE-8788
    patch](https://magento.com/security/patches/supee-8788), make sure
    the following files have been deleted:

        skin/adminhtml/default/default/media/flex.swf
        skin/adminhtml/default/default/media/uploader.swf
        skin/adminhtml/default/default/media/uploaderSingle.swf

    If the files are present, delete them to avoid a potential security
    exploit. As of Magento CE 1.9.0.0 and Magento EE 1.14.0.0, we no
    longer distribute `.swf` files with the Magento software.

## [Apply SUPEE-8788 ](#apply-8788-new)

To apply patch SUPEE-8788:

1.  Open `<your Magento install dir>/app/etc/applied.patches.list` in a
    text editor.
    
    This file lists all currently applied patches.

2.  Verify SUPEE-8788 is _not_ applied. If it is, and it\'s version 1,
    see [Replace SUPEE-8788 version 1 with version 2 or
    later](#apply-8788-v1).

3.  Verify whether or not patch SUPEE-1533 is applied. If it is, _and_
    your Magento version is earlier than EE 1.14.1.0 or CE 1.9.1.0,
    [revert](#revert) SUPEE-1533.

4.  If your Magento version is earlier than EE 1.14.1.0 or CE 1.9.1.0,
    _and_ SUPEE-3941 is not applied, [apply SUPEE-3941](#apply).

5.  [Get](#get) version 2 or later of SUPEE-8788.

6.  [Apply](#apply) version 2 or later of SUPEE-8788.

7.  _Magento EE 1.14.2 only_. After applying the SUPEE-8788 patch,
    remove `test_oauth.php` from your Magento base directory.

8.  If you upgraded to Magento CE 1.9.3 or Magento EE 1.14.3 after
    applying the [SUPEE-8788
    patch](https://magento.com/security/patches/supee-8788), make sure
    the following files have been deleted:

        skin/adminhtml/default/default/media/flex.swf
        skin/adminhtml/default/default/media/uploader.swf
        skin/adminhtml/default/default/media/uploaderSingle.swf

    If the files are present, delete them to avoid a potential security
    exploit. As of Magento CE 1.9.0.0 and Magento EE 1.14.0.0, we no
    longer distribute `.swf` files with the Magento software.

## [Listing Patches You Have Installed ](#id)

If you\'re not sure which patches are already applied, open
`<your Magento install dir>/app/etc/applied.patches.list`.

## [How to Revert a Magento Patch ](#revert)

If applying the patch results in errors, [contact Magento
Support](http://support.magentocommerce.com/). If you are instructed to
do so, revert the patch:

1.  Change to your Magento installation directory.

2.  Enter the following command as a user with sufficient privileges to
    write to Magento files (typically, the web server user or `root`):

        sh patch-file-name.sh -R

## [Troubleshooting ](#trouble)

If you get an error when you run the patch, use the following
suggestions:

-   Verify the patch is located in your Magento installation root
    directory.
    Ubuntu example: `/var/www/magento`
    CentOS example: `/var/www/html/magento`
-   Verify you\'re running the patch with sufficient privileges.
    Typically, this means running it as the web server user or as a user
    with `root` privileges.
-   Try running the patch again.
     If problems persist, [contact Magento
    Support](http://support.magentocommerce.com/).
    
