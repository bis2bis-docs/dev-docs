

Welcome to the Magento 1.x REST API home page.

[Introduction to the Magento 1.x REST
API](rest/introduction.html)

[Authentication](rest/authentication/oauth_authentication.html)

-   [OAuth
    Configuration](rest/authentication/oauth_configuration.html)

[Common HTTP Status
Codes](rest/common_http_status_codes.html)

[HTTP
Methods](rest/http_methods.html)

[GET
Filters](rest/get_filters.html)

[Permission
Settings](rest/permission_settings/permission_settings.html)

-   [Roles
    Configuration](rest/permission_settings/roles_configuration.html)
-   [Attributes
    Configuration](rest/permission_settings/attributes_configuration.html)
-   [Attributes
    Description](rest/permission_settings/attributes_description.html)

[Resources](rest/Resources/resources.html)

[Inventory](rest/Resources/inventory.html)

[Sales
Orders](rest/Resources/Orders/sales_orders.html)

-   [Order
    Addresses](rest/Resources/Orders/order_addresses.html)
-   [Order
    Comments](rest/Resources/Orders/order_comments.html)
-   [Order
    Items](rest/Resources/Orders/order_items.html)

[Products](rest/Resources/Products/products.html)

-   [Product
    Categories](rest/Resources/Products/product_categories.html)
-   [Product
    Images](rest/Resources/Products/product_images.html)
-   [Product
    Websites](rest/Resources/Products/product_websites.html)

[Customer
Addresses](rest/Resources/resource_customer_addresses.html)

[Customers](rest/Resources/resource_customers.html)

[Response
Formats](rest/response_formats.html)

[Testing REST
Resources](rest/testing_rest_resources.html)
