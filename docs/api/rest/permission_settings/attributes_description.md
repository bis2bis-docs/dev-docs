

-   [Order/Orders](#RESTAttributes-Description-Order%2FOrders)
    -   [Order Addresses](#RESTAttributes-Description-OrderAddresses)
    -   [Order Items](#RESTAttributes-Description-OrderItems)
-   [Stock Item](#RESTAttributes-Description-StockItem)
-   [Customer](#RESTAttributes-Description-Customer)
-   [Customer Address](#RESTAttributes-Description-CustomerAddress)
-   [Product](#RESTAttributes-Description-Product)
    -   [Product Category](#RESTAttributes-Description-ProductCategory)
    -   [Product Image](#RESTAttributes-Description-ProductImage)

The following table describes REST attributes that can be managed in the
Magento Admin Panel.
To access these attributes, go to **System \> Web Services \> REST
Attributes** and select the type of the user for which attributes will
be managed.

### []{#RESTAttributes-Description-Order%2FOrders}Order/Orders

---

Attribute Name Attribute Description Notes

---

Order ID Sales order ID

Order Date\ Date when the sales  
order was placed

Order Status Sales order status. Can  
have the following  
 values: Pending,  
 Processing, Complete,  
 Closed, Holded, Pending
PayPal, and Payment  
 Review.

Shipping Method Shipping method  
selected during the  
 checkout process (e.g.,
Flat rate - Fixed)

Payment Method Payment method selected  
during the checkout  
 process (e.g.,  
 Check/money order)

Base Currency\ Base currency code  
(e.g., USD)

Order Currency Order currency code  
(e.g., EUR)

Store Name\ Name of the store from  
which the order was  
 placed

Placed from IP\ IP address from which  
the order was placed

Store Currency to Base Store currency to base  
Currency Rate\ currency rate

Subtotal\ Subtotal amount in  
order currency  
 (excluding shipping and
tax)

Subtotal Including Tax\ Subtotal amount  
including tax (in order
currency)

Discount\ Discount amount applied  
in the sales order in  
 order currency

Grand Total to Be Total amount of money  
Charged\ to be paid for the  
 order in base currency  
 (including tax)

Grand Total\ Grand total amount in  
order currency  
 (including tax and  
 shipping)

Shipping\ Shipping amount applied  
in the sales order in  
 order currency

Shipping Including Tax\ Shipping amount  
including tax (in order
currency)

Shipping Tax\ Tax amount for shipping  
in order currency

Tax Amount\ Tax amount applied in  
the sales order in  
 order currency

Tax Name\ Name of the applied tax

Tax Rate\ Tax rate applied in the  
order (in order  
 currency)

Gift Cards Amount\ Gift card pricing This attribute is
amount available only in
Magento EE

Reward Points Balance\ Reward points amount This attribute is
(that can be converted available only in
to currency) Magento EE

Reward Currency Amount\ Reward currency amount This attribute is
available only in
Magento EE

Coupon Code\ Coupon code that was  
applied in the order

Base Discount\ Amount of applied  
discount in base  
 currency

Base Subtotal\ Subtotal amount for all  
products in the order  
 in base currency  
 (excluding tax and  
 shipping)

Base Shipping\ Amount of money to be  
paid for shipping in  
 base currency

Base Shipping Tax\ Tax amount for shipping  
in base currency

Base Tax Amount\ Tax amount applied to  
the order items in base
currency

Total Paid\ Total amount paid for  
the order (in order  
 currency)

Base Total Paid\ Total amount paid for  
the order (in base  
 currency)

Total Refunded\ Total refunded amount  
in order currency

Base Total Refunded\ Total amount refunded  
for the order (in base  
 currency)

Base Subtotal Including Subtotal amount  
Tax\ including tax but  
 excluding the discount  
 amount (in base  
 currency)

Base Total Due\ The rest of the money  
to be paid for the  
 order in base currency  
 (e.g., when partial  
 invoice is applied)

Total Due\ The rest of the money  
to be paid for the  
 order in order currency
(e.g., when partial  
 invoice is applied)

Shipping Discount\ Discount amount for  
shipping (in order  
 currency)

Base Shipping Discount\ Discount amount for  
shipping (in base  
 currency)

Discount Description\ Discount code (coupon  
code applied in the  
 order)

Customer Balance\ Customer balance (in  
order currency)

Base Customer Balance\ Customer balance (in  
base currency)

Base Gift Cards Amount\ Gift card pricing This attribute is
amount (in base available only in
currency) Magento EE

Base Rewards Currency\ Reward currency amount This attribute is
(in base currency) available only in
Magento EE

---

### [Order Addresses](#RESTAttributes-Description-OrderAddresses)

---

Attribute Name Attribute Description

---

Customer Last Name Customer last name

Customer First Name\ Customer first name

Customer Middle Name\ Customer middle name or initial

Customer Prefix\ Customer prefix

Customer Suffix\ Customer suffix

Company\ Company name

Street\ Street address

City\ City

State\ State

ZIP/Postal Code\ ZIP or postal code

Country\ Country name

Phone Number\ Customer phone number

Address Type\ Address type. Can have the
following values: billing or
shipping

---

### [Order Items](#RESTAttributes-Description-OrderItems)

---

Attribute Name Attribute Description

---

Base Discount Amount Discount amount applied to the row
in base currency

Base Item Subtotal Row subtotal in base currency

Base Item Subtotal Including tax Row subtotal including tax in base
currency

Base Original Price Original item price in base
currency

Base Price Item price in base currency

Base Price Including tax Item price including tax in base
currency

Base Tax Amount Tax amount applied to the row in
base currency

Canceled Qty Number of canceled order items

Discount Amount Discount amount applied to the row
in order currency

Invoiced Qty Number of invoiced order items

Item Subtotal Row subtotal in order currency

Item Subtotal Including Tax Row subtotal including tax in order
currency

Order Item ID Order item ID

Ordered Qty Number of ordered items

Original Price Original item price in order
currency

Parent Order Item ID ID of the configurable product to
which the simple product is
assigned

Price Item price in order currency

Price Including Tax Item price including tax in order
currency

Product and Custom Options Name Name of the product (custom options
name)

Refunded Qty Number of refunded order items

SKU Product SKU

Shipped Qty Number of shipped order items

Tax Amount Tax amount applied to the row in
order currency

Tax Percent Tax percent applied to the row

---

## [Stock Item](#RESTAttributes-Description-StockItem)

---

Attribute Name Attribute Description

---

Automatically Return Credit Memo Defines whether products can be
Item to Stock automatically returned to stock
when the refund for an order is
created

Backorders Defines whether the customer can
place the order for products that
are out of stock at the moment. Can
have the following values: 0 - No
Backorders, 1 - Allow Qty Below 0,
and 2 - Allow Qty Below 0 and
Notify Customer

Can Be Divided into Multiple Boxes Defines whether the stock items can
for Shipping be divided into multiple boxes for
shipping

Enable Qty Increments Defines whether the customer can
add products only in increments to
the shopping cart

Item ID Stock item ID

Low Stock Date Date when the number of stock items
became lower than the number
defined in the Notify for Quantity
Below option

Manage Stock Choose whether to view and specify
the product quantity and
availability and whether the
product is in stock management. Can
have the following values: 0 - No,
1 - Yes

Maximum Qty Allowed in Shopping Maximum number of items in the
Cart shopping cart to be sold

Minimum Qty Allowed in Shopping Minimum number of items in the
Cart shopping cart to be sold

Notify for Quantity Below The number of inventory items below
which the customer will be notified
via the RSS feed

Product ID Product ID

Qty Quantity of stock items for the
current product

Qty Increments The product quantity increment
value

Qty Uses Decimals Choose whether the product can be
sold using decimals (e.g., you can
buy 2.5 product)

Qty for Item\'s Status to Become Quantity for stock items to become
Out of Stock out of stock

Stock Availability Defines whether the product is
available for selling. Can have the
following values: 0 - Out of Stock,
1 - In Stock

Stock ID Stock ID

Use Config Settings for Backorders Choose whether the Config settings
will be applied for the Backorders
option

Use Config Settings for Enable Qty Choose whether the Config settings
Increments will be applied for the Enable Qty
Increments option

Use Config Settings for Manage Choose whether the Config settings
Stock will be applied for the Manage
Stock option

Use Config Settings for Maximum Qty Choose whether the Config settings
Allowed in Shopping Cart will be applied for the Maximum Qty
Allowed in Shopping Cart option

Use Config Settings for Minimum Qty Choose whether the Config settings
Allowed in Shopping Cart will be applied for the Minimum Qty
Allowed in Shopping Cart option

Use Config Settings for Notify for Choose whether the Config settings
Quantity Below will be applied for the Notify for
Quantity Below option

Use Config Settings for Qty Choose whether the Config settings
Increments will be applied for the Qty
Increments option

Use Config Settings for Qty for Choose whether the Config settings
Item\'s Status to Become Out of will be applied for the Qty for
Stock Item\'s Status to Become Out of
Stock option

---

**Notes**: The Admin user type has restrictions concerning the WRITE
operations for definite stock item attributes. These are as follows:

Attribute Name Admin

---

Item ID No
Product ID No
Stock ID No
Low Stock Date No

However, these attributes are available for READ operations.

## [Customer](#RESTAttributes-Description-Customer)

Attribute Name Attribute Description

---

Customer ID Customer ID
Last Logged In Date when the customer was logged in last
Is Confirmed Defines whether the email confirmation is sent to the customer
Created At Date when the customer was created
Associate to Website Website ID to which the customer is associated
Created From Store view from which the customer was created
Group Customer group ID
Disable automatic group change Defines whether the automatic group change will be applied to the customer
Prefix Customer prefix
First Name Customer first name
Middle Name/Initial Customer middle name or initial
Last Name Customer last name
Suffix Customer suffix
Email Customer email address
Date Of Birth Customer date of birth
Tax/VAT Number Customer tax or VAT number
Gender Customer gender (male or female)

## [Customer Address](#RESTAttributes-Description-CustomerAddress)

Attribute Name Attribute Description

---

City City name
Company Company name
Country Country
Customer Address ID Customer address ID
Fax Fax number
First Name Customer first name
Is Default Billing Address Defines whether the address is a default one for billing
Is Default Shipping Address Defines whether the address is a default one for shipping
Last Name Customer last name
Middle Name/Initial Customer middle name or initial
Prefix Customer prefix
State/Province Customer state/region
Street Address Customer street address
Suffix Customer suffix
Telephone Customer phone number
VAT Number Customer VAT number
ZIP/Postal Code Customer ZIP or postal code

## [Product](#RESTAttributes-Description-Product)

Attributes for the product resource are divided into those available for
the Admin type of user and those available for the Customer and Guest
types of user.

---

Attribute Name Attribute Description Notes

---

Product ID Product ID Available only for
Admin

name Product Name

Product Type Product type. Can have  
the following values:  
 Simple, Grouped,  
 Configurable, Virtual,  
 Bundle, or Downloadable

Attribute Set Name Name of the attribute Available only for
set which the product Admin
 is based on

sku\ Product SKU

price Product price

visibility Product visibility in Available only for
the store. Can have the Admin
 following values:  
 Catalog, Search;  
 Search; Catalog; Not  
 Visible Individually

description Product description

short_description Product short  
description

weight Product weight Available only for
Admin

news_from_date Date starting from Available only for
which the product is Admin
 promoted as a new  
 product

news_to_date Date till which the Available only for
product is promoted as Admin
 a new product

status Product status in the Available only for
store. Can have the Admin
 following values:  
 Enabled or Disabled

url_key A friendly URL path for Available only for
the product Admin

Create Permanent Redirect for Old Defines whether the Available only for
URL redirect to an original Admin; available only
URL will be applied for product update
 (when the existing URL  
 for a product is  
 edited)

country_of_manufacture Product country of Available only for
manufacture Admin

is_returnable Defines whether the Available only for
product can be returned Admin

special_price Product special price Available only for
Admin

special_from_date Date starting from Available only for
which the special price Admin
 will be applied for the
product

special_to_date Date till which the Available only for
special price will be Admin
 applied for the product

group_price Product group price Available only for
Admin

tier_price Product tier price

msrp_enabled The Apply MAP option. Available only for
Defines whether the Admin
 price in the catalog in
the frontend is  
 substituted with a  
 Click for price link

msrp_display_actual_price_type Defines how the price Available only for
will be displayed in Admin
 the frontend. Can have  
 the following values:  
 In Cart, Before Order  
 Confirmation, and On  
 Gesture

msrp The Manufacturer\'s Available only for
Suggested Retail Price Admin
 option. The price that  
 a manufacturer suggests
to sell the product at

enable_googlecheckout Defines whether the Available only for
product can be Admin
 purchased with the help
of the Google Checkout  
 payment service. Can  
 have the following  
 values: Yes and No

tax_class_id The product tax class Available only for
to which the product Admin
 will be associated

meta_title Product meta title

meta_keyword Product meta keywords

meta_description Product meta  
description

custom_design Custom design applied Available only for
for the product page Admin

custom_design_from Date starting from Available only for
which the custom design Admin
 will be applied for the
product page

custom_design_to Date till which the Available only for
custom design will be Admin
 applied for the product
page

custom_layout_update An XML block to alter Available only for
the page layout Admin

page_layout Page template that can Available only for
be applied to the Admin
 product page

options_container Defines how the custom Available only for
options for the product Admin
 will be displayed. Can  
 have the following  
 values: Block after  
 Info Column or Product  
 Info Column

gift_message_available Defines whether the Available only for
gift message is Admin
 available for the  
 product

Use Config Settings for Allow Gift Defines whether the Available only for
Message configuration settings Admin
 will be used for the  
 Allow Gift Message  
 option

gift_wrapping_available Defines whether the Available only for
gift wrapping is Admin. This attribute
available for the is available in Magento
product EE

Use Config Settings for Allow Gift Defines whether the Available only for
Wrapping configuration settings Admin. This attribute
will be used for the is available in Magento
Allow Gift Wrapping EE
 option

gift_wrapping_price Price for the gift Available only for
wrapping (available in Admin
 Magento EE)

Inventory Data Product inventory data Available only for
Admin

Custom attr Product custom The customer can see
attributes only attributes that
are set as visible on
frontend

Regular Price The original product Available only for
price displayed in the Customer and Guest
 frontend

Final Price The final product price Available only for
Customer and Guest

Final Price with Tax\ The final product price Available only for
with tax Customer and Guest

Final Price Without Tax\ The final product price Available only for
without tax Customer and Guest

Stock Status\ The product stock Available only for
status (availability) Customer and Guest

Product Is Saleable\ Defines whether the Available only for
product can be sold Customer and Guest

Total Reviews Number\ The number of all Available only for
reviews for a product Customer and Guest

Product URL Link\ A link to the product Available only for
without the assigned Customer and Guest
 category

Buy Now Link\ A link that adds a Available only for
product to the shopping Customer and Guest
 cart

Product Has Custom Options\ Defines whether the Available only for
product has custom Customer and Guest
 options or not

Default Product Image\ Default product image Available only for
Customer and Guest

---

### [Product Category](#RESTAttributes-Description-ProductCategory)

Attribute Name Attribute Description

---

Category ID ID of the category to which the product is assigned

### [Product Image](#RESTAttributes-Description-ProductImage)

---

Attribute Name Attribute Description Notes

---

Exclude Defines whether the  
image will associate  
 only to one of the  
 three image types.

ID Image file ID Available only for READ
operations

Label A label that will be  
displayed on the  
 frontend when pointing  
 to the image

Position The Sort Order option.  
The order in which the  
 images are displayed in
the MORE VIEWS  
 section.

Type Image type. Can have  
the following values:  
 Base Image, Small  
 Image, or Thumbnail.

URL Image file URL path Available only for READ
operations

File Content Image file content Available only for
(base_64 encoded) WRITE operations

File MIME Type File MIME type. Can Available only for
have the following WRITE operations
 values: image/jpeg,  
 image/png, image/gif,  
 etc.

File Name Image file name Available only for
WRITE operations

---


Create the Magento file system owner
