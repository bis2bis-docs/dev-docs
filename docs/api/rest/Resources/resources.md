

-   [Products](Products/products.html "REST API - Resource - Products")
    -   [Product
        Categories](Products/product_categories.html "REST API - Resource - Product Categories")
    -   [Product
        Images](Products/product_images.html "REST API - Resource - Product Images")
    -   [Product
        Websites](Products/product_websites.html "REST API - Resource - Product Websites")
-   [Customers](resource_customers.html "REST API - Resource - Customers")
-   [Customer
    Addresses](resource_customer_addresses.html "REST API - Resource - Customer Addresses")
-   [Inventory](inventory.html "REST API - Resource - Stock Items")
-   [Sales
    Orders](Orders/sales_orders.html "REST API - Resource - Sales Orders")
    -   [Order
        Addresses](Orders/order_addresses.html "REST API - Resource - Order Addresses")
    -   [Order
        Comments](Orders/order_comments.html "REST API - Resource - Order Comments")
    -   [Order
        Items](Orders/order_items.html "REST API - Resource - Order Items")

The Magento REST API allows you to manage customers, customer addresses,
sales orders, inventory, and products. REST API is organized into the
following categories:

### [Products](#Resources-Products)

Retrieve the list of products, create, update, delete a product.

**Resource Structure**: http://magentohost/api/rest/products

#### [Product Categories](#Resources-ProductCategories)

Retrieve the list of categories assigned to a product, assign and
unassign the category from a product.

**Resource Structure**:
http://magentohost/api/rest/products/:id/categories

#### [Product Images](#Resources-ProductImages)

Retrieve the list of images assigned to a product, add, update, remove
an image to/from a product.

**Resource Structure**: http://magentohost/api/rest/products/:id/images

#### [Product Websites](#Resources-ProductWebsites)

Retrieve the list of websites assigned to a product, assign, unassign a
website to/from a product.

**Resource Structure**:
http://magentohost/api/rest/products/:id/websites

### [Customers](#Resources-Customers)

Retrieve the list of customers, create, delete a customer, and update
the customer information.

**Resource Structure**: http://magentohost/api/rest/customers

### [Customer Addresses](#Resources-CustomerAddresses)

Retrieve the list of customer addresses, create, update, and delete the
customer address.

**Resource Structure**:
http://magentohost/api/rest/customers/:id/addresses

### [Inventory](#Resources-Inventory)

Retrieve the list of stock items, update required stock items.

**Resource Structure**: http://magentohost/api/rest/stockitems

### [Sales Orders](#Resources-SalesOrders)

Retrieve the list of sales orders with detailed information on order
addresses, items, and comments.

**Resource Structure**: http://magentohost/api/rest/orders

#### [Order Addresses](#Resources-OrderAddresses)

Retrieve information on order billing and shipping addresses.

**Resource Structure**: http://magentohost/api/rest/orders/:id/addresses

#### [Order Comments](#Resources-OrderComments)

Retrieve information on the specified order comments.

**Resource Structure**: http://magentohost/api/rest/orders/:id/comments

#### [Order Items](#Resources-OrderItems)

Retrieve information on specified order items.

**Resource Structure**: http://magentohost/api/rest/orders/:id/items
