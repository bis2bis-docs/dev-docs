

-   [REST API: Stock
    Items](#RESTAPI-Resource-StockItems-RESTAPI-StockItems)
    -   [URI: /stockitems](#RESTAPI-Resource-StockItems-URI--stockitems)
        -   [HTTP Method: GET
            /stockitems](#RESTAPI-Resource-StockItems-HTTPMethod-GET-stockitems)
        -   [HTTP Method: PUT
            /stockitems](#RESTAPI-Resource-StockItems-HTTPMethod-PUT-stockitems)
-   [REST API: Stock
    Item](#RESTAPI-Resource-StockItems-RESTAPI-StockItem)
    -   [URI:
        /stockitems/:id](#RESTAPI-Resource-StockItems-URI--stockitems--id)
        -   [HTTP Method : GET
            /stockitems/:id](#RESTAPI-Resource-StockItems-HTTPMethod-GET-stockitems--id)
        -   [HTTP Method : PUT
            /stockitems/:id](#RESTAPI-Resource-StockItems-HTTPMethod-PUT-stockitems--id)
        -   [HTTP Method : DELETE
            /stockitems/:id](#RESTAPI-Resource-StockItems-HTTPMethod-DELETE-stockitems--id)


![important](~@assets/icon-important.png) **Important**:
The default response for all APIs is JSON; however, this page currently
uses mostly XML examples. We don\'t maintain this documentation much
anymore so if you\'d like to contribute some JSON, we\'ll acknowledge
your help on this page. [Edit this page on
GitHub](rest/Resources/inventory.html)
if you can help us out.


### [REST API](#RESTAPI-Resource-StockItems-RESTAPI-StockItems): Stock Items

## [URI](#RESTAPI-Resource-StockItems-URI--stockitems): /stockitems

Allows you to manage existing stock items. Inventory management is
available only for Admin.

**URL Structure**: http://magentohost/api/rest/stockitems
**Version**: 1

## [HTTP Method](#RESTAPI-Resource-StockItems-HTTPMethod-GET-stockitems): GET /stockitems

**Description**: Allows you to retrieve the list of existing stock
items.
**Notes**: The list of attributes that will be returned for stock items
is configured in the Magento Admin Panel.

**Authentication**: Admin
**Default Format**: JSON
**Parameters** :
_No Parameters_

**Response Example: XML**



---

GET http://magentohost/api/rest/stockitems

---








<div>

```
<?xml version="1.0"?>
<magento_api>
  <data_item>
    <item_id>1</item_id>
    <qty>100.0000</qty>
    <backorders>0</backorders>
    <min_sale_qty>1.0000</min_sale_qty>
    <max_sale_qty>0.0000</max_sale_qty>
    <low_stock_date></low_stock_date>
    <manage_stock>0</manage_stock>
    <stock_status_changed_auto>0</stock_status_changed_auto>
    <enable_qty_increments>0</enable_qty_increments>
  </data_item>
  <data_item>
    <item_id>2</item_id>
    <qty>100.0000</qty>
    <backorders>0</backorders>
    <min_sale_qty>1.0000</min_sale_qty>
    <max_sale_qty>0.0000</max_sale_qty>
    <low_stock_date></low_stock_date>
    <manage_stock>0</manage_stock>
    <stock_status_changed_auto>0</stock_status_changed_auto>
    <enable_qty_increments>0</enable_qty_increments>
  </data_item>
  <data_item>
    <item_id>3</item_id>
    <qty>1.0000</qty>
    <backorders>0</backorders>
    <min_sale_qty>1.0000</min_sale_qty>
    <max_sale_qty>0.0000</max_sale_qty>
    <low_stock_date></low_stock_date>
    <manage_stock>0</manage_stock>
    <stock_status_changed_auto>0</stock_status_changed_auto>
    <enable_qty_increments>0</enable_qty_increments>
  </data_item>
  <data_item>
    <item_id>4</item_id>
    <qty>0.0000</qty>
    <backorders>0</backorders>
    <min_sale_qty>1.0000</min_sale_qty>
    <max_sale_qty>0.0000</max_sale_qty>
    <low_stock_date></low_stock_date>
    <manage_stock>0</manage_stock>
    <stock_status_changed_auto>1</stock_status_changed_auto>
    <enable_qty_increments>0</enable_qty_increments>
  </data_item>
</magento_api>
```

</div>





**Response Example: JSON**



---

GET http://magentohost/api/rest/stockitems

---








<div>

```
[{"item_id":"1","qty":"100.0000","backorders":"0","min_sale_qty":"1.0000","max_sale_qty":"0.0000","low_stock_date":null,"manage_stock":"0","stock_status_changed_auto":"0","enable_qty_increments":"0"},{"item_id":"2","qty":"100.0000","backorders":"0","min_sale_qty":"1.0000","max_sale_qty":"0.0000","low_stock_date":null,"manage_stock":"0","stock_status_changed_auto":"0","enable_qty_increments":"0"},{"item_id":"3","qty":"1.0000","backorders":"0","min_sale_qty":"1.0000","max_sale_qty":"0.0000","low_stock_date":null,"manage_stock":"0","stock_status_changed_auto":"0","enable_qty_increments":"0"},{"item_id":"4","qty":"0.0000","backorders":"0","min_sale_qty":"1.0000","max_sale_qty":"0.0000","low_stock_date":null,"manage_stock":"0","stock_status_changed_auto":"1","enable_qty_increments":"0"}]
```

</div>





## [HTTP Method](#RESTAPI-Resource-StockItems-HTTPMethod-PUT-stockitems): PUT /stockitems

**Description**: Allows you to update existing stock items.

**Authentication**: Admin
**Default Format**: JSON

**Notes**: The Content-Type: text/xml parameter must be added to the
request header.

**Parameters**:

Name Description Type Example Value

---

item_id Item ID int 1
product_id Product ID int 1
stock_id Stock ID int 1
qty Quantity of stock items for the current product string 20
min_qty Quantity for stock items to become out of stock string 0
use_config_min_qty Choose whether the Config settings will be applied for the Qty for Item\'s Status to Become Out of Stock option int 1
is_qty_decimal Choose whether the product can be sold using decimals (e.g., you can buy 2.5 product) int 0
backorders The customer can place the order for products that are out of stock at the moment (0 - No Backorders, 1 - Allow Qty Below 0, and 2 - Allow Qty Below 0 and Notify Customer) int 0
use_config_backorders Choose whether the Config settings will be applied for the Backorders option int 1
min_sale_qty Minimum number of items in the shopping cart to be sold string 10
use_config_min_sale_qty Choose whether the Config settings will be applied for the Minimum Qty Allowed in Shopping Cart option int 0
max_sale_qty Maximum number of items in the shopping cart to be sold string 100
use_config_max_sale_qty Choose whether the Config settings will be applied for the Maximum Qty Allowed in Shopping Cart option int 0
is_in_stock Defines whether the product is available for selling (0 - Out of Stock, 1 - In Stock) int 1
low_stock_date Date when the number of stock items became lower than the number defined in the Notify for Quantity Below option string 2012-02-24 12:37:51
notify_stock_qty The number of inventory items below which the customer will be notified via the RSS feed string 10
use_config_notify_stock_qty Choose whether the Config settings will be applied for the Notify for Quantity Below option int 0
manage_stock Choose whether to view and specify the product quantity and availability and whether the product is in stock management( 0 - No, 1 - Yes) int 0
use_config_manage_stock Choose whether the Config settings will be applied for the Manage Stock option int 1
stock_status_changed_auto Defines whether products can be automatically returned to stock when the refund for an order is created int 0
use_config_qty_increments Choose whether the Config settings will be applied for the Enable Qty Increments option int 1
qty_increments The product quantity increment value string 5
use_config_enable_qty_inc Choose whether the Config settings will be applied for the Qty Increments option int 1
enable_qty_increments Defines whether the customer can add products only in increments to the shopping cart int 0
is_decimal_divided Defines whether the stock items can be divided into multiple boxes for shipping. int 0

**Example: XML**



---

PUT http://magentohost/api/rest/stockitems

---



**Request Body**:






<div>

```
<?xml version="1.0"?>
<magento_api>
  <data_item item_id="157">
    <product_id>262</product_id>
    <stock_id>1</stock_id>
    <qty>100.0000</qty>
    <min_qty>0.0000</min_qty>
  </data_item>
  <data_item item_id="158">
    <product_id>263</product_id>
    <stock_id>1</stock_id>
    <qty>100.0000</qty>
    <min_qty>0.0000</min_qty>
  </data_item>
  <data_item item_id="159">
    <product_id>264</product_id>
    <stock_id>1</stock_id>
    <qty>120.0000</qty>
    <min_qty>0.0000</min_qty>
  </data_item>
  <data_item item_id="153">
    <product_id> </product_id>
    <qty>110.0000</qty>
    <min_qty>0.0000</min_qty>
  </data_item>
</magento_api>
```

</div>





**Response Body**:






<div>

```
<?xml version="1.0"?>
<magento_api>
  <success>
    <data_item>
      <message>Resource updated successful.</message>
      <code>200</code>
      <item_id>157</item_id>
    </data_item>
  </success>
  <error>
    <data_item>
      <message>Resource not found.</message>
      <code>404</code>
      <item_id>158</item_id>
    </data_item>
    <data_item>
      <message>Resource not found.</message>
      <code>404</code>
      <item_id>159</item_id>
    </data_item>
    <data_item>
      <message>Empty value for "product_id" in request.</message>
      <code>400</code>
      <item_id>153</item_id>
    </data_item>
  </error>
</magento_api>
```

</div>





### [REST API](#RESTAPI-Resource-StockItems-RESTAPI-StockItem): Stock Item

## [URI](#RESTAPI-Resource-StockItems-URI--stockitems--id): /stockitems/:id

Allows you to update, delete, or retrieve information on a single stock
item.
**Notes**: The list of attributes that will be returned for stock items
is configured in the Magento Admin Panel.

**URL Structure**: <http://magentohost/api/rest/stockitems/:id>
**Version**: 1

## [HTTP Method ](#RESTAPI-Resource-StockItems-HTTPMethod-GET-stockitems--id): GET /stockitems/:id

**Description**: Allows you to retrieve the stock item information.
**Authentication**: Admin
**Default Format**: JSON

**Response Example: XML**



---

GET http://magentohost/api/rest/stockitems/1

---



**Response Body**:






<div>

```
<?xml version="1.0"?>
<magento_api>
  <item_id>1</item_id>
  <product_id>1</product_id>
  <stock_id>1</stock_id>
  <qty>200.0000</qty>
  <min_qty>0.0000</min_qty>
  <use_config_min_qty>1</use_config_min_qty>
  <is_qty_decimal>1</is_qty_decimal>
  <backorders>0</backorders>
  <use_config_backorders>1</use_config_backorders>
  <min_sale_qty>1.0000</min_sale_qty>
  <use_config_min_sale_qty>1</use_config_min_sale_qty>
  <max_sale_qty>0.0000</max_sale_qty>
  <use_config_max_sale_qty>1</use_config_max_sale_qty>
  <is_in_stock>1</is_in_stock>
  <low_stock_date></low_stock_date>
  <notify_stock_qty>10.0000</notify_stock_qty>
  <use_config_notify_stock_qty>0</use_config_notify_stock_qty>
  <manage_stock>0</manage_stock>
  <use_config_manage_stock>1</use_config_manage_stock>
  <stock_status_changed_auto>0</stock_status_changed_auto>
  <use_config_qty_increments>1</use_config_qty_increments>
  <qty_increments>0.0000</qty_increments>
  <use_config_enable_qty_inc>1</use_config_enable_qty_inc>
  <enable_qty_increments>0</enable_qty_increments>
  <is_decimal_divided>1</is_decimal_divided>
</magento_api>
```

</div>





**Response Example: JSON**



---

GET http://magentohost/api/rest/stockitems/157

---



**Response Body**:






<div>

```
{"item_id":"1","product_id":"1","stock_id":"1","qty":"200.0000","min_qty":"0.0000","use_config_min_qty":"1","is_qty_decimal":"1","backorders":"0","use_config_backorders":"1","min_sale_qty":"1.0000","use_config_min_sale_qty":"1","max_sale_qty":"0.0000","use_config_max_sale_qty":"1","is_in_stock":"1","low_stock_date":null,"notify_stock_qty":"10.0000","use_config_notify_stock_qty":"0","manage_stock":"0","use_config_manage_stock":"1","stock_status_changed_auto":"0","use_config_qty_increments":"1","qty_increments":"0.0000","use_config_enable_qty_inc":"1","enable_qty_increments":"0","is_decimal_divided":"1"}
```

</div>





## [HTTP Method ](#RESTAPI-Resource-StockItems-HTTPMethod-PUT-stockitems--id): PUT /stockitems/:id

**Description**: Allows you to update existing stock item data.
**Notes**: The Content-Type: text/xml parameter must be added to the
request header.
**Authentication**: Admin
**Default Format**: JSON
**Parameters**:
_Enter only those parameters which you want to update._

**Example: XML**



---

PUT http://magentohost/api/rest/stockitems/1

---



**Request Body**:






<div>

```
<?xml version="1.0"?>
<magento_api>
  <qty>99</qty>
</magento_api>
```

</div>





**Example: JSON**



---

PUT http://magentohost/api/rest/stockitems/1

---



**Request Body**:






<div>

```
{
"qty":"99"
}
```

</div>





## [HTTP Method ](#RESTAPI-Resource-StockItems-HTTPMethod-DELETE-stockitems--id): DELETE /stockitems/:id

**Description**: Not allowed. The DELETE method is not allowed because
you cannot delete a stock item. The required stock item is deleted
together with the product which it is associated to.

**Possible HTTP Status Codes:**

Error Code Error Message Error Description

---

200 Resource updated successful. The required resource was successfully updated.
404 Resource not found. The required resource is not found or does not exist.
400 Empty value for \<name of the parameter\> in request. Value is not defined for the specified parameter in the request body.
400 Invalid value for \"item_id\" in request. The specified value for \"item_id\" is not valid.
400 Missing \<name of the parameter\> in request. The specified parameter is missing in the request body.
500 Resource internal error. Resource internal error.
