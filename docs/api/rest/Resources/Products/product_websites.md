

-   [REST API: Product
    Websites](#RESTAPI-Resource-ProductWebsites-RESTAPI-ProductWebsites)
    -   [URI:
        /products/:product_id/websites](#RESTAPI-Resource-ProductWebsites-URI--products--productid-websites)
        -   [HTTP Method:
            GET](#RESTAPI-Resource-ProductWebsites-HTTPMethod-GET)
        -   [HTTP Method:
            POST](#RESTAPI-Resource-ProductWebsites-HTTPMethod-POST)
            -   [Website
                Assignment](#RESTAPI-Resource-ProductWebsites-WebsiteAssignment)
            -   [Website Assignment with Product Data
                Copying](#RESTAPI-Resource-ProductWebsites-WebsiteAssignmentwithProductDataCopying)
            -   [Multi-Website
                Assignment](#RESTAPI-Resource-ProductWebsites-MultiWebsiteAssignment)
            -   [Multi-Website Assignment with Product Data
                Copying](#RESTAPI-Resource-ProductWebsites-MultiWebsiteAssignmentwithProductDataCopying)
    -   [URI:
        /products/:product_id/websites/:website_id](#RESTAPI-Resource-ProductWebsites-URI--products--productid-websites--websiteid)
        -   [HTTP Method:
            DELETE](#RESTAPI-Resource-ProductWebsites-HTTPMethod-DELETE)
-   [Possible HTTP Status
    Codes](#RESTAPI-Resource-ProductWebsites-PossibleHTTPStatusCodes)

### [REST API](#RESTAPI-Resource-ProductWebsites-RESTAPI-ProductWebsites): Product Websites

## [URI](#RESTAPI-Resource-ProductWebsites-URI--products--productid-websites): /products/:product_id/websites

Allows you to retrieve information about websites assigned to a product,
assign a website to a product, and copy data for a product from a
specified store view.

**URL Structure**:
http://magentohost/api/rest/products/:product\_id/websites
**Version**: 1

## [HTTP Method](#RESTAPI-Resource-ProductWebsites-HTTPMethod-GET): GET

**Description**: Allows you to retrieve information about websites
assigned to the specified product.

**Authentication**: Admin
**Default Format**: XML
**Parameters**:
_No Parameters_

**Example:**

---

GET http://magentohost/api/rest/products/8/websites

---



**Response Body:**

<div>

```
<?xml version="1.0"?>
<magento_api>
  <data_item>
    <website_id>2</website_id>
  </data_item>
</magento_api>
```

</div>





## [HTTP Method](#RESTAPI-Resource-ProductWebsites-HTTPMethod-POST): POST

#### [Website Assignment](#RESTAPI-Resource-ProductWebsites-WebsiteAssignment)

**Description**: Allows you to assign a website to a specified product.

**Authentication**: Admin
**Default Format**: XML
**Parameters**:

Name Description Required Type Example Value

---

website_id The website ID required int 2

**Example:**

---

POST http://magentohost/api/rest/products/8/websites

---



**Request Body:**

<div>

```
<?xml version="1.0"?>
<magento_api>
    <website_id>1</website_id>
</magento_api>
```

</div>





**Response Body:**

<div>

```
<?xml version="1.0"?>
<magento_api>
  <success>
    <data_item>
      <website_id>1</website_id>
      <product_id>8</product_id>
      <message>Resource updated successful.</message>
      <code>200</code>
    </data_item>
  </success>
</magento_api>
```

</div>








#### [Website Assignment with Product Data Copying](#RESTAPI-Resource-ProductWebsites-WebsiteAssignmentwithProductDataCopying)

**Description**: Allows you to assign a website and copy product data
from the attached store to the one being attached. Only product data
that is set on the Store View level is copied. All other data set on the
Website or Global levels is not copied.

**Authentication**: Admin
**Default Format**: XML
**Parameters**:

Name Description Required Type Example Value

---

website_id The website ID required int 2
store_from The store ID from which data will be copied required int 1
store_to The store ID to which data will be copied required int 2

**Notes:** The store_to parameter must belong to the website which we
want to assign to a product.

**Example:**

---

POST http://magentohost/api/rest/products/8/websites

---



**Request Body:**

<div>

```
<?xml version="1.0"?>
<magento_api>
     <website_id>2</website_id>
      <copy_to_stores>
          <data_item>
               <store_from>1</store_from>
               <store_to>2</store_to>
           </data_item>
      </copy_to_stores>
</magento_api>
```

</div>








#### [Multi-Website Assignment](#RESTAPI-Resource-ProductWebsites-MultiWebsiteAssignment)

**Description**: Allows you to assign multiple websites to a product.

**Authentication**: Admin
**Default Format**: XML
**Parameters**:

Name Description Required Type Example Value

---

website_id The website ID required int 2

**Example:**

---

POST http://magentohost/api/rest/products/8/websites

---



**Request Body:**

<div>

```
<?xml version="1.0"?>
<magento_api>
  <data_item>
    <website_id>1</website_id>
  </data_item>
  <data_item>
    <website_id>3</website_id>
  </data_item>
</magento_api>
```

</div>





**Response Body:**

<div>

```
<?xml version="1.0"?>
<magento_api>
  <success>
    <data_item>
      <website_id>1</website_id>
      <product_id>8</product_id>
      <message>Resource updated successful.</message>
      <code>200</code>
    </data_item>
    <data_item>
      <website_id>3</website_id>
      <product_id>8</product_id>
      <message>Resource updated successful.</message>
      <code>200</code>
    </data_item>
  </success>
</magento_api>
```

</div>








#### [Multi-Website Assignment with Product Data Copying](#RESTAPI-Resource-ProductWebsites-MultiWebsiteAssignmentwithProductDataCopying)

**Description**: Allows you to assign multiple websites to a product
together with copying product data from the attached store to the one
being attached. Only product data that is set on the Store View level is
copied. All other data set on the Website or Global levels is not
copied.

**Authentication**: Admin
**Default Format**: XML
**Parameters**:

Name Description Required Type Example Value

---

website_id The website ID required int 2
store_from The store ID from which data will be copied required int 1
store_to The store ID to which data will be copied required int 2

**Example:**

---

POST http://magentohost/api/rest/products/8/websites

---



**Request Body:**

<div>

```
<?xml version="1.0"?>
<magento_api>
    <data_item>
        <website_id>2</website_id>
        <copy_to_stores>
            <data_item>
                <store_from>1</store_from>
                <store_to>2</store_to>
            </data_item>
        </copy_to_stores>
    </data_item>
    <data_item>
        <website_id>3</website_id>
        <copy_to_stores>
            <data_item>
                <store_from>1</store_from>
                <store_to>5</store_to>
            </data_item>
        </copy_to_stores>
    </data_item>
</magento_api>
```

</div>





**Response Body:**

<div>

```
<?xml version="1.0"?>
<magento_api>
  <success>
    <data_item>
      <website_id>2</website_id>
      <product_id>8</product_id>
      <message>Resource updated successful.</message>
      <code>200</code>
    </data_item>
    <data_item>
      <website_id>3</website_id>
      <product_id>8</product_id>
      <message>Resource updated successful.</message>
      <code>200</code>
    </data_item>
  </success>
</magento_api>
```

</div>








## [URI](#RESTAPI-Resource-ProductWebsites-URI--products--productid-websites--websiteid): /products/:product_id/websites/:website_id

Allows you to unassign a website from a specified product.

**URL Structure**:
http://magentohost/api/rest/products/:product\_id/websites/:website\_id
**Version**: 1

## [HTTP Method](#RESTAPI-Resource-ProductWebsites-HTTPMethod-DELETE): DELETE

**Description**: Allows you to unassign a website from a specified
product.

**Authentication**: Admin
**Default Format**: XML
**Parameters**:
_No Parameters_

**Example:**

---

DELETE http://magentohost/api/rest/products/8/websites/1

---



## [Possible HTTP Status Codes](#RESTAPI-Resource-ProductWebsites-PossibleHTTPStatusCodes)

---

Status Code Message Description

---

404 Product not found The specified product
is not found or does
not exist.

404 Website not found The specified website
is not found or does
not exist.

400 Invalid value for The entered value for
\"store_from\" for the \"store_from\" is not
website with ID \<ID valid.
value\>.

400 Invalid value for The entered value for
\"store_to\" for the \"store_to\" is not
website with ID \<ID valid.
value\>.

400 Store not found \<store The specified store is
ID\> for website not found or does not
\<website ID\>. exist.

400 Store \<store ID\> from The specified store is
which we will copy the not assigned to the
information does not product.
belong to the product  
 \<product ID\> being  
 edited.

400 Store \<store ID\> to The specified store
which we will copy the does not belong to the
information does not website.
belong to the website  
 \<website ID\> being  
 added.

400 Product \<product ID\> The specified product
isn\'t assigned to is not assigned to the
website \<website ID\>. website.

400 Invalid value for The value for
\"website_id\" in \"website_id\" is not
request. valid.

---
