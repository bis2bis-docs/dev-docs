

-   [REST API: Product
    Images](#RESTAPI-Resource-ProductImages-RESTAPI-ProductImages)
    -   [URI:
        /products/:product_id/images](#RESTAPI-Resource-ProductImages-URI--products--productid-images)
        -   [HTTP Method:
            GET](#RESTAPI-Resource-ProductImages-URI--products--productid-images-GET)
        -   [HTTP Method:
            POST](#RESTAPI-Resource-ProductImages-URI--products--productid-images-POST)
    -   [URI:
        /products/:product_id/images/store/:store_id](#RESTAPI-Resource-ProductImages-URI--products--productid-images-store--storeid)
        -   [HTTP Method:
            GET](#RESTAPI-Resource-ProductImages-URI--products--productid-images-store--storeid-GET)
        -   [HTTP Method:
            POST](#RESTAPI-Resource-ProductImages-URI--products--productid-images-store--storeid-POST)
    -   [URI:
        /products/:product_id/images/:image_id](#RESTAPI-Resource-ProductImages-URI--products--productid-images--imageid)
        -   [HTTP Method:
            GET](#RESTAPI-Resource-ProductImages-URI--products--productid-images--imageid-GET)
        -   [HTTP Method:
            PUT](#RESTAPI-Resource-ProductImages-URI--products--productid-images--imageid-PUT)
        -   [HTTP Method:
            DELETE](#RESTAPI-Resource-ProductImages-URI--products--productid-images--imageid-DELETE)
    -   [URI:
        /products/:product_id/images/:image_id/store/:store_id](#RESTAPI-Resource-ProductImages-URI--products--productid-images--imageid-store--storeid)
        -   [HTTP Method:
            GET](#RESTAPI-Resource-ProductImages-URI--products--productid-images--imageid-store--storeid-GET)
        -   [HTTP Method:
            PUT](#RESTAPI-Resource-ProductImages-URI--products--productid-images--imageid-store--storeid-PUT)
        -   [HTTP Method:
            DELETE](#RESTAPI-Resource-ProductImages-URI--products--productid-images--imageid-store--storeid-DELETE)
-   [Possible HTTP Status
    Codes](#RESTAPI-Resource-ProductImages-PossibleHTTPStatusCodes)

### [REST API](#RESTAPI-Resource-ProductImages-RESTAPI-ProductImages): Product Images

## [URI](#RESTAPI-Resource-ProductImages-URI--products--productid-images): /products/:product_id/images

Allows you to manage images of the required product.

**URL Structure**:
http://magentohost/api/rest/products/:product\_id/images
**Version**: 1

## [HTTP Method](#RESTAPI-Resource-ProductImages-URI--products--productid-images-GET): GET

**Description**: Allows you to retrieve information about all images of
a specified product.
**Notes**: If there are custom attributes with the **Catalog Input Type
for Store Owner** option set to **Media Image**, these attributes will
be also returned in the response as an image type.

**Authentication**: Admin, Customer, Guest
**Default Format**: XML
**Parameters**:
_No Parameters_

**Example:**

---

GET http://magentohost/api/rest/products/8/images

---



**Response Body:**

<div>

```
<?xml version="1.0"?>
<magento_api>
  <data_item>
    <id>5</id>
    <label></label>
    <position>1</position>
    <exclude>0</exclude>
    <url>http://magento/media/catalog/product/v/e/ve2011.jpg</url>
    <types>
      <data_item>image</data_item>
      <data_item>thumbnail</data_item>
    </types>
  </data_item>
  <data_item>
    <id>7</id>
    <label>second dress</label>
    <position>2</position>
    <exclude>1</exclude>
    <url>http://magento/media/catalog/product/1/2/12.jpg</url>
    <types>
      <data_item>small_image</data_item>
    </types>
  </data_item>
</magento_api>
```

</div>





## [HTTP Method](#RESTAPI-Resource-ProductImages-URI--products--productid-images-POST): POST

**Description**: Allows you to add an image for the required product.

**Authentication**: Admin
**Default Format**: XML
**Parameters**:

Name Description Required Type Example Value

---

file_mime_type File mime type. Can have the following values: image/jpeg, image/png, etc. required string image/jpeg
file_content Graphic image file content (base_64 encoded) required string iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAWtJREFUeNpi/P//P8NgBkwMgxyMOnDUgTDAyMhIDNYF4vNA/B+IDwCxHLoakgEoFxODiQRXQUYi4e3k2gfDjMRajsP3zED8F8pmA+JvUDEYeArEMugOpFcanA/Ef6A0CPwC4uNoag5SnAjJjGI2tKhkg4rLAfFGIH4IxEuBWIjSKKYkDfZCHddLiwChVhokK8YGohwEZYy3aBmEKmDEhOCgreomo+VmZHxsMEQxIc2MAx3FO/DI3RxMmQTZkI9ALDCaSUYdOOrAIeRAPzQ+PxCHUM2FFDb5paGNBPRa5C20bUhxc4sSB4JaLnvxVHWHsbVu6OnACjyOg+HqgXKgGRD/JMKBoD6LDb0dyAPE94hwHAw/hGYcujlwEQmOg+EV9HJgLBmOg+FMWjsQVKR8psCBoDSrQqoDSSmoG6Hpj1wA6ju30LI9+BBX4UsC+Ai0T4BWVd1EIL5PgeO+APECmoXgaGtm1IE0AgABBgAJAICuV8dAUAAAAABJRU5ErkJggg==
file_name Name for the added image file optional string new image

**Note**: If the file_name parameter is not defined, the original file
name is set for the image. The first created image will be called
\"image\", the second created image will be called \"image_2\", etc.

**Example:**

---

POST http://magentohost/api/rest/products/1/images

---



**Request Body:**

<div>

```
<?xml version="1.0"?>
<magento_api>
  <file_mime_type>image/jpeg</file_mime_type>     <file_content>iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAWtJREFUeNpi/P//P8NgBkwMgxyMOnDUgTDAyMhIDNYF4vNA/B+IDwCxHLoakgEoFxODiQRXQUYi4e3k2gfDjMRajsP3zED8F8pmA+JvUDEYeArEMugOpFcanA/Ef6A0CPwC4uNoag5SnAjJjGI2tKhkg4rLAfFGIH4IxEuBWIjSKKYkDfZCHddLiwChVhokK8YGohwEZYy3aBmEKmDEhOCgreomo+VmZHxsMEQxIc2MAx3FO/DI3RxMmQTZkI9ALDCaSUYdOOrAIeRAPzQ+PxCHUM2FFDb5paGNBPRa5C20bUhxc4sSB4JaLnvxVHWHsbVu6OnACjyOg+HqgXKgGRD/JMKBoD6LDb0dyAPE94hwHAw/hGYcujlwEQmOg+EV9HJgLBmOg+FMWjsQVKR8psCBoDSrQqoDSSmoG6Hpj1wA6ju30LI9+BBX4UsC+Ai0T4BWVd1EIL5PgeO+APECmoXgaGtm1IE0AgABBgAJAICuV8dAUAAAAABJRU5ErkJggg==</file_content>
</magento_api>
```

</div>





## [URI](#RESTAPI-Resource-ProductImages-URI--products--productid-images-store--storeid): /products/:product_id/images/store/:store_id

Allows you to manage product images for a specified store.

**URL Structure**:
http://magentohost/api/rest/products/:product\_id/images/store/:store\_id
**Version**: 1

## [HTTP Method](#RESTAPI-Resource-ProductImages-URI--products--productid-images-store--storeid-GET): GET

**Description**: Allows you to retrieve information about product images
for a specified store view.
**Notes:** Images can have different labels for different stores. For
example, image label \"flower\" in the English store view can be set as
\"fleur\" in the French store view. If there are custom attributes with
the **Catalog Input Type for Store Owner** option set to **Media
Image**, these attributes will be also returned in the response as an
image type.

**Authentication**: Admin, Customer, Guest
**Default Format**: XML
**Parameters**:
_No Parameters_

**Example:**

---

GET http://magentohost/api/rest/products/8/images/store/2

---



**Response Body:**

<div>

```
<?xml version="1.0"?>
<magento_api>
  <data_item>
    <id>6</id>
    <label>white dress</label>
    <position>1</position>
    <exclude>0</exclude>
    <url>http://magento/media/catalog/product/1/2/12.jpg</url>
    <types>
      <data_item>image</data_item>
      <data_item>small_image</data_item>
      <data_item>thumbnail</data_item>
    </types>
  </data_item>
</magento_api>
```

</div>





## [HTTP Method](#RESTAPI-Resource-ProductImages-URI--products--productid-images-store--storeid-POST): POST

**Description**: Allows you to add an image for the required product
with image settings for a specific store.
**Notes**: The image is added on the Global level; specified image
parameters are set for a specific store.

**Authentication**: Admin
**Default Format**: XML
**Parameters**:

Name Description Required Type Example Value

---

file_mime_type File mime type. Can have the following values: image/jpeg, image/png, etc. required string image/png
file_content Graphic image file content (base_64 encoded) required string iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAWtJREFUeNpi/P//P8NgBkwMgxyMOnDUgTDAyMhIDNYF4vNA/B+IDwCxHLoakgEoFxODiQRXQUYi4e3k2gfDjMRajsP3zED8F8pmA+JvUDEYeArEMugOpFcanA/Ef6A0CPwC4uNoag5SnAjJjGI2tKhkg4rLAfFGIH4IxEuBWIjSKKYkDfZCHddLiwChVhokK8YGohwEZYy3aBmEKmDEhOCgreomo+VmZHxsMEQxIc2MAx3FO/DI3RxMmQTZkI9ALDCaSUYdOOrAIeRAPzQ+PxCHUM2FFDb5paGNBPRa5C20bUhxc4sSB4JaLnvxVHWHsbVu6OnACjyOg+HqgXKgGRD/JMKBoD6LDb0dyAPE94hwHAw/hGYcujlwEQmOg+EV9HJgLBmOg+FMWjsQVKR8psCBoDSrQqoDSSmoG6Hpj1wA6ju30LI9+BBX4UsC+Ai0T4BWVd1EIL5PgeO+APECmoXgaGtm1IE0AgABBgAJAICuV8dAUAAAAABJRU5ErkJggg==
file_name Name for the added image file optional string test image

**Note**: If the file_name parameter is not defined, the original file
name is set for the image. The first created image will be called
\"image\", the second created image will be called \"image_2\", etc.

**Example:**

---

POST http://magentohost/api/rest/products/8/images/store/3

---



**Request Body:**

<div>

```
<?xml version="1.0"?>
<magento_api>
  <file_mime_type>image/jpeg</file_mime_type>    <file_content>iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAWtJREFUeNpi/P//P8NgBkwMgxyMOnDUgTDAyMhIDNYF4vNA/B+IDwCxHLoakgEoFxODiQRXQUYi4e3k2gfDjMRajsP3zED8F8pmA+JvUDEYeArEMugOpFcanA/Ef6A0CPwC4uNoag5SnAjJjGI2tKhkg4rLAfFGIH4IxEuBWIjSKKYkDfZCHddLiwChVhokK8YGohwEZYy3aBmEKmDEhOCgreomo+VmZHxsMEQxIc2MAx3FO/DI3RxMmQTZkI9ALDCaSUYdOOrAIeRAPzQ+PxCHUM2FFDb5paGNBPRa5C20bUhxc4sSB4JaLnvxVHWHsbVu6OnACjyOg+HqgXKgGRD/JMKBoD6LDb0dyAPE94hwHAw/hGYcujlwEQmOg+EV9HJgLBmOg+FMWjsQVKR8psCBoDSrQqoDSSmoG6Hpj1wA6ju30LI9+BBX4UsC+Ai0T4BWVd1EIL5PgeO+APECmoXgaGtm1IE0AgABBgAJAICuV8dAUAAAAABJRU5ErkJggg==</file_content>
</magento_api>
```

</div>





## [URI](#RESTAPI-Resource-ProductImages-URI--products--productid-images--imageid): /products/:product_id/images/:image_id

Allows you to manage a specified product image.

**URL Structure**:
http://magentohost/api/rest/products/:product\_id/images/:image\_id
**Version**: 1

## [HTTP Method](#RESTAPI-Resource-ProductImages-URI--products--productid-images--imageid-GET): GET

**Description**: Allows you to retrieve information about a specified
product image.
**Notes**: If there are custom attributes with the **Catalog Input Type
for Store Owner** option set to **Media Image**, these attributes will
be also returned in the response as an image type.

**Authentication**: Admin, Customer, Guest
**Default Format**: XML
**Parameters**:
_No Parameters_

**Example:**

---

GET http://magentohost/api/rest/products/8/images/7

---



**Response Body:**

<div>

```
<?xml version="1.0"?>
<magento_api>
  <id>7</id>
  <label>second dress</label>
  <position>2</position>
  <exclude>1</exclude>
  <url>http://magento/media/catalog/product/1/2/12.jpg</url>
  <types>
    <data_item>small_image</data_item>
  </types>
</magento_api>
```

</div>





## [HTTP Method](#RESTAPI-Resource-ProductImages-URI--products--productid-images--imageid-PUT): PUT

**Description**: Allows you to update information for the specified
product image.
**Notes**: When updating information, you need to pass only those
parameters that you want to be updated. Parameters that were not passed
in the request, will preserve the previous values.

**Authentication**: Admin
**Default Format**: XML
**Parameters**:

Name Description Required Type Example

---

exclude Defines whether the image will associate only to one of the three image types. optional int 0
file_content Image file content (base_64 encoded). optional string base_64 encoded file content
file_mime_type File mime type. Can have the following values: image/jpeg, image/png, etc. optional string image/png
file_name Image file name. optional  string test name
label A label that will be displayed on the frontend when pointing to the image optional string test label
position The Sort Order option. The order in which the images are displayed in the MORE VIEWS section. optional int 1
types Array of image types. Can have the following values: image, small_image, and thumbnail. optional array thumbnail

**Example:**

---

PUT http://magentohost/api/rest/products/8/images/7

---



**Request Body:**

<div>

```
<?xml version="1.0"?>
<magento_api>
  <label>English store image</label>
  <position>3</position>
  <exclude>0</exclude>
  <types>
      <data_item>image</data_item>
      <data_item>small_image</data_item>
      <data_item>thumbnail</data_item>
   </types>
</magento_api>
```

</div>





## [HTTP Method](#RESTAPI-Resource-ProductImages-URI--products--productid-images--imageid-DELETE): DELETE

**Description**: Allows you to remove the specified image from a
product.
**Notes**: The image will not be deleted physically, the image
parameters will be set to No Image.

**Authentication**: Admin
**Default Format**: XML
**Parameters**:
_No Parameters_

**Example:**

---

DELETE http://magentohost/api/rest/products/14/images/6

---



## [URI](#RESTAPI-Resource-ProductImages-URI--products--productid-images--imageid-store--storeid): /products/:product_id/images/:image_id/store/:store_id

Allows you to manage a specified product image for a specified store.

**URL Structure**:
http://magentohost/api/rest/products/:product\_id/images/:image\_id/store/:store\_id
**Version**: 1

## [HTTP Method](#RESTAPI-Resource-ProductImages-URI--products--productid-images--imageid-store--storeid-GET): GET

**Description**: Allows you to retrieve information about the specified
product image from a specified store.
**Notes**: If there are custom attributes with the **Catalog Input Type
for Store Owner** option set to **Media Image**, these attributes will
be also returned in the response as an image type.

**Authentication**: Admin, Customer, Guest
**Default Format**: XML
**Parameters**:
_No Parameters_

**Example:**

---

GET http://magentohost/api/rest/products/8/images/7/store/3

---



**Response Body:**

<div>

```
<?xml version="1.0"?>
<magento_api>
  <id>10</id>
  <label></label>
  <position>5</position>
  <exclude>1</exclude>
  <url>http://magento/media/catalog/product/n/e/new_image.gif</url>
  <types/>
</magento_api>
```

</div>





## [HTTP Method](#RESTAPI-Resource-ProductImages-URI--products--productid-images--imageid-store--storeid-PUT): PUT

**Description**: Allows you to update the specified product image
information for s specified store.
**Notes**: When updating information, you need to pass only those
parameters that you want to be updated. Parameters that were not passed
in the request will preserve the previous values.

**Authentication**: Admin
**Default Format**: XML
**Parameters**:

Name Description Required Type Example

---

exclude Defines whether the image will associate only to one of the three image types. optional int 0
file_content Image file content (base_64 encoded). optional string base_64 encoded file content
file_mime_type File mime type. Can have the following values: image/jpeg, image/png, etc. optional string image/png
file_name Image file name. optional string test name
label A label that will be displayed on the frontend when pointing to the image optional string test label
position The Sort Order option. The order in which the images are displayed in the MORE VIEWS section. optional int 1
types Array of image types. Can have the following values: image, small_image, and thumbnail. optional array thumbnail

**Example:**

---

PUT http://magentohost/api/rest/products/8/images/7/store/3

---



**Request Body:**

<div>

```
<?xml version="1.0"?>
<magento_api>
  <position>3</position>
  <exclude>0</exclude>
  <types>
      <data_item>image</data_item>
   </types>
</magento_api>
```

</div>





## [HTTP Method](#RESTAPI-Resource-ProductImages-URI--products--productid-images--imageid-store--storeid-DELETE): DELETE

**Description**: Allows you to remove an image from the required product
in the specified store.
**Notes**: The image will not be deleted physically, the image
parameters will be set to No Image for the current store.

**Authentication**: Admin
**Default Format**: XML
**Parameters**:
_No Parameters_

**Example:**

---

DELETE http://magentohost/api/rest/products/14/images/6/store/3

---



## [Possible HTTP Status Codes](#RESTAPI-Resource-ProductImages-PossibleHTTPStatusCodes)

---

Status Code Message Description

---

400 Requested store is The specified store is
invalid\ not valid or does not
exist

404 Resource not found The specified resource
is not found or does
not exist

404 Product image not found The specified product
image is not found or
does not exist

400 Resource data Resource validation
pre-validator error error

400 The image content must The image file content
be valid base64 encoded must be in the base_64
data\ encoded format (when
image content does not
contain supported
letters and symbols)

400 Requested product does The specified product
not support images\ does not support images
adding

400 Unsupported image MIME The image MIME type is
type\ not supported (e.g.,
image/bmp)

400 Resource unknown error\ Resource unknown error

500 Resource internal Resource internal error
error

\- The image is not The image is not
specified\ specified during the
POST request.

---
