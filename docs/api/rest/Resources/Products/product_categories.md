

-   [REST API: Product
    Categories](#RESTAPI-Resource-ProductCategories-RESTAPI-ProductCategories)
    -   [URI:
        /products/productid/categories](#RESTAPI-Resource-ProductCategories-URI--products-productid-categories)
        -   [HTTP Method:
            GET](#RESTAPI-Resource-ProductCategories-HTTPMethod-GET)
        -   [HTTP Method:
            POST](#RESTAPI-Resource-ProductCategories-HTTPMethod-POST)
    -   [URI:
        /products/productid/categories/categoryid](#RESTAPI-Resource-ProductCategories-URI--products-productid-categories-categoryid)
        -   [HTTP Method:
            DELETE](#RESTAPI-Resource-ProductCategories-HTTPMethod-DELETE)
    -   [Possible HTTP Status
        Codes](#RESTAPI-Resource-ProductCategories-PossibleHTTPStatusCodes)

### [REST API](#RESTAPI-Resource-ProductCategories-RESTAPI-ProductCategories): Product Categories

## [URI](#RESTAPI-Resource-ProductCategories-URI--products-productid-categories): /products/productid/categories

Allows you to retrieve information about assigned categories, assign,
and unassign a category from/to a product.

**URL Structure**:
http://magentohost/api/rest/products/productid/categories
**Version**: 1

## [HTTP Method](#RESTAPI-Resource-ProductCategories-HTTPMethod-GET): GET

**Description**: Allows you to retrieve information about categories
assigned to the specified product.

**Authentication**: Admin, Customer
**Default Format**: JSON
**Parameters**:
_No Parameters_

**Example:**

---

GET http://magentohost/api/rest/products/8/categories

---



**Response Body:**

<div>

```
{
     category_id: 8
}
```

</div>





## [HTTP Method](#RESTAPI-Resource-ProductCategories-HTTPMethod-POST): POST

**Description**: Allows you to assign a category to a specified product.

**Authentication**: Admin
**Default Format**: JSON
**Parameters**:

Name Description Required Type Example Value

---

category_id The category ID required int 2

**Example:**

---

POST http://magentohost/api/rest/products/8/categories

---



**Request Body:**

<div>

```
{
"category_id":"2"
}
```

</div>





As a result, the category with ID equal to 2 will be assigned to the
specified product.

## [URI](#RESTAPI-Resource-ProductCategories-URI--products-productid-categories-categoryid): /products/productid/categories/categoryid

## [HTTP Method](#RESTAPI-Resource-ProductCategories-HTTPMethod-DELETE): DELETE

**Description**: Allows you to unassign a category from a specified
product.

**Authentication**: Admin
**Default Format**: JSON
**Parameters**:
_No Parameters_

**Example:**

---

DELETE http://magentohost/api/rest/products/8/categories/2

---



### [Possible HTTP Status Codes](#RESTAPI-Resource-ProductCategories-PossibleHTTPStatusCodes)

Status Code Message Description

---

400 Product \<product ID\> is already assigned to category \<category ID\> The message is returned when the required category is already assigned to the product
400 Category not found The specified category is not found
405 Resource method not implemented yet The specified method is not implemented yet
