

-   [REST API: Products](#RESTAPI-Resource-Products-RESTAPI-Products)
    -   [URI: /products](#RESTAPI-Resource-Products-URI--products)
        -   [HTTP Method: GET
            /products](#RESTAPI-Resource-Products-HTTPMethod-GET-products)
        -   [HTTP Method: GET
            /products/category_id=:id](#RESTAPI-Resource-Products-HTTPMethod-GET-products-categoryid--id)
        -   [HTTP Method: POST
            /products](#RESTAPI-Resource-Products-HTTPMethod-POST-products)
    -   [URI:
        /products/:id](#RESTAPI-Resource-Products-URI--products--id)
        -   [HTTP Method: GET
            /products/:id](#RESTAPI-Resource-Products-HTTPMethod-GET-products--id)
        -   [HTTP Method: PUT
            /products/:id](#RESTAPI-Resource-Products-HTTPMethod-PUT-products--id)
        -   [HTTP Method: DELETE
            /products/:id](#RESTAPI-Resource-Products-HTTPMethod-DELETE-products--id)
    -   [Possible HTTP Status
        Codes](#RESTAPI-Resource-Products-PossibleHTTPStatusCodes)

### [REST API](#RESTAPI-Resource-Products-RESTAPI-Products): Products

## [URI](#RESTAPI-Resource-Products-URI--products): /products

Allows you to retrieve the list of all products with detailed
information.

**URL Structure**: http://magentohost/api/rest/products
**Version**: 1

## [HTTP Method](#RESTAPI-Resource-Products-HTTPMethod-GET-products): GET /products

**Description**: Allows you to retrieve the list of all products with
detailed information.
**Notes**: The list of attributes that will be returned in the response
is configured in the Magento Admin Panel. The list of attributes differs
for each type of user.

**Authentication**: Admin
**Default Format**: XML
**Parameters**:
_No Parameters_

---

GET http://magentohost/api/rest/products

---



**Response Example: XML**

<div>

```
<?xml version="1.0"?>
<magento_api>
  <data_item>
    <entity_id>1</entity_id>
    <type_id>simple</type_id>
    <sku>dress_test</sku>
    <status>1</status>
    <visibility>4</visibility>
    <tax_class_id>2</tax_class_id>
    <weight>1.0000</weight>
    <price>1500.0000</price>
    <special_price>1000.0000</special_price>
    <name>Wedding dress</name>
    <url_key>dress</url_key>
    <country_of_manufacture>AO</country_of_manufacture>
    <msrp_display_actual_price_type>2</msrp_display_actual_price_type>
    <gift_message_available>1</gift_message_available>
    <news_from_date>2012-03-21 00:00:00</news_from_date>
    <news_to_date>2012-03-24 00:00:00</news_to_date>
    <special_from_date>2012-03-21 00:00:00</special_from_date>
    <special_to_date>2012-03-24 00:00:00</special_to_date>
    <description>White wedding dress</description>
  </data_item>
  <data_item>
    <entity_id>2</entity_id>
    <type_id>simple</type_id>
    <sku>black_sunglasses</sku>
    <status>1</status>
    <visibility>4</visibility>
    <tax_class_id>2</tax_class_id>
    <weight>0.2000</weight>
    <price>500.0000</price>
    <special_price>300.0000</special_price>
    <name>Sunglasses</name>
    <url_key>sunglasses</url_key>
    <country_of_manufacture>AR</country_of_manufacture>
    <msrp_display_actual_price_type>2</msrp_display_actual_price_type>
    <gift_message_available></gift_message_available>
    <news_from_date></news_from_date>
    <news_to_date></news_to_date>
    <special_from_date>2012-03-21 00:00:00</special_from_date>
    <special_to_date>2012-03-24 00:00:00</special_to_date>
    <description>Black sunglasses</description>
  </data_item>
</magento_api>
```

</div>





**Response Example: JSON**

<div>

```
{"1":{"entity_id":"1","type_id":"simple","sku":"dress_test","status":"1","visibility":"4","tax_class_id":"2","weight":"1.0000","price":"1500.0000","special_price":"1000.0000","name":"Wedding dress","url_key":"dress","country_of_manufacture":"AO","msrp_display_actual_price_type":"2","gift_message_available":"1","news_from_date":"2012-03-21 00:00:00","news_to_date":"2012-03-24 00:00:00","special_from_date":"2012-03-21 00:00:00","special_to_date":"2012-03-24 00:00:00","description":"White wedding dress"},"2":{"entity_id":"2","type_id":"simple","sku":"black_sunglasses","status":"1","visibility":"4","tax_class_id":"2","weight":"0.2000","price":"500.0000","special_price":"300.0000","name":"Sunglasses","url_key":"sunglasses","country_of_manufacture":"AR","msrp_display_actual_price_type":"2","gift_message_available":null,"news_from_date":null,"news_to_date":null,"special_from_date":"2012-03-21 00:00:00","special_to_date":"2012-03-24 00:00:00","description":"Black sunglasses"\}\}
```

</div>





**Authentication**: Customer, Guest
**Default Format**: XML
**Parameters**:
_No Parameters_

---

GET http://magentohost/api/rest/products

---



**Response Example: XML**

<div>

```
<?xml version="1.0"?>
<magento_api>
  <data_item>
    <entity_id>1</entity_id>
    <type_id>simple</type_id>
    <sku>dress_test</sku>
    <description>White wedding dress</description>
    <short_description>dress</short_description>
    <meta_keyword>white, dress</meta_keyword>
    <name>Wedding dress</name>
    <meta_title>dress</meta_title>
    <meta_description>A nice wedding dress</meta_description>
    <regular_price_with_tax>1500</regular_price_with_tax>
    <regular_price_without_tax>1500</regular_price_without_tax>
    <final_price_with_tax>1000</final_price_with_tax>
    <final_price_without_tax>1000</final_price_without_tax>
    <is_saleable>1</is_saleable>
    <image_url>http://magentohost/media/catalog/product/cache/0/image/9df78eab33525d08d6e5fb8d27136e95/1/2/wedding_dress.jpg</image_url>
  </data_item>
  <data_item>
    <entity_id>2</entity_id>
    <type_id>simple</type_id>
    <sku>black_sunglasses</sku>
    <description>Black sunglasses</description>
    <short_description>Black sunglasses</short_description>
    <meta_keyword>sunglasses, black</meta_keyword>
    <name>Sunglasses</name>
    <meta_title>sunglasses</meta_title>
    <meta_description>black sunglasses</meta_description>
    <regular_price_with_tax>500</regular_price_with_tax>
    <regular_price_without_tax>500</regular_price_without_tax>
    <final_price_with_tax>300</final_price_with_tax>
    <final_price_without_tax>300</final_price_without_tax>
    <is_saleable>1</is_saleable>
    <image_url>http://magentohost/media/catalog/product/cache/0/image/9df78eab33525d08d6e5fb8d27136e95/a/l/sunglasses-1.jpg</image_url>
  </data_item>
</magento_api>
```

</div>





**Response Example: JSON**

<div>

```
{"1":{"entity_id":"1","type_id":"simple","sku":"dress_test","description":"White wedding dress","short_description":"dress","meta_keyword":"white, dress","name":"Wedding dress","meta_title":"dress","meta_description":"A nice wedding dress","regular_price_with_tax":1500,"regular_price_without_tax":1500,"final_price_with_tax":1000,"final_price_without_tax":1000,"is_saleable":"1","image_url":"http:\/\/magentohost\/media\/catalog\/product\/cache\/0\/image\/9df78eab33525d08d6e5fb8d27136e95\/1\/2\/wedding_dress.jpg"},"2":{"entity_id":"2","type_id":"simple","sku":"black_sunglasses","description":"Black sunglasses","short_description":"Black sunglasses","meta_keyword":"sunglasses, black","name":"Sunglasses","meta_title":"sunglasses","meta_description":"black sunglasses","regular_price_with_tax":500,"regular_price_without_tax":500,"final_price_with_tax":300,"final_price_without_tax":300,"is_saleable":"1","image_url":"http:\/\/magentohost\/media\/catalog\/product\/cache\/0\/image\/9df78eab33525d08d6e5fb8d27136e95\/a\/l\/sunglasses-1.jpg"\}\}
```

</div>





## [HTTP Method](#RESTAPI-Resource-Products-HTTPMethod-GET-products-categoryid--id): GET /products/category_id=:id

**Description**: Allows you to retrieve the list of products of a
specified category. These products will be returned in the product
position ascending order.

In the following example, product with ID=4 has position equal to 7 and
the product with ID=3 has position equal to 1. The list of products,
therefore, is sorted by the product position in the category.

---

GET http://magentohost/api/rest/products?category\_id=5

---



**Response Example: XML**

<div>

```
<?xml version="1.0"?>
<magento_api>
  <data_item>
    <entity_id>4</entity_id>
    <attribute_set_id>4</attribute_set_id>
    <type_id>simple</type_id>
    <tax_class_id>2</tax_class_id>
    <weight>1.0000</weight>
    <price>329.9900</price>
  </data_item>
  <data_item>
    <entity_id>3</entity_id>
    <attribute_set_id>4</attribute_set_id>
    <type_id>simple</type_id>
    <tax_class_id>2</tax_class_id>
    <weight>1.0000</weight>
    <price>550.0000</price>
  </data_item>
</magento_api>
```

</div>





## [HTTP Method](#RESTAPI-Resource-Products-HTTPMethod-POST-products): POST /products

**Description**: Allows you to create a new simple product.

**Authentication**: Admin
**Default Format**: JSON
**Parameters**:

---

Name Description Required Type Example Value

---

type_id Product type. Can required string simple
have the  
 \"simple\" value.

attribute_set_id Attribute set for required int 4
the product.

sku Product SKU required string new_product

name Product name required\ string New product

meta_title Product meta optional string new product
title

meta_description\ Product meta optional string This is a new
description product

url_key\ A friendly URL optional string new-product
path for the  
 product

custom_design\ Custom design optional string enterprise/default
applied for the  
 product page

page_layout\ Page template optional string one_column
that can be  
 applied to the  
 product page

options_container\ Defines how the optional string container2
custom options  
 for the product  
 will be  
 displayed. Can  
 have the  
 following values:  
 Block after Info  
 Column or Product  
 Info Column

country_of_manufacture\ Product country optional string AD
of manufacture

msrp_enabled\ The Apply MAP optional int 1
option. Defines  
 whether the price  
 in the catalog in  
 the frontend is  
 substituted with  
 a Click for price  
 link

msrp_display_actual_price_type\ Defines how the optional int 2
price will be  
 displayed in the  
 frontend. Can  
 have the  
 following values:  
 In Cart, Before  
 Order  
 Confirmation, and  
 On Gesture

gift_message_available\ Defines whether optional int 1
the gift message  
 is available for  
 the product

price Product price required\ string 2000

special_price\ Product special optional string 150
price

weight Product weight required\ string 0.5

msrp\ The optional string 140
Manufacturer\'s  
 Suggested Retail  
 Price option. The  
 price that a  
 manufacturer  
 suggests to sell  
 the product at

status Product status. required\ int 1
Can have the  
 following values:  
 1- Enabled, 2 -  
 Disabled.

visibility Product required\ int 4
visibility. Can  
 have the  
 following values:  
 1 - Not Visible  
 Individually, 2 -  
 Catalog, 3 -  
 Search, 4 -  
 Catalog, Search.

enable_googlecheckout\ Defines whether optional int 1
the product can  
 be purchased with  
 the help of the  
 Google Checkout  
 payment service.  
 Can have the  
 following values:  
 Yes and No

tax_class_id Product tax required\ int 7
class. Can have  
 the following  
 values: 0 - None,  
 2 - taxable  
 Goods, 4 -  
 Shipping, etc.,  
 depending on  
 created tax  
 classes.

description Product required\ string This is a new
description. product.

short_description Product short required\ string A new product.
description.

meta_keyword\ Product meta optional string new
keywords

custom_layout_update\ An XML block to optional string XML body
alter the page  
 layout

special_from_date\ Date starting optional string 2012-03-15 00:00:00
from which the  
 special price  
 will be applied  
 to the product

special_to_date\ Date till which optional string 2012-03-15 00:00:00
the special price  
 will be applied  
 to the product

news_from_date\ Date starting optional string 2012-03-15 00:00:00
from which the  
 product is  
 promoted as a new  
 product

news_to_date\ Date till which optional string 2012-03-15 00:00:00
the product is  
 promoted as a new  
 product

custom_design_from\ Date starting optional string 2012-03-15 00:00:00
from which the  
 custom design  
 will be applied  
 to the product  
 page

custom_design_to\ Date till which optional string 2012-03-15 00:00:00
the custom design  
 will be applied  
 to the product  
 page

group_price\ Product group optional array array of group price
price

tier_price\ Product tier optional array array of tier price
price

stock_data\ Product inventory optional array array of stock data
data

---

Array of **Group Price** attributes is as follows:

---

Name Description Required Type Example Value

---

website_id Website ID optional int 0

cust_group Customer group optional int 1

price\ Group price optional string 140

---

Array of **Tier Price** attributes is as follows:

Name Description Required Type Example Value

---

website_id Website ID optional int 0
cust_group Customer group optional int 1
price Tier price optional string 140
price_qty Price quantity optional string 10

Array of **Stock Data** attributes is as follows:

---

Name Description Required Type Example Value

---

qty Quantity of optional string 99
stock items  
 for the  
 current  
 product

min_qty Quantity for optional string 5
stock items to  
 become out of  
 stock

use_config_min_qty Choose whether optional int 1
the Config  
 settings will  
 be applied for  
 the Qty for  
 Item\'s Status  
 to Become Out  
 of Stock  
 option

is_qty_decimal Choose whether optional int 1
the product  
 can be sold  
 using decimals  
 (e.g., you can  
 buy 2.5  
 product)

backorders\ Defines optional\ int 0
whether the  
 customer can  
 place the  
 order for  
 products that  
 are out of  
 stock at the  
 moment.

use_config_backorders\ Choose whether optional\ int 1
the Config  
 settings will  
 be applied for  
 the Backorders  
 option

min_sale_qty\ Minimum number optional\ string 10
of items in  
 the shopping  
 cart to be  
 sold

use_config_min_sale_qty\ Choose whether optional\ int 1
the Config  
 settings will  
 be applied for  
 the Minimum  
 Qty Allowed in  
 Shopping Cart  
 option

max_sale_qty\ Maximum number optional\ string 50
of items in  
 the shopping  
 cart to be  
 sold

use_config_max_sale_qty\ Choose whether optional\ int 1
the Config  
 settings will  
 be applied for  
 the Maximum  
 Qty Allowed in  
 Shopping Cart  
 option

is_in_stock\ Defines optional\ int 1
whether the  
 product is  
 available for  
 selling.

notify_stock_qty\ The number of optional\ string 5
inventory  
 items below  
 which the  
 customer will  
 be notified

use_config_notify_stock_qty\ Choose whether optional\ int 1
the Config  
 settings will  
 be applied for  
 the Notify for  
 Quantity Below  
 option

manage_stock\ Choose whether optional\ int 1
to view and  
 specify the  
 product  
 quantity and  
 availability  
 and whether  
 the product is  
 in stock  
 management.

use_config_manage_stock\ Choose whether optional\ int 1
the Config  
 settings will  
 be applied for  
 the Manage  
 Stock option

use_config_qty_increments\ Choose whether optional\ int 1
the Config  
 settings will  
 be applied for  
 the Qty  
 Increments  
 option

qty_increments\ The product optional\ string 3
quantity  
 increment  
 value

use_config_enable_qty_inc\ Choose whether optional\ int 1
the Config  
 settings will  
 be applied for  
 the Enable Qty  
 Increments  
 option

enable_qty_increments\ Defines optional\ int 0
whether the  
 customer can  
 add products  
 only in  
 increments to  
 the shopping  
 cart

is_decimal_divided\ Defines optional\ int 0
whether the  
 stock items  
 can be divided  
 into multiple  
 boxes for  
 shipping

---

---

POST http://magentohost/api/rest/products

---



**Request Example: JSON**

<div>

```
{
  "custom_design" : "default/blank",
  "custom_design_from" : "02/16/2012",
  "custom_design_to" : "05/01/2012",
  "description" : "Test description",
  "gift_message_available" : 1,
  "meta_description" : "Test meta",
  "meta_keyword" : "Test keyword",
  "meta_title" : "Test title",
  "msrp" : 11.015000000000001,
  "msrp_display_actual_price_type" : 1,
  "msrp_enabled" : 1,
  "name" : "Test",
  "news_from_date" : "02/16/2012",
  "news_to_date" : "16.02.2012",
  "options_container" : "container1",
  "page_layout" : "one_column",
  "price" : 25.5,
  "attribute_set_id" : "4",
  "short_description" : "Test short description",
  "sku" : "simple4f5490f31959f",
  "special_from_date" : "02/16/2012",
  "special_price" : 11.199999999999999,
  "special_to_date" : "03/17/2012",
  "status" : 1,
  "stock_data" : { "backorders" : 1,
      "enable_qty_increments" : 0,
      "is_in_stock" : 0,
      "is_qty_decimal" : 0,
      "manage_stock" : 1,
      "max_sale_qty" : 1,
      "min_qty" : 1.5600000000000001,
      "min_sale_qty" : 1,
      "notify_stock_qty" : -50.990000000000002,
      "qty" : 1,
      "use_config_manage_stock" : 1,
      "use_config_min_qty" : 1,
      "use_config_min_sale_qty": 1,
      "use_config_max_sale_qty" : 1,
      "use_config_backorders": 1,
      "use_config_enable_qty_inc":1,
      "use_config_notify_stock_qty":1 },
  "tax_class_id" : "2",
  "type_id" : "simple",
  "use_config_gift_message_available" : 0,
  "visibility" : "4",
  "weight" : 125
}
```

</div>





**Request Example: XML**

<div>

```
<?xml version="1.0"?>
<magento_api>
  <attribute_set_id>4</attribute_set_id>
  <type_id>simple</type_id>
  <sku>test_dress</sku>
  <name>Test_dress</name>
  <country_of_manufacture>AD</country_of_manufacture>
  <price>2000.0000</price>
  <special_price>1500.0000</special_price>
  <weight>0.5000</weight>
  <status>1</status>
  <visibility>4</visibility>
  <tax_class_id>7</tax_class_id>
  <description>dress</description>
  <short_description>Wedding dress</short_description>
</magento_api>
```

</div>





## [URI](#RESTAPI-Resource-Products-URI--products--id): /products/:id

Allows you to retrieve, update, and delete a specified product in
Magento.

**URL Structure**: http://magentohost/api/rest/products/:id
**Version**: 1

## [HTTP Method](#RESTAPI-Resource-Products-HTTPMethod-GET-products--id): GET /products/:id

**Description**: Allows you to retrieve information on a required simple
product.
**Notes**: The list of attributes that will be returned in the response
is configured in the Magento Admin Panel. The list of attributes differs
for each type of user.

**Authentication**: Admin
**Default Format**: XML
**Parameters**:
_No Parameters_

---

GET http://magentohost/api/rest/products/8

---



**Response Example: XML**

<div>

```
<?xml version="1.0"?>
<magento_api>
  <entity_id>8</entity_id>
  <attribute_set_id>4</attribute_set_id>
  <type_id>simple</type_id>
  <sku>dress</sku>
  <name>Dress</name>
  <url_key>my-new-dress</url_key>
  <country_of_manufacture>AD</country_of_manufacture>
  <msrp_display_actual_price_type>2</msrp_display_actual_price_type>
  <gift_message_available>1</gift_message_available>
  <price>2000.0000</price>
  <weight>0.5000</weight>
  <status>1</status>
  <visibility>4</visibility>
  <tax_class_id>7</tax_class_id>
  <description>dress</description>
  <group_price>
    <data_item>
      <website_id>0</website_id>
      <cust_group>1</cust_group>
      <price>1400.0000</price>
    </data_item>
  </group_price>
  <tier_price>
    <data_item>
      <website_id>0</website_id>
      <cust_group>1</cust_group>
      <price>1300.0000</price>
      <price_qty>1.0000</price_qty>
    </data_item>
  </tier_price>
  <stock_data>
    <qty>50.0000</qty>
    <min_qty>0.0000</min_qty>
    <use_config_min_qty>1</use_config_min_qty>
    <is_qty_decimal>0</is_qty_decimal>
    <backorders>0</backorders>
    <use_config_backorders>1</use_config_backorders>
    <min_sale_qty>1.0000</min_sale_qty>
    <use_config_min_sale_qty>1</use_config_min_sale_qty>
    <max_sale_qty>0.0000</max_sale_qty>
    <use_config_max_sale_qty>1</use_config_max_sale_qty>
    <is_in_stock>1</is_in_stock>
    <notify_stock_qty></notify_stock_qty>
    <use_config_notify_stock_qty>1</use_config_notify_stock_qty>
    <manage_stock>0</manage_stock>
    <use_config_manage_stock>1</use_config_manage_stock>
    <use_config_qty_increments>1</use_config_qty_increments>
    <qty_increments>0.0000</qty_increments>
    <use_config_enable_qty_inc>1</use_config_enable_qty_inc>
    <enable_qty_increments>0</enable_qty_increments>
    <is_decimal_divided>0</is_decimal_divided>
    <use_config_enable_qty_increments>1</use_config_enable_qty_increments>
  </stock_data>
</magento_api>
```

</div>





**Response Example: JSON**

<div>

```
{"entity_id":"8","attribute_set_id":"4","type_id":"simple","sku":"dress","name":"Dress","url_key":"my-new-dress","country_of_manufacture":"AD","msrp_display_actual_price_type":"2","gift_message_available":"1","price":"2000.0000","weight":"0.5000","status":"1","visibility":"4","tax_class_id":"7","description":"dress","group_price":[{"website_id":"0","cust_group":"1","price":"1400.0000"}],"tier_price":[{"website_id":"0","cust_group":"1","price":"1300.0000","price_qty":"1.0000"}],"stock_data":{"qty":"50.0000","min_qty":"0.0000","use_config_min_qty":"1","is_qty_decimal":"0","backorders":"0","use_config_backorders":"1","min_sale_qty":"1.0000","use_config_min_sale_qty":"1","max_sale_qty":"0.0000","use_config_max_sale_qty":"1","is_in_stock":"1","notify_stock_qty":null,"use_config_notify_stock_qty":"1","manage_stock":"0","use_config_manage_stock":"1","use_config_qty_increments":"1","qty_increments":"0.0000","use_config_enable_qty_inc":"1","enable_qty_increments":"0","is_decimal_divided":"0","use_config_enable_qty_increments":"1"\}\}
```

</div>





**Authentication**: Customer, Guest
**Default Format**: XML
**Parameters**:
_No Parameters_

---

GET http://magentohost/api/rest/products/8

---



**Response Example: XML**

<div>

```
<?xml version="1.0"?>
<magento_api>
  <entity_id>1</entity_id>
  <type_id>simple</type_id>
  <sku>dress_test</sku>
  <name>Wedding dress</name>
  <meta_title>dress</meta_title>
  <meta_description>A nice wedding dress</meta_description>
  <description>White wedding dress</description>
  <short_description>dress</short_description>
  <meta_keyword>white, dress</meta_keyword>
  <tier_price/>
  <is_in_stock>1</is_in_stock>
  <regular_price_with_tax>1500</regular_price_with_tax>
  <regular_price_without_tax>1500</regular_price_without_tax>
  <final_price_with_tax>1000</final_price_with_tax>
  <final_price_without_tax>1000</final_price_without_tax>
  <is_saleable>1</is_saleable>
  <image_url>http://magento/media/catalog/product/cache/0/image/9df78eab33525d08d6e5fb8d27136e95/1/2/wedding_dress.jpg</image_url>
  <url>http://magento/index.php/catalog/product/view/id/1/s/dress/</url>
  <buy_now_url>http://magento/index.php/checkout/cart/add/uenc/aHR0cDovLzEyNy4wLjAuMS9Tb3VyY2VzL2FwaS9yZXN0L3Byb2R1Y3RzLzE,/product/1/</buy_now_url>
  <total_reviews_count>0</total_reviews_count>
  <has_custom_options></has_custom_options>
</magento_api>
```

</div>





**Response Example: JSON**

<div>

```
{"entity_id":"1","type_id":"simple","sku":"dress_test","name":"Wedding dress","meta_title":"dress","meta_description":"A nice wedding dress","description":"White wedding dress","short_description":"dress","meta_keyword":"white, dress","tier_price":[],"is_in_stock":"1","regular_price_with_tax":1500,"regular_price_without_tax":1500,"final_price_with_tax":1000,"final_price_without_tax":1000,"is_saleable":"1","image_url":"http:\/\/magento\/media\/catalog\/product\/cache\/0\/image\/9df78eab33525d08d6e5fb8d27136e95\/1\/2\/wedding_dress.jpg","url":"http:\/\/magento\/index.php\/catalog\/product\/view\/id\/1\/s\/dress\/","buy_now_url":"http:\/\/magento\/index.php\/checkout\/cart\/add\/uenc\/aHR0cDovLzEyNy4wLjAuMS9Tb3VyY2VzL2FwaS9yZXN0L3Byb2R1Y3RzLzE,\/product\/1\/","total_reviews_count":"0","has_custom_options":false}
```

</div>





## [HTTP Method](#RESTAPI-Resource-Products-HTTPMethod-PUT-products--id): PUT /products/:id

**Description**: Allows you to update an existing product.

**Authentication**: Admin
**Default Format**: JSON
**Parameters**:
_Enter only those parameters which you want to update._

---

PUT http://magentohost/api/rest/products/8
PUT for specific store http://magentohost/api/rest/products/8/store/3

---



**Request Example: JSON**

<div>

```
{
"attribute_set_id":"4",
"type_id":"simple",
"sku":"wedding dress",
"name":"Dress_test",
"meta_title":"dress",
"meta_description":"a wedding dress",
"price":"2000.0000",
"weight":"0.5000",
"status":"1",
"visibility":"4",
"enable_googlecheckout":"1",
"tax_class_id":"7",
"description":"dress",
"meta_keyword":"dress, wedding"
}
```

</div>





**Request Example: XML**

<div>

```
<?xml version="1.0"?>
<magento_api>
  <attribute_set_id>4</attribute_set_id>
  <type_id>simple</type_id>
  <sku>wedding dress</sku>
  <name>Dress_test</name>
  <meta_title>dress</meta_title>
  <price>2000.0000</price>
  <weight>0.5000</weight>
  <status>1</status>
  <visibility>4</visibility>
  <enable_googlecheckout>1</enable_googlecheckout>
  <tax_class_id>7</tax_class_id>
  <description>dress</description>
  <meta_keyword>dress, wedding</meta_keyword>
</magento_api>
```

</div>





## [HTTP Method](#RESTAPI-Resource-Products-HTTPMethod-DELETE-products--id): DELETE /products/:id

**Description**: Allows you to delete an existing product.

**Authentication**: Admin
**Default Format**: JSON
**Parameters**: _no parameters_
**Request Example**:

---

DELETE http://magentohost/api/rest/products/1

---



### [Possible HTTP Status Codes](#RESTAPI-Resource-Products-PossibleHTTPStatusCodes)

Status Code Message Description

---

404 Resource not found. The required resource is not found.
405 Resource method not implemented yet. The required method is not implemented yet.
405 Resource does not support method. The current resource does not support the specified method.
