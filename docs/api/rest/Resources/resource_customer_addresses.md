 JSON responses
on this page contributed by Tim Reynolds

-   [REST API: Customer
    Addresses](#RESTAPI-Resource-CustomerAddresses-RESTAPI-CustomerAddresses)
    -   [URI:
        /customers/:customer_id/addresses](#RESTAPI-Resource-CustomerAddresses-URI--customers--customerid-addresses)
        -   [HTTP Method: GET
            /customers/:customer_id/addresses](#RESTAPI-Resource-CustomerAddresses-HTTPMethod-GET-customers--customerid-addresses)
        -   [HTTP Method: POST
            /customers/:customer_id/addresses](#RESTAPI-Resource-CustomerAddresses-HTTPMethod-POST-customers--customerid-addresses)
        -   [HTTP Method: PUT
            /customers/:customer_id/addresses](#RESTAPI-Resource-CustomerAddresses-HTTPMethod-PUT-customers--customerid-addresses)
        -   [HTTP Method: DELETE
            /customers/:customer_id/addresses](#RESTAPI-Resource-CustomerAddresses-HTTPMethod-DELETE-customers--customerid-addresses)
    -   [URI:
        /customers/addresses/:address_id](#RESTAPI-Resource-CustomerAddresses-URI--customers-addresses--addressid)
        -   [HTTP Method: GET
            /customers/addresses/:address_id](#RESTAPI-Resource-CustomerAddresses-HTTPMethod-GET-customers-addresses--addressid)
        -   [HTTP Method: POST
            /customers/addresses/:address_id](#RESTAPI-Resource-CustomerAddresses-HTTPMethod-POST-customers-addresses--addressid)
        -   [HTTP Method: PUT
            /customers/addresses/:address_id](#RESTAPI-Resource-CustomerAddresses-HTTPMethod-PUT-customers-addresses--addressid)
        -   [HTTP Method: DELETE
            /customers/addresses/:address_id](#RESTAPI-Resource-CustomerAddresses-HTTPMethod-DELETE-customers-addresses--addressid)
-   [Possible HTTP Status
    Codes](#RESTAPI-Resource-CustomerAddresses-PossibleHTTPStatusCodes)

### [REST API](#RESTAPI-Resource-CustomerAddresses-RESTAPI-CustomerAddresses): Customer Addresses

## [URI](#RESTAPI-Resource-CustomerAddresses-URI--customers--customerid-addresses): /customers/:customer_id/addresses

Allows you to manage existing customer addresses.

**URL Structure**:
http://magentohost/api/rest/customers/:customer\_id/addresses
**Version**: 1

## [HTTP Method](#RESTAPI-Resource-CustomerAddresses-HTTPMethod-GET-customers--customerid-addresses): GET /customers/:customer_id/addresses

**Description**: Allows you to retrieve the list of existing customer
addresses.
**Notes**: The list of attributes that will be returned for customer
addresses is configured in the Magento Admin Panel. The Customer user
type has access only to his/her own addresses. Also, Admin can add
additional non-system customer address attributes by selecting
**Customers** \> **Attributes** \> **Manage Customer Address
Attributes** (available only in Magento Enterprise Edition). If these
attributes are set as visible on frontend, they will be returned in the
response.

**Authentication**: Admin, Customer
**Default Format**: XML
**Parameters**:
_No Parameters_

**Example:**

---

GET http://magentohost/api/rest/customers/1/addresses

---



**Response Body:**

<div>

```
<?xml version="1.0"?>
<magento_api>
  <data_item>
    <entity_id>2</entity_id>
    <firstname>John</firstname>
    <lastname>Doe</lastname>
    <city>PA</city>
    <region>Palau</region>
    <postcode>19103</postcode>
    <country_id>US</country_id>
    <telephone>610-634-1181</telephone>
    <prefix>Dr.</prefix>
    <middlename></middlename>
    <suffix>Jr.</suffix>
    <company></company>
    <fax></fax>
    <vat_id>123456789</vat_id>
    <street>
      <data_item>2356 Jody Road Philadelphia</data_item>
      <data_item>844 Jefferson Street; 4510 Willis Avenue</data_item>
    </street>
    <is_default_billing>1</is_default_billing>
    <is_default_shipping>1</is_default_shipping>
  </data_item>
</magento_api>
```

</div>





**response body: json**:

<div>

```

[
  {
    "entity_id": "98",
    "firstname": "John",
    "middlename": null,
    "lastname": "Doe",
    "company": null,
    "city": "Philly",
    "country_id": "US",
    "region": "California",
    "postcode": "94131",
    "telephone": "888-555-1212",
    "fax": null,
    "street": [
      "123 Center St"
    ],
    "is_default_billing": 0,
    "is_default_shipping": 1
  },
  {
    "entity_id": "99",
    "firstname": "John",
    "lastname": "Doe",
    "city": "San Jose",
    "country_id": "US",
    "region": "California",
    "postcode": "94070",
    "telephone": "898-555-1212",
    "street": [
      "123 Easy St"
    ],
    "is_default_billing": 1,
    "is_default_shipping": 0
  }
]

```

</div>





**Notes**: If the customer has more than two street addresses, they will
be returned in the following form: first address in the first string and
all other addresses in the second string separated with a semicolon
(like in the example above).

## [HTTP Method](#RESTAPI-Resource-CustomerAddresses-HTTPMethod-POST-customers--customerid-addresses): POST /customers/:customer_id/addresses

**Description**: Allows you to create a new address for the required
customer.
**Notes**: The Customer user type can create addresses only for
themselves.

When adding a street address for the customer, it should look like the
following:

<div>

```
<street>
    <data_item>street address 1</data_item>
    <data_item>street address 2</data_item>
    <data_item>street address 3</data_item>
</street>
```

</div>





**Authentication**: Admin, Customer
**Default Format**: XML

**Parameters**:

---

Name Description Required Type Example Value

---

firstname Customer first required string John
name

lastname Customer last required\ string Doe
name

street\ Customer required\ string 2800 Watson
street Lane
 address. There  
 can be more  
 than one  
 street  
 address.

city\ Name of the required\ string Asheville
 city

country_id\ Name of the required\ string US
 country

region Region name or required for string Palau
code countries with  
 regions (USA,  
 Canada, etc.)

postcode\ Customer required\ string 28803
 ZIP/postal  
 code

telephone\ Customer phone required\ string 828-775-0519
 number

---

**Example:**

---

POST http://magentohost/api/rest/customers/1/addresses

---



**Request Body:**

<div>

```
<?xml version="1.0"?>
<magento_api>
    <firstname>Johny</firstname>
    <lastname>Doe</lastname>
    <city>PA</city>
    <region>Palau</region>
    <postcode>19103</postcode>
    <country_id>US</country_id>
    <telephone>611-634-1181</telephone>
    <street>
      <data_item>2354 Jody Road Philadelphia</data_item>
      <data_item>844 Jefferson Street; 4510 Willis Avenue</data_item>
    </street>
</magento_api>
```

</div>





## [HTTP Method](#RESTAPI-Resource-CustomerAddresses-HTTPMethod-PUT-customers--customerid-addresses): PUT /customers/:customer_id/addresses

**Update Customer Address**: not allowed.

## [HTTP Method](#RESTAPI-Resource-CustomerAddresses-HTTPMethod-DELETE-customers--customerid-addresses): DELETE /customers/:customer_id/addresses

**Description**: Not allowed.

## [URI](#RESTAPI-Resource-CustomerAddresses-URI--customers-addresses--addressid): /customers/addresses/:address_id

Allows you to manage an existing customer address.

**URL Structure**:
http://magentohost/api/rest/customers/addresses/:address\_id
**Version**: 1

## [HTTP Method](#RESTAPI-Resource-CustomerAddresses-HTTPMethod-GET-customers-addresses--addressid): GET /customers/addresses/:address_id

**Description**: Allows you to retrieve an existing customer address.
**Notes**: The list of attributes that will be returned for customer
address is configured in the Magento Admin Panel. The Customer user type
has access only to his/her own addresses. Also, Admin can add additional
non-system customer address attributes by selecting **Customers** \>
**Attributes** \> **Manage Customer Address Attributes** (available only
in Magento Enterprise Edition). If these attributes are set as visible
on frontend, they will be returned in the response.

**Authentication**: Admin, Customer
**Default Format**: XML
**Parameters**:
_No Parameters_

**Example:**

---

GET http://magentohost/api/rest/customers/addresses/2

---



**Response Body:**

<div>

```
<?xml version="1.0"?>
<magento_api>
  <entity_id>2</entity_id>
  <firstname>John</firstname>
  <lastname>Doe</lastname>
  <city>PA</city>
  <region>Palau</region>
  <postcode>19103</postcode>
  <country_id>US</country_id>
  <telephone>610-634-1181</telephone>
  <prefix>Dr.</prefix>
  <middlename></middlename>
  <suffix>Jr.</suffix>
  <company></company>
  <fax></fax>
  <vat_id>123456789</vat_id>
  <street>
    <data_item>2356 Jody Road Philadelphia</data_item>
    <data_item>844 Jefferson Street; 4510 Willis Avenue</data_item>
  </street>
  <is_default_billing>1</is_default_billing>
  <is_default_shipping>1</is_default_shipping>
</magento_api>
```

</div>





**response body: json**:

<div>

```

{
  "entity_id": "2",
  "firstname": "John",
  "lastname": "Doe",
  "city": "San Jose",
  "country_id": "US",
  "region": "California",
  "postcode": "94070",
  "telephone": "898-555-1212",
  "street": [
    "123 Easy St"
  ],
  "is_default_billing": 1,
  "is_default_shipping": 0
}
```

</div>





## [HTTP Method](#RESTAPI-Resource-CustomerAddresses-HTTPMethod-POST-customers-addresses--addressid): POST /customers/addresses/:address_id

**Description**: Not allowed.

## [HTTP Method](#RESTAPI-Resource-CustomerAddresses-HTTPMethod-PUT-customers-addresses--addressid): PUT /customers/addresses/:address_id

**Description**: Allows you to update an existing customer address.
**Notes**: The list of attributes that will be updated for customer
address is configured in the Magento Admin Panel. The Customer user type
has access only to his/her own addresses.

If you want to add more addresses, it should look like the following:

<div>

```
<street>
    <data_item>street address 1</data_item>
    <data_item>street address 2</data_item>
    <data_item>street address 3</data_item>
</street>
```

</div>





**Authentication**: Admin, Customer
**Default Format**: XML

**Example:**

---

PUT http://magentohost/api/rest/customers/addresses/7

---



**Request Body:**

<div>

```
<?xml version="1.0"?>
<magento_api>
  <firstname>Johny</firstname>
  <lastname>Doe</lastname>
  <city>PA</city>
  <region>Palau</region>
  <postcode>19103</postcode>
  <country_id>US</country_id>
  <telephone>610-634-1181</telephone>
  <street>
    <data_item>2356 Jody Road Philadelphia</data_item>
    <data_item>844 Jefferson Street</data_item>
  </street>
  <is_default_billing>1</is_default_billing>
  <is_default_shipping>1</is_default_shipping>
</magento_api>
```

</div>





**request body: json**:

<div>

```

{
  "entity_id": "99",
  "firstname": "John",
  "lastname": "Doe",
  "city": "San Jose",
  "country_id": "US",
  "region": "California",
  "postcode": "94070",
  "telephone": "898-555-1212",
  "street": [
    "123 Easy St"
  ],
  "is_default_billing": 1,
  "is_default_shipping": 0
}
```

</div>





## [HTTP Method](#RESTAPI-Resource-CustomerAddresses-HTTPMethod-DELETE-customers-addresses--addressid): DELETE /customers/addresses/:address_id

**Description**: Allows you to delete an existing customer address.
**Notes**: The Customer user type can delete only his/her own addresses.

**Authentication**: Admin, Customer
**Default Format**: XML
**Parameters**:
_No Parameters_

**Example:**

---

DELETE http://magentohost/api/rest/customers/addresses/7

---



## [Possible HTTP Status Codes](#RESTAPI-Resource-CustomerAddresses-PossibleHTTPStatusCodes)

---

Status Code Message Description

---

405 Resource method not The required method is
implemented yet. not implemented or not
allowed.

404 Resource not found.\ The specified resource
is not found or does
not exist.

400 Invalid country The \<country_id\>
identifier type value is passed not as
a string type.

400 \<value name\> is a The specified value is
required value.\ a required one.

400 Country does not exist\ The specified country
does not exist.

400 Country is required The \<country_id\>
value is required.

400 Country is not between The country code is not
\'2\' and \'3\' in the range of 2 and 3
inclusively.\ symbols inclusively.

400 Invalid State/Province The \<region\> value is
type\ invalid (value is empty
or passed as an array)

400 State/Province is The \<region\> value is
required\ required for the
specified country.

400 State/Province is The entered value for
invalid\ \<region\> is invalid.
It must be the region
code (TX) or region
name (Texas).

400 State/Province does not The specified region
exist\ does not exist (only
for the country with
predefined regions).

400 Address is default for The address cannot be
customer so is not deleted because it is
allowed to be deleted\ set as a default one
for billing or
shipping.

---
