

-   [REST API: Order
    Comments](#RESTAPI-Resource-OrderComments-RESTAPI-OrderComments)
    -   [URI:
        /orders/:orderid/comments](#RESTAPI-Resource-OrderComments-URI--orders--orderid-comments)
        -   [HTTP Method:
            GET](#RESTAPI-Resource-OrderComments-HTTPMethod-GET)
        -   [HTTP Method:
            POST](#RESTAPI-Resource-OrderComments-HTTPMethod-POST)
        -   [HTTP Method:
            PUT](#RESTAPI-Resource-OrderComments-HTTPMethod-PUT)
        -   [HTTP Method:
            DELETE](#RESTAPI-Resource-OrderComments-HTTPMethod-DELETE)
-   [Order Comments
    Attributes](#RESTAPI-Resource-OrderComments-OrderCommentsAttributes)

### [REST API](#RESTAPI-Resource-OrderComments-RESTAPI-OrderComments): Order Comments

## [URI](#RESTAPI-Resource-OrderComments-URI--orders--orderid-comments): /orders/:orderid/comments

Allows you to retrieve information about comments of the required order.

**URL Structure**: http://magentohost/api/rest/orders/:orderid/comments
**Version**: 1

## [HTTP Method](#RESTAPI-Resource-OrderComments-HTTPMethod-GET): GET

**Description**: Allows you to retrieve information about comments of
the required order.

**Authentication**: Admin
**Default Format**: XML
**Parameters**:
_No Parameters_

**Example:**

---

GET http://magentohost/api/rest/orders/33/comments

---



**Response Body:**

<div>

```
<?xml version="1.0"?>
<magento_api>
  <data_item>
    <created_at>2012-03-09 11:20:49</created_at>
    <comment></comment>
    <is_customer_notified>1</is_customer_notified>
    <is_visible_on_front>0</is_visible_on_front>
    <status>pending</status>
  </data_item>
  <data_item>
    <created_at>2012-03-09 11:21:32</created_at>
    <comment>This is a new order for John Doe.</comment>
    <is_customer_notified>1</is_customer_notified>
    <is_visible_on_front>1</is_visible_on_front>
    <status>pending</status>
  </data_item>
</magento_api>
```

</div>





**Authentication**: Customer
**Default Format**: XML
**Parameters**:
_No Parameters_

**Example:**

---

GET http://magentohost/api/rest/orders/33/comments

---



**Response Body:**

<div>

```
<?xml version="1.0"?>
<magento_api>
  <data_item>
    <created_at>2012-03-09 11:21:32</created_at>
    <comment>This is a new order for John Doe.</comment>
  </data_item>
</magento_api>
```

</div>





## [HTTP Method](#RESTAPI-Resource-OrderComments-HTTPMethod-POST): POST

**Description**: Not allowed.

## [HTTP Method](#RESTAPI-Resource-OrderComments-HTTPMethod-PUT): PUT

**Description**: Not allowed.

## [HTTP Method](#RESTAPI-Resource-OrderComments-HTTPMethod-DELETE): DELETE

**Description**: Not allowed.

## [Order Comments Attributes](#RESTAPI-Resource-OrderComments-OrderCommentsAttributes)

---

Attribute Name Attribute Description Notes

---

Comment Date Date when the comment Admin and Customer
was added

Comment Text Comment text Admin and Customer

Is Customer Notified Defines whether the Admin only
customer is notified  
 about the comment. Can  
 have the following  
 values: 0 - Customer is
not notified, 1 -  
 Customer is notified.

Is Comment Visible on Defines whether the Admin only
 Frontend comment is visible on  
 the frontend. Can have  
 the following values: 0 - Comment is not  
 visible, 1 - Comment is
visible.

Comment Status Comment status. Admin only

---
