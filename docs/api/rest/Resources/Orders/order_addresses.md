

-   [REST API: Order
    Addresses](#RESTAPI-Resource-OrderAddresses-RESTAPI-OrderAddresses)
    -   [URI:
        /orders/:orderid/addresses](#RESTAPI-Resource-OrderAddresses-URI--orders--orderid-addresses)
        -   [HTTP Method:
            GET](#RESTAPI-Resource-OrderAddresses-URI--orders--orderid-addresses-GET)
        -   [HTTP Method:
            POST](#RESTAPI-Resource-OrderAddresses-URI--orders--orderid-addresses-POST)
        -   [HTTP Method:
            PUT](#RESTAPI-Resource-OrderAddresses-URI--orders--orderid-addresses-PUT)
        -   [HTTP Method:
            DELETE](#RESTAPI-Resource-OrderAddresses-URI--orders--orderid-addresses-DELETE)
    -   [URI:
        /orders/:orderid/addresses/billing](#RESTAPI-Resource-OrderAddresses-URI--orders--orderid-addresses-billing)
        -   [HTTP Method:
            GET](#RESTAPI-Resource-OrderAddresses-URI--orders--orderid-addresses-billing-GET)
        -   [HTTP Method:
            POST](#RESTAPI-Resource-OrderAddresses-URI--orders--orderid-addresses-billing-POST)
        -   [HTTP Method:
            PUT](#RESTAPI-Resource-OrderAddresses-URI--orders--orderid-addresses-billing-PUT)
        -   [HTTP Method:
            DELETE](#RESTAPI-Resource-OrderAddresses-URI--orders--orderid-addresses-billing-DELETE)
    -   [URI:
        /orders/:orderid/addresses/shipping](#RESTAPI-Resource-OrderAddresses-URI--orders--orderid-addresses-shipping)
        -   [HTTP Method:
            GET](#RESTAPI-Resource-OrderAddresses-URI--orders--orderid-addresses-shipping-GET)
        -   [HTTP Method:
            POST](#RESTAPI-Resource-OrderAddresses-URI--orders--orderid-addresses-shipping-POST)
        -   [HTTP Method:
            PUT](#RESTAPI-Resource-OrderAddresses-URI--orders--orderid-addresses-shipping-PUT)
        -   [HTTP Method:
            DELETE](#RESTAPI-Resource-OrderAddresses-URI--orders--orderid-addresses-shipping-DELETE)
-   [Order Addresses
    Attributes](#RESTAPI-Resource-OrderAddresses-OrderAddressesAttributes)

### [REST API](#RESTAPI-Resource-OrderAddresses-RESTAPI-OrderAddresses): Order Addresses

## [URI](#RESTAPI-Resource-OrderAddresses-URI--orders--orderid-addresses): /orders/:orderid/addresses

Allows you to retrieve information about billing and shipping addresses
of the required order.

**URL Structure**:
http://magentohost/api/rest/orders/:orderid/addresses
**Version**: 1

## [HTTP Method](#RESTAPI-Resource-OrderAddresses-URI--orders--orderid-addresses-GET): GET

**Description**: Allows you to retrieve information on billing and
shipping addresses from the required order.
**Notes**: Customers can retrieve addresses only from their orders.

**Authentication**: Admin, Customer
**Default Format**: XML
**Parameters**:
_No Parameters_

**Example:**

---

GET http://magentohost/api/rest/orders/32/addresses

---



**Response Body:**

<div>

```
<?xml version="1.0"?>
<magento_api>
  <data_item>
    <region>Palau</region>
    <postcode>19103</postcode>
    <lastname>Doe</lastname>
    <street>2356 Jody Road Philadelphia
844 Jefferson Street; 4510 Willis Avenue</street>
    <city>PA</city>
    <telephone>610-634-1181</telephone>
    <country_id>US</country_id>
    <firstname>John</firstname>
    <address_type>billing</address_type>
    <prefix>Dr.</prefix>
    <middlename></middlename>
    <suffix>Jr.</suffix>
    <company></company>
  </data_item>
  <data_item>
    <region>Massachusetts</region>
    <postcode>01852</postcode>
    <lastname>Doe</lastname>
    <street>1073 Smith Street</street>
    <city>Lowell</city>
    <telephone>508-857-6870</telephone>
    <country_id>US</country_id>
    <firstname>John</firstname>
    <address_type>shipping</address_type>
    <prefix></prefix>
    <middlename></middlename>
    <suffix></suffix>
    <company></company>
  </data_item>
</magento_api>
```

</div>





## [HTTP Method](#RESTAPI-Resource-OrderAddresses-URI--orders--orderid-addresses-POST): POST

**Description**: Not allowed.

## [HTTP Method](#RESTAPI-Resource-OrderAddresses-URI--orders--orderid-addresses-PUT): PUT

**Description**: Not allowed.

## [HTTP Method](#RESTAPI-Resource-OrderAddresses-URI--orders--orderid-addresses-DELETE): DELETE

**Description**: Not allowed.

## [URI](#RESTAPI-Resource-OrderAddresses-URI--orders--orderid-addresses-billing): /orders/:orderid/addresses/billing

## [HTTP Method](#RESTAPI-Resource-OrderAddresses-URI--orders--orderid-addresses-billing-GET): GET

**Description**: Allows you to retrieve information on the order billing
address.
**Notes**: Customers can retrieve information on billing addresses only
from their own orders.

**Authentication**: Admin, Customer
**Default Format**: XML
**Parameters**:
_No Parameters_

**Example:**

---

GET http://magentohost/api/rest/orders/32/addresses/billing

---



**Response example:**

<div>

```
<?xml version="1.0"?>
<magento_api>
  <region>Palau</region>
  <postcode>19103</postcode>
  <lastname>Doe</lastname>
  <street>2356 Jody Road Philadelphia
844 Jefferson Street; 4510 Willis Avenue</street>
  <city>PA</city>
  <telephone>610-634-1181</telephone>
  <country_id>US</country_id>
  <firstname>John</firstname>
  <address_type>billing</address_type>
  <prefix>Dr.</prefix>
  <middlename></middlename>
  <suffix>Jr.</suffix>
  <company></company>
</magento_api>
```

</div>





## [HTTP Method](#RESTAPI-Resource-OrderAddresses-URI--orders--orderid-addresses-billing-POST): POST

**Description**: Not allowed.

## [HTTP Method](#RESTAPI-Resource-OrderAddresses-URI--orders--orderid-addresses-billing-PUT): PUT

**Description**: Not allowed.

## [HTTP Method](#RESTAPI-Resource-OrderAddresses-URI--orders--orderid-addresses-billing-DELETE): DELETE

**Description**: Not allowed.

## [URI](#RESTAPI-Resource-OrderAddresses-URI--orders--orderid-addresses-shipping): /orders/:orderid/addresses/shipping

## [HTTP Method](#RESTAPI-Resource-OrderAddresses-URI--orders--orderid-addresses-shipping-GET): GET

**Description**: Allows you to retrieve information on the order
shipping address.
**Notes**: Customers can retrieve information on shipping addresses only
from their own orders.

**Authentication**: Admin, Customer
**Default Format**: XML
**Parameters**:
_No Parameters_

**Example:**

---

GET http://magentohost/api/rest/orders/32/addresses/shipping

---



**Response example:**

<div>

```
<?xml version="1.0"?>
<magento_api>
  <region>Massachusetts</region>
  <postcode>01852</postcode>
  <lastname>Doe</lastname>
  <street>1073 Smith Street</street>
  <city>Lowell</city>
  <telephone>508-857-6870</telephone>
  <country_id>US</country_id>
  <firstname>John</firstname>
  <address_type>shipping</address_type>
  <prefix></prefix>
  <middlename></middlename>
  <suffix></suffix>
  <company></company>
</magento_api>
```

</div>





## [HTTP Method](#RESTAPI-Resource-OrderAddresses-URI--orders--orderid-addresses-shipping-POST): POST

**Description**: Not allowed.

## [HTTP Method](#RESTAPI-Resource-OrderAddresses-URI--orders--orderid-addresses-shipping-PUT): PUT

**Description**: Not allowed.

## [HTTP Method](#RESTAPI-Resource-OrderAddresses-URI--orders--orderid-addresses-shipping-DELETE): DELETE

**Description**: Not allowed.

## [Order Addresses Attributes](#RESTAPI-Resource-OrderAddresses-OrderAddressesAttributes)

---

Attribute Name Attribute Description

---

Customer Last Name Customer last name

Customer First Name\ Customer first name

Customer Middle Name\ Customer middle name or initial

Customer Prefix\ Customer prefix

Customer Suffix\ Customer suffix

Company\ Company name

Street\ Street address

City\ City

State\ State

ZIP/Postal Code\ ZIP or postal code

Country\ Country name

Phone Number\ Customer phone number

Address Type\ Address type. Can have the
following values: billing or
shipping

---

Create the Magento file system owner
