

-   [REST API: Sales
    Orders](#RESTAPI-Resource-SalesOrders-RESTAPI-SalesOrders)
    -   [URI: /orders](#RESTAPI-Resource-SalesOrders-URI--orders)
        -   [HTTP Method: GET
            /orders](#RESTAPI-Resource-SalesOrders-HTTPMethod-GET-orders)
        -   [HTTP Method: POST
            /orders](#RESTAPI-Resource-SalesOrders-HTTPMethod-POST-orders)
        -   [HTTP Method: PUT
            /orders](#RESTAPI-Resource-SalesOrders-HTTPMethod-PUT-orders)
        -   [HTTP Method: DELETE
            /orders](#RESTAPI-Resource-SalesOrders-HTTPMethod-DELETE-orders)
-   [REST API: Sales
    Order](#RESTAPI-Resource-SalesOrders-RESTAPI-SalesOrder)
    -   [URI:
        /orders/:id](#RESTAPI-Resource-SalesOrders-URI--orders--id)
        -   [HTTP Method: GET
            /orders/:id](#RESTAPI-Resource-SalesOrders-HTTPMethod-GET-orders--id)
        -   [HTTP Method: POST
            /orders/:id](#RESTAPI-Resource-SalesOrders-HTTPMethod-POST-orders--id)
        -   [HTTP Method: PUT
            /orders/:id](#RESTAPI-Resource-SalesOrders-HTTPMethod-PUT-orders--id)
        -   [HTTP Method: DELETE
            /orders/:id](#RESTAPI-Resource-SalesOrders-HTTPMethod-DELETE-orders--id)

### [REST API](#RESTAPI-Resource-SalesOrders-RESTAPI-SalesOrders): Sales Orders

## [URI](#RESTAPI-Resource-SalesOrders-URI--orders): /orders

Allows you to retrieve information on existing order/orders.

**URL Structure**: http://magentohost/api/rest/orders
**Version**: 1

## [HTTP Method](#RESTAPI-Resource-SalesOrders-HTTPMethod-GET-orders): GET /orders

**Description**: Allows you to retrieve the list of existing orders.
Each order contains the following information: general order
information, information on ordered items, order comments, and order
addresses (both billing and shipping).
The list of attributes that will be returned for the order is configured
in the Magento Admin Panel.

**Authentication**: Admin, Customer, Guest
**Default Format**: XML

**Response Example: XML**

---

GET http://magentohost/api/rest/orders

---



<div>

```
<?xml version="1.0"?>
<magento_api>
  <data_item_1>
    <customer_id>3</customer_id>
    <base_discount_amount>0.0000</base_discount_amount>
    <base_shipping_amount>455.0000</base_shipping_amount>
    <base_shipping_tax_amount>0.0000</base_shipping_tax_amount>
    <base_subtotal>13650.0000</base_subtotal>
    <base_tax_amount>0.0000</base_tax_amount>
    <base_total_paid></base_total_paid>
    <base_total_refunded></base_total_refunded>
    <tax_amount>0.0000</tax_amount>
    <total_paid></total_paid>
    <total_refunded></total_refunded>
    <base_shipping_discount_amount>0.0000</base_shipping_discount_amount>
    <base_subtotal_incl_tax>13650.0000</base_subtotal_incl_tax>
    <base_total_due>14105.0000</base_total_due>
    <total_due>14105.0000</total_due>
    <base_currency_code>USD</base_currency_code>
    <tax_name></tax_name>
    <tax_rate></tax_rate>
    <addresses>
      <data_item>
        <region>Palau</region>
        <postcode>19103</postcode>
        <lastname>Doe</lastname>
        <street>2356 Jody Road Philadelphia, PA 19103</street>
        <city>PA</city>
        <telephone>610-634-1181</telephone>
        <country_id>US</country_id>
        <firstname>John</firstname>
        <address_type>billing</address_type>
        <prefix></prefix>
        <middlename></middlename>
        <suffix></suffix>
        <company></company>
      </data_item>
      <data_item>
        <region>Palau</region>
        <postcode>19103</postcode>
        <lastname>Doe</lastname>
        <street>2356 Jody Road Philadelphia, PA 19103</street>
        <city>PA</city>
        <telephone>610-634-1181</telephone>
        <country_id>US</country_id>
        <firstname>John</firstname>
        <address_type>shipping</address_type>
        <prefix></prefix>
        <middlename></middlename>
        <suffix></suffix>
        <company></company>
      </data_item>
    </addresses>
    <order_items>
      <data_item>
        <sku>Sunglasses_1</sku>
        <price>150.0000</price>
        <base_price>150.0000</base_price>
        <base_original_price>150.0000</base_original_price>
        <tax_percent>0.0000</tax_percent>
        <tax_amount>0.0000</tax_amount>
        <base_tax_amount>0.0000</base_tax_amount>
        <base_discount_amount>0.0000</base_discount_amount>
        <base_row_total>13650.0000</base_row_total>
        <base_price_incl_tax>150.0000</base_price_incl_tax>
        <base_row_total_incl_tax>13650.0000</base_row_total_incl_tax>
      </data_item>
    </order_items>
  </data_item_1>
  <data_item_2>
    <customer_id>3</customer_id>
    <base_discount_amount>0.0000</base_discount_amount>
    <base_shipping_amount>95.0000</base_shipping_amount>
    <base_shipping_tax_amount>0.0000</base_shipping_tax_amount>
    <base_subtotal>3350.0000</base_subtotal>
    <base_tax_amount>0.0000</base_tax_amount>
    <base_total_paid>2445.0000</base_total_paid>
    <base_total_refunded>1845.0000</base_total_refunded>
    <tax_amount>0.0000</tax_amount>
    <total_paid>2445.0000</total_paid>
    <total_refunded>1845.0000</total_refunded>
    <base_shipping_discount_amount>0.0000</base_shipping_discount_amount>
    <base_subtotal_incl_tax>3350.0000</base_subtotal_incl_tax>
    <base_total_due>1000.0000</base_total_due>
    <total_due>1000.0000</total_due>
    <base_currency_code>USD</base_currency_code>
    <tax_name></tax_name>
    <tax_rate></tax_rate>
    <addresses>
      <data_item>
        <region>Palau</region>
        <postcode>19103</postcode>
        <lastname>Doe</lastname>
        <street>2356 Jody Road Philadelphia, PA 19103</street>
        <city>PA</city>
        <telephone>610-634-1181</telephone>
        <country_id>US</country_id>
        <firstname>John</firstname>
        <address_type>billing</address_type>
        <prefix></prefix>
        <middlename></middlename>
        <suffix></suffix>
        <company></company>
      </data_item>
      <data_item>
        <region>Palau</region>
        <postcode>19103</postcode>
        <lastname>Doe</lastname>
        <street>2356 Jody Road Philadelphia, PA 19103</street>
        <city>PA</city>
        <telephone>610-634-1181</telephone>
        <country_id>US</country_id>
        <firstname>John</firstname>
        <address_type>shipping</address_type>
        <prefix></prefix>
        <middlename></middlename>
        <suffix></suffix>
        <company></company>
      </data_item>
    </addresses>
    <order_items>
      <data_item>
        <sku>Sunglasses_1</sku>
        <price>150.0000</price>
        <base_price>150.0000</base_price>
        <base_original_price>150.0000</base_original_price>
        <tax_percent>0.0000</tax_percent>
        <tax_amount>0.0000</tax_amount>
        <base_tax_amount>0.0000</base_tax_amount>
        <base_discount_amount>0.0000</base_discount_amount>
        <base_row_total>1350.0000</base_row_total>
        <base_price_incl_tax>150.0000</base_price_incl_tax>
        <base_row_total_incl_tax>1350.0000</base_row_total_incl_tax>
      </data_item>
      <data_item>
        <sku>Sun_glasses</sku>
        <price>200.0000</price>
        <base_price>200.0000</base_price>
        <base_original_price>200.0000</base_original_price>
        <tax_percent>0.0000</tax_percent>
        <tax_amount>0.0000</tax_amount>
        <base_tax_amount>0.0000</base_tax_amount>
        <base_discount_amount>0.0000</base_discount_amount>
        <base_row_total>2000.0000</base_row_total>
        <base_price_incl_tax>200.0000</base_price_incl_tax>
        <base_row_total_incl_tax>2000.0000</base_row_total_incl_tax>
      </data_item>
    </order_items>
  </data_item_2>
</magento_api>
```

</div>





## [HTTP Method](#RESTAPI-Resource-SalesOrders-HTTPMethod-POST-orders): POST /orders

**Description**: Not implemented.

## [HTTP Method](#RESTAPI-Resource-SalesOrders-HTTPMethod-PUT-orders): PUT /orders

**Description**: Not implemented.

## [HTTP Method](#RESTAPI-Resource-SalesOrders-HTTPMethod-DELETE-orders): DELETE /orders

**Description**: Not implemented.

### [REST API](#RESTAPI-Resource-SalesOrders-RESTAPI-SalesOrder): Sales Order

## [URI](#RESTAPI-Resource-SalesOrders-URI--orders--id): /orders/:id

Allows you to retrieve information on a single order.
The list of attributes that will be returned for the order is configured
in the Magento Admin Panel.

**URL Structure**: http://magentohost/api/rest/orders/:id
**Version**: 1

## [HTTP Method](#RESTAPI-Resource-SalesOrders-HTTPMethod-GET-orders--id): GET /orders/:id

**Description**: Allows you to retrieve the order information.
**Authentication**: Admin, Customer
**Default Format**: XML

**Response Example: XML**

---

GET http://magentohost/api/rest/orders/32

---



<div>

```
<?xml version="1.0"?>
<magento_api>
  <customer_id>3</customer_id>
  <base_discount_amount>0.0000</base_discount_amount>
  <base_shipping_amount>0.0000</base_shipping_amount>
  <base_shipping_tax_amount>0.0000</base_shipping_tax_amount>
  <base_subtotal>5220.0000</base_subtotal>
  <base_tax_amount>430.6500</base_tax_amount>
  <base_total_paid></base_total_paid>
  <base_total_refunded></base_total_refunded>
  <tax_amount>304.3300</tax_amount>
  <total_paid></total_paid>
  <total_refunded></total_refunded>
  <base_shipping_discount_amount>0.0000</base_shipping_discount_amount>
  <base_subtotal_incl_tax></base_subtotal_incl_tax>
  <base_total_due></base_total_due>
  <total_due></total_due>
  <base_currency_code>USD</base_currency_code>
  <tax_name>US-CA-*-Rate 1</tax_name>
  <tax_rate>8.2500</tax_rate>
  <addresses>
    <data_item>
      <region>Palau</region>
      <postcode>19103</postcode>
      <lastname>Doe</lastname>
      <street>2356 Jody Road Philadelphia
844 Jefferson Street; 4510 Willis Avenue</street>
      <city>PA</city>
      <telephone>610-634-1181</telephone>
      <country_id>US</country_id>
      <firstname>John</firstname>
      <address_type>billing</address_type>
      <prefix>Dr.</prefix>
      <middlename></middlename>
      <suffix>Jr.</suffix>
      <company></company>
    </data_item>
    <data_item>
      <region>Palau</region>
      <postcode>19103</postcode>
      <lastname>Doe</lastname>
      <street>2356 Jody Road Philadelphia
844 Jefferson Street; 4510 Willis Avenue</street>
      <city>PA</city>
      <telephone>610-634-1181</telephone>
      <country_id>US</country_id>
      <firstname>John</firstname>
      <address_type>shipping</address_type>
      <prefix>Dr.</prefix>
      <middlename></middlename>
      <suffix>Jr.</suffix>
      <company></company>
    </data_item>
  </addresses>
  <order_items>
    <data_item>
      <sku>Sun_glasses</sku>
      <price>141.3400</price>
      <base_price>200.0000</base_price>
      <base_original_price>200.0000</base_original_price>
      <tax_percent>8.2500</tax_percent>
      <tax_amount>11.6600</tax_amount>
      <base_tax_amount>16.5000</base_tax_amount>
      <base_discount_amount>0.0000</base_discount_amount>
      <base_row_total>200.0000</base_row_total>
      <base_price_incl_tax>216.5000</base_price_incl_tax>
      <base_row_total_incl_tax>216.5000</base_row_total_incl_tax>
    </data_item>
    <data_item>
      <sku>Virtual_product</sku>
      <price>14.1340</price>
      <base_price>20.0000</base_price>
      <base_original_price>20.0000</base_original_price>
      <tax_percent>8.2500</tax_percent>
      <tax_amount>1.1700</tax_amount>
      <base_tax_amount>1.6500</base_tax_amount>
      <base_discount_amount>0.0000</base_discount_amount>
      <base_row_total>20.0000</base_row_total>
      <base_price_incl_tax>21.6500</base_price_incl_tax>
      <base_row_total_incl_tax>21.6500</base_row_total_incl_tax>
    </data_item>
    <data_item>
      <sku>test_simple_product</sku>
      <price>353.3500</price>
      <base_price>500.0000</base_price>
      <base_original_price>500.0000</base_original_price>
      <tax_percent>8.2500</tax_percent>
      <tax_amount>291.5000</tax_amount>
      <base_tax_amount>412.5000</base_tax_amount>
      <base_discount_amount>0.0000</base_discount_amount>
      <base_row_total>5000.0000</base_row_total>
      <base_price_incl_tax>541.2500</base_price_incl_tax>
      <base_row_total_incl_tax>5412.5000</base_row_total_incl_tax>
    </data_item>
  </order_items>
</magento_api>
```

</div>





## [HTTP Method](#RESTAPI-Resource-SalesOrders-HTTPMethod-POST-orders--id): POST /orders/:id

**Description**: Not implemented.

## [HTTP Method](#RESTAPI-Resource-SalesOrders-HTTPMethod-PUT-orders--id): PUT /orders/:id

**Description**: Not implemented.

## [HTTP Method](#RESTAPI-Resource-SalesOrders-HTTPMethod-DELETE-orders--id): DELETE /orders/:id

**Description**: Not implemented.
