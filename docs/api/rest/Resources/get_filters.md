\-\-- layout: m1x\_rest title: GET Filters \-\--

Some requests use GET parameters in the URL. These are as follows:

-   **filter** - specifies the filters for returned data
-   **page** - specifies the page number which items will be returned
    -   e.g., http://magentohost/api/rest/products?page=1
-   **order**, **dir** - specifies the sort order of returned items and
    the order direction: \'asc\' - returns items in the ascending order;
    \'dsc\' - returns items in the descending order.
    -   e.g., http://magentohost/api/rest/products?order=name&dir=dsc
    -   e.g., http://magentohost/api/rest/products?order=name&dir=asc
-   **limit** - limits the number of returned items in the response.
    Note that by default, 10 items are returned in the response. The
    maximum number is 100 items.
    -   e.g., http://magentohost/api/rest/products?limit=2
-   **neq** - \"not equal to\" - returns items with the specified
    attribute that is not equal to the defined value
    -   e.g.,
        [http://magentohost/api/rest/products?filter\[1\]\[attribute\]=entity\_id&filter\[1\]\[neq\]=3](http://magentohost/api/rest/products?filter\%5B1\%5D\%5Battribute\%5D=entity_id&filter\%5B1\%5D\%5Bneq\%5D=3)
-   **in** - \"equals any of\" - returns items that are equal to the
    item(s) with the specified attribute(s)
    -   e.g.,
        [http://magentohost/api/rest/products?filter\[1\]\[attribute\]=entity\_id&filter\[1\]\[in\]=3](http://magentohost/api/rest/products?filter\%5B1\%5D\%5Battribute\%5D=entity_id&filter\%5B1\%5D\%5Bin\%5D=3)
-   **nin** - \"not equals any of\" - returns items excluding the item
    with the specified attribute
    -   e.g.,
        [http://magentohost/api/rest/products?filter\[1\]\[attribute\]=entity\_id&filter\[1\]\[nin\]=3](http://magentohost/api/rest/products?filter\%5B1\%5D\%5Battribute\%5D=entity_id&filter\%5B1\%5D\%5Bnin\%5D=3)
-   **gt** - \"greater than\" - returns items with the specified
    attribute that is greater than the defined value
    -   e.g.,
        [http://magentohost/api/rest/products?filter\[1\]\[attribute\]=entity\_id&filter\[1\]\[gt\]=3](http://magentohost/api/rest/products?filter\%5B1\%5D\%5Battribute\%5D=entity_id&filter\%5B1\%5D\%5Bgt\%5D=3)
    -   e.g.,
        [http://magentohost/api/rest/products?filter\[1\]\[attribute\]=price&filter\[1\]\[gt\]=300](http://magentohost/api/rest/products?filter\%5B1\%5D\%5Battribute\%5D=price&filter\%5B1\%5D\%5Bgt\%5D=300)
-   **lt** - \"less than\" - returns items with the specified attribute
    that is less than the defined value
    -   e.g.,
        [http://magentohost/api/rest/products?filter\[1\]\[attribute\]=entity\_id&filter\[1\]\[lt\]=4](http://magentohost/api/rest/products?filter\%5B1\%5D\%5Battribute\%5D=entity_id&filter\%5B1\%5D\%5Blt\%5D=4)
-   **from**, **to** - specifies the range of attributes according to
    which items will be returned
    -   e.g.,
        [http://magentohost/api/rest/products?filter\[1\]\[attribute\]=entity\_id&filter\[1\]\[from\]=1&filter\[1\]\[to\]=3](http://magentohost/api/rest/products?filter\%5B1\%5D\%5Battribute\%5D=entity_id&filter\%5B1\%5D\%5Bfrom\%5D=1&filter\%5B1\%5D\%5Bto\%5D=3)
    -   e.g.,
        [http://magentohost/api/rest/products?filter\[1\]\[attribute\]=price&filter\[1\]\[from\]=150&filter\[1\]\[to\]=350](http://magentohost/api/rest/products?filter\%5B1\%5D\%5Battribute\%5D=price&filter\%5B1\%5D\%5Bfrom\%5D=150&filter\%5B1\%5D\%5Bto\%5D=350)

If the attribute value consists of several words separated by a
whitespace, the \'%20\' sign is used:

-   e.g.,
    [http://magentohost/api/rest/products?filter\[1\]\[attribute\]=name&filter\[1\]\[in\]=BlackBerry%208100%20Pearl](http://magentohost/api/rest/products?filter\%5B1\%5D\%5Battribute\%5D=name&filter\%5B1\%5D\%5Bin\%5D=BlackBerry%208100%20Pearl)

For example, to filter products with the description equal to simple01:

[http://magentohost/api/rest/products/?order=entity\_id&filter\[0\]\[attribute\]=description&filter\[0\]\[in\]\[0\]=simple01](http://magentohost/api/rest/products/?order=entity_id&filter\%5B0\%5D\%5Battribute\%5D=description&filter\%5B0\%5D\%5Bin\%5D\%5B0\%5D=simple01)
