

-   [REST API Response Formats](#ResponseFormats-RESTAPIResponseFormats)
    -   [XML Format](#ResponseFormats-XMLFormat)
    -   [JSON Format](#ResponseFormats-JSONFormat)
        -   [Response Structure](#ResponseFormats-ResponseStructure)
        -   [JSON Responses](#ResponseFormats-JSONResponses)

If you make a Magento API call, you are guaranteed to receive some kind
of a response. If you make a successful call, you will receive an HTTP
response with a 200 OK status.

# [](#ResponseFormats-RESTAPIResponseFormats)REST API Response Formats

You can view the response data from any Magento API call in one of the
following two formats:

-   XML
-   JSON

The format of returned data is defined in the request header. The format
you choose depends on what you are familiar with most or tools available
to you.

## [](#ResponseFormats-XMLFormat)XML Format

The XML response format is a simple XML block.
To set the response format to XML, add the Accept request header with
the text/xml value.

A successful call will return the following response (example of
retrieving information about stock items):

<div>

```
<?xml version="1.0"?>
<magento_api>
  <data_item>
    <item_id>1</item_id>
    <product_id>1</product_id>
    <stock_id>1</stock_id>
    <qty>99.0000</qty>
    <low_stock_date></low_stock_date>
  </data_item>
  <data_item>
    <item_id>2</item_id>
    <product_id>2</product_id>
    <stock_id>1</stock_id>
    <qty>100.0000</qty>
    <low_stock_date></low_stock_date>
  </data_item>
</magento_api>
```

</div>





If an error occurs, the call may return the following response:

<div>

```
<?xml version="1.0"?>
<magento_api>
  <messages>
    <error>
      <data_item>
        <code>404</code>
        <message>Resource not found.</message>
      </data_item>
    </error>
  </messages>
</magento_api>
```

</div>





## [](#ResponseFormats-JSONFormat)JSON Format

JSON (JavaScript Object Notation) is a lightweight data-interchange
format.
To set the response format to JSON, add the Accept request header with
the application/json value.

## [Response Structure](#ResponseFormats-ResponseStructure)

The JSON objects represent a direct mapping of the XML block from the
XML response format.

A simple XML error

<div>

```
<messages>
    <error>
      <data_item>
        <code>404</code>
        <message>Resource not found.</message>
      </data_item>
    </error>
  </messages>
```

</div>





will be transformed to

<div>

```
{"messages":{"error":[{"code":404,"message":"Resource not found."}]\}\}
```

</div>





## [JSON Responses](#ResponseFormats-JSONResponses)

A successful API call to the Stock Items resource will return the
following XML response:

<div>

```
<?xml version="1.0"?>
<magento_api>
  <data_item>
    <item_id>1</item_id>
    <product_id>1</product_id>
    <stock_id>1</stock_id>
    <qty>99.0000</qty>
    <low_stock_date></low_stock_date>
  </data_item>
  <data_item>
    <item_id>2</item_id>
    <product_id>2</product_id>
    <stock_id>1</stock_id>
    <qty>100.0000</qty>
    <low_stock_date></low_stock_date>
  </data_item>
</magento_api>
```

</div>





The JSON equivalent will be as follows:

<div>

```
[{"item_id":"1","product_id":"1","stock_id":"1","qty":"99.0000","low_stock_date":null},{"item_id":"2","product_id":"2","stock_id":"1","qty":"100.0000","low_stock_date":null}]
```

</div>





The list of HTTP status codes that are returned in the API response is
described in the [Common HTTP Status
Codes](http://www.magentocommerce.com/api/rest/common_http_status_codes.html "Common HTTP Status Codes")
part of the documentation. There, you can find the list of codes
themselves together with their description.
