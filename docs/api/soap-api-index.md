

Welcome to the Magento 1.x SOAP API home page.

[Introduction to the Magento 1.x SOAP
API](soap/introduction.html)

**[Catalog](soap/catalog/catalog.html)**

[Catalog
Category](soap/catalog/catalogCategory/catalogCategory.html)

-   [Assigned
    Products](soap/catalog/catalogCategory/catalog_category.assignedProducts.html)
-   [Assign
    Product](soap/catalog/catalogCategory/catalog_category.assignProduct.html)
-   [Create
    Category](soap/catalog/catalogCategory/catalog_category.create.html)
-   [Current
    Store](soap/catalog/catalogCategory/catalog_category.currentStore.html)
-   [Category
    Delete](soap/catalog/catalogCategory/catalog_category.delete.html)
-   [Category
    Info](soap/catalog/catalogCategory/catalog_category.info.html)
-   [Category
    Level](soap/catalog/catalogCategory/catalog_category.level.html)
-   [Category
    Move](soap/catalog/catalogCategory/catalog_category.move.html)
-   [Remove
    Product](soap/catalog/catalogCategory/catalog_category.removeProduct.html)
-   [Category
    Tree](soap/catalog/catalogCategory/catalog_category.tree.html)
-   [Category
    Update](soap/catalog/catalogCategory/catalog_category.update.html)
-   [Category Product
    Update](soap/catalog/catalogCategory/catalog_category.updateProduct.html)

[Category
Attributes](soap/catalog/catalogCategoryAttributes/categoryAttributes.html)

-   [Current
    Store](soap/catalog/catalogCategoryAttributes/catalog_category_attribute.currentStore.html)
-   [Attribute
    List](soap/catalog/catalogCategoryAttributes/catalog_category_attribute.list.html)
-   [Attribute
    Options](soap/catalog/catalogCategoryAttributes/catalog_category_attribute.options.html)

[Catalog
Product](soap/catalog/catalogProduct/catalogProduct.html)

-   [Product
    Create](soap/catalog/catalogProduct/catalog_product.create.html)
-   [Current Product
    Store](soap/catalog/catalogProduct/catalog_product.currentStore.html)
-   [Product
    Delete](soap/catalog/catalogProduct/catalog_product.delete.html)
-   [Get Special
    Price](soap/catalog/catalogProduct/catalog_product.getSpecialPrice.html)
-   [Product
    Info](soap/catalog/catalogProduct/catalog_product.info.html)
-   [Product
    List](soap/catalog/catalogProduct/catalog_product.list.html)
-   [List of Additional
    Attributes](soap/catalog/catalogProduct/catalog_product.listOfAdditionalAttributes.html)
-   [Set Special
    Price](soap/catalog/catalogProduct/catalog_product.setSpecialPrice.html)
-   [Product
    Update](soap/catalog/catalogProduct/catalog_product.update.html)

[Catalog Product
Attribute](soap/catalog/catalogProductAttribute/catalogProductAttribute.html)

-   [Add
    Option](soap/catalog/catalogProductAttribute/product_attribute.addOption.html)
-   [Create
    Attribute](soap/catalog/catalogProductAttribute/product_attribute.create.html)
-   [Current
    Store](soap/catalog/catalogProductAttribute/product_attribute.currentStore.html)
-   [Attribute
    Info](soap/catalog/catalogProductAttribute/product_attribute.info.html)
-   [Attribute
    List](soap/catalog/catalogProductAttribute/product_attribute.list.html)
-   [Attribute
    Options](soap/catalog/catalogProductAttribute/product_attribute.options.html)
-   [Attribute
    Remove](soap/catalog/catalogProductAttribute/product_attribute.remove.html)
-   [Remove
    Option](soap/catalog/catalogProductAttribute/product_attribute.removeOption.html)
-   [Attribute
    Types](soap/catalog/catalogProductAttribute/product_attribute.types.html)
-   [Attribute
    Update](soap/catalog/catalogProductAttribute/product_attribute.update.html)

[Catalog Product Attribute
Media](soap/catalog/catalogProductAttributeMedia/productImages.html)

-   [Create](soap/catalog/catalogProductAttributeMedia/catalog_product_attribute_media.create.html)
-   [Current
    Store](soap/catalog/catalogProductAttributeMedia/catalog_product_attribute_media.currentStore.html)
-   [Media
    Info](soap/catalog/catalogProductAttributeMedia/catalog_product_attribute_media.info.html)
-   [Media
    List](soap/catalog/catalogProductAttributeMedia/catalog_product_attribute_media.list.html)
-   [Media
    Remove](soap/catalog/catalogProductAttributeMedia/catalog_product_attribute_media.remove.html)
-   [Media
    Types](soap/catalog/catalogProductAttributeMedia/catalog_product_attribute_media.types.html)
-   [Media
    Update](soap/catalog/catalogProductAttributeMedia/catalog_product_attribute_media.update.html)

[Product Attribute
Set](soap/catalog/catalogProductAttributeSet/productAttributeSet.html)

-   [Attribute Set
    Add](soap/catalog/catalogProductAttributeSet/product_attribute_set.attributeAdd.html)
-   [Attribute Set
    Remove](soap/catalog/catalogProductAttributeSet/product_attribute_set.attributeRemove.html)
-   [Attribute Set
    Create](soap/catalog/catalogProductAttributeSet/product_attribute_set.create.html)
-   [Attribute Set Group
    Add](soap/catalog/catalogProductAttributeSet/product_attribute_set.groupAdd.html)
-   [Attribute Set Group
    Remove](soap/catalog/catalogProductAttributeSet/product_attribute_set.groupRemove.html)
-   [Attribute Set Group
    Rename](soap/catalog/catalogProductAttributeSet/product_attribute_set.groupRename.html)
-   [Attribute Set
    List](soap/catalog/catalogProductAttributeSet/product_attribute_set.list.html)
-   [Attribute Set
    Remove](soap/catalog/catalogProductAttributeSet/product_attribute_set.remove.html)

[Catalog Product Custom
Option](soap/catalog/catalogProductCustomOption/catalogProductCustomOption.html)

-   [Product Custom Option
    Add](soap/catalog/catalogProductCustomOption/product_custom_option.add.html)
-   [Product Custom Option
    Info](soap/catalog/catalogProductCustomOption/product_custom_option.info.html)
-   [Product Custom Option
    List](soap/catalog/catalogProductCustomOption/product_custom_option.list.html)
-   [Product Custom Option
    Remove](soap/catalog/catalogProductCustomOption/product_custom_option.remove.html)
-   [Product Custom Option
    Types](soap/catalog/catalogProductCustomOption/product_custom_option.types.html)
-   [Product Custom Option
    Update](soap/catalog/catalogProductCustomOption/product_custom_option.update.html)

[Catalog Product Custom Option
Value](soap/catalog/catalogProductCustomOptionValue/catalogProductCustomOptionValue.html)

-   [Product Custom Option Value
    Add](soap/catalog/catalogProductCustomOptionValue/product_custom_option_value.add.html)
-   [Product Custom Option Value
    Info](soap/catalog/catalogProductCustomOptionValue/product_custom_option_value.info.html)
-   [Product Custom Option Value
    List](soap/catalog/catalogProductCustomOptionValue/product_custom_option_value.list.html)
-   [Product Custom Option Value
    Remove](soap/catalog/catalogProductCustomOptionValue/product_custom_option_value.remove.html)
-   [Product Custom Option Value
    Update](soap/catalog/catalogProductCustomOptionValue/product_custom_option_value.update.html)

[Catalog Product Downloadable
Link](soap/catalog/catalogProductDownloadableLink/catalogProductDownloadableLink.html)

-   [Downloadable Link
    Add](soap/catalog/catalogProductDownloadableLink/product_downloadable_link.add.html)
-   [Downloadable Link
    List](soap/catalog/catalogProductDownloadableLink/product_downloadable_link.list.html)
-   [Downloadable Link
    Remove](soap/catalog/catalogProductDownloadableLink/product_downloadable_link.remove.html)

[Catalog Product
Link](soap/catalog/catalogProductLink/catalogProductLink.html)

-   [Product Link
    Assign](soap/catalog/catalogProductLink/catalog_product_link.assign.html)
-   [Product Link
    Attributes](soap/catalog/catalogProductLink/catalog_product_link.attributes.html)
-   [Product Link
    List](soap/catalog/catalogProductLink/catalog_product_link.list.html)
-   [Product Link
    Remove](soap/catalog/catalogProductLink/catalog_product_link.remove.html)
-   [Product Link
    Types](soap/catalog/catalogProductLink/catalog_product_link.types.html)
-   [Product Link
    Update](soap/catalog/catalogProductLink/catalog_product_link.update.html)

[Catalog Product
Tag](soap/catalog/catalogProductTag/catalogProductTag.html)

-   [Product Tag
    Add](soap/catalog/catalogProductTag/product_tag.add.html)
-   [Product Tag
    Info](soap/catalog/catalogProductTag/product_tag.info.html)
-   [Product Tag
    List](soap/catalog/catalogProductTag/product_tag.list.html)
-   [Product Tag
    Remove](soap/catalog/catalogProductTag/product_tag.remove.html)
-   [Product Tag
    Update](soap/catalog/catalogProductTag/product_tag.update.html)

[Catalog Product Attribute Tier
Price](soap/catalog/catalogProductTierPrice/catalogProductTierPrice.html)

-   [Product Attribute Tier Price
    Info](soap/catalog/catalogProductTierPrice/catalog_product_attribute_tier_price.info.html)
-   [Product Attribute Tier Price
    Update](soap/catalog/catalogProductTierPrice/catalog_product_attribute_tier_price.update.html)

[Catalog Product
Types](soap/catalog/catalogProductTypes/productTypes.html)

-   [Product Type
    List](soap/catalog/catalogProductTypes/catalog_product_type.list.html)

**[Catalog
Inventory](soap/catalogInventory/Inventory.html)**

-   [Inventory Stock Item
    List](soap/catalogInventory/cataloginventory_stock_item.list.html)
-   [Inventory Stock Item
    Update](soap/catalogInventory/cataloginventory_stock_item.update.html)

**[Checkout](soap/checkout/checkout.html)**

[Cart](soap/checkout/cart/cart.html)

-   [Cart
    Create](soap/checkout/cart/cart.create.html)
-   [Cart
    Info](soap/checkout/cart/cart.info.html)
-   [Cart
    License](soap/checkout/cart/cart.license.html)
-   [Cart
    Order](soap/checkout/cart/cart.order.html)
-   [Cart
    Totals](soap/checkout/cart/cart.totals.html)

[Cart
Coupon](soap/checkout/cartCoupon/cartCoupon.html)

-   [Coupon
    Add](soap/checkout/cartCoupon/cart_coupon.add.html)
-   [Coupon
    Remove](soap/checkout/cartCoupon/cart_coupon.remove.html)

[Cart
Customer](soap/checkout/cartCustomer/cartCustomer.html)

-   [Customer
    Addresses](soap/checkout/cartCustomer/cart_customer.addresses.html)
-   [Customer
    Set](soap/checkout/cartCustomer/cart_customer.set.html)

[Cart
Payment](soap/checkout/cartPayment/cartPayment.html)

-   [Payment
    List](soap/checkout/cartPayment/cart_payment.list.html)
-   [Payment
    Method](soap/checkout/cartPayment/cart_payment.method.html)

[Cart
Product](soap/checkout/cartProduct/cartProduct.html)

-   [Product
    Add](soap/checkout/cartProduct/cart_product.add.html)
-   [Product
    List](soap/checkout/cartProduct/cart_product.list.html)
-   [Product Move To Customer
    Quote](soap/checkout/cartProduct/cart_product.moveToCustomerQuote.html)
-   [Product
    Remove](soap/checkout/cartProduct/cart_product.remove.html)
-   [Product
    Update](soap/checkout/cartProduct/cart_product.update.html)

[Cart
Shipping](soap/checkout/cartShipping/cartShipping.html)

-   [Shipping
    List](soap/checkout/cartShipping/cart_shipping.list.html)
-   [Shipping
    Method](soap/checkout/cartShipping/cart_shipping.method.html)

**[Create Your Own
API](soap/create_your_own_api.html)**

**[Customer](soap/customer/customer.html)**

[Customer
Group](soap/customer/customer_group.html)

[Customer
Create](soap/customer/customer.create.html)

[Customer
Delete](soap/customer/customer.delete.html)

[Customer
Info](soap/customer/customer.info.html)

[Customer
List](soap/customer/customer.list.html)

[Customer
Update](soap/customer/customer.update.html)

[Customer
Address](soap/customer/customerAddress/customerAddress.html)

-   [Address
    Create](soap/customer/customerAddress/customer_address.create.html)
-   [Address
    Delete](soap/customer/customerAddress/customer_address.delete.html)
-   [Address
    Info](soap/customer/customerAddress/customer_address.info.html)
-   [Address
    List](soap/customer/customerAddress/customer_address.list.html)
-   [Address
    Update](soap/customer/customerAddress/customer_address.update.html)

**[Directory](soap/directory/directory.html)**

-   [Country
    List](soap/directory/directory_country.list.html)
-   [Region
    List](soap/directory/directory_region.list.html)

**[Sales](soap/sales/sales.html)**

[Sales
Order](soap/sales/salesOrder/salesOrder.html)

-   [Add
    Comment](soap/sales/salesOrder/sales_order.addComment.html)
-   [Order
    Cancel](soap/sales/salesOrder/sales_order.cancel.html)
-   [Order
    Hold](soap/sales/salesOrder/sales_order.hold.html)
-   [Order
    Info](soap/sales/salesOrder/sales_order.info.html)
-   [Order
    List](soap/sales/salesOrder/sales_order.list.html)
-   [Order
    Unhold](soap/sales/salesOrder/sales_order.unhold.html)

[Sales Order Credit
Memo](soap/sales/salesOrderCreditMemo/salesOrderCreditMemo.html)

-   [Add
    Comment](soap/sales/salesOrderCreditMemo/sales_order_creditmemo.addComment.html)
-   [Memo
    Cancel](soap/sales/salesOrderCreditMemo/sales_order_creditmemo.cancel.html)
-   [Memo
    Create](soap/sales/salesOrderCreditMemo/sales_order_creditmemo.create.html)
-   [Memo
    Info](soap/sales/salesOrderCreditMemo/sales_order_creditmemo.info.html)
-   [Memo
    List](soap/sales/salesOrderCreditMemo/sales_order_creditmemo.list.html)

[Sales Order
Invoice](soap/sales/salesOrderInvoice/salesOrderInvoice.html)

-   [Add
    Comment](soap/sales/salesOrderInvoice/sales_order_invoice.addComment.html)
-   [Invoice
    Cancel](soap/sales/salesOrderInvoice/sales_order_invoice.cancel.html)
-   [Invoice
    Capture](soap/sales/salesOrderInvoice/sales_order_invoice.capture.html)
-   [Invoice
    Create](soap/sales/salesOrderInvoice/sales_order_invoice.create.html)
-   [Invoice
    Info](soap/sales/salesOrderInvoice/sales_order_invoice.info.html)
-   [Invoice
    List](soap/sales/salesOrderInvoice/sales_order_invoice.list.html)

[Sales Order
Shipment](soap/sales/salesOrderShipment/salesOrderShipment.html)

-   [Add
    Comment](soap/sales/salesOrderShipment/sales_order_shipment.addComment.html)
-   [Add
    Track](soap/sales/salesOrderShipment/sales_order_shipment.addTrack.html)
-   [Shipment
    Create](soap/sales/salesOrderShipment/sales_order_shipment.create.html)
-   [Shipment Get
    Carriers](soap/sales/salesOrderShipment/sales_order_shipment.getCarriers.html)
-   [Shipment
    Info](soap/sales/salesOrderShipment/sales_order_shipment.info.html)
-   [Shipment
    List](soap/sales/salesOrderShipment/sales_order_shipment.list.html)
-   [Remove
    Track](soap/sales/salesOrderShipment/sales_order_shipment.removeTrack.html)

**[Enterprise Customer
Balance](soap/enterprise_customerbalance/enterprise_customerbalance.html)**

[Customer
Balance](soap/enterprise_customerbalance/customerBalance/storecredit.html)

-   [Store
    Credit](soap/enterprise_customerbalance/customerBalance/storecredit.balance.html)
-   [Credit
    History](soap/enterprise_customerbalance/customerBalance/storecredit.history.html)

[Shopping Cart Customer
Balance](soap/enterprise_customerbalance/shoppingCartCustomerBalance/storecredit_quote.html)

-   [Balance Remove
    Amount](soap/enterprise_customerbalance/shoppingCartCustomerBalance/storecredit_quote.removeAmount.html)
-   [Balance Set
    Amount](soap/enterprise_customerbalance/shoppingCartCustomerBalance/storecredit_quote.setAmount.html)

**[Enterprise Gift
Card](soap/enterpriseGiftCard/enterprise_giftCard.html)**

[Cart Gift
Card](soap/enterpriseGiftCard/cartGiftCard/cartGiftCard.html)

-   [Gift Card
    Add](soap/enterpriseGiftCard/cartGiftCard/cart_giftcard.add.html)
-   [Gift Card
    List](soap/enterpriseGiftCard/cartGiftCard/cart_giftcard.list.html)
-   [Gift Card
    Remove](soap/enterpriseGiftCard/cartGiftCard/cart_giftcard.remove.html)

[Gift Card
Account](soap/enterpriseGiftCard/giftCardAccount/giftCardAccount.html)

-   [Account
    Create](soap/enterpriseGiftCard/giftCardAccount/giftcard_account.create.html)
-   [Account
    Info](soap/enterpriseGiftCard/giftCardAccount/giftcard_account.info.html)
-   [Account
    List](soap/enterpriseGiftCard/giftCardAccount/giftcard_account.list.html)
-   [Account
    Remove](soap/enterpriseGiftCard/giftCardAccount/giftcard_account.remove.html)
-   [Account
    Update](soap/enterpriseGiftCard/giftCardAccount/giftcard_account.update.html)

[Gift Card
Customer](soap/enterpriseGiftCard/giftCardCustomer/giftCardCustomer.html)

-   [Customer
    Info](soap/enterpriseGiftCard/giftCardCustomer/giftcard_customer.info.html)
-   [Customer
    Redeem](soap/enterpriseGiftCard/giftCardCustomer/giftcard_customer.redeem.html)

**[Enterprise Gift
Message](soap/enterpriseGiftMessage/giftMessage.html)**

-   [Set For
    Quote](soap/enterpriseGiftMessage/giftmessage.setForQuote.html)
-   [Set For Quote
    Item](soap/enterpriseGiftMessage/giftmessage.setForQuoteItem.html)
-   [Set For Quote
    Product](soap/enterpriseGiftMessage/giftmessage.setForQuoteProduct.html)

**[Miscellaneous](soap/miscellaneous/miscellaneous.html)**

-   [Magento
    Info](soap/miscellaneous/magento.info.html)
-   [Store
    Info](soap/miscellaneous/store.info.html)
-   [Store
    List](soap/miscellaneous/store.list.html)

**[WS-I
Compliance](soap/wsi_compliance.html)**
