

## [Module: Mage_Customer](#customer.delete-Module:MageCustomer)

Allows you to export/import customers from/to Magento

### [Resource: customer](#customer.delete-Resource:customer)

## [Method:](#customer.delete-Method:)

-   customer.delete (SOAP V1)
-   customerCustomerDelete (SOAP V2)

Delete the required customer.

**Arguments:**

Type Name Description

---

string sessionId Session ID
int customerId Customer ID

**Returns**:

Type Description

---

boolean True if the customer is deleted

### [Examples](#customer.delete-Examples)

### [Request Example SOAP V1](#customer.delete-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'customer.delete', '2');
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#customer.delete-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->customerCustomerDelete($sessionId, '2');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#customer.delete-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->customerCustomerDelete((object)array('sessionId' => $sessionId->result, 'customerId' => '2'));
var_dump($result->result);
```

</div>


