

## [Module: Mage_Customer](#customer.list-Module:MageCustomer)

### [Resource: customer](#customer.list-Resource:customer)

## [Method:](#customer.list-Method:)

-   customer.list (SOAP V1)
-   customerCustomerList (SOAP V2)

Allows you to retrieve the list of customers.

**Arguments:**

Type Name Description

---

string sessionId Session ID
array filters Array of filters by customer attributes (optional)

**Returns**:

Type Name Description

---

array storeView Array of customerCustomerEntity

The **customerCustomerEntity** content is as follows:

Type Name Description

---

int customer_id ID of the customer
string created_at Date when the customer was created
string updated_at Date of when the customer was updated
string increment_id Increment ID
int store_id Store ID
int website_id Website ID
string created_in Created in
string email Customer email
string firstname Customer first name
string middlename Customer middle name
string lastname Customer last name
int group_id Group ID
string prefix Customer prefix
string suffix Customer suffix
string dob Customer date of birth
string taxvat Taxvat value
boolean confirmation Confirmation flag
string password_hash Password hash

**Note**: The password_hash parameter will only match exactly with the
same MD5 and salt as was used when Magento stored the value. If you try
to match with an unsalted MD5 hash, or any salt other than what Magento
used, it will not match. This is just a straight string comparison.

### [Examples](#customer.list-Examples)

### [Request Example SOAP V1](#customer.list-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'customer.list');
var_dump ($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



## [Request Example SOAP V2 (List of All Customers)](#customer.list-RequestExampleSOAPV2%28ListofAllCustomers%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->customerCustomerList($sessionId);
var_dump($result);
```

</div>



## [Request Example SOAP V2 (Complex Filter)](#customer.list-RequestExampleSOAPV2%28ComplexFilter%29)

<div>

```
$client = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

// If some stuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');
$complexFilter = array(
    'complex_filter' => array(
        array(
            'key' => 'group_id',
            'value' => array('key' => 'in', 'value' => '1,3')
        )
    )
);
$result = $client->customerCustomerList($session, $complexFilter);

var_dump ($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#customer.list-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->customerCustomerList((object)array('sessionId' => $sessionId->result, 'filters' => null));
var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#customer.list-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
    array
      'customer_id' => string '2' (length=1)
      'created_at' => string '2012-03-29 12:37:23' (length=19)
      'updated_at' => string '2012-04-03 11:20:18' (length=19)
      'store_id' => string '2' (length=1)
      'website_id' => string '2' (length=1)
      'created_in' => string 'English' (length=7)
      'default_billing' => string '3' (length=1)
      'default_shipping' => string '3' (length=1)
      'disable_auto_group_change' => string '0' (length=1)
      'email' => string 'test@example.com' (length=16)
      'firstname' => string 'John' (length=4)
      'group_id' => string '1' (length=1)
      'lastname' => string 'Doe' (length=3)
      'password_hash' => string 'cccfb3ecf54c9644a34106783148eff2:sp' (length=35)
      'rp_token' => string '15433dd072f1f4e5aae83231b93f72d0' (length=32)
      'rp_token_created_at' => string '2012-03-30 15:10:31' (length=19)
  1 =>
    array
      'customer_id' => string '4' (length=1)
      'created_at' => string '2012-04-03 11:21:15' (length=19)
      'updated_at' => string '2012-04-03 11:22:57' (length=19)
      'store_id' => string '0' (length=1)
      'website_id' => string '2' (length=1)
      'created_in' => string 'Admin' (length=5)
      'default_billing' => string '8' (length=1)
      'default_shipping' => string '8' (length=1)
      'disable_auto_group_change' => string '0' (length=1)
      'email' => string 'shon@example.com' (length=16)
      'firstname' => string 'Shon' (length=4)
      'group_id' => string '1' (length=1)
      'lastname' => string 'McMiland' (length=8)
      'password_hash' => string '5670581cabba4e2189e5edee99ed0c86:5q' (length=35)
```

</div>


