

## [Module: Mage_Customer](#customer.create-Module:MageCustomer)

Allows you to export/import customers from/to Magento.

### [Resource: customer](#customer.create-Resource:customer)

## [Method:](#customer.create-Method:)

-   customer.create (SOAP V1)
-   customerCustomerCreate (SOAP V2)

Create a new customer.

**Arguments:**

---

Type Name Description

---

string sessionId Session ID

array customerData Array of
customerCustomerEntityToCreate

---

**Returns**:

Type Name Description

---

int result ID of the created customer

The **customerCustomerEntityToCreate** content is as follows:

Type Name Description

---

string email Customer email
string firstname Customer first name
string lastname Customer last name
string password Customer password
int website_id Website ID
int store_id Store ID
int group_id Group ID
string prefix Customer prefix (optional)
string suffix Customer suffix (optional)
string dob Customer date of birth (optional)
string taxvat Customer tax/VAT number (optional)
int gender Customer gender: 1 - Male, 2 - Female (optional)
string middlename Customer middle name/initial (optional)

### [Examples](#customer.create-Examples)

### [Request Example SOAP V1](#customer.create-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');
$result = $client->call($session,'customer.create',array(array('email' => 'mail@example.org', 'firstname' => 'Dough', 'lastname' => 'Deeks', 'password' => 'password', 'website_id' => 1, 'store_id' => 1, 'group_id' => 1)));

var_dump ($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#customer.create-RequestExampleSOAPV2)

<div>

```
$client = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

// If some stuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');
$result = $client->customerCustomerCreate($session, array('email' => 'customer-mail@example.org', 'firstname' => 'Dough', 'lastname' => 'Deeks', 'password' => 'password', 'website_id' => 1, 'store_id' => 1, 'group_id' => 1));

var_dump ($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#customer.create-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->customerCustomerCreate((object)array('sessionId' => $sessionId->result, 'customerData' => ((object)array(
'email' => 'customer-mail@example.org',
'firstname' => 'John',
'lastname' => 'Dou',
'password' => '123123',
'website_id' => '0',
'group_id' => '1'
))));
var_dump($result->result);
```

</div>


