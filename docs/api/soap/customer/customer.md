

## [Module: Mage_Customer](#MAPI-Customer-Module:MageCustomer)

The Mage_Customer module allows you to create, retrieve, update, and
delete customers and customer addresses.

### [Customer](#MAPI-Customer-Customer)

Allows you to create, retrieve, update, and delete data about customers.

**Resource Name**: customer

**Methods**:

-   [customer.list](customer.list.html "customer.list") - Retrieve the
    list of customers
-   [customer.create](customer.create.html "customer.create") - Create a
    new customer
-   [customer.info](customer.info.html "customer.info") - Retrieve the
    customer data
-   [customer.update](customer.update.html "customer.update") - Update
    the customer data
-   [customer.delete](customer.delete.html "customer.delete") - Delete a
    required customer

### [Faults](#MAPI-Customer-Faults)

Fault Code Fault Message

---

100 Invalid customer data. Details in error message.
101 Invalid filters specified. Details in error message.
102 Customer does not exist.
103 Customer not deleted. Details in error message.

### [Examples](#MAPI-Customer-Examples)

## [Example 1](#MAPI-Customer-Example1.View,create,update,anddeleteacustomer). View, create, update, and delete a customer

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');

$newCustomer = array(
    'firstname'  => 'First',
    'lastname'   => 'Last',
    'email'      => 'test@example.com',
    //for my version of magento (1.3.2.4) you SHOULD NOT
    // hash the password, as in:
    // 'password_hash' => 'password'
    'password_hash'   => md5('password'),
    // password hash can be either regular or salted md5:
    // $hash = md5($password);
    // $hash = md5($salt.$password).':'.$salt;
    // both variants are valid
    'store_id'   => 0,
    'website_id' => 0
);

$newCustomerId = $proxy->call($sessionId, 'customer.create', array($newCustomer));

// Get new customer info
var_dump($proxy->call($sessionId, 'customer.info', $newCustomerId));


// Update customer
$update = array('firstname'=>'Changed Firstname');
$proxy->call($sessionId, 'customer.update', array($newCustomerId, $update));

var_dump($proxy->call($sessionId, 'customer.info', $newCustomerId));

// Delete customer
$proxy->call($sessionId, 'customer.delete', $newCustomerId);
```

</div>



### [Customer Groups](#MAPI-Customer-CustomerGroups)

Allows you to retrieve the customer groups.

**Resource Name**: customer_group

**Methods**:

-   [customer_group.list](customer_group.html "customer_group.list") -
    Retrieve the list of customer groups
