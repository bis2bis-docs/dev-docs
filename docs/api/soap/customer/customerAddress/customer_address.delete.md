

## [Module: Mage_Customer](#customer_address.delete-Module:MageCustomer)

### [Resource: customer_address](#customer_address.delete-Resource:customeraddress)

## [Method:](#customer_address.delete-Method:)

-   customer_address.delete (SOAP V1)
-   customerAddressDelete (SOAP V2)

Delete the required customer address.

**Arguments:**

Type Name Description

---

string sessionId Session ID
int addressId Address ID

**Returns**:

Type Description

---

boolean True if the customer address is deleted

### [Examples](#customer_address.delete-Examples)

### [Request Example SOAP V1](#customer_address.delete-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'customer_address.delete', '4');
var_dump ($result);
```

</div>



### [Request Example SOAP V2](#customer_address.delete-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->customerAddressDelete($sessionId, '4');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#customer_address.delete-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->customerAddressDelete((object)array('sessionId' => $sessionId->result, 'addressId' => '4'));
var_dump($result->result);
```

</div>


