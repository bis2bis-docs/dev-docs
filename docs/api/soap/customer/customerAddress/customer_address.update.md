

## [Module: Mage_Customer](#customer_address.update-Module:MageCustomer)

### [Resource: customer_address](#customer_address.update-Resource:customeraddress)

## [Method:](#customer_address.update-Method:)

-   customer_address.update (SOAP V1)
-   customerAddressUpdate (SOAP V2)

Update address data of the required customer

**Arguments:**

Type Name Description

---

string sessionId Session ID
int addressId Address ID
array addressdata Array of customerAddressEntityCreate

**Returns**:

Type Description

---

boolean True if the customer address is updated

The **customerAddressEntityCreate** content is as follows:

---

Type Name Description

---

string city\ Name of the city

string\ company\ Name of the company

string\ country_id\ Country ID

string\ fax\ Fax

string\ firstname\ Customer first name

string\ lastname\ Customer last name

string\ middlename\ Customer middle name

string\ postcode\ Postcode

string\ prefix\ Customer prefix

int region_id\ ID of the region

string\ region\ Name of the region

ArrayOfString street\ Array of streets

string\ suffix\ Customer suffix

string\ telephone\ Telephone number

boolean is_default_billing\ True if the address is
the default one for
billing

boolean is_default_shipping\ True if the address is
the default one for
shipping

---

### [Examples](#customer_address.update-Examples)

### [Request Example SOAP V1](#customer_address.update-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call(
$session,
'customer_address.update',
array('addressId' => 8, 'addressdata' => array('firstname' => 'John', 'lastname' => 'Doe', 'street' => array('Street line 1', 'Streer line 2'), 'city' => 'Weaverville', 'country_id' => 'US', 'region' => 'Texas', 'region_id' => 3, 'postcode' => '96093', 'telephone' => '530-623-2513', 'is_default_billing' => TRUE, 'is_default_shipping' => FALSE)));
var_dump ($result);
```

</div>



### [Request Example SOAP V2](#customer_address.update-RequestExampleSOAPV2)

<div>

```
$client = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

// If some stuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');
$result = $client->customerAddressUpdate($session, '8', array('firstname' => 'John', 'lastname' => 'Doe', 'street' => array('Street line 1', 'Streer line 2'), 'city' => 'Weaverville', 'country_id' => 'US', 'region' => 'Texas', 'region_id' => 3, 'postcode' => '96093', 'telephone' => '530-623-2513', 'is_default_billing' => FALSE, 'is_default_shipping' => FALSE));

var_dump ($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#customer_address.update-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->customerAddressUpdate((object)array('sessionId' => $sessionId->result, 'addressId' => '8', 'addressData' => ((object)array(
'firstname' => 'John',
'lastname' => 'Doe',
'street' => array('Street line 1', 'Streer line 2'),
'city' => 'Weaverville',
'country_id' => 'US',
'region' => 'Texas',
'region_id' => 3,
'postcode' => '96093',
'telephone' => '530-623-2513',
'is_default_billing' => TRUE,
'is_default_shipping' => TRUE
))));
var_dump($result->result);
```

</div>


