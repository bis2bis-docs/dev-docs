

## [Module: Mage_Customer](#customer_address.list-Module:MageCustomer)

### [Resource: customer_address](#customer_address.list-Resource:customeraddress)

## [Method:](#customer_address.list-Method:)

-   customer_address.list (SOAP V1)
-   customerAddressList (SOAP V2)

Retrieve the list of customer addresses.

**Arguments:**

Type Name Description

---

string sessionId Session ID
int customerId Customer ID

**Returns:**

Type Name Description

---

array result Array of customerAddressEntity

The **customerAddressEntity** content is as follows:

---

Type Name Description

---

int customer_address_id\ ID of the customer
address

string created_at\ Date when the address
was created

string updated_at\ Date when the address
was updated

string increment_id\ Increment ID

string city\ City

string company\ Name of the company

string country_id\ ID of the country

string fax\ Fax

string firstname\ Customer first name

string lastname\ Customer last name

string middlename\ Customer middle name

string postcode\ Customer postcode

string prefix\ Customer prefix

string region\ Name of the region

int region_id\ Region ID

string street\ Name of the street

string suffix\ Customer suffix

string telephone\ Telephone number

boolean is_default_billing\ True if the address is
the default one for
billing

boolean is_default_shipping\ True if the address is
the default one for
shipping

---

### [Examples](#customer_address.list-Examples)

### [Request Example SOAP V1](#customer_address.list-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'customer_address.list', '2');
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#customer_address.list-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->customerAddressList($sessionId, '2');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#customer_address.list-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->customerAddressList((object)array('sessionId' => $sessionId->result, 'customerId' => '2'));
var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#customer_address.list-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
    array
      'customer_address_id' => string '2' (length=1)
      'created_at' => string '2012-03-29 13:20:08' (length=19)
      'updated_at' => string '2012-03-29 13:39:29' (length=19)
      'city' => string 'Las Vegas' (length=9)
      'country_id' => string 'US' (length=2)
      'firstname' => string 'johny' (length=5)
      'lastname' => string 'doe' (length=3)
      'postcode' => string '89032' (length=5)
      'region' => string 'Nevada' (length=6)
      'region_id' => string '39' (length=2)
      'street' => string '3406 Hiney Road' (length=15)
      'telephone' => string '702-283-9556' (length=12)
      'is_default_billing' => boolean false
      'is_default_shipping' => boolean true
  1 =>
    array
      'customer_address_id' => string '3' (length=1)
      'created_at' => string '2012-03-29 13:39:29' (length=19)
      'updated_at' => string '2012-03-29 13:39:29' (length=19)
      'city' => string 'Corpus Christi' (length=14)
      'country_id' => string 'US' (length=2)
      'firstname' => string 'johny' (length=5)
      'lastname' => string 'doe' (length=3)
      'postcode' => string '78476' (length=5)
      'region' => string 'Texas' (length=5)
      'region_id' => string '57' (length=2)
      'street' => string '3672 Boone Street' (length=17)
      'telephone' => string '361-280-8488' (length=12)
      'vat_id' => string 'GB999 9999 73' (length=13)
      'is_default_billing' => boolean false
      'is_default_shipping' => boolean false
```

</div>


