

## [Module: Mage_Customer](#customer_address.info-Module:MageCustomer)

### [Resource: customer_address](#customer_address.info-Resource:customeraddress)

## [Method:](#customer_address.info-Method:)

-   customer_address.info (SOAP V1)
-   customerAddressInfo (SOAP V2)

Retrieve information about the required customer address.

**Arguments:**

Type Name Description

---

string sessionId Session ID
int addressId Address ID

**Returns**:

Type Name Description

---

array info Array of customerAddressEntityItem

The **customerAddressEntityItem** content is as follows:

---

Type Name Description

---

int customer_address_id\ ID of the customer
address

string created_at\ Date when the address
was created

string updated_at\ Date when the address
was updated

string increment_id\ Increment ID

string city\ name of the city

string company\ Name of the company

string country_id\ ID of the country

string fax\ Fax

string firstname\ Customer first name

string lastname\ Customer last name

string middlename\ Customer middle name

string postcode\ Customer postcode

string prefix\ Customer prefix

string region\ Name of the region

int region_id\ Region ID

string street\ Name of the street

string suffix\ Customer suffix

string telephone\ Telephone number

boolean is_default_billing\ True if the address is
the default one for
billing

boolean is_default_shipping\ True if the address is
the default one for
shipping

---

### [Examples](#customer_address.info-Examples)

### [Request Example SOAP V1](#customer_address.info-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'customer_address.info', '2');
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#customer_address.info-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->customerAddressInfo($sessionId, '2');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#customer_address.info-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->customerAddressInfo((object)array('sessionId' => $sessionId->result, 'addressId' => '2'));
var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#customer_address.info-ResponseExampleSOAPV1)

<div>

```
array
  'customer_address_id' => string '2' (length=1)
  'created_at' => string '2012-03-29 13:20:08' (length=19)
  'updated_at' => string '2012-03-29 13:20:08' (length=19)
  'increment_id' => null
  'city' => string 'Las Vegas' (length=9)
  'company' => null
  'country_id' => string 'US' (length=2)
  'fax' => null
  'firstname' => string 'johny' (length=5)
  'lastname' => string 'doe' (length=3)
  'middlename' => null
  'postcode' => string '89032' (length=5)
  'prefix' => null
  'region' => string 'Nevada' (length=6)
  'region_id' => string '39' (length=2)
  'street' => string '3406 Hiney Road' (length=15)
  'suffix' => null
  'telephone' => string '702-283-9556' (length=12)
  'vat_id' => null
  'vat_is_valid' => null
  'vat_request_date' => null
  'vat_request_id' => null
  'vat_request_success' => null
  'is_default_billing' => boolean false
  'is_default_shipping' => boolean true
```

</div>


