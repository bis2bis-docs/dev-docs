

## [Module: Mage_Customer](#customer_address.create-Module:MageCustomer)

### [Resource: customer_address](#customer_address.create-Resource:customeraddress)

## [Method:](#customer_address.create-Method:)

-   customer_address.create (SOAP V1)
-   customerAddressCreate (SOAP V2)

Create a new address for the customer

**Arguments:**

Type Name Description

---

string sessionId Session ID
int customerId Customer ID
array addressdata Array of customerAddressEntityCreate

**Returns**:

Type Name Description

---

int result ID of the created customer address

The **customerAddressEntityCreate** content is as follows:

---

Type Name Description

---

string city\ Name of the city

string\ company\ Name of the company

string\ country_id\ Country ID

string\ fax\ Fax

string\ firstname\ Customer first name

string\ lastname\ Customer last name

string\ middlename\ Customer middle name

string\ postcode\ Postcode

string\ prefix\ Customer prefix

int region_id\ ID of the region

string\ region\ Name of the region

ArrayOfString street\ Array of street
addresses

string\ suffix\ Customer suffix

string\ telephone\ Telephone number

boolean is_default_billing\ True if the address is
the default one for
billing

boolean is_default_shipping\ True if the address is
the default one for
shipping

---

**Note**: If you want to leave any address fields empty, specify them as
empty ones in the request body.

### [Examples](#customer_address.create-Examples)

### [Request Example SOAP V1](#customer_address.create-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call(
$session,
'customer_address.create',
array('customerId' => 2, 'addressdata' => array('firstname' => 'John', 'lastname' => 'Doe', 'street' => array('Street line 1', 'Streer line 2'), 'city' => 'Weaverville', 'country_id' => 'US', 'region' => 'Texas', 'region_id' => 3, 'postcode' => '96093', 'telephone' => '530-623-2513', 'is_default_billing' => FALSE, 'is_default_shipping' => FALSE)));
var_dump ($result);
```

</div>



### [Request Example SOAP V2](#customer_address.create-RequestExampleSOAPV2)

<div>

```
$client = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

// If some stuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');
$result = $client->customerAddressCreate($session, '2', array('firstname' => 'John', 'lastname' => 'Doe', 'street' => array('Street line 1', 'Streer line 2'), 'city' => 'Weaverville', 'country_id' => 'US', 'region' => 'Texas', 'region_id' => 3, 'postcode' => '96093', 'telephone' => '530-623-2513', 'is_default_billing' => FALSE, 'is_default_shipping' => FALSE));

var_dump ($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#customer_address.create-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->customerAddressCreate((object)array('sessionId' => $sessionId->result, 'customerId' => '2', 'addressData' => ((object)array(
'city' => 'Weaverville',
'country_id' => 'US',
'postcode' => '96093',
'region' => 'Texas',
'street' => array('Street line 1', 'Streer line 2'),
'telephone' => '847-431-7700',
'lastname' => 'Doe',
'firstname' => 'John',
'is_default_billing' => true
))));
var_dump($result->result);
```

</div>


