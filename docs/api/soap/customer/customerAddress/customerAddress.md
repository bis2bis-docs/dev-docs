

### [Customer Address](#CustomerAddress-CustomerAddress)

Allows you to create, retrieve, update, and delete address data for a
required customer.

**Resource Name**: customer_address

**Methods**:

-   [customer_address.list](customer_address.list.html "customer_address.list") -
    Retrieve the list of customer addresses
-   [customer_address.create](customer_address.create.html "customer_address.create") -
    Create a new address for a customer
-   [customer_address.info](customer_address.info.html "customer_address.info") -
    Retrieve the specified customer address
-   [customer_address.update](customer_address.update.html "customer_address.update") -
    Update the customer address
-   [customer_address.delete](customer_address.delete.html "customer_address.delete") -
    Delete the customer address

### [Faults](#CustomerAddress-Faults)

Fault Code Fault Message

---

100 Invalid address data. Details in error message.
101 Customer not exists.
102 Address not exists.
103 Address not deleted. Details in error message.

### [Examples](#CustomerAddress-Examples)

### [Example 1. Working with customer address](#CustomerAddress-Example1.Workingwithcustomeraddress)

<div>

```
$proxy = new SoapClient('http://magentohost/api/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');


// Create new customer
$newCustomer = array(
    'firstname'  => 'First',
    'lastname'   => 'Last',
    'email'      => 'test@example.com',
    'password'   => 'password',
    'store_id'   => 0,
    'website_id' => 0
);

$newCustomerId = $proxy->call($sessionId, 'customer.create', array($newCustomer));

//Create new customer address
$newCustomerAddress = array(
    'firstname'  => 'First',
    'lastname'   => 'Last',
    'country_id' => 'USA',
    'region_id'  => '43',
    'region'     => 'New York',
    'city'       => 'New York',
    'street'     => array('street1','street2'),
    'telephone'  => '5555-555',
    'postcode'   => 10021,

    'is_default_billing'  => true,
    'is_default_shipping' => true
);

$newAddressId = $proxy->call($sessionId, 'customer_address.create', array($newCustomerId, $newCustomerAddress));

var_dump($proxy->call($sessionId, 'customer_address.list', $newCustomerId));

//Update customer address
$proxy->call($sessionId, 'customer_address.update', array($newAddressId, array('firstname'=>'Changed Firstname')));

var_dump($proxy->call($sessionId, 'customer_address.list', $newCustomerId));

// Delete customer address
$proxy->call($sessionId, 'customer_address.delete', $newAddressId);

var_dump($proxy->call($sessionId, 'customer_address.list', $newCustomerId));
```

</div>



Create the Magento file system owner
