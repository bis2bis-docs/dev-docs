

## [Module: Mage_Customer](#customer.update-Module:MageCustomer)

### [Resource: customer](#customer.update-Resource:customer)

## [Method:](#customer.update-Method:)

-   customer.update (SOAP V1)
-   customerCustomerUpdate (SOAP V2)

Update information about the required customer. Note that you need to
pass only those arguments which you want to be updated.

**Arguments:**

Type Name Description

---

string sessionId Session ID
int customerId Customer ID
array customerData Array of customerCustomerEntityToCreate

**Returns**:

Type Description

---

boolean True if the customer is updated

The **customerCustomerEntityToCreate** content is as follows:

Type Name Description

---

int customer_id Customer ID
string email Customer email
string firstname Customer first name
string lastname Customer last name
string password Customer password
int group_id Group ID
string prefix Customer prefix
string suffix Customer suffix
string dob Customer date of birth
string taxvat Customer tax/VAT number
int gender Customer gender: 1 - Male, 2 - Female
string middlename Customer middle name/initial

### [Examples](#customer.update-Examples)

### [Request Example SOAP V1](#customer.update-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'customer.update', array('customerId' => '2', 'customerData' => array('firstname' => 'John', 'lastname' => 'Doe', 'email' => 'test@example.com', 'password' => 'john22')));
var_dump ($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#customer.update-RequestExampleSOAPV2)

<div>

```
$client = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

// If some stuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');
$result = $client->customerCustomerUpdate($session, '2', array('email' => 'customer-mail@example.org', 'firstname' => 'Dough', 'lastname' => 'Deekson', 'password' => 'password', 'website_id' => 1, 'store_id' => 1, 'group_id' => 1));

var_dump ($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#customer.update-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->customerCustomerUpdate((object)array('sessionId' => $sessionId->result, 'customerId' => '2', 'customerData' =>  ((object)array(
'email' => 'customer-mail@example.org',
'firstname' => 'Dough',
'lastname' => 'Deekson'
))));
var_dump($result->result);
```

</div>


