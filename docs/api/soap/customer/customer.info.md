

## [Module: Mage_Customer](#customer.info-Module:MageCustomer)

Allows you to export/import customers from/to Magento.

### [Resource: customer](#customer.info-Resource:customer)

## [Method:](#customer.info-Method:)

-   customer.info (SOAP V1)
-   customerCustomerInfo (SOAP V2)

Retrieve information about the specified customer.

**Arguments:**

Type Name Description

---

string sessionId Session ID
int customerId ID of the required customer
ArrayOfString attributes Array of attributes

-   attributes (optional depending on the version) - only specified
    attributes will be returned. The customer_id value is always
    returned.

**Returns:**

Type Name Description

---

array customerInfo Array of customerCustomerEntity

The **customerCustomerEntity** content is as follows:

Type Name Description

---

int customer_id ID of the customer
string created_at Date when the customer was created
string updated_at Date when the customer was updated
string increment_id Increment ID
int store_id Store ID
int website_id Website ID
string created_in Store view the customer was created in
string email Customer email
string firstname Customer first name
string middlename Customer middle name
string lastname Customer last name
int group_id Customer group ID
string prefix Customer prefix
string suffix Customer suffix
string dob Customer date of birth
string taxvat Tax/VAT number
boolean confirmation Confirmation flag
string password_hash Password hash
string rp_token Reset password token
string rp_token_created_at Date when the password was reset

### [Examples](#customer.info-Examples)

### [Request Example SOAP V1](#customer.info-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'customer.info', '2');
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#customer.info-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->customerCustomerInfo($sessionId, '2');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#customer.info-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->customerCustomerInfo((object)array('sessionId' => $sessionId->result, 'customerId' => '2'));
var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#customer.info-ResponseExampleSOAPV1)

<div>

```
array
  'customer_id' => string '2' (length=1)
  'created_at' => string '2012-03-29 12:37:23' (length=19)
  'updated_at' => string '2012-03-30 12:59:21' (length=19)
  'increment_id' => null
  'store_id' => string '2' (length=1)
  'website_id' => string '2' (length=1)
  'confirmation' => null
  'created_in' => string 'English' (length=7)
  'default_billing' => null
  'default_shipping' => string '2' (length=1)
  'disable_auto_group_change' => string '0' (length=1)
  'dob' => null
  'email' => string 'john@example.com' (length=16)
  'firstname' => string 'johny' (length=5)
  'gender' => null
  'group_id' => string '1' (length=1)
  'lastname' => string 'doe' (length=3)
  'middlename' => null
  'password_hash' => string 'cccfb3ecf54c9644a34106783148eff2:sp' (length=35)
  'prefix' => null
  'rp_token' => string '15433dd072f1f4e5aae83231b93f72d0' (length=32)
  'rp_token_created_at' => string '2012-03-30 15:10:31' (length=19)
  'suffix' => null
  'taxvat' => null
```

</div>


