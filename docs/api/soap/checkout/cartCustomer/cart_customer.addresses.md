

## [Mage_Checkout](#cart_customer.addresses-MageCheckout)

### [Module: Shopping Cart API](#cart_customer.addresses-Module:ShoppingCartAPI)

## [Resource: cart_customer](#cart_customer.addresses-Resource:cartcustomer)

## [Method:](#cart_customer.addresses-Method:)

-   cart_customer.addresses (SOAP V1)
-   shoppingCartCustomerAddresses (SOAP V2)

Allows you to set the customer addresses in the shopping cart (quote).

**Arguments:**

Type Name Description

---

string sessionId Session ID
int quoteId Shopping cart ID
array customerAddressData Array of shoppingCartCustomerAddressEntity
string store Store view ID or code (optional)

**Return:**

Type Name Description

---

boolean result True if the address is set

The **shoppingCartCustomerAddressEntity** content is as follows:

---

Type Name Description

---

string mode\ Mode: billing or
shipping

string\ address_id\ Address ID

string\ firstname\ Customer first name

string\ lastname\ Customer last name

string\ company\ Company name

string\ street\ Street

string\ city\ City

string\ region\ Region

string\ region_id\ Region ID

string\ postcode\ Post code

string\ country_id\ Country ID

string\ telephone\ Telephone number

string\ fax\ Fax number

int is_default_billing\ Defines whether the
address is a default
billing address

int is_default_shipping\ Defines whether the
address is a default
shipping address

---

**Faults:**
_No Faults._

### [Examples](#cart_customer.addresses-Examples)

### [Request Example SOAP V1](#cart_customer.addresses-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$shoppingCartId = $proxy->call( $sessionId, 'cart.create', array( 'magento_store' ) );

$arrAddresses = array(
    array(
        "mode" => "shipping",
        "firstname" => "testFirstname",
        "lastname" => "testLastname",
        "company" => "testCompany",
        "street" => "testStreet",
        "city" => "testCity",
        "region" => "testRegion",
        "postcode" => "testPostcode",
        "country_id" => "id",
        "telephone" => "0123456789",
        "fax" => "0123456789",
        "is_default_shipping" => 0,
        "is_default_billing" => 0
    ),
    array(
        "mode" => "billing",
        "address_id" => "customer_address_id"
    )
);

$resultCustomerAddresses = $proxy->call(
    $sessionId,
    "cart_customer.addresses",
    array(
        $shoppingCartId,
        $arrAddresses,
    )
);
```

</div>



### [Request Example SOAP V2](#cart_customer.addresses-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); 
 
$sessionId = $proxy->login('apiUser', 'apiKey'); 
  
$result = $proxy->shoppingCartCustomerAddresses($sessionId, 10, array(array(
'mode' => 'billing',
'firstname' => 'first name',
'lastname' => 'last name',
'street' => 'street address',
'city' => 'city',
'region' => 'region',
'postcode' => 'postcode',
'country_id' => 'US',
'telephone' => '123456789',
'is_default_billing' => 1
)));   
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#cart_customer.addresses-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->shoppingCartCustomerAddresses((object)array('sessionId' => $sessionId->result, 'quoteId' => 10, 'customerAddressData' => array(array(
'mode' => 'billing',
'firstname' => 'first name',
'lastname' => 'last name',
'street' => 'street address',
'city' => 'city',
'region' => 'region',
'postcode' => 'postcode',
'country_id' => 'US',
'telephone' => '123456789',
'is_default_billing' => 1
))));
var_dump($result->result);
```

</div>


