

## [Mage_Checkout](#cart_customer.set-MageCheckout)

### [Module: Shopping Cart API](#cart_customer.set-Module:ShoppingCartAPI)

## [Resource: cart_customer](#cart_customer.set-Resource:cartcustomer)

## [Method:](#cart_customer.set-Method:)

-   cart_customer.set (SOAP V1)
-   shoppingCartCustomerSet (SOAP V2)

Allows you to add information about the customer to a shopping cart
(quote).

**Arguments:**

Type Name Description

---

string sessionId Session ID
int quoteId Shopping cart ID
array customerData Array of shoppingCartCustomerEntity
string store Store view ID or code (optional)

**Return:**

Type Name Description

---

boolean result True if information is added

The **shoppingCartCustomerEntity** content is as follows:

---

Type Name Description

---

string mode\ Customer mode

int customer_id\ Customer ID

string email\ Customer email address

string\ firstname\ Customer first name

string\ lastname\ Customer last name

string\ password\ Customer password

string\ confirmation\ Confirmation flag

int website_id\ Website ID

int store_id\ Store ID

int group_id\ Group ID

---

**Faults:**
_No Faults._

### [Examples](#cart_customer.set-Examples)

### [Request Example SOAP V1](#cart_customer.set-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$shoppingCartId = $proxy->call( $sessionId, 'cart.create', array( 'magento_store' ) );
$customerAsGuest = array(
    "firstname" => "testFirstname",
    "lastname" => "testLastName",
    "email" => "testEmail",
    "website_id" => "0",
    "store_id" => "0",
    "mode" => "guest"
);
$resultCustomerSet = $proxy->call($sessionId, 'cart_customer.set', array( $shoppingCartId, $customerAsGuest) );
```

</div>



### [Request Example SOAP V2](#cart_customer.set-RequestExampleSOAPV2)

<div>

```
$client = new SoapClient('http://magentohost/api/v2_soap/?wsdl');
$session = $client->login('apiUser', 'apiKey');

$quoteId = $client->shoppingCartCreate($session);

$customerData = array(
    "firstname" => "testFirstname",
    "lastname" => "testLastName",
    "email" => "testEmail@mail.com",
    "mode" => "guest",
 "website_id" => "0"
      );

$resultCustomerSet = $client->shoppingCartCustomerSet($session, $quoteId, $customerData);
```

</div>


