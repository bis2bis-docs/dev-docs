

## [Module: Mage_Checkout](#MAPI-CartCustomer-Module:MageCheckout)

The Mage_Checkout module allows you to manage shopping carts and the
checkout process. This module allows you to create an order once filling
the shopping cart is complete.

### [Cart Customer](#MAPI-CartCustomer-CartCustomer)

Allows you to add customer information and addresses into a shopping
cart.

**Resource Name**: cart_customer

**Methods**:

-   [cart_customer.set](cart_customer.set.html "cart_customer.set") -
    Add customer information into a shopping cart
-   [cart_customer.addresses](cart_customer.addresses.html "cart_customer.addresses") -
    Set the customer addresses (shipping and billing) into a shopping
    cart

### [Faults](#MAPI-CartCustomer-Faults)

Fault Code Fault Message

---

1001 Can not make operation because store is not exists
1002 Can not make operation because quote is not exists
1041 Customer is not set.
1042 The customer's identifier is not valid or customer is not existed
1043 Customer could not be created.
1044 Customer data is not valid.
1045 Customer's mode is unknown
1051 Customer address data is empty.
1052 Customer's address data is not valid.
1053 The customer's address identifier is not valid
1054 Customer address is not set.
1055 Customer address identifier do not belong customer, which set in quote

Create the Magento file system owner
