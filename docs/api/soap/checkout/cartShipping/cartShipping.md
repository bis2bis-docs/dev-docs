

## [Module: Mage_Checkout](#MAPI-CartShipping-Module:MageCheckout)

The Mage_Checkout module allows you to manage shopping carts and the
checkout process. This module allows you to create an order once filling
the shopping cart is complete.

### [Cart Shipping](#MAPI-CartShipping-CartShipping)

Allows you to retrieve and set shipping methods for a shopping cart.

**Resource Name**: cart_shipping

**Methods**:

-   [cart_shipping.method](cart_shipping.method.html "cart_shipping.method") -
    Set a shipping method for a shopping cart
-   [cart_shipping.list](cart_shipping.list.html "cart_shipping.list") -
    Retrieve the list of available shipping methods for a shopping cart

### [Faults](#MAPI-CartShipping-Faults)

Fault Code Fault Message

---

1001 Can not make operation because store is not exists
1002 Can not make operation because quote is not exists
1061 Can not make operation because of customer shipping address is not set
1062 Shipping method is not available
1063 Can not set shipping method.
1064 Can not receive list of shipping methods.

Create the Magento file system owner
