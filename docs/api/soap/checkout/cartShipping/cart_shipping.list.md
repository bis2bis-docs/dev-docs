

## [Mage_Checkout](#cart_shipping.list-MageCheckout)

### [Module: Shopping Cart API](#cart_shipping.list-Module:ShoppingCartAPI)

## [Resource: cart_shipping](#cart_shipping.list-Resource:cartshipping)

## [Method:](#cart_shipping.list-Method:)

-   cart_shipping.list (SOAP V1)
-   shoppingCartShippingList (SOAP V2)

Allows you to retrieve the list of available shipping methods for a
shopping cart (quote).

**Arguments:**

Type Name Description

---

string sessionId Session ID
int quoteId Shopping cart ID
string storeId Store view ID or code (optional)

**Returns:**

Type Name Description

---

array result Array of shoppingCartShippingMethodEntity

The **shoppingCartShippingMethodEntity** content is as follows:

---

Type Name Description

---

string code\ Code

string\ carrier\ Carrier

string\ carrier_title\ Carrier title

string\ method\ Shipping method

string\ method_title\ Shipping method title

string\ method_description\ Shipping method
description

double price\ Shipping price

---

**Faults:**
_No Faults._

### [Examples](#cart_shipping.list-Examples)

### [Request Example SOAP V1](#cart_shipping.list-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');

$sessionId = $proxy->login('apiUser', 'apiKey');

$result = $proxy->call($sessionId, 'cart_shipping.list', 10);


var_dump($result);
```

</div>



### [Request Example SOAP V2](#cart_shipping.list-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login('apiUser', 'apiKey');

$result = $proxy->shoppingCartShippingList($sessionId, 10);

var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#cart_shipping.list-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->shoppingCartShippingList((object)array('sessionId' => $sessionId->result, 'quoteId' => 10));
var_dump($result->result);
```

</div>


