

## [Mage_Checkout](#cart_shipping.method-MageCheckout)

### [Module: Shopping Cart API](#cart_shipping.method-Module:ShoppingCartAPI)

## [Resource: cart_shipping](#cart_shipping.method-Resource:cartshipping)

## [Method:](#cart_shipping.method-Method:)

-   cart_shipping.method (SOAP V1)
-   shoppingCartShippingMethod (SOAP V2)

Allows you to set a shipping method for a shopping cart (quote).

**Arguments:**

---

Type Name Description

---

string sessionId\ Session ID

int quoteId Shopping cart ID

string method Shipping method code

string storeId Store view ID or code
(optional)

---

**Return:**

Type Name Description

---

boolean result True if the shipping method is set

**Faults:**
_No Faults._

### [Examples](#cart_shipping.method-Examples)

### [Request Example SOAP V1](#cart_shipping.method-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');

$sessionId = $proxy->login('apiUser', 'apiKey');

$result = $proxy->call($sessionId, 'cart_shipping.method', array(10, 'freeshipping_freeshipping'));


var_dump($result);
```

</div>



### [Request Example SOAP V2](#cart_shipping.method-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login('apiUser', 'apiKey');

$result = $proxy->shoppingCartShippingMethod($sessionId, 10, 'freeshipping_freeshipping');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#cart_shipping.method-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->shoppingCartShippingMethod((object)array('sessionId' => $sessionId->result, 'quoteId' => 10, 'shippingMethod' => 'freeshipping_freeshipping'));
var_dump($result->result);
```

</div>


