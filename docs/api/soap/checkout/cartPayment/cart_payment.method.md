

## [Mage_Checkout](#cart_payment.method-MageCheckout)

### [Module: Shopping Cart API](#cart_payment.method-Module:ShoppingCartAPI)

## [Resource: cart_payment](#cart_payment.method-Resource:cartpayment)

## [Method:](#cart_payment.method-Method:)

-   cart_payment.method (SOAP V1)
-   shoppingCartPaymentMethod (SOAP V2)

Allows you to set a payment method for a shopping cart (quote).

**Arguments:**

Type Name Description

---

string sessionId Session ID
int quoteId Shopping cart ID
array method Array of shoppingCartPaymentMethodEntity
string store Store view ID or code (optional)

**Return:**

Type Description

---

boolean True on success

The **shoppingCartPaymentMethodEntity** content is as follows:

---

Type Name Description

---

string po_number\ Purchase order number

string\ method\ Payment method

string\ cc_cid\ Credit card CID

string\ cc_owner\ Credit card owner

string\ cc_number\ Credit card number

string\ cc_type\ Credit card type

string\ cc_exp_year\ Credit card expiration
year

string\ cc_exp_month\ Credit card expiration
month

---

**Faults:**
_No Faults._

### [Examples](#cart_payment.method-Examples)

### [Request Example SOAP V1](#cart_payment.method-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$shoppingCartId = $proxy->call( $sessionId, 'cart.create', array( 'magento_store' ) );

$paymentMethod = array(
    "method" => "checkmo"
);

$resultPaymentMethod = $proxy->call(
    $sessionId,
    "cart_payment.method",
    array(
        $shoppingCartId,
        $paymentMethod
    )
);
```

</div>



### [Request Example SOAP V2](#cart_payment.method-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login('apiUser', 'apiKey');

$result = $proxy->shoppingCartPaymentMethod($sessionId, 10, array(
'po_number' => null,
'method' => 'checkmo',
'cc_cid' => null,
'cc_owner' => null,
'cc_number' => null,
'cc_type' => null,
'cc_exp_year' => null,
'cc_exp_month' => null
));

var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#cart_payment.method-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->shoppingCartPaymentMethod((object)array('sessionId' => $sessionId->result, 'quoteId' => 10, 'method' => array(
'po_number' => null,
'method' => 'checkmo',
'cc_cid' => null,
'cc_owner' => null,
'cc_number' => null,
'cc_type' => null,
'cc_exp_year' => null,
'cc_exp_month' => null
)));

var_dump($result->result);
```

</div>


