

## [Module: Mage_Checkout](#MAPI-CartPayment-Module:MageCheckout)

The Mage_Checkout module allows you to manage shopping carts and the
checkout process. This module allows you to create an order once filling
the shopping cart is complete.

### [Cart Payment](#MAPI-CartPayment-CartPayment)

Allows you to retrieve and set payment methods for a shopping cart.

**Resource Name**: cart_payment

**Methods**:

-   [cart_payment.method](cart_payment.method.html "cart_payment.method") -
    Set a payment method for a shopping cart
-   [cart_payment.list](cart_payment.list.html "cart_payment.list") -
    Get the list of available payment methods for a shopping cart

### [Faults](#MAPI-CartPayment-Faults)

Fault Code Fault Message

---

1001 Can not make operation because store is not exists
1002 Can not make operation because quote is not exists
1071 Payment method data is empty.
1072 Customer's billing address is not set. Required for payment method data.
1073 Customer's shipping address is not set. Required for payment method data.
1074 Payment method is not allowed
1075 Payment method is not set.

Create the Magento file system owner
