

## [Mage_Checkout](#cart_payment.list-MageCheckout)

### [Module: Shopping Cart API](#cart_payment.list-Module:ShoppingCartAPI)

## [Resource: cart_payment](#cart_payment.list-Resource:cartpayment)

## [Method:](#cart_payment.list-Method:)

-   cart_payment.list (SOAP V1)
-   shoppingCartPaymentList (SOAP V2)

Allows you to retrieve a list of available payment methods for a
shopping cart (quote).

**Arguments:**

---

Type Name Description

---

string sessionId Session ID

int quoteId\ Shopping cart ID

string store\ Store view ID or code
(optional)

---

**Return:**

Type Name Description

---

array result Array of shoppingCartPaymentMethodResponseEntity

The **shoppingCartPaymentMethodResponseEntity** content is as follows:

---

Type Name Description

---

string code\ Payment method code

string title\ Payment method title

associativeArray cc_types\ Array of credit card
types

---

### [Examples](#cart_payment.list-Examples)

### [Request Example SOAP V1](#cart_payment.list-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'cart_payment.list', 'quoteId');
var_dump($result);
```

</div>



### [Response Example SOAP V1](#cart_payment.list-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
    array
      'code' => string 'checkmo' (length=7)
      'title' => string 'Check / Money order' (length=19)
      'ccTypes' => null
  1 =>
    array
      'code' => string 'ccsave' (length=6)
      'title' => string 'Credit Card (saved)' (length=19)
      'ccTypes' =>
        array
          'AE' => string 'American Express' (length=16)
          'VI' => string 'Visa' (length=4)
          'MC' => string 'MasterCard' (length=10)
          'DI' => string 'Discover' (length=8)
```

</div>



Create the Magento file system owner
