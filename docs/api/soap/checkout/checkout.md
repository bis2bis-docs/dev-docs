

## [Module: Mage_Checkout](#Mage_Checkout-Module:MageCheckout)

The Mage_Checkout module allows you to manage shopping carts and the
checkout process. This module allows you to create an order once filling
the shopping cart is complete.

### [Cart Coupon](#Mage_Checkout-CartCoupon)

Allows you to add and remove coupon codes for a shopping cart.

**Resource Name**: cart_coupon

**Methods**:

-   [cart_coupon.add](cartCoupon/cart_coupon.add.html "cart_coupon.add") -
    Add a coupon code to a quote
-   [cart_coupon.remove](cartCoupon/cart_coupon.remove.html "cart_coupon.remove") -
    Remove a coupon code from a quote

### [Cart Customer](#Mage_Checkout-CartCustomer)

Allows you to add customer information and addresses into a shopping
cart.

**Resource Name**: cart_customer

**Methods**:

-   [cart_customer.set](cartCustomer/cart_customer.set.html "cart_customer.set") -
    Add customer information into a shopping cart
-   [cart_customer.addresses](cartCustomer/cart_customer.addresses.html "cart_customer.addresses") -
    Set the customer addresses (shipping and billing) into a shopping
    cart

### [Cart Payment](#Mage_Checkout-CartPayment)

Allows you to retrieve and set payment methods for a shopping cart.

**Resource Name**: cart_payment

**Methods**:

-   [cart_payment.method](cartPayment/cart_payment.method.html "cart_payment.method") -
    Set a payment method for a shopping cart
-   [cart_payment.list](cartPayment/cart_payment.list.html "cart_payment.list") -
    Get the list of available payment methods for a shopping cart

### [Cart Product](#Mage_Checkout-CartProduct)

Allows you to manage products in a shopping cart.

**Resource Name**: cart_product

**Methods**:

-   [cart_product.add](cartProduct/cart_product.add.html "cart_product.add") -
    Add one or more products to a shopping cart
-   [cart_product.update](cartProduct/cart_product.update.html "cart_product.update") -
    Update one or more products in a shopping cart
-   [cart_product.remove](cartProduct/cart_product.remove.html "cart_product.remove") -
    Remove one or more products from a shopping cart
-   [cart_product.list](cartProduct/cart_product.list.html "cart_product.list") -
    Get a list of products in a shopping cart
-   [cart_product.moveToCustomerQuote](cartProduct/cart_product.moveToCustomerQuote.html "cart_product.moveToCustomerQuote") -
    Move one or more products from the quote to the customer shopping
    cart

### [Cart Shipping](#Mage_Checkout-CartShipping)

Allows you to retrieve and set shipping methods for a shopping cart.

**Resource Name**: cart_shipping

**Methods**:

-   [cart_shipping.method](cartShipping/cart_shipping.method.html "cart_shipping.method") -
    Set a shipping method for a shopping cart
-   [cart_shipping.list](cartShipping/cart_shipping.list.html "cart_shipping.list") -
    Retrieve the list of available shipping methods for a shopping cart

### [Shopping Cart](#Mage_Checkout-ShoppingCart)

Allows you to manage shopping carts.

**Resource Name**: cart

**Methods**:

-   [cart.create](cart/cart.create.html "cart.create") - Create a blank
    shopping cart
-   [cart.order](cart/cart.order.html "cart.order") - Create an order
    from a shopping cart
-   [cart.info](cart/cart.info.html "cart.info") - Get full information
    about the current shopping cart
-   [cart.totals](cart/cart.totals.html "cart.totals") - Get all
    available prices for items in shopping cart, using additional
    parameters
-   [cart.license](cart/cart.license.html "cart.license") - Get website
    license agreement
