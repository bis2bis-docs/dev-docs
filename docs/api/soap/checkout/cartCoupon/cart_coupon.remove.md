

## [Mage_Checkout](#cart_coupon.remove-MageCheckout)

### [Module: Shopping Cart API](#cart_coupon.remove-Module:ShoppingCartAPI)

## [Resource: cart_coupon](#cart_coupon.remove-Resource:cartcoupon)

## [Method:](#cart_coupon.remove-Method:)

-   cart_coupon.remove (SOAP V1)
-   shoppingCartCouponRemove (SOAP V2)

Allows you to remove a coupon code from a shopping cart (quote).

**Arguments:**

---

Type Name Description

---

string sessionId\ Session ID

int quoteId Shopping cart ID

string store Store view ID or code
(optional)

---

**Return:**

Type Description

---

boolean True if the coupon code is removed

**Faults:**
_No Faults._

### [Examples](#cart_coupon.remove-Examples)

### [Request Example SOAP V1](#cart_coupon.remove-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$shoppingCartId = $proxy->call( $sessionId, 'cart.create', array( 'magento_store' ) );
$resultCartCouponRemove = $proxy->call(
    $sessionId,
    "cart_coupon.remove",
    array(
        $shoppingCartId
    )
);
```

</div>



### [Request Example SOAP V2](#cart_coupon.remove-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->shoppingCartCouponRemove($sessionId, '15');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#cart_coupon.remove-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->shoppingCartCouponRemove((object)array('sessionId' => $sessionId->result, 'quoteId' => 15, 'store' => '3'));
var_dump($result->result);
```

</div>


