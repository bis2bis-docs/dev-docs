

## [Module: Mage_Checkout](#MAPI-CartCoupon-Module:MageCheckout)

The Mage_Checkout module allows you to manage shopping carts and the
checkout process. This module allows you to create an order once filling
the shopping cart is complete.

### [Cart Coupon](#MAPI-CartCoupon-CartCoupon)

Allows you to add and remove coupon codes for a shopping cart.

**Resource Name**: cart_coupon

**Methods**:

-   [cart_coupon.add](cart_coupon.add.html "cart_coupon.add") - Add a
    coupon code to a quote
-   [cart_coupon.remove](cart_coupon.remove.html "cart_coupon.remove") -
    Remove a coupon code from a quote

**Note**: In Magento, quotes and shopping carts are logically related,
but technically different. The shopping cart is a wrapper for a quote,
and it is used primarily by the frontend logic. The cart is represented
by the Mage_Checkout_Model_Cart class and the quote is represented by
the Mage_Sales_Model_Quote class.

### [Faults](#MAPI-CartCoupon-Faults)

Fault Code Fault Message

---

1001 Can not make operation because store is not exists
1002 Can not make operation because quote is not exists
1081 Coupon could not be applied because quote is empty.
1082 Coupon could not be applied.
1083 Coupon is not valid.
