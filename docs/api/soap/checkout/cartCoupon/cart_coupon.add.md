

## [Mage_Checkout](#cart_coupon.add-MageCheckout)

## [Module: Shopping Cart API](#cart_coupon.add-Module:ShoppingCartAPI)

## [Resource: cart_coupon](#cart_coupon.add-Resource:cartcoupon)

## [Method:](#cart_coupon.add-Method:)

-   cart_coupon.add (SOAP V1)
-   shoppingCartCouponAdd (SOAP V2)

Allows you to add a coupon code for a shopping cart (quote). The
shopping cart must not be empty.

**Arguments:**

---

Type Name Description

---

string sessionId\ Session ID

int quoteId Shopping cart ID

string couponCode Coupon code

string store Store view ID or code
(optional)

---

**Return:**

Type Description

---

boolean True if the coupon code is added

**Faults:**

### [Examples](#cart_coupon.add-Examples)

### [Request Example SOAP V1](#cart_coupon.add-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$shoppingCartId = $proxy->call( $sessionId, 'cart.create', array( 'magento_store' ) );
$couponCode = "aCouponCode";
$resultCartCouponRemove = $proxy->call(
    $sessionId,
    "cart_coupon.add",
    array(
        $shoppingCartId,
        $couponCode
    )
);
```

</div>



### [Request Example SOAP V2](#cart_coupon.add-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->shoppingCartCouponAdd($sessionId, '15', 'aCouponCode');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#cart_coupon.add-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->shoppingCartCouponAdd((object)array('sessionId' => $sessionId->result, 'quoteId' => 15, 'couponCode' => 'aCouponCode', 'store' => '3'));
var_dump($result->result);
```

</div>


