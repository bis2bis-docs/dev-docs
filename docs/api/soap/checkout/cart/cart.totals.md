

## [Mage_Checkout](#cart.totals-MageCheckout)

### [Module: Shopping Cart API](#cart.totals-Module:ShoppingCartAPI)

## [Resource: cart](#cart.totals-Resource:cart)

## [Method:](#cart.totals-Method:)

-   cart.totals (SOAP V1)
-   shoppingCartTotals (SOAP V2)

Allows you to retrieve total prices for a shopping cart (quote).

**Arguments:**

Type Name Description

---

string sessionId Session ID
int quoteId Shopping cart ID (quote identifier)
string store Store view ID or code (optional)

**Return:**

Type Name Description

---

array result Array of shoppingCartTotalsEntity

The **shoppingCartTotalsEntity** content is as follows:

---

Type Name Description

---

string title\ Title

float amount\ Total amount

---

### [Examples](#cart.totals-Examples)

### [Request Example SOAP V1](#cart.totals-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'cart.totals', '15');
var_dump ($result);
```

</div>



### [Request Example SOAP V2](#cart.totals-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->shoppingCartTotals($sessionId, '15');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#cart.totals-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->shoppingCartTotals((object)array('sessionId' => $sessionId->result, 'quoteId' => 15));
var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#cart.totals-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
    array
      'title' => string 'Subtotal' (length=8)
      'amount' => float 388.69
  1 =>
    array
      'title' => string '0 Reward points' (length=15)
      'amount' => float 0
  2 =>
    array
      'title' => string 'Gift Cards' (length=10)
      'amount' => float 0
  3 =>
    array
      'title' => string 'Store Credit' (length=12)
      'amount' => float 0
  4 =>
    array
      'title' => string 'Grand Total' (length=11)
      'amount' => float 388.69
  5 =>
    array
      'title' => null
      'amount' => null
```

</div>


