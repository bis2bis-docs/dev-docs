

## [Module: Mage_Checkout](#cart.create-Module:MageCheckout)

## [Resource: cart](#cart.create-Resource:cart)

## [Method:](#cart.create-Method:)

-   cart.create (SOAP V1)
-   shoppingCartCreate (SOAP V2)

Allows you to create an empty shopping cart.

**Arguments**:

---

Type Name Description

---

string sessionId\ Session ID

string storeId\ Store view ID or code
(optional)

---

**Returns**:

Type Description

---

int ID of the created empty shopping cart

**Faults:**
_No Faults_

### [Examples](#cart.create-Examples)

### [Request Example SOAP V1](#cart.create-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$shoppingCartIncrementId = $proxy->call( $sessionId, 'cart.create', array( 'magento_store' ) );
```

</div>



### [Request Example SOAP V2](#cart.create-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->shoppingCartCreate($sessionId, '3');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#cart.create-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->shoppingCartCreate((object)array('sessionId' => $sessionId->result, 'store' => '3'));

var_dump($result->result);
```

</div>


