

## [Module: Mage_Checkout](#MAPI-ShoppingCart-Module:MageCheckout)

The Mage_Checkout module allows you to manage shopping carts and the
checkout process. This module allows you to create an order once filling
the shopping cart is complete.

### [Shopping Cart](#MAPI-ShoppingCart-ShoppingCart)

Allows you to manage shopping carts.

**Resource Name**: cart

**Methods**:

-   [cart.create](cart.create.html "cart.create") - Create a blank
    shopping cart
-   [cart.order](cart.order.html "cart.order") - Create an order from a
    shopping cart
-   [cart.info](cart.info.html "cart.info") - Get full information about
    the current shopping cart
-   [cart.totals](cart.totals.html "cart.totals") - Get all available
    prices for items in shopping cart, using additional parameters
-   [cart.licenseAgreement](cart.license.html "cart.license") - Get
    website license agreement

### [Faults](#MAPI-ShoppingCart-Faults)

Fault Code Fault Message

---

1001 Can not make operation because store is not exists
1002 Can not make operation because quote is not exists
1003 Can not create a quote.
1004 Can not create a quote because quote with such identifier is already exists
1005 You did not set all required agreements
1006 The checkout type is not valid. Select single checkout type.
1007 Checkout is not available for guest
1008 Can not create an order.

### [Example](#MAPI-ShoppingCart-Example)

The following example illustrates the work with shopping cart (creation
of a shopping cart, setting customer and customer addresses, adding
products to the shopping cart, updating products in the shopping cart,
removing products from the shopping cart, getting the list of
products/shipping methods/payment methods, setting payment/shipping
methods,  adding/removing coupon, getting total prices/full information
about shopping cart/list of licenses, and creating an order.

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

// Create a quote, get quote identifier
$shoppingCartId = $proxy->call( $sessionId, 'cart.create', array( 'magento_store' ) );

// Set customer, for example guest
$customerAsGuest = array(
    "firstname" => "testFirstname",
    "lastname" => "testLastName",
    "email" => "testEmail",
    "website_id" => "0",
    "store_id" => "0",
    "mode" => "guest"
);
$resultCustomerSet = $proxy->call($sessionId, 'cart_customer.set', array( $shoppingCartId, $customerAsGuest) );

// Set customer addresses, for example guest's addresses
$arrAddresses = array(
    array(
        "mode" => "shipping",
        "firstname" => "testFirstname",
        "lastname" => "testLastname",
        "company" => "testCompany",
        "street" => "testStreet",
        "city" => "testCity",
        "region" => "testRegion",
        "postcode" => "testPostcode",
        "country_id" => "id",
        "telephone" => "0123456789",
        "fax" => "0123456789",
        "is_default_shipping" => 0,
        "is_default_billing" => 0
    ),
    array(
        "mode" => "billing",
        "firstname" => "testFirstname",
        "lastname" => "testLastname",
        "company" => "testCompany",
        "street" => "testStreet",
        "city" => "testCity",
        "region" => "testRegion",
        "postcode" => "testPostcode",
        "country_id" => "id",
        "telephone" => "0123456789",
        "fax" => "0123456789",
        "is_default_shipping" => 0,
        "is_default_billing" => 0
    )
);
$resultCustomerAddresses = $proxy->call($sessionId, "cart_customer.addresses", array($shoppingCartId, $arrAddresses));

// add products into shopping cart
$arrProducts = array(
    array(
        "product_id" => "1",
        "qty" => 2
    ),
    array(
        "sku" => "testSKU",
        "quantity" => 4
    )
);
$resultCartProductAdd = $proxy->call($sessionId, "cart_product.add", array($shoppingCartId, $arrProducts));

// update product in shopping cart
$arrProducts = array(
    array(
        "product_id" => "1",
        "qty" => 5
    ),
);
$resultCartProductUpdate = $proxy->call($sessionId, "cart_product.update", array($shoppingCartId, $arrProducts));

// remove products from shopping cart, for example by SKU
$arrProducts = array(
    array(
        "sku" => "testSKU"
    ),
);
$resultCartProductRemove = $proxy->call($sessionId, "cart_product.remove", array($shoppingCartId, $arrProducts));

// get list of products
$shoppingCartProducts = $proxy->call($sessionId, "cart_product.list", array($shoppingCartId));
print_r( $shoppingCartProducts );

// get list of shipping methods
$resultShippingMethods = $proxy->call($sessionId, "cart_shipping.list", array($shoppingCartId));
print_r( $resultShippingMethods );

// set shipping method
$randShippingMethodIndex = rand(1, count($resultShippingMethods) );
$shippingMethod = $resultShippingMethods[$randShippingMethodIndex]["code"];

$resultShippingMethod = $proxy->call($sessionId, "cart_shipping.method", array($shoppingCartId, $shippingMethod));

// get list of payment methods
$resultPaymentMethods = $proxy->call($sessionId, "cart_payment.list", array($shoppingCartId));
print_r($resultPaymentMethods);

// set payment method
$paymentMethod = array(
    "method" => "checkmo"
);
$resultPaymentMethod = $proxy->call($sessionId, "cart_payment.method", array($shoppingCartId, $paymentMethod));

// add coupon
$couponCode = "aCouponCode";
$resultCartCouponRemove = $proxy->call($sessionId, "cart_coupon.add", array($shoppingCartId, $couponCode));

// remove coupon
$resultCartCouponRemove = $proxy->call($sessionId, "cart_coupon.remove", array($shoppingCartId));

// get total prices
$shoppingCartTotals = $proxy->call($sessionId, "cart.totals", array($shoppingCartId));
print_r( $shoppingCartTotals );

// get full information about shopping cart
$shoppingCartInfo = $proxy->call($sessionId, "cart.info", array($shoppingCartId));
print_r( $shoppingCartInfo );

// get list of licenses
$shoppingCartLicenses = $proxy->call($sessionId, "cart.licenseAgreement", array($shoppingCartId));
print_r( $shoppingCartLicences );

// check if license is existed
$licenseForOrderCreation = null;
if (count($shoppingCartLicenses)) {
    $licenseForOrderCreation = array();
    foreach ($shoppingCartLicenses as $license) {
        $licenseForOrderCreation[] = $license['agreement_id'];
    }
}

// create order
$resultOrderCreation = $proxy->call($sessionId,"cart.order",array($shoppingCartId, null, $licenseForOrderCreation));
```

</div>


