

## [Mage_Checkout](#cart.order-MageCheckout)

### [Module: Shopping Cart API](#cart.order-Module:ShoppingCartAPI)

## [Resource: cart](#cart.order-Resource:cart)

## [Method:](#cart.order-Method:)

-   cart.order (SOAP V1)
-   shoppingCartOrder (SOAP V2)

Allows you to create an order from a shopping cart (quote).
Before placing the order, you need to add the customer, customer
address, shipping and payment methods.

**Arguments:**

---

Type Name Description

---

string sessionId Session ID

int quoteId Shopping Cart ID (quote
ID)

string storeId Store view ID or code
(optional)

ArrayOfString licenses\ Website license ID
(optional)

---

**Return:**

Type Name Description

---

string result Result of creating an order

**Faults:**
_No Faults._

### [Examples](#cart.order-Examples)

### [Request Example SOAP V1](#cart.order-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$shoppingCartIncrementId = $proxy->call( $sessionId, 'cart.create', array( 'magento_store' ) );

$resultOrderCreation = $proxy->call(
  $sessionId,
  "cart.order",
  array(
    $shoppingCartId
  )
);
```

</div>



### [Request Example SOAP V2](#cart.order-RequestExampleSOAPV2)

<div>

```
/**
 * Example of order creation
 * Preconditions are as follows:
 * 1. Create a customer
 * 2. Create a simple product */

$user = 'apiUser';
$password = 'apiKey';
    $proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');
    $sessionId = $proxy->login($user, $password);
    $cartId = $proxy->shoppingCartCreate($sessionId, 1);
    // load the customer list and select the first customer from the list
    $customerList = $proxy->customerCustomerList($sessionId, array());
    $customer = (array) $customerList[0];
    $customer['mode'] = 'customer';
    $proxy->shoppingCartCustomerSet($sessionId, $cartId, $customer);
    // load the product list and select the first product from the list
    $productList = $proxy->catalogProductList($sessionId);
    $product = (array) $productList[0];
    $product['qty'] = 1;
    $proxy->shoppingCartProductAdd($sessionId, $cartId, array($product));

    $address = array(
        array(
            'mode' => 'shipping',
            'firstname' => $customer['firstname'],
            'lastname' => $customer['lastname'],
            'street' => 'street address',
            'city' => 'city',
            'region' => 'region',
            'telephone' => 'phone number',
            'postcode' => 'postcode',
            'country_id' => 'country ID',
            'is_default_shipping' => 0,
            'is_default_billing' => 0
        ),
        array(
            'mode' => 'billing',
            'firstname' => $customer['firstname'],
            'lastname' => $customer['lastname'],
            'street' => 'street address',
            'city' => 'city',
            'region' => 'region',
            'telephone' => 'phone number',
            'postcode' => 'postcode',
            'country_id' => 'country ID',
            'is_default_shipping' => 0,
            'is_default_billing' => 0
        ),
    );
     // add customer address
    $proxy->shoppingCartCustomerAddresses($sessionId, $cartId, $address);
    // add shipping method
    $proxy->shoppingCartShippingMethod($sessionId, $cartId, 'flatrate_flatrate');

    $paymentMethod =  array(
        'po_number' => null,
        'method' => 'checkmo',
        'cc_cid' => null,
        'cc_owner' => null,
        'cc_number' => null,
        'cc_type' => null,
        'cc_exp_year' => null,
        'cc_exp_month' => null
    );
     // add payment method
    $proxy->shoppingCartPaymentMethod($sessionId, $cartId, $paymentMethod);
     // place the order
    $orderId = $proxy->shoppingCartOrder($sessionId, $cartId, null, null);
```

</div>


