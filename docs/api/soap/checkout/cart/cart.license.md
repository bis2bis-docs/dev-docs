

## [Mage_Checkout](#cart.license-MageCheckout)

### [Module: Shopping Cart API](#cart.license-Module:ShoppingCartAPI)

## [Resource: cart](#cart.license-Resource:cart)

## [Method:](#cart.license-Method:)

-   cart.license (SOAP V1)
-   shoppingCartLicense (SOAP V2)

## [Aliases: cart](#cart.license-Aliases:cart.license).license

Allows you to retrieve the website license agreement for the quote
according to the website (store).

**Arguments:**

Type Name Description

---

string sessionId Session ID
int quoteId Shopping cart ID (quote identifier)
string store Store view ID or code (optional)

**Return:**

Type Name Description

---

array result Array of shoppingCartLicenseEntity

The **shoppingCartLicenseEntity** content is as follows:

---

Type Name Description

---

string agreement_id\ License agreement ID

string\ name\ License name

string\ content\ License content

int is_active\ Defines whether the
license is active

int is_html\ Defines whether the
license is HTML

---

**Faults:**

### [Examples](#cart.license-Examples)

### [Request Example SOAP V1](#cart.license-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$result = $client->call($session, 'cart.license', '15');
var_dump ($result);
```

</div>



### [Request Example SOAP V2](#cart.license-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->shoppingCartLicense($sessionId, '15');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#cart.license-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->shoppingCartLicense((object)array('sessionId' => $sessionId->result, 'quoteId' => 15));
var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#cart.license-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
    array
      'agreement_id' => string '1' (length=1)
      'name' => string 'license' (length=4)
      'content' => string 'terms and conditions' (length=20)
      'content_height' => null
      'checkbox_text' => string 'terms' (length=5)
      'is_active' => string '1' (length=1)
      'is_html' => string '0' (length=1)
```

</div>


