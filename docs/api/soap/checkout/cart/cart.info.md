

## [Mage_Checkout](#cart.info-MageCheckout)

### [Module: Shopping Cart API](#cart.info-Module:ShoppingCartAPI)

## [Resource: cart](#cart.info-Resource:cart)

## [Method:](#cart.info-Method:)

-   cart.info (SOAP V1)
-   shoppingCartInfo (SOAP V2)

Allows you to retrieve full information about the shopping cart (quote).

**Arguments:**

---

Type Name Description

---

string sessionId\ Session ID

int quoteId Shopping cart ID (quote
ID)

string store Store view ID or code
(optional)

---

**Return:**

Type Name Description

---

array result Array of shoppingCartInfoEntity

The **shoppingCartInfoEntity** content is as follows:

---

Type Name Description

---

string store_id\ Store ID

string\ created_at\ Date of creation

string\ updated_at\ Date of updating

string\ converted_at\ Date of conversion

int quote_id\ Quote ID

int is_active\ Active flag

int is_virtual\ Defines whether the product
is a virtual one

int is_multi_shipping\ Defines whether multi
shipping is available

double items_count\ Items quantity

double items_qty\ Total items quantity

string\ orig_order_id\ Original order ID

string\ store_to_base_rate\ Store to base rate

string\ store_to_quote_rate\ Store to quote rate

string\ base_currency_code\ Base currency code

string\ store_currency_code\ Store currency code

string\ quote_currency_code\ Quote currency code

string\ grand_total\ Grand total

string\ base_grand_total\ Base grand total

string\ checkout_method\ Checkout method

string\ customer_id\ Customer ID

string\ customer_tax_class_id\ Customer tax class ID

int customer_group_id\ Customer group ID

string\ customer_email\ Customer email address

string\ customer_prefix\ Customer prefix

string\ customer_firstname\ Customer first name

string\ customer_middlename\ Customer middle name

string\ customer_lastname\ Customer last name

string\ customer_suffix\ Customer suffix

string\ customer_note\ Customer note

string\ customer_note_notify\ Customer notification flag

string\ customer_is_guest\ Defines whether the
customer is a guest

string\ applied_rule_ids\ Applied rule IDs

string\ reserved_order_id\ Reserved order ID

string\ password_hash\ Password hash

string\ coupon_code\ Coupon code

string\ global_currency_code\ Global currency code

double base_to_global_rate\ Base to global rate

double base_to_quote_rate\ Base to quote rate

string\ customer_taxvat\ Customer taxvat value

string\ customer_gender\ Customer gender

double subtotal\ Subtotal

double base_subtotal\ Base subtotal

double subtotal_with_discount\ Subtotal with discount

double base_subtotal_with_discount\ Base subtotal with discount

string\ ext_shipping_info

string\ gift_message_id\ Gift message ID

string\ gift_message\ Gift message

double customer_balance_amount_used\ Used customer balance
amount

double base_customer_balance_amount_used\ Used base customer balance
amount

string\ use_customer_balance\ Defines whether to use the
customer balance

string\ gift_cards_amount\ Gift cards amount

string\ base_gift_cards_amount\ Base gift cards amount

string\ gift_cards_amount_used\ Used gift cards amount

string\ use_reward_points\ Defines whether to use
reward points

string\ reward_points_balance\ Reward points balance

string\ base_reward_currency_amount\ Base reward currency amount

string\ reward_currency_amount\ Reward currency amount

array shipping_address\ Array of
shoppingCartAddressEntity

array billing_address\ Array of
shoppingCartAddressEntity

array items\ Array of
shoppingCartItemEntity

array payment\ Array of
shoppingCartPaymentEntity

---

The **shoppingCartAddressEntity** content is as follows:

---

Type Name Description

---

string address_id\ Shopping cart address
ID

string\ created_at\ Date of creation

string\ updated_at\ Date of updating

string\ customer_id\ Customer ID

int save_in_address_book\ Defines whether to save
the address in the
address book

string\ customer_address_id\ Customer address ID

string\ address_type\ Address type

string\ email\ Email address

string\ prefix\ Customer prefix

string\ firstname\ Customer first name

string\ middlename\ Customer middle name

string\ lastname\ Customer last name

string\ suffix\ Customer suffix

string\ company\ Company name

string\ street\ Street

string\ city\ City

string\ region\ Region

string\ region_id\ Region ID

string\ postcode\ ﻿Postcode

string\ country_id\ Country ID

string\ telephone\ Telephone number

string\ fax\ Fax

int same_as_billing\ Defines whether the
address is the same as
the billing one

int free_shipping\ Defines whether free
shipping is used

string\ shipping_method\ Shipping method

string\ shipping_description\ Shipping description

double weight\ Weight

---

The **shoppingCartItemEntity** content is as follows:

---

Type Name Description

---

string item_id\ Cart item ID

string\ created_at\ Date of creation

string\ updated_at\ Date of updating

string\ product_id\ Product ID

string\ store_id\ Store ID

string\ parent_item_id\ Parent item ID

int is_virtual\ Defines whether the
product is a virtual
one

string\ sku\ Product SKU

string\ name\ Product name

string\ description\ Description

string\ applied_rule_ids\ Applied rule IDs

string\ additional_data\ Additional data

string\ free_shipping\ Free shipping

string\ is_qty_decimal\ Defines whether the
quantity is decimal

string\ no_discount\ Defines whether no
discount is applied

double weight\ Weight

double\ qty\ Quantity

double\ price\ Price

double\ base_price\ Base price

double\ custom_price\ Custom price

double\ discount_percent\ Discount percent

double\ discount_amount\ Discount amount

double\ base_discount_amount\ Base discount amount

double\ tax_percent\ Tax percent

double\ tax_amount\ Tax amount

double\ base_tax_amount\ Base tax amount

double\ row_total\ Row total

double\ base_row_total\ Base row total

double\ row_total_with_discount\ Row total with discount

double\ row_weight\ Row weight

string product_type\ Product type

double\ base_tax_before_discount\ Base tax before
discount

double\ tax_before_discount\ Tax before discount

double\ original_custom_price\ Original custom price

double\ base_cost\ Base cost

double\ price_incl_tax\ Price including tax

double\ base_price_incl_tax\ Base price including
tax

double\ row_total_incl_tax\ Row total including tax

double base_row_total_incl_tax\ Base row total
including tax

string gift_message_id\ Gift message ID

string\ gift_message\ Gift message

string\ gift_message_available\ Defines whether the
gift message is
available

double\ weee_tax_applied\ Applied fix product tax

double\ weee_tax_applied_amount\ Applied fix product tax
amount

double\ weee_tax_applied_row_amount\ Applied fix product tax
row amount

double\ base_weee_tax_applied_amount\ Applied fix product tax
amount (in base
currency)

double\ base_weee_tax_applied_row_amount\ Applied fix product tax
row amount (in base
currency)

double\ weee_tax_disposition\ Fixed product tax
disposition

double\ weee_tax_row_disposition\ Fixed product tax row
disposition

double\ base_weee_tax_disposition\ Fixed product tax
disposition (in base
currency)

double base_weee_tax_row_disposition\ Fixed product tax row
disposition (in base
currency)

string\ tax_class_id\ Tax class ID

---

The **shoppingCartPaymentEntity** content is as follows:

---

Type Name Description

---

string payment_id\ Payment ID

string\ created_at\ Date of creation

string\ updated_at\ Date of updating

string\ method\ Payment method

string\ cc_type\ Credit card type

string\ cc_number_enc\ Credit card number

string\ cc_last4\ Last four digits on the
credit card

string\ cc_cid_enc\ Credit card CID

string\ cc_owner\ Credit card owner

string\ cc_exp_month\ Credit card expiration
month

string\ cc_exp_year\ Credit card expiration
year

string\ cc_ss_owner\ Credit card owner
(Switch/Solo)

string\ cc_ss_start_month\ Credit card start month
(Switch/Solo)

string\ cc_ss_start_year\ Credit card start year
(Switch/Solo)

string\ cc_ss_issue\ Credit card issue
number (Switch/Solo)

string\ po_number\ Purchase order number

string\ additional_data\ Additional data

string\ additional_information\ Additional information

---

### [Examples](#cart.info-Examples)

### [Request Example SOAP V1](#cart.info-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'cart.info', '15');
var_dump ($result);
```

</div>



### [Request Example SOAP V2](#cart.info-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->shoppingCartInfo($sessionId, '15');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#cart.info-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->shoppingCartInfo((object)array('sessionId' => $sessionId->result, 'quoteId' => '15'));

var_dump($result->result);
```

</div>


