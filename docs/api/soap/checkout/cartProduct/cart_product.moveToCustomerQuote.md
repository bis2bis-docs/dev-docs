

## [Mage_Checkout](#cart_product.moveToCustomerQuote-MageCheckout)

### [Module: Shopping Cart API](#cart_product.moveToCustomerQuote-Module:ShoppingCartAPI)

## [Resource: cart_product](#cart_product.moveToCustomerQuote-Resource:cartproduct)

## [Method:](#cart_product.moveToCustomerQuote-Method:)

-   cart_product.moveToCustomerQuote (SOAP V1)
-   shoppingCartProductMoveToCustomerQuote (SOAP V2)

Allows you to move products from the current quote to a customer quote.

**Arguments:**

Type Name Description

---

string sessionId Session ID
int quoteId Shopping cart ID
array productsData Array of shoppingCartProductEntity
string store Store view ID or code (optional)

**Return:**

Type Name Description

---

boolean result True if the product is moved to customer quote

The **shoppingCartProductEntity** content is as follows:

---

Type Name Description

---

string product_id Product ID

string sku\ Product SKU

double qty\ Product quantity

associativeArray\ options\ Product custom options

associativeArray\ bundle_option\ An array of bundle item
options (optional)

associativeArray bundle_option_qty\ An array of bundle
items quantity
(optional)

ArrayOfString links\ An array of links
(optional)

---

**Faults:**
_No Faults._

### [Examples](#cart_product.moveToCustomerQuote-Examples)

### [Request Example SOAP V1](#cart_product.moveToCustomerQuote-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$shoppingCartIncrementId = $proxy->call( $sessionId, 'cart.create', array( 'magento_store' ) );
$arrProducts = array(
    array(
        "product_id" => "1",
        "qty" => 2
    ),
    array(
        "sku" => "testSKU",
        "quantity" => 4
    )
);
$resultCartProductAdd = $proxy->call(
    $sessionId,
    "cart_product.add",
    array(
        $shoppingCartId,
        $arrProducts
    )
);
$arrProducts = array(
    array(
        "product_id" => "1"
    ),
);
$resultCartProductMove = $proxy->call(
    $sessionId,
    "cart_product.moveToCustomerQuote",
    array(
        $shoppingCartId,
        $arrProducts
    )
);
```

</div>



Create the Magento file system owner
