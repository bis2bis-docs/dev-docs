

## [](#MAPI-CartProduct-MageCheckout)Mage_Checkout

The Mage_Checkout module allows you to manage shopping carts and the
checkout process. This module allows you to create an order once filling
the shopping cart is complete.

### [Cart Product](#MAPI-CartProduct-CartProduct)

Allows you to manage products in a shopping cart.

**Resource Name**: cart_product

**Methods**:

-   [cart_product.add](cart_product.add.html "cart_product.add") - Add
    one or more products to a shopping cart
-   [cart_product.update](cart_product.update.html "cart_product.update") -
    Update one or more products in a shopping cart
-   [cart_product.remove](cart_product.remove.html "cart_product.remove") -
    Remove one or more products from a shopping cart
-   [cart_product.list](cart_product.list.html "cart_product.list") -
    Get a list of products in a shopping cart
-   [cart_product.moveToCustomerQuote](cart_product.moveToCustomerQuote.html "cart_product.moveToCustomerQuote") -
    Move one or more products from the quote to the customer shopping
    cart

### [Faults](#MAPI-CartProduct-Faults)

Fault Code Fault Message

---

1001 Can not make operation because store is not exists
1002 Can not make operation because quote is not exists
1021 Product's data is not valid.
1022 Product(s) could not be added.
1023 Quote could not be saved during adding product(s) operation.
1024 Product(s) could not be updated.
1025 Quote could not be saved during updating product(s) operation.
1026 Product(s) could not be removed.
1027 Quote could not be saved during removing product(s) operation.
1028 Customer is not set for quote.
1029 Customer's quote is not existed.
1030 Quotes are identical.
1031 Product(s) could not be moved.
1032 One of quote could not be saved during moving product(s) operation.

Create the Magento file system owner
