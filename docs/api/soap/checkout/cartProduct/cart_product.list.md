

## [Mage_Checkout](#cart_product.list-MageCheckout)

### [Module: Shopping Cart API](#cart_product.list-Module:ShoppingCartAPI)

## [Resource: cart_product](#cart_product.list-Resource:cartproduct)

## [Method:](#cart_product.list-Method:)

-   cart_product.list (SOAP V1)
-   shoppingCartProductList (SOAP V2)

Allows you to retrieve the list of products in the shopping cart
(quote).

**Arguments:**

Type Name Description

---

string sessionId Session ID
int quoteId Shopping cart ID
string store Store view ID or code (optional)

**Return:**

Type Name Description

---

array result Array of shoppingCartProductResponseEntity

The **shoppingCartProductResponseEntity** (**catalogProductEntity**)
content is as follows:

---

Type Name Description

---

string product_id Product ID

string\ sku\ Product SKU

string\ name\ Product name

string\ set\ Product attribute set

string\ type\ Product type

ArrayOfString\ category_ids\ Array of category IDs

ArrayOfString\ website_ids\ Array of website IDs

---

**Faults:**

### [Examples](#cart_product.list-Examples)

### [Request Example SOAP V1](#cart_product.list-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'cart_product.list', '15');
var_dump ($result);
```

</div>



### [Request Example SOAP V2](#cart_product.list-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->shoppingCartProductList($sessionId, '15');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#cart_product.list-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->shoppingCartProductList((object)array('sessionId' => $sessionId->result, 'quoteId' => 15));
var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#cart_product.list-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
    array
      'product_id' => string '3' (length=1)
      'sku' => string 'canonxt' (length=7)
      'name' => string 'Canon Digital Rebel XT 8MP Digital SLR Camera' (length=45)
      'set' => string '4' (length=1)
      'type' => string 'simple' (length=6)
      'category_ids' =>
        array
          0 => string '5' (length=1)
      'website_ids' =>
        array
          0 => string '2' (length=1)
```

</div>


