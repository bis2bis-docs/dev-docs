

## [Mage_Checkout](#cart_product.update-MageCheckout)

### [Module: Shopping Cart API](#cart_product.update-Module:ShoppingCartAPI)

## [Resource: cart_product](#cart_product.update-Resource:cartproduct)

## [Method:](#cart_product.update-Method:)

-   cart_product.update (SOAP V1)
-   shoppingCartProductUpdate (SOAP V2)

Allows you to update one or several products in the shopping cart
(quote).

**Arguments:**

Type Name Description

---

string sessionId Session ID
int quoteId Shopping cart ID
array productsData Array of shoppingCartProductEntity
string store Store view ID or code (optional)

**Return:**

Type Description

---

boolean True if the product is updated

The **shoppingCartProductEntity** content is as follows:

---

Type Name Description

---

string product_id Product ID

string sku\ Product SKU

double qty\ Product quantity

associativeArray\ options\ Product custom options

associativeArray\ bundle_option\ An array of bundle item
options (optional)

associativeArray bundle_option_qty\ An array of bundle
items quantity
(optional)

ArrayOfString links\ An array of links
(optional)

---

**Faults:**
_No Faults._

### [Examples](#cart_product.update-Examples)

### [Request Example SOAP V1](#cart_product.update-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$shoppingCartIncrementId = $proxy->call( $sessionId, 'cart.create', array( 'magento_store' ) );
$arrProducts = array(
    array(
        "product_id" => "1",
        "qty" => 2
    ),
    array(
        "sku" => "testSKU",
        "quantity" => 4
    )
);
$resultCartProductAdd = $proxy->call(
    $sessionId,
    "cart_product.add",
    array(
        $shoppingCartId,
        $arrProducts
    )
);
$arrProducts = array(
    array(
        "product_id" => "1",
        "qty" => 5
    ),
);
$resultCartProductUpdate = $proxy->call(
    $sessionId,
    "cart_product.update",
    array(
        $shoppingCartId,
        $arrProducts
    )
);
```

</div>



### [Request Example SOAP V2](#cart_product.update-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login('apiUser', 'apiKey');

$result = $proxy->shoppingCartProductUpdate($sessionId, 10, array(array(
'product_id' => '4',
'sku' => 'simple_product',
'qty' => '2',
'options' => null,
'bundle_option' => null,
'bundle_option_qty' => null,
'links' => null
)));


var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#cart_product.update-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->shoppingCartProductUpdate((object)array('sessionId' => $sessionId->result, 'quoteId' => 10, 'productsData' => array(array(
'product_id' => '4',
'sku' => 'simple_product',
'qty' => '5',
'options' => null,
'bundle_option' => null,
'bundle_option_qty' => null,
'links' => null
))));


var_dump($result->result);
```

</div>


