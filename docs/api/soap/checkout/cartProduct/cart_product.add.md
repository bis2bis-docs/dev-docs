

## [Shopping Cart API](#cart_product.add-ShoppingCartAPI)

Allows you to create/modify shopping cart and create an order after
complete filling the shopping cart. Consists of two main parts: Shopping
Cart and Checkout processes.

**Module**: Mage_Checkout

**Resource:** cart_product

### [Method:](#cart_product.add-Method:)

-   cart_product.add (SOAP V1)
-   shoppingCartProductAdd (SOAP V2)

Allows you to add one or more products to the shopping cart (quote).

**Arguments**:

---

Type Name Description

---

string sessionId Session ID

int quoteId Shopping cart ID (quote ID)

array\ products\\productsData\ An array with the list of
shoppingCartProductEntity

string\ storeId\ Store view ID or code
(optional)

---

**Returns**:

Type Description

---

boolean True on success (if the product is added to the shopping cart)

The shoppingCartProductEntity array attributes are as follows:

---

Type Name Description

---

string product_id ID of the product to be
added to the shopping
cart (quote) (optional)

string\ sku\ SKU of the product to
be added to the
shopping cart (quote)
(optional)

double\ qty\ Number of products to
be added to the
shopping cart (quote)
(optional)

associativeArray\ options\ An array in the form of
option_id =\> content
(optional)

associativeArray\ bundle_option\ An array of bundle item
options (optional)

associativeArray\ bundle_option_qty\ An array of bundle
items quantity
(optional)

ArrayOfString\ links\ An array of links
(optional)

---

**Faults**:
_No Faults._

### [Examples](#cart_product.add-Examples)

### [Request Example SOAP V1](#cart_product.add-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$quoteId = $proxy->call( $sessionId, 'cart.create', array( 'magento_store' ) );
$arrProducts = array(
    array(
        "product_id" => "1",
        "qty" => 2
                "options" => array(         
                    optionId_1 => optionValue_1,
                    ...,
                    optionId_n => optionValue_n
                 )
    ),
    array(
        "sku" => "testSKU",
        "quantity" => 4
    )
);
$resultCartProductAdd = $proxy->call(
    $sessionId,
    "cart_product.add",
    array(
        $quoteId,
        $arrProducts
    )
);
```

</div>



### [Request Example SOAP V2](#cart_product.add-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login('apiUser', 'apiKey');

$result = $proxy->shoppingCartProductAdd($sessionId, 10, array(array(
'product_id' => '4',
'sku' => 'simple_product',
'qty' => '5',
'options' => null,
'bundle_option' => null,
'bundle_option_qty' => null,
'links' => null
)));


var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#cart_product.add-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->shoppingCartProductAdd((object)array('sessionId' => $sessionId->result, 'quoteId' => 10, 'productsData' => array(array(
'product_id' => '4',
'sku' => 'simple_product',
'qty' => '1',
'options' => null,
'bundle_option' => null,
'bundle_option_qty' => null,
'links' => null
))));


var_dump($result->result);
```

</div>


