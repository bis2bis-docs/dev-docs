

## [Mage_Checkout](#cart_product.remove-MageCheckout)

### [Module: Shopping Cart API](#cart_product.remove-Module:ShoppingCartAPI)

## [Resource: cart_product](#cart_product.remove-Resource:cartproduct)

## [Method:](#cart_product.remove-Method:)

-   cart_product.remove (SOAP V1)
-   shoppingCartProductRemove (SOAP V2)

Allows you to remove one or several products from a shopping cart
(quote).

**Arguments:**

Type Name Description

---

string sessionId Session ID
int quoteId Shopping cart ID
array productsData Array of shoppingCartProductEntity
string store Store view ID or code (optional)

**Return:**

Type Description

---

boolean True if the product is removed

The **shoppingCartProductEntity** content is as follows:

---

Type Name Description

---

string product_id Product ID

string sku\ Product SKU

double qty\ Product quantity

associativeArray\ options\ Product custom options

associativeArray\ bundle_option\ An array of bundle item
options (optional)

associativeArray bundle_option_qty\ An array of bundle
items quantity
(optional)

ArrayOfString links\ An array of links
(optional)

---

**Faults:**
_No Faults._

### [Examples](#cart_product.remove-Examples)

### [Request Example SOAP V1](#cart_product.remove-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$shoppingCartIncrementId = $proxy->call( $sessionId, 'cart.create', array( 'magento_store' ) );
$arrProducts = array(
    array(
        "product_id" => "1",
        "qty" => 2
    ),
    array(
        "sku" => "testSKU",
        "quantity" => 4
    )
);
$resultCartProductAdd = $proxy->call(
    $sessionId,
    "cart_product.add",
    array(
        $shoppingCartId,
        $arrProducts
    )
);
$arrProducts = array(
    array(
        "product_id" => "1"
    ),
);
$resultCartProductUpdate = $proxy->call(
    $sessionId,
    "cart_product.remove",
    array(
        $shoppingCartId,
        $arrProducts
    )
);
```

</div>



### [Request Example SOAP V2](#cart_product.remove-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login('apiUser', 'apiKey');

$result = $proxy->shoppingCartProductRemove($sessionId, 10, array(array(
'product_id' => '4',
'sku' => 'simple_product',
'qty' => '1',
'options' => null,
'bundle_option' => null,
'bundle_option_qty' => null,
'links' => null
)));


var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#cart_product.remove-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->shoppingCartProductRemove((object)array('sessionId' => $sessionId->result, 'quoteId' => 10, 'productsData' => array(array(
'product_id' => '4',
'sku' => 'simple_product',
'qty' => '1',
'options' => null,
'bundle_option' => null,
'bundle_option_qty' => null,
'links' => null
))));


var_dump($result->result);
```

</div>


