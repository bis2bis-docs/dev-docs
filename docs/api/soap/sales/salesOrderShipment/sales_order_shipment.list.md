

## [Module: Mage_Sales](#sales_order_shipment.list-Module:MageSales)

## [Resource: sales_order_shipment](#sales_order_shipment.list-Resource:salesordershipment)

**Aliases**:

-   order_shipment

## [Method:](#sales_order_shipment.list-Method:)

-   sales_order_shipment.list (SOAP V1)
-   salesOrderShipmentList (SOAP V2)

Allows you to retrieve the list of order shipments. Additional filters
can be applied.

**Aliases**:

-   order_shipment.list

**Arguments**:

---

Type Name Description

---

string sessionId\ Session ID

array filters\ Array of filters for
the list of shipments

---

Returns:

Type Name Description

---

array result Array of salesOrderShipmentEntity

The **salesOrderShipmentEntity** content is as follows:

---

Type Name Description

---

string increment_id\ Increment ID

string\ created_at\ Date of shipment
creation

string\ total_qty\ Total quantity of items
to ship

string\ shipment_id\ Shipment ID

---

### [Examples](#sales_order_shipment.list-Examples)

### [Request Example SOAP V1](#sales_order_shipment.list-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'sales_order_shipment.list');
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



## [Request Example SOAP V2 (List of All Shipments)](#sales_order_shipment.list-RequestExampleSOAPV2%28ListofAllShipments%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->salesOrderShipmentList($sessionId);
var_dump($result);
```

</div>



## [Request Example SOAP V2 (Complex Filter)](#sales_order_shipment.list-RequestExampleSOAPV2%28ComplexFilter%29)

<div>

```
$client = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

// If some stuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');
$complexFilter = array(
    'complex_filter' => array(
        array(
            'key' => 'created_at',
            'value' => array('key' => 'in', 'value' => '2012-03-30 12:54:46')
        )
    )
);
$result = $client->salesOrderShipmentList($session, $complexFilter);

var_dump ($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#sales_order_shipment.list-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->salesOrderShipmentList((object)array('sessionId' => $sessionId->result));
var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#sales_order_shipment.list-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
    array
      'increment_id' => string '200000001' (length=9)
      'created_at' => string '2012-03-30 12:35:29' (length=19)
      'total_qty' => string '2.0000' (length=6)
      'shipment_id' => null
  1 =>
    array
      'increment_id' => string '200000002' (length=9)
      'created_at' => string '2012-03-30 12:54:46' (length=19)
      'total_qty' => string '1.0000' (length=6)
      'shipment_id' => null
```

</div>


