

## [Module: Mage_Sales](#sales_order_shipment.create-Module:MageSales)

## [Resource: sales_order_shipment](#sales_order_shipment.create-Resource:salesordershipment)

**Aliases**:

-   order_shipment

## [Method:](#sales_order_shipment.create-Method:)

-   sales_order_shipment.create (SOAP V1)
-   salesOrderShipmentCreate (SOAP V2)

Allows you to create a new shipment for an order.

**Aliases**:

-   order_shipment.create

**Arguments**:

---

Type Name Description

---

string sessionId Session ID

string orderIncrementId\ Order increment ID

array itemsQty\ Array of orderItemIdQty
(optional)

string comment\ Shipment comment
(optional)

int email\ Send email flag
(optional)

int includeComment\ Include comment in
email flag (optional)

---

**Returns**:

---

Type Name Description

---

string shipmentIncrementId\ Shipment increment ID

---

The **orderItemIdQty** content is as follows:

---

Type Name Description

---

int order_item_id\ Order item ID

double qty\ Quantity of items to be
shipped

---

**Notes**: The array of orderItemQty is used for partial shipment. To
create shipment for all order items, you do not need to specify these
attributes.

### [Examples](#sales_order_shipment.create-Examples)

### [Request Example SOAP V1](#sales_order_shipment.create-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');

$session = $proxy->login('apiUser', 'apiKey');

$orderIncrementId = '200000006';
$orderItemId = 3;
$qty = 5;
$itemsQty = array(
    $orderItemId => $qty,
    );

$result = $proxy->call(
    $session,
    'order_shipment.create',
    array(
        $orderIncrementId,
        $itemsQty
    )
);

var_dump ($result);
```

</div>



### [Request Example SOAP V2](#sales_order_shipment.create-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$itemsQty = array(
    array(
        'order_item_id' => 3,
        'qty' => 3
    ),
    array(
        'order_item_id' => 4,
        'qty' => 5
    ));

$result = $proxy->salesOrderShipmentCreate($sessionId, '200000006', $itemsQty, 'shipment comment');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#sales_order_shipment.create-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$itemsQty = array(
    array(
        'order_item_id' => 3,
        'qty' => 3
    ),
    array(
        'order_item_id' => 4,
        'qty' => 5
    ));

$result = $proxy->salesOrderShipmentCreate((object)array(
    'sessionId' => $sessionId->result,
    'orderIncrementId' => '200000006',
    'itemsQty' => $itemsQty,
    'comment' => 'shipment comment',
    'email' => null, 'includeComment' => null));

var_dump($result->result);
```

</div>


