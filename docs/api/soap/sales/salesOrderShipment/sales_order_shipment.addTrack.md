

## [Module: Mage_Sales](#sales_order_shipment.addTrack-Module:MageSales)

## [Resource: sales_order_shipment](#sales_order_shipment.addTrack-Resource:salesordershipment)

**Aliases**:

-   order_shipment

## [Method:](#sales_order_shipment.addTrack-Method:)

-   sales_order_shipment.addTrack (SOAP V1)
-   salesOrderShipmentAddTrack (SOAP V2)

Allows you to add a new tracking number to the order shipment.

**Aliases**:

-   order_shipment.addTrack

**Arguments**:

---

Type Name Description

---

string sessionId\ Session ID

string\ shipmentIncrementId\ Shipment increment ID

string\ carrier\ Carrier code (ups,
usps, dhl, fedex, or
dhlint)

string\ title\ Tracking title

string\ trackNumber\ Tracking number

---

**Returns**:

Type Description

---

int Tracking number ID

### [Examples](#sales_order_shipment.addTrack-Examples)

### [Request Example SOAP V1](#sales_order_shipment.addTrack-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'sales_order_shipment.addTrack', array('shipmentIncrementId' => '200000002', 'carrier' => 'ups', 'title' => 'tracking title', 'trackNumber' => '123123'));
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#sales_order_shipment.addTrack-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->salesOrderShipmentAddTrack($sessionId, '200000002', 'ups', 'tracking title', '123123');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#sales_order_shipment.addTrack-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->salesOrderShipmentAddTrack((object)array('sessionId' => $sessionId->result, 'shipmentIncrementId' => '200000002', 'carrier' => 'ups', 'title' => 'tracking title', 'trackNumber' => '123123'));
var_dump($result->result);
```

</div>


