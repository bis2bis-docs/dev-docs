

## [Module: Mage_Sales](#sales_order_shipment.getCarriers-Module:MageSales)

## [Resource: sales_order_shipment](#sales_order_shipment.getCarriers-Resource:salesordershipment)

**Aliases**:

-   order_shipment

## [Method:](#sales_order_shipment.getCarriers-Method:)

-   sales_order_shipment.getCarriers (SOAP V1)
-   salesOrderShipmentGetCarriers (SOAP V2)

Allows you to retrieve the list of allowed carriers for an order.

**Aliases**:

-   order_shipment.getCarriers

**Arguments**:

---

Type Name Description

---

string sessionId\ Session ID

string orderIncrementId\ Order increment ID

---

**Returns**:

Type Name Description

---

associativeArray result Array of carriers

### [Examples](#sales_order_shipment.getCarriers-Examples)

### [Request Example SOAP V1](#sales_order_shipment.getCarriers-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'sales_order_shipment.getCarriers', '200000010');
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#sales_order_shipment.getCarriers-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->salesOrderShipmentGetCarriers($sessionId, '200000010');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#sales_order_shipment.getCarriers-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->salesOrderShipmentGetCarriers((object)array('sessionId' => $sessionId->result, 'orderIncrementId' => '200000010'));
var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#sales_order_shipment.getCarriers-ResponseExampleSOAPV1)

<div>

```
array
  'custom' => string 'Custom Value' (length=12)
  'dhl' => string 'DHL (Deprecated)' (length=16)
  'fedex' => string 'Federal Express' (length=15)
  'ups' => string 'United Parcel Service' (length=21)
  'usps' => string 'United States Postal Service' (length=28)
  'dhlint' => string 'DHL' (length=3)
```

</div>


