

## [Module: Mage_Sales](#MAPI-Shipment-Module:MageSales)

The Mage_Sales module allows you to manage sales orders, invoices,
shipments, and credit memos.

### [Shipment](#MAPI-Shipment-Shipment)

Allows you to manage shipments and tracking numbers.

**Resource Name**: sales_order_shipment

**Aliases**:

-   order_shipment

**Methods**:

-   [sales_order_shipment.list](sales_order_shipment.list.html "sales_order_shipment.list") -
    Retrieve a list of shipments using filters
-   [sales_order_shipment.info](sales_order_shipment.info.html "sales_order_shipment.info") -
    Retrieve information about the shipment
-   [sales_order_shipment.create](sales_order_shipment.create.html "sales_order_shipment.create") -
    Create a new shipment for an order
-   [sales_order_shipment.addComment](sales_order_shipment.addComment.html "sales_order_shipment.addComment") -
    Add a new comment to a shipment
-   [sales_order_shipment.addTrack](sales_order_shipment.addTrack.html "sales_order_shipment.addTrack") -
    Add a new tracking number to a shipment
-   [sales_order_shipment.removeTrack](sales_order_shipment.removeTrack.html "sales_order_shipment.removeTrack") -
    Remove tracking number from a shipment
-   [sales_order_shipment.getCarriers](sales_order_shipment.getCarriers.html "sales_order_shipment.getCarriers") -
    Retrieve a list of allowed carriers for an order

### [Faults](#MAPI-Shipment-Faults)

Fault Code Fault Message

---

100 Requested shipment not exists.
101 Invalid filters given. Details in error message.
102 Invalid data given. Details in error message.
103 Requested order not exists.
104 Requested tracking not exists.
105 Tracking not deleted. Details in error message.

### [Examples](#MAPI-Shipment-Examples)

### [Example 1. Basic working with shipments](#MAPI-Shipment-Example1.Basicworkingwithshipments)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$notShipedOrderId  = '100000003';

// Create new shipment
$newShipmentId = $proxy->call($sessionId, 'sales_order_shipment.create', array($notShipedOrderId, array(), 'Shipment Created', true, true));

// View new shipment
$shipment = $proxy->call($sessionId, 'sales_order_shipment.info', $newShipmentId);

var_dump($shipment);


// Get allowed carriers for shipping
$allowedCarriers = $proxy->call($sessionId, 'sales_order_shipment.getCarriers', $notShipedOrderId);

end($allowedCarriers);

$choosenCarrier = key($allowedCarriers);

var_dump($allowedCarriers);
var_dump($choosenCarrier);

// Add tracking
$newTrackId = $proxy->call($sessionId, 'sales_order_shipment.addTrack', array($newShipmentId, $choosenCarrier, 'My Track', rand(5000, 9000)));

$shipment = $proxy->call($sessionId, 'sales_order_shipment.info', $newShipmentId);

var_dump($shipment);
```

</div>



Create the Magento file system owner
