

## [Module: Mage_Sales](#sales_order_shipment.removeTrack-Module:MageSales)

## [Resource: sales_order_shipment](#sales_order_shipment.removeTrack-Resource:salesordershipment)

**Aliases**:

-   order_shipment

## [Method:](#sales_order_shipment.removeTrack-Method:)

-   sales_order_shipment.removeTrack (SOAP V1)
-   salesOrderShipmentRemoveTrack (SOAP V2)

Allows you to remove a tracking number from the order shipment.

**Aliases**:

-   order_shipment.removeTrack

**Arguments**:

---

Type Name Description

---

string sessionId\ Session ID

string shipmentIncrementId\ Shipment increment ID

string trackId\ Track ID

---

**Returns**:

Type Description

---

boolean\\int True (1) if the tracking number is removed from the shipment

### [Examples](#sales_order_shipment.removeTrack-Examples)

### [Request Example SOAP V1](#sales_order_shipment.removeTrack-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'sales_order_shipment.removeTrack', array('shipmentIncrementId' => '200000002', 'trackId' => '2'));
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#sales_order_shipment.removeTrack-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->salesOrderShipmentRemoveTrack($sessionId, '200000002', '2');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#sales_order_shipment.removeTrack-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->salesOrderShipmentRemoveTrack((object)array('sessionId' => $sessionId->result, 'shipmentIncrementId' => '200000002', 'trackId' => '2'));
var_dump($result->result);
```

</div>


