

## [Module: Mage_Sales](#sales_order_shipment.info-Module:MageSales)

## [Resource: sales_order_shipment](#sales_order_shipment.info-Resource:salesordershipment)

**Aliases**:

-   order_shipment

## [Method:](#sales_order_shipment.info-Method:)

-   sales_order_shipment.info (SOAP V1)
-   salesOrderShipmentInfo (SOAP V2)

Allows you to retrieve the shipment information.

**Aliases**:

-   order_shipment.info

**Arguments**:

---

Type Name Description

---

string sessionId Session ID

string shipmentIncrementId\ Order shipment
increment ID

---

**Returns**:

Type Name Description

---

array result Array of salesOrderShipmentEntity

The **salesOrderShipmentEntity** content is as follows:

---

Type Name Description

---

string increment_id\ Shipment increment ID

string\ store_id\ Store ID

string\ created_at\ Date of shipment creation

string\ updated_at\ Date of shipment updating

string\ shipping_address_id\ Shipping address ID

string\ order_id\ Order ID

string\ total_qty\ Total quantity of items to ship

string\ shipment_id\ Shipment ID

array items\ Array of
salesOrderShipmentItemEntity

array tracks\ Array of
salesOrderShipmentTrackEntity

array comments\ Array of
salesOrderShipmentCommentEntity

---

The **salesOrderShipmentItemEntity** content is as follows:

---

Type Name Description

---

string\ parent_id\ Parent ID

string\ sku\ Shipment item SKU

string\ name\ Shipment item name

string\ order_item_id\ Order item ID

string\ product_id\ Product ID

string\ weight\ Weight

string\ price\ Price

string\ qty\ Quantity of items

string\ item_id\ Item ID

---

The **salesOrderShipmentTrackEntity** content is as follows:

---

Type Name Description

---

string\ parent_id\ Parent ID

string\ created_at\ Date of tracking number
creation

string\ updated_at\ Date of tracking number
updating

string\ carrier_code\ Carrier code

string\ title\ Track title

string\ number\ Tracking number

string\ order_id\ Order ID

string\ track_id\ Track ID

---

The **salesOrderShipmentCommentEntity** content is as follows:

---

Type Name Description

---

string\ parent_id\ Parent ID

string\ created_at\ Date of comment
creation

string\ comment\ Shipment comment text

string\ is_customer_notified\ Defines whether the
customer is notified

string\ comment_id\ Comment ID

---

### [Examples](#sales_order_shipment.info-Examples)

### [Request example SOAP V1](#sales_order_shipment.info-RequestexampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');
$session = $client->login('apiUser', 'apiKey');
$result = $client->call($session, 'sales_order_shipment.info', '200000003');
var_dump($result);
```

</div>



### [Request example SOAP V2](#sales_order_shipment.info-RequestexampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->salesOrderShipmentInfo($sessionId, '200000003');
var_dump($result);
```

</div>



## [Request example SOAP V2 (WS-I Compliance Mode)](#sales_order_shipment.info-RequestexampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->salesOrderShipmentInfo((object)array('sessionId' => $sessionId->result, 'shipmentIncrementId' => '200000003'));
var_dump($result->result);
```

</div>


