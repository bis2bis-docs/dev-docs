

## [Module: Mage_Sales](#sales_order_shipment.addComment-Module:MageSales)

## [Resource: sales_order_shipment](#sales_order_shipment.addComment-Resource:salesordershipment)

**Aliases**:

-   order_shipment

## [Method:](#sales_order_shipment.addComment-Method:)

-   sales_order_shipment.addComment (SOAP V1)
-   salesOrderShipmentAddComment (SOAP V2)

Allows you to add a new comment to the order shipment.

**Aliases**:

-   order_shipment.addComment

**Arguments**:

---

Type Name Description

---

string sessionId\ Session ID

string\ shipmentIncrementId\ Shipment increment ID

string\ comment\ Shipment comment
(optional)

string\ email\ Send email flag
(optional)

string\ includeInEmail\ Include comment in
email flag (optional)

---

**Returns**:

Type Description

---

boolean\\int True (1) if the comment is added to the order shipment

### [Examples](#sales_order_shipment.addComment-Examples)

### [Request Example SOAP V1](#sales_order_shipment.addComment-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'sales_order_shipment.addComment', array('shipmentIncrementId' => '200000002', 'comment' => 'comment for the shipment', 'email' => null));
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#sales_order_shipment.addComment-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->salesOrderShipmentAddComment($sessionId, '200000002');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS- I Compliance Mode)](#sales_order_shipment.addComment-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->salesOrderShipmentAddComment((object)array('sessionId' => $sessionId->result, 'shipmentIncrementId' => '200000002', 'comment' => 'comment for the shipment', 'email' => null, 'includeInEmail' => null));
var_dump($result->result);
```

</div>


