

## [Module: Mage_Sales](#Mage_Sales-Module:MageSales)

The Mage_Sales module allows you to manage sales orders, invoices,
shipments, and credit memos.

### [Order](#Mage_Sales-Order)

Allows you to manage orders.

**Resource Name**: sales_order

**Aliases**:

-   order

**Methods**:

-   [sales_order.list](salesOrder/sales_order.list.html "sales_order.list") -
    Retrieve the list of orders using filters
-   [sales_order.info](salesOrder/sales_order.info.html "sales_order.info") -
    Retrieve the order information
-   [sales_order.addComment](salesOrder/sales_order.addComment.html "sales_order.addComment") -
    Add a comment to an order
-   [sales_order.hold](salesOrder/sales_order.hold.html "sales_order.hold") -
    Hold an order
-   [sales_order.unhold](salesOrder/sales_order.unhold.html "sales_order.unhold") -
    Unhold an order
-   [sales_order.cancel](salesOrder/sales_order.cancel.html "sales_order.cancel") -
    Cancel an order

### [Invoice](#Mage_Sales-Invoice)

Allows you to manage invoices.

**Resource Name**: sales_order_invoice

**Aliases**:

-   order_invoice

**Methods**:

-   [sales_order_invoice.list](salesOrderInvoice/sales_order_invoice.list.html "sales_order_invoice.list") -
    Retrieve a list of invoices using filters
-   [sales_order_invoice.info](salesOrderInvoice/sales_order_invoice.info.html "sales_order_invoice.info") -
    Retrieve information about the invoice
-   [sales_order_invoice.create](salesOrderInvoice/sales_order_invoice.create.html "sales_order_invoice.create") -
    Create a new invoice for an order
-   [sales_order_invoice.addComment](salesOrderInvoice/sales_order_invoice.addComment.html "sales_order_invoice.addComment") -
    Add a new comment to an invoice
-   [sales_order_invoice.capture](salesOrderInvoice/sales_order_invoice.capture.html "sales_order_invoice.capture") -
    Capture an invoice
-   [sales_order_invoice.cancel](salesOrderInvoice/sales_order_invoice.cancel.html "sales_order_invoice.cancel") -
    Cancel an invoice

### [Shipment](#Mage_Sales-Shipment)

Allows you to manage shipments and tracking numbers.

**Resource Name**: sales_order_shipment

**Aliases**:

-   order_shipment

**Methods**:

-   [sales_order_shipment.list](salesOrderShipment/sales_order_shipment.list.html "sales_order_shipment.list") -
    Retrieve a list of shipments using filters
-   [sales_order_shipment.info](salesOrderShipment/sales_order_shipment.info.html "sales_order_shipment.info") -
    Retrieve information about the shipment
-   [sales_order_shipment.create](salesOrderShipment/sales_order_shipment.create.html "sales_order_shipment.create") -
    Create a new shipment for an order
-   [sales_order_shipment.addComment](salesOrderShipment/sales_order_shipment.addComment.html "sales_order_shipment.addComment") -
    Add a new comment to a shipment
-   [sales_order_shipment.addTrack](salesOrderShipment/sales_order_shipment.addTrack.html "sales_order_shipment.addTrack") -
    Add a new tracking number to a shipment
-   [sales_order_shipment.removeTrack](salesOrderShipment/sales_order_shipment.removeTrack.html "sales_order_shipment.removeTrack") -
    Remove tracking number from a shipment
-   [sales_order_shipment.getCarriers](salesOrderShipment/sales_order_shipment.getCarriers.html "sales_order_shipment.getCarriers") -
    Retrieve a list of allowed carriers for an order

### [Credit Memo](#Mage_Sales-CreditMemo)

Allows you to manage order credit memos.

**Resource Name**: sales_order_creditmemo

**Aliases**:

-   order_creditmemo

## [Methods:](#Mage_Sales-Methods:)

-   [sales_order_creditmemo.list](salesOrderCreditMemo/sales_order_creditmemo.list.html "sales_order_creditmemo.list") -
    Retrieve the list of credit memos by filters
-   [sales_order_creditmemo.info](salesOrderCreditMemo/sales_order_creditmemo.info.html "sales_order_creditmemo.info") -
    Retrieve the credit memo information
-   [sales_order_creditmemo.create](salesOrderCreditMemo/sales_order_creditmemo.create.html "sales_order_creditmemo.create") -
    Create a new credit memo for order
-   [sales_order_creditmemo.addComment](salesOrderCreditMemo/sales_order_creditmemo.addComment.html "sales_order_creditmemo.addComment") -
    Add a new comment to the credit memo
-   [sales_order_creditmemo.cancel](salesOrderCreditMemo/sales_order_creditmemo.cancel.html "sales_order_creditmemo.cancel") -
    Cancel the credit memo

Create the Magento file system owner
