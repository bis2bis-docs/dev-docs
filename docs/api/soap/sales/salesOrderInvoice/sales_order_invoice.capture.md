

## [Module: Mage_Sales](#sales_order_invoice.capture-Module:MageSales)

## [Resource: sales_order_invoice](#sales_order_invoice.capture-Resource:salesorderinvoice)

**Aliases**:

-   order_invoice

## [Method:](#sales_order_invoice.capture-Method:)

-   sales_order_invoice.capture (SOAP V1)
-   salesOrderInvoiceCapture (SOAP V2)

Allows you to capture the required invoice. Note that not all order
invoices can be captured. Only some payment methods support capturing
the order invoice (e.g., PayPal Pro).

**Aliases**:

-   order_invoice.capture

**Arguments**:

---

Type Name Description

---

string sessionId Session ID

string invoiceIncrementId\ Invoice increment ID

---

**Returns**:

Type Description

---

boolean\\int True (1) if the order invoice is captured.

**Notes**:

You should check the invoice to see if it can be captured before
attempting to capture the invoice. Otherwise, the API call will generate
an error.

Invoices have states as defined in the model
Mage_Sales_Model_Order_Invoice:

-   STATE_OPEN = 1
-   STATE_PAID = 2
-   STATE_CANCELED = 3

Also note that there is a method call in the model that checks this for
you - canCapture(). And it also verifies that the payment can be
captured, so the invoice state might not be the only condition that is
required to allow it to be captured.

### [Examples](#sales_order_invoice.capture-Examples)

### [Request Example SOAP V1](#sales_order_invoice.capture-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$orderIncrementId = '100000016';

//Create invoice for order
$invoiceIncrementId = $proxy->call(
    $session,
    'sales_order_invoice.create',
    array(
        'orderIncrementId' => $orderIncrementId,
        array('order_item_id' => '15', 'qty' => '1')
    )
);

//Capture invoice amount
$result = $proxy->call(
    $session,
    'sales_order_invoice.capture',
    $invoiceIncrementId
);
```

</div>



### [Request Example SOAP V2](#sales_order_invoice.capture-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');
$sessionID = $proxy->login('apiUser', 'apiKey');

$orderIncrementId = '100000016';

//Create invoice for order
$qty = array(
    array('order_item_id' => '15', 'qty' => '1')
);
$invoiceIncrementId = $proxy->salesOrderInvoiceCreate(
     $sessionID,
     $orderIncrementId,
     $qty);

//Capture invoice amount
$result = $proxy->salesOrderInvoiceCapture(
     $sessionID,
     $invoiceIncrementId
);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#sales_order_invoice.capture-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->salesOrderInvoiceCapture((object)array('sessionId' => $sessionId->result, 'invoiceIncrementId' => '100000016'));

var_dump($result->result);
```

</div>


