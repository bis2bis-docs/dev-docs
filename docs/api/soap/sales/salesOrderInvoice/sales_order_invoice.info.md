

## [Module: Mage_Sales](#sales_order_invoice.info-Module:MageSales)

## [Resource: sales_order_invoice](#sales_order_invoice.info-Resource:salesorderinvoice)

**Aliases**:

-   order_invoice

## [Method:](#sales_order_invoice.info-Method:)

-   sales_order_invoice.info (SOAP V1)
-   salesOrderInvoiceInfo (SOAP V2)

Allows you to retrieve information about the required invoice.

**Aliases**:

-   order_invoice.info

**Arguments**:

Type Name Description

---

string sessionId Session ID
string invoiceIncrementId Invoice increment ID

**Returns**:

---

Type Name Description

---

array result Array of
salesOrderInvoiceEntity\

---

The **salesOrderInvoiceEntity** content is as follows:

---

Type Name Description

---

string increment_id\ Increment ID

string\ parent_id\ Parent ID

string\ store_id\ Store ID

string\ created_at\ Date of creation

string\ updated_at\ Date of updating

string\ is_active\ Defines whether the invoice is
active

string\ global_currency_code\ Global currency code

string\ base_currency_code\ Base currency code

string\ store_currency_code\ Store currency code

string\ order_currency_code\ Order currency code

string\ store_to_base_rate\ Store to base rate

string\ store_to_order_rate\ Store to order rate

string\ base_to_global_rate\ Base to global rate

string\ base_to_order_rate\ Base to order rate

string\ subtotal\ Subtotal

string\ base_subtotal\ Base subtotal

string\ base_grand_total\ Base grand total

string\ discount_amount\ Discount amount

string\ base_discount_amount\ Base discount amount

string\ shipping_amount\ Shipping amount

string\ base_shipping_amount\ Base shipping amount

string\ tax_amount\ Tax amount

string\ base_tax_amount\ Base tax amount

string\ billing_address_id\ Billing address ID

string\ billing_firstname\ First name in the billing
address

string\ billing_lastname\ Last name in the billing address

string\ order_id\ Order ID

string\ order_increment_id\ Order increment ID

string\ order_created_at\ Date of order creation

string\ state\ Order state

string\ grand_total\ Grand total

string\ invoice_id\ Invoice ID

array items\ Array of
salesOrderInvoiceItemEntity

array comments\ Array of
salesOrderInvoiceCommentEntity

---

The **salesOrderInvoiceItemEntity** content is as follows:

---

Type Name Description

---

string increment_id\ Increment ID

string\ parent_id\ Parent ID

string\ created_at\ Date of creation

string\ updated_at\ Date of updating

string\ is_active\ Active flag

string\ weee_tax_applied\ Applied fixed product
tax

string\ qty\ Quantity

string\ cost\ Cost

string\ price\ Price

string\ tax_amount\ Tax amount

string\ row_total\ Row total

string\ base_price\ Base price

string\ base_tax_amount\ Base tax amount

string\ base_row_total\ Base row total

string\ base_weee_tax_applied_amount\ Applied fixed product
tax amount (in base
currency)

string\ base_weee_tax_applied_row_amount\ Applied fixed product
tax row amount (in base
currency)

string\ weee_tax_applied_amount\ Applied fixed product
tax amount

string\ weee_tax_applied_row_amount\ Applied fixed product
tax row amount

string\ weee_tax_disposition\ Fixed product tax
disposition

string\ weee_tax_row_disposition\ Fixed product tax row
disposition

string\ base_weee_tax_disposition\ Fixed product tax
disposition (in base
currency)

string\ base_weee_tax_row_disposition\ Fixed product tax row
disposition (in base
currency)

string\ sku\ SKU

string\ name\ Name

string\ order_item_id\ Order item ID

string\ product_id\ Product ID

string\ item_id\ Item ID

---

The **salesOrderInvoiceCommentEntity** content is as follows:

---

Type Name Description

---

string increment_id\ Increment ID

string\ parent_id\ Parent ID

string\ created_at\ Date of creation

string\ updated_at\ Date of updating

string\ is_active\ Active flag

string\ comment\ Invoice comment

string\ is_customer_notified\ Defines whether the
customer is notified

string\ comment_id\ Comment ID

---

### [Examples](#sales_order_invoice.info-Examples)

### [Request Example SOAP V1](#sales_order_invoice.info-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'sales_order_invoice.info', '200000006');
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#sales_order_invoice.info-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->salesOrderInvoiceInfo($sessionId, '200000006');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#sales_order_invoice.info-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->salesOrderInvoiceInfo((object)array('sessionId' => $sessionId->result, 'invoiceIncrementId' => '200000006'));
var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#sales_order_invoice.info-ResponseExampleSOAPV1)

<div>

```
array
  'store_id' => string '2' (length=1)
  'base_grand_total' => string '384.9800' (length=8)
  'shipping_tax_amount' => string '0.0000' (length=6)
  'tax_amount' => string '0.0000' (length=6)
  'base_tax_amount' => string '0.0000' (length=6)
  'store_to_order_rate' => string '1.0000' (length=6)
  'base_shipping_tax_amount' => string '0.0000' (length=6)
  'base_discount_amount' => string '0.0000' (length=6)
  'base_to_order_rate' => string '1.0000' (length=6)
  'grand_total' => string '384.9800' (length=8)
  'shipping_amount' => string '5.0000' (length=6)
  'subtotal_incl_tax' => string '379.9800' (length=8)
  'base_subtotal_incl_tax' => string '379.9800' (length=8)
  'store_to_base_rate' => string '1.0000' (length=6)
  'base_shipping_amount' => string '5.0000' (length=6)
  'total_qty' => string '1.0000' (length=6)
  'base_to_global_rate' => string '1.0000' (length=6)
  'subtotal' => string '379.9800' (length=8)
  'base_subtotal' => string '379.9800' (length=8)
  'discount_amount' => string '0.0000' (length=6)
  'billing_address_id' => string '3' (length=1)
  'is_used_for_refund' => null
  'order_id' => string '2' (length=1)
  'email_sent' => null
  'can_void_flag' => string '0' (length=1)
  'state' => string '2' (length=1)
  'shipping_address_id' => string '4' (length=1)
  'store_currency_code' => string 'USD' (length=3)
  'transaction_id' => null
  'order_currency_code' => string 'USD' (length=3)
  'base_currency_code' => string 'USD' (length=3)
  'global_currency_code' => string 'USD' (length=3)
  'increment_id' => string '200000006' (length=9)
  'created_at' => string '2012-03-30 12:02:19' (length=19)
  'updated_at' => string '2012-03-30 12:02:19' (length=19)
  'hidden_tax_amount' => string '0.0000' (length=6)
  'base_hidden_tax_amount' => string '0.0000' (length=6)
  'shipping_hidden_tax_amount' => string '0.0000' (length=6)
  'base_shipping_hidden_tax_amnt' => null
  'shipping_incl_tax' => string '5.0000' (length=6)
  'base_shipping_incl_tax' => string '5.0000' (length=6)
  'base_total_refunded' => null
  'cybersource_token' => null
  'invoice_id' => string '1' (length=1)
  'order_increment_id' => string '200000002' (length=9)
  'items' =>
    array
      0 =>
        array
          'parent_id' => string '1' (length=1)
          'base_price' => string '379.9800' (length=8)
          'tax_amount' => string '0.0000' (length=6)
          'base_row_total' => string '379.9800' (length=8)
          'discount_amount' => null
          'row_total' => string '379.9800' (length=8)
          'base_discount_amount' => null
          'price_incl_tax' => string '379.9800' (length=8)
          'base_tax_amount' => string '0.0000' (length=6)
          'base_price_incl_tax' => string '379.9800' (length=8)
          'qty' => string '1.0000' (length=6)
          'base_cost' => null
          'price' => string '379.9800' (length=8)
          'base_row_total_incl_tax' => string '379.9800' (length=8)
          'row_total_incl_tax' => string '379.9800' (length=8)
          'product_id' => string '1' (length=1)
          'order_item_id' => string '3' (length=1)
          'additional_data' => null
          'description' => null
          'sku' => string 'n2610-slider' (length=12)
          'name' => string 'Nokia 2610 Phone' (length=16)
          'hidden_tax_amount' => string '0.0000' (length=6)
          'base_hidden_tax_amount' => string '0.0000' (length=6)
          'base_weee_tax_applied_amount' => string '0.0000' (length=6)
          'base_weee_tax_applied_row_amnt' => string '0.0000' (length=6)
          'base_weee_tax_applied_row_amount' => string '0.0000' (length=6)
          'weee_tax_applied_amount' => string '0.0000' (length=6)
          'weee_tax_applied_row_amount' => string '0.0000' (length=6)
          'weee_tax_applied' => string 'a:0:{}' (length=6)
          'weee_tax_disposition' => string '0.0000' (length=6)
          'weee_tax_row_disposition' => string '0.0000' (length=6)
          'base_weee_tax_disposition' => string '0.0000' (length=6)
          'base_weee_tax_row_disposition' => string '0.0000' (length=6)
          'item_id' => string '1' (length=1)
  'comments' =>
    array
      empty
```

</div>


