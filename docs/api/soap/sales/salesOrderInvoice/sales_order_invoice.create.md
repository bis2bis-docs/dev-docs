

## [Module: Mage_Sales](#sales_order_invoice.create-Module:MageSales)

## [Resource: sales_order_invoice](#sales_order_invoice.create-Resource:salesorderinvoice)

**Aliases**:

-   order_invoice

## [Method:](#sales_order_invoice.create-Method:)

-   sales_order_invoice.create (SOAP V1)
-   salesOrderInvoiceCreate (SOAP V2)

Allows you to create a new invoice for an order.

**Aliases**:

-   order_invoice.create

**Arguments**:

---

Type Name Description

---

string sessionId\ Session ID

string\ invoiceIncrementId\ Order increment ID

array itemsQty\ Array of orderItemIdQty
(quantity of items to
invoice)

string\ comment\ Invoice comment
(optional)

string\ email\ Send invoice on email
(optional)

string\ includeComment\ Include comments in
email (optional)

---

**Returns**:

Type Description

---

string ID of the created invoice

The **orderItemIdQty** content is as follows:

---

Type Name Description

---

int order_item_id\ Order item ID

double qty\ Quantity

---

### [Examples](#sales_order_invoice.create-Examples)

### [Request Example SOAP V1](#sales_order_invoice.create-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call(
    $session,
    'sales_order_invoice.create',
    array('orderIncrementId' => '200000008', array('15' => '1', '16' => '1'))
    // orderItemIdQty Array is Keyed with Order Item ID, with Value of qty to invoice
);
var_dump ($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#sales_order_invoice.create-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

//Create invoice for order

// orderItemIdQty Array is Keyed with Order Item ID, with Value of qty to invoice
$qty = array('15' => '1', '16' => '1');

$invoiceIncrementId = $proxy->salesOrderInvoiceCreate(
    $sessionID,
    '200000008',
    $qty
);
var_dump($invoiceIncrementId);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#sales_order_invoice.create-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->salesOrderInvoiceCreate((object)array('sessionId' => $sessionId->result, 'orderIncrementId' => '200000008', 'itemsQty' => array('15' => '1',  '16' => '1'), 'comment' => null,
'email' => null,
'includeComment' => null
));
var_dump($result->result);
```

</div>


