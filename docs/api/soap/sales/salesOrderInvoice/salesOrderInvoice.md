

## [Module: Mage_Sales](#MAPI-Invoice-Module:MageSales)

The Mage_Sales module allows you to manage sales orders, invoices,
shipments, and credit memos.

### [Invoice](#MAPI-Invoice-Invoice)

Allows you to manage invoices.

**Resource Name**: sales_order_invoice

**Aliases**:

-   order_invoice

**Methods**:

-   [sales_order_invoice.list](sales_order_invoice.list.html "sales_order_invoice.list") -
    Retrieve a list of invoices using filters
-   [sales_order_invoice.info](sales_order_invoice.info.html "sales_order_invoice.info") -
    Retrieve information about the invoice
-   [sales_order_invoice.create](sales_order_invoice.create.html "sales_order_invoice.create") -
    Create a new invoice for an order
-   [sales_order_invoice.addComment](sales_order_invoice.addComment.html "sales_order_invoice.addComment") -
    Add a new comment to an invoice
-   [sales_order_invoice.capture](sales_order_invoice.capture.html "sales_order_invoice.capture") -
    Capture an invoice
-   [sales_order_invoice.cancel](sales_order_invoice.cancel.html "sales_order_invoice.cancel") -
    Cancel an invoice

### [Faults](#MAPI-Invoice-Faults)

Fault Code Fault Message

---

100 Requested shipment does not exists.
101 Invalid filters given. Details in error message.
102 Invalid data given. Details in error message.
103 Requested order does not exists
104 Invoice status not changed.

### [Examples](#MAPI-Invoice-Examples)

### [Example 1. Basic working with invoices.](#MAPI-Invoice-Example1.Basicworkingwithinvoices.)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$notInvoicedOrderId  = '100000003';

// Create new invoice
$newInvoiceId = $proxy->call($sessionId, 'sales_order_invoice.create', array($notInvoicedOrderId, array(), 'Invoice Created', true, true));

// View new invoice
$invoice = $proxy->call($sessionId, 'sales_order_invoice.info', $newInvoiceId);

var_dump($invoice);

// Add Comment
$proxy->call($sessionId, 'sales_order_invoice.addComment', array($newInvoiceId, 'Invoice comment, some text', true, false));

// View invoice with new comment
$invoice = $proxy->call($sessionId, 'sales_order_invoice.info', $newInvoiceId);

var_dump($invoice);

$proxy->call($sessionId, 'sales_order_invoice.capture', $newInvoiceId);

// View captured invoice
$invoice = $proxy->call($sessionId, 'sales_order_invoice.info', $newInvoiceId);
var_dump($invoice);
```

</div>



Create the Magento file system owner
