

## [Module: Mage_Sales](#sales_order_invoice.addComment-Module:MageSales)

## [Resource: sales_order_invoice](#sales_order_invoice.addComment-Resource:salesorderinvoice)

**Aliases**:

-   order_invoice

## [Method:](#sales_order_invoice.addComment-Method:)

-   sales_order_invoice.addComment (SOAP V1)
-   salesOrderInvoiceAddComment (SOAP V2)

Allows you to add a new comment to the order invoice.

**Aliases**:

-   order_invoice.addComment

**Arguments**:

---

Type Name Description

---

string sessionId\ Session ID

string\ invoiceIncrementId\ Invoice increment ID

string\ comment\ Invoice comment
(optional)

int\ email\ Send invoice on email
flag (optional)

int\ includeComment\ Include comment in
email flag (optional)

---

**Returns**:

Type Description

---

boolean True if the comment is added to the invoice

### [Examples](#sales_order_invoice.addComment-Examples)

### [Request Example SOAP V1](#sales_order_invoice.addComment-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apikey');

$result = $client->call($session, 'sales_order_invoice.addComment', array('invoiceIncrementId' => '200000006', 'comment' => 'invoice comment'));
var_dump ($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#sales_order_invoice.addComment-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->salesOrderInvoiceAddComment($sessionId, '200000006');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#sales_order_invoice.addComment-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->salesOrderInvoiceAddComment((object)array('sessionId' => $sessionId->result, 'invoiceIncrementId' => '200000006', 'comment' => 'invoice comment', 'email' => null, 'includeComment' => null));
var_dump($result->result);
```

</div>


