

## [Module: Mage_Sales](#sales_order_invoice.list-Module:MageSales)

## [Resource: sales_order_invoice](#sales_order_invoice.list-Resource:salesorderinvoice)

**Aliases**:

-   order_invoice

## [Method:](#sales_order_invoice.list-Method:)

-   sales_order_invoice.list (SOAP V1)
-   salesOrderInvoiceList (SOAP V2)

Allows you to retrieve the list of order invoices. Additional filters
can also be applied.

**Aliases**:

-   order_invoice.list

**Arguments**:

---

Type Name Description

---

string sessionId\ Session ID

array filters\ Array of filters for
the list of invoices
(optional)

---

**Returns**:

Type Name Description

---

array result Array of salesOrderInvoiceEntity

The **salesOrderInvoiceEntity** content is as follows:

---

Type Name Description

---

string increment_id\ Increment ID

string\ created_at\ Date of invoice
creation

string\ order_currency_code\ Order currency code
(e.g., EUR)

string\ order_id\ Order ID

string\ state\ Order state

string\ grand_total\ Grand total amount
invoiced

string\ invoice_id\ Invoice ID

---

### [Examples](#sales_order_invoice.list-Examples)

### [Request Example SOAP V1](#sales_order_invoice.list-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'sales_order_invoice.list');
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



## [Request Example SOAP V2 (List of All Invoices)](#sales_order_invoice.list-RequestExampleSOAPV2%28ListofAllInvoices%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->salesOrderInvoiceList($sessionId);
var_dump($result);
```

</div>



## [Request Example SOAP V2 (Complex Filter)](#sales_order_invoice.list-RequestExampleSOAPV2%28ComplexFilter%29)

<div>

```
$client = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

// If some stuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');
$complexFilter = array(
    'complex_filter' => array(
        array(
            'key' => 'state',
            'value' => array('key' => 'in', 'value' => '2,3')
        )
    )
);
$result = $client->salesOrderInvoiceList($session, $complexFilter);

var_dump ($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#sales_order_invoice.list-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->salesOrderInvoiceList((object)array('sessionId' => $sessionId->result));
var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#sales_order_invoice.list-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
    array
      'order_id' => string '2' (length=1)
      'increment_id' => string '200000001' (length=9)
      'created_at' => string '2012-03-30 12:02:19' (length=19)
      'state' => string '2' (length=1)
      'grand_total' => string '384.9800' (length=8)
      'order_currency_code' => string 'USD' (length=3)
      'invoice_id' => null
  1 =>
    array
      'order_id' => string '3' (length=1)
      'increment_id' => string '200000002' (length=9)
      'created_at' => string '2012-03-30 12:06:20' (length=19)
      'state' => string '2' (length=1)
      'grand_total' => string '339.9900' (length=8)
      'order_currency_code' => string 'USD' (length=3)
      'invoice_id' => null
```

</div>


