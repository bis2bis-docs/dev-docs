

## [Module: Mage_Sales](#sales_order_invoice.cancel-Module:MageSales)

## [Resource: sales_order_invoice](#sales_order_invoice.cancel-Resource:salesorderinvoice)

**Aliases**:

-   order_invoice

## [Method:](#sales_order_invoice.cancel-Method:)

-   sales_order_invoice.cancel (SOAP V1)
-   salesOrderInvoiceCancel (SOAP V2)

Allows you to cancel the required invoice. Note that not all order
invoices can be canceled. Only some payment methods support canceling
the order invoice (e.g., Google Checkout, PayPal Pro, PayPal Express
Checkout).

**Aliases**:

-   order_invoice.cancel

**Arguments**:

---

Type Name Description

---

string sessionId\ Session ID

string invoiceIncrementId\ Invoice increment ID

---

**Returns**:

Type Description

---

boolean True if the order invoice is canceled.

### [Examples](#sales_order_invoice.cancel-Examples)

### [Request Example SOAP V1](#sales_order_invoice.cancel-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$invoiceIncrementId = '100000013';

$result = $proxy->call(
    $session,
    'sales_order_invoice.cancel',
    $invoiceIncrementId
);
```

</div>



Create the Magento file system owner
