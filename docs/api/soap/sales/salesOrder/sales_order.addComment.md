

## [Module: Mage_Sales](#sales_order.addComment-Module:MageSales)

## [Resource: sales_order](#sales_order.addComment-Resource:salesorder)

**Aliases**:

-   order

## [Method:](#sales_order.addComment-Method:)

-   sales_order.addComment (SOAP V1)
-   salesOrderAddComment (SOAP V2)

Allows you to add a new comment to the order.

**Aliases**:

-   order.addComment

**Arguments**:

---

Type Name Description

---

string sessionId\ Session ID

string orderIncrementId\ Order increment ID

string status\ Order status (pending,
processing, etc.)

string comment\ Order comment
(optional)

string notify\ Notification flag
(optional)

---

**Returns**:

Type Description

---

boolean\\int True (1) if the comment is added to the order

### [Examples](#sales_order.addComment-Examples)

### [Request Example SOAP V1](#sales_order.addComment-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'sales_order.addComment', array('orderIncrementId' => '200000004', 'status' => 'processing'));
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#sales_order.addComment-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->salesOrderAddComment($sessionId, '200000004', 'processing');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#sales_order.addComment-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->salesOrderAddComment((object)array('sessionId' => $sessionId->result, 'orderIncrementId' => '200000004', 'status' => 'processing', 'comment' => 'comment to the order', 'notify' => null));
var_dump($result->result);
```

</div>


