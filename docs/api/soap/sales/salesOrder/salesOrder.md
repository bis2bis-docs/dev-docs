

## [Module: Mage_Sales](#MAPI-Order-Module:MageSales)

The Mage_Sales module allows you to manage sales orders, invoices,
shipments, and credit memos.

### [Order](#MAPI-Order-Order)

Allows you to manage orders.

**Resource Name**: sales_order

**Aliases**:

-   order

**Methods**:

-   [sales_order.list](sales_order.list.html "sales_order.list") -
    Retrieve the list of orders using filters
-   [sales_order.info](sales_order.info.html "sales_order.info") -
    Retrieve the order information
-   [sales_order.addComment](sales_order.addComment.html "sales_order.addComment") -
    Add a comment to an order
-   [sales_order.hold](sales_order.hold.html "sales_order.hold") - Hold
    an order
-   [sales_order.unhold](sales_order.unhold.html "sales_order.unhold") -
    Unhold an order
-   [sales_order.cancel](sales_order.cancel.html "sales_order.cancel") -
    Cancel an order

### [Faults](#MAPI-Order-Faults)

Fault Code Fault Message

---

100 Requested order not exists.
101 Invalid filters given. Details in error message.
102 Invalid data given. Details in error message.
103 Order status not changed. Details in error message.

### [Examples](#MAPI-Order-Examples)

### [Example 1. Work with orders](#MAPI-Order-Example1.Workwithorders)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

// Getting list of orders created by John Doe
var_dump($proxy->call($sessionId, 'sales_order.list', array(array('customer_firstname'=>array('eq'=>'John'), 'customer_lastname'=>array('eq'=>'Doe')))));


// Get order info 100000003
var_dump($proxy->call($sessionId, 'sales_order.info', '100000003'));


// Hold order 100000003
$proxy->call($sessionId, 'sales_order.hold', '100000003');

// Unhold order 100000003
$proxy->call($sessionId, 'sales_order.unhold', '100000003');

// Hold order and add comment 100000003
$proxy->call($sessionId, 'sales_order.addComment', array('100000003', 'holded',  'You order is holded',  true));

// Unhold order and add comment 100000003
$proxy->call($sessionId, 'sales_order.addComment', array('100000003', 'pending', 'You order is pending', true));

// Get order info 100000003
var_dump($proxy->call($sessionId, 'sales_order.info', '100000003'));
```

</div>


