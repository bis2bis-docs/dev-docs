

## [Module: Mage_Sales](#sales_order.info-Module:MageSales)

## [Resource: sales_order](#sales_order.info-Resource:salesorder)

**Aliases**:

-   order

## [Method:](#sales_order.info-Method:)

-   sales_order.info (SOAP V1)
-   salesOrderInfo (SOAP V2)

Allows you to retrieve the required order information.

**Aliases**:

-   order.info

**Arguments**:

---

Type Name Description

---

string sessionId Session ID

string orderIncrementId\ Order increment ID

---

**Returns**:

Type Name Description

---

array result Array of salesOrderEntity

The **salesOrderEntity** content is as follows:

---

Type Name Description

---

string increment_id\ Increment ID

string\ parent_id\ Parent ID

string\ store_id\ Store ID

string\ created_at\ Date of creation

string\ updated_at\ Date of updating

string\ is_active\ Defines whether the order is
active

string\ customer_id\ Customer ID

string\ tax_amount\ Tax amount

string\ shipping_amount\ Shipping amount

string\ discount_amount\ Discount amount

string\ subtotal\ Subtotal sum

string\ grand_total\ Grand total sum

string\ total_paid\ Total paid

string\ total_refunded\ Total refunded

string\ total_qty_ordered\ Total quantity ordered

string\ total_canceled\ Total canceled

string\ total_invoiced\ Total invoiced

string\ total_online_refunded\ Total online refunded

string\ total_offline_refunded\ Total offline refunded

string\ base_tax_amount\ Base tax amount

string\ base_shipping_amount\ Base shipping amount

string\ base_discount_amount\ Base discount amount

string\ base_subtotal\ Base subtotal

string\ base_grand_total\ Base grand total

string\ base_total_paid\ Base total paid

string\ base_total_refunded\ Base total refunded

string\ base_total_qty_ordered\ Base total quantity ordered

string\ base_total_canceled\ Base total canceled

string\ base_total_invoiced\ Base total invoiced

string\ base_total_online_refunded\ Base total online refunded

string\ base_total_offline_refunded\ Base total offline refunded

string\ billing_address_id\ Billing address ID

string\ billing_firstname\ First name in the billing
address

string\ billing_lastname\ Last name in the billing
address

string\ shipping_address_id\ Shipping address ID

string\ shipping_firstname\ First name in the shipping
address

string\ shipping_lastname\ Last name in the shipping
address

string\ billing_name\ Billing name

string\ shipping_name\ Shipping name

string\ store_to_base_rate\ Store to base rate

string\ store_to_order_rate\ Store to order rate

string\ base_to_global_rate\ Base to global rate

string\ base_to_order_rate\ Base to order rate

string\ weight\ Weight

string\ store_name\ Store name

string\ remote_ip\ Remote IP

string\ status\ Order status

string\ state\ Order state

string\ applied_rule_ids\ Applied rule IDs

string\ global_currency_code\ Global currency code

string\ base_currency_code\ Base currency code

string\ store_currency_code\ Store currency code

string\ order_currency_code\ Order currency code

string\ shipping_method\ Shipping method

string\ shipping_description\ Shipping description

string\ customer_email\ Email address of the customer

string\ customer_firstname\ Customer first name

string\ customer_lastname\ Customer last name

string\ quote_id\ Shopping cart ID

string\ is_virtual\ Defines whether the product is
a virtual one

string\ customer_group_id\ Customer group ID

string\ customer_note_notify\ Customer notification

string\ customer_is_guest\ Defines whether the customer is
a guest

string\ email_sent\ Defines whether the email
notification is sent

string\ order_id\ Order ID

string\ gift_message_id\ Gift message ID

string\ gift_message\ Gift message

array shipping_address\ Array of
salesOrderAddressEntity

array billing_address\ Array of
salesOrderAddressEntity

array items\ Array of salesOrderItemEntity

array payment\ Array of
salesOrderPaymentEntity

array status_history\ Array of
salesOrderStatusHistoryEntity

---

The **salesOrderAddressEntity** content is as follows:

---

Type Name Description

---

string increment_id\ Increment ID

string\ parent_id\ Parent ID

string\ created_at\ Date of creation

string\ updated_at\ Date of updating

string\ is_active\ Defines whether the
address is active

string\ address_type\ Address type

string\ firstname\ First name

string\ lastname\ Last name

string\ company\ Company name

string\ street\ Street name

string\ city\ City

string\ region\ Region

string\ postcode\ Post code

string\ country_id\ Country ID

string\ telephone\ Telephone number

string\ fax\ Fax number

string\ region_id\ Region ID

string\ address_id\ Address ID

---

The **salesOrderItemEntity** content is as follows:

---

Type Name Description

---

string item_id\ Item ID

string\ order_id\ Order ID

string\ quote_item_id\ Shopping cart item ID

string\ created_at\ Date of creation

string\ updated_at\ Date of updating

string\ product_id\ Product ID

string\ product_type\ Product type

string\ product_options\ Product options

string\ weight\ Weight

string\ is_virtual\ Defines whether the
product is a virtual
one

string\ sku\ ﻿Product SKU

string\ name\ ﻿Product name

string\ applied_rule_ids\ Applied rule IDs

string\ free_shipping\ Defines whether free
shipping is applied

string\ is_qty_decimal\ Defines whether the
items quantity is
decimal

string\ no_discount\ Defines whether no
discount is applied

string\ qty_canceled\ Items quantity canceled

string\ qty_invoiced\ Items quantity invoiced

string\ qty_ordered\ Items quantity ordered

string\ qty_refunded\ Items quantity refunded

string\ qty_shipped\ Items quantity shipped

string\ cost\ Cost

string\ price\ Price

string\ base_price\ Base price

string\ original_price\ Original price

string\ base_original_price\ Base original price

string\ tax_percent\ Tax percent

string\ tax_amount\ Tax amount

string\ base_tax_amount\ Base tax amount

string\ tax_invoiced\ Tax invoiced

string\ base_tax_invoiced\ Base tax invoiced

string\ discount_percent\ Discount percent

string\ discount_amount\ Discount amount

string\ base_discount_amount\ Base discount amount

string\ discount_invoiced\ Discount invoiced

string\ base_discount_invoiced\ Base discount invoiced

string\ amount_refunded\ Amount refunded

string\ base_amount_refunded\ Base amount refunded

string\ row_total\ Row total

string\ base_row_total\ Base row total

string\ row_invoiced\ Row invoiced

string\ base_row_invoiced\ Base row invoiced

string\ row_weight\ Row weight

string\ gift_message_id\ Gift message ID

string\ gift_message\ Gift message

string\ gift_message_available\ Defines whether the
gift message is
available

string\ base_tax_before_discount\ Base tax before
discount

string\ tax_before_discount\ Tax before discount

string\ weee_tax_applied\ Applied fixed product
tax

string\ weee_tax_applied_amount\ Applied fixed product
tax amount

string\ weee_tax_applied_row_amount\ Applied fixed product
tax row amount

string\ base_weee_tax_applied_amount\ Applied fixed product
tax amount (in base
currency)

string\ base_weee_tax_applied_row_amount\ Applied fixed product
tax row amount (in base
currency)

string\ weee_tax_disposition\ Fixed product tax
disposition

string\ weee_tax_row_disposition\ Fixed product tax row
disposition

string\ base_weee_tax_disposition\ Fixed product tax
disposition (in base
currency)

string\ base_weee_tax_row_disposition\ Fixed product tax row
disposition (in base
currency)

---

The **salesOrderPaymentEntity** content is as follows:

---

Type Name Description

---

string increment_id\ Increment ID

string\ parent_id\ Parent ID

string\ created_at\ Date of creation

string\ updated_at\ Date of updating

string\ is_active\ Active flag

string\ amount_ordered\ Amount ordered

string\ shipping_amount\ Shipping amount

string\ base_amount_ordered\ Base amount ordered

string\ base_shipping_amount\ Base shipping amount

string\ method\ Payment method

string\ po_number\ Purchase order number

string\ cc_type\ Credit card type

string\ cc_number_enc\ Credit card number

string\ cc_last4\ Credit card last 4
digits

string\ cc_owner\ Credit card owner

string\ cc_exp_month\ Credit card expiration
month

string\ cc_exp_year\ Credit card expiration
year

string\ cc_ss_start_month\ Credit card start month
(Switch/Solo)

string\ cc_ss_start_year\ Credit card start year
(Switch/Solo)

string\ payment_id\ Payment ID

---

The **salesOrderStatusHistoryEntity** content is as follows:

---

Type Name Description

---

string increment_id\ Increment ID

string\ parent_id\ Parent ID

string\ created_at\ Date of creation

string\ updated_at\ Date of updating

string\ is_active\ Active flag

string\ is_customer_notified\ Defines whether the
customer is notified

string\ status\ Order status

string\ comment\ Order comment

---

### [Examples](#sales_order.info-Examples)

### [Request Example SOAP V1](#sales_order.info-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'sales_order.info', 'orderIncrementId');
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#sales_order.info-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->salesOrderInfo($sessionId, '200000006');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#sales_order.info-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->salesOrderInfo((object)array('sessionId' => $sessionId->result, 'orderIncrementId' => '200000006'));
var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#sales_order.info-ResponseExampleSOAPV1)

<div>

```
array
  'state' => string 'new' (length=3)
  'status' => string 'pending' (length=7)
  'coupon_code' => null
  'protect_code' => string 'defe18' (length=6)
  'shipping_description' => string 'Flat Rate - Fixed' (length=17)
  'is_virtual' => string '0' (length=1)
  'store_id' => string '2' (length=1)
  'customer_id' => string '2' (length=1)
  'base_discount_amount' => string '0.0000' (length=6)
  'base_discount_canceled' => null
  'base_discount_invoiced' => null
  'base_discount_refunded' => null
  'base_grand_total' => string '619.9700' (length=8)
  'base_shipping_amount' => string '10.0000' (length=7)
  'base_shipping_canceled' => null
  'base_shipping_invoiced' => null
  'base_shipping_refunded' => null
  'base_shipping_tax_amount' => string '0.0000' (length=6)
  'base_shipping_tax_refunded' => null
  'base_subtotal' => string '609.9700' (length=8)
  'base_subtotal_canceled' => null
  'base_subtotal_invoiced' => null
  'base_subtotal_refunded' => null
  'base_tax_amount' => string '0.0000' (length=6)
  'base_tax_canceled' => null
  'base_tax_invoiced' => null
  'base_tax_refunded' => null
  'base_to_global_rate' => string '1.0000' (length=6)
  'base_to_order_rate' => string '1.0000' (length=6)
  'base_total_canceled' => null
  'base_total_invoiced' => null
  'base_total_invoiced_cost' => null
  'base_total_offline_refunded' => null
  'base_total_online_refunded' => null
  'base_total_paid' => null
  'base_total_qty_ordered' => null
  'base_total_refunded' => null
  'discount_amount' => string '0.0000' (length=6)
  'discount_canceled' => null
  'discount_invoiced' => null
  'discount_refunded' => null
  'grand_total' => string '619.9700' (length=8)
  'shipping_amount' => string '10.0000' (length=7)
  'shipping_canceled' => null
  'shipping_invoiced' => null
  'shipping_refunded' => null
  'shipping_tax_amount' => string '0.0000' (length=6)
  'shipping_tax_refunded' => null
  'store_to_base_rate' => string '1.0000' (length=6)
  'store_to_order_rate' => string '1.0000' (length=6)
  'subtotal' => string '609.9700' (length=8)
  'subtotal_canceled' => null
  'subtotal_invoiced' => null
  'subtotal_refunded' => null
  'tax_amount' => string '0.0000' (length=6)
  'tax_canceled' => null
  'tax_invoiced' => null
  'tax_refunded' => null
  'total_canceled' => null
  'total_invoiced' => null
  'total_offline_refunded' => null
  'total_online_refunded' => null
  'total_paid' => null
  'total_qty_ordered' => string '2.0000' (length=6)
  'total_refunded' => null
  'can_ship_partially' => null
  'can_ship_partially_item' => null
  'customer_is_guest' => string '0' (length=1)
  'customer_note_notify' => string '1' (length=1)
  'billing_address_id' => string '1' (length=1)
  'customer_group_id' => string '1' (length=1)
  'edit_increment' => null
  'email_sent' => string '1' (length=1)
  'forced_shipment_with_invoice' => null
  'payment_auth_expiration' => null
  'quote_address_id' => null
  'quote_id' => string '1' (length=1)
  'shipping_address_id' => string '2' (length=1)
  'adjustment_negative' => null
  'adjustment_positive' => null
  'base_adjustment_negative' => null
  'base_adjustment_positive' => null
  'base_shipping_discount_amount' => string '0.0000' (length=6)
  'base_subtotal_incl_tax' => string '609.9700' (length=8)
  'base_total_due' => null
  'payment_authorization_amount' => null
  'shipping_discount_amount' => string '0.0000' (length=6)
  'subtotal_incl_tax' => string '609.9700' (length=8)
  'total_due' => null
  'weight' => string '1.2000' (length=6)
  'customer_dob' => null
  'increment_id' => string '200000001' (length=9)
  'applied_rule_ids' => null
  'base_currency_code' => string 'USD' (length=3)
  'customer_email' => string 'john@example.com' (length=16)
  'customer_firstname' => string 'johny' (length=5)
  'customer_lastname' => string 'doe' (length=3)
  'customer_middlename' => null
  'customer_prefix' => null
  'customer_suffix' => null
  'customer_taxvat' => null
  'discount_description' => null
  'ext_customer_id' => null
  'ext_order_id' => null
  'global_currency_code' => string 'USD' (length=3)
  'hold_before_state' => null
  'hold_before_status' => null
  'order_currency_code' => string 'USD' (length=3)
  'original_increment_id' => null
  'relation_child_id' => null
  'relation_child_real_id' => null
  'relation_parent_id' => null
  'relation_parent_real_id' => null
  'remote_ip' => string '127.0.0.1' (length=9)
  'shipping_method' => string 'flatrate_flatrate' (length=17)
  'store_currency_code' => string 'USD' (length=3)
  'store_name' => string 'website
English store
English' (length=29)
  'x_forwarded_for' => null
  'customer_note' => null
  'created_at' => string '2012-03-30 11:40:30' (length=19)
  'updated_at' => string '2012-03-30 11:40:32' (length=19)
  'total_item_count' => string '2' (length=1)
  'customer_gender' => null
  'hidden_tax_amount' => string '0.0000' (length=6)
  'base_hidden_tax_amount' => string '0.0000' (length=6)
  'shipping_hidden_tax_amount' => string '0.0000' (length=6)
  'base_shipping_hidden_tax_amnt' => string '0.0000' (length=6)
  'hidden_tax_invoiced' => null
  more elements...
```

</div>


