

## [Module: Mage_Sales](#sales_order.list-Module:MageSales)

## [Resource: sales_order](#sales_order.list-Resource:salesorder)

**Aliases**:

-   order

## [Method:](#sales_order.list-Method:)

-   sales_order.list (SOAP V1)
-   salesOrderList (SOAP V2)

Allows you to retrieve the list of orders. Additional filters can be
applied.

**Aliases**:

-   order.list
-   salesOrderList (SOAP V2 method name)

**Arguments**:

Type Name Description

---

string sessionId Session ID
array filters Array of filters for the list of sales orders (optional)

**Returns**:

Type name Description

---

array result Array of salesOrderEntity

The **salesOrderEntity** content is as follows:

---

Type Name Description

---

string increment_id\ Increment ID

string\ parent_id\ Parent ID

string\ store_id\ Store ID

string\ created_at\ Date of creation

string\ updated_at\ Date of updating

string\ is_active\ Defines whether the
order is active

string\ customer_id\ Customer ID

string\ tax_amount\ Tax amount

string\ shipping_amount\ Shipping amount

string\ discount_amount\ Discount amount

string\ subtotal\ Subtotal sum

string\ grand_total\ Grand total sum

string\ total_paid\ Total paid

string\ total_refunded\ Total refunded

string\ total_qty_ordered\ Total quantity ordered

string\ total_canceled\ Total canceled

string\ total_invoiced\ Total invoiced

string\ total_online_refunded\ Total online refunded

string\ total_offline_refunded\ Total offline refunded

string\ base_tax_amount\ Base tax amount

string\ base_shipping_amount\ Base shipping amount

string\ base_discount_amount\ Base discount amount

string\ base_subtotal\ Base subtotal

string\ base_grand_total\ Base grand total

string\ base_total_paid\ Base total paid

string\ base_total_refunded\ Base total refunded

string\ base_total_qty_ordered\ Base total quantity
ordered

string\ base_total_canceled\ Base total canceled

string\ base_total_invoiced\ Base total invoiced

string\ base_total_online_refunded\ Base total online
refunded

string\ base_total_offline_refunded\ Base total offline
refunded

string\ billing_address_id\ Billing address ID

string\ billing_firstname\ First name in the
billing address

string\ billing_lastname\ Last name in the
billing address

string\ shipping_address_id\ Shipping address ID

string\ shipping_firstname\ First name in the
shipping address

string\ shipping_lastname\ Last name in the
shipping address

string\ billing_name\ Billing name

string\ shipping_name\ Shipping name

string\ store_to_base_rate\ Store to base rate

string\ store_to_order_rate\ Store to order rate

string\ base_to_global_rate\ Base to global rate

string\ base_to_order_rate\ Base to order rate

string\ weight\ Weight

string\ store_name\ Store name

string\ remote_ip\ Remote IP

string\ status\ Order status

string\ state\ Order state

string\ applied_rule_ids\ Applied rule IDs

string\ global_currency_code\ Global currency code

string\ base_currency_code\ Base currency code

string\ store_currency_code\ Store currency code

string\ order_currency_code\ Order currency code

string\ shipping_method\ Shipping method

string\ shipping_description\ Shipping description

string\ customer_email\ Email address of the
customer

string\ customer_firstname\ Customer first name

string\ customer_lastname\ Customer last name

string\ quote_id\ Shopping cart ID

string\ is_virtual\ Defines whether the
product is a virtual
one

string\ customer_group_id\ Customer group ID

string\ customer_note_notify\ Customer notification

string\ customer_is_guest\ Defines whether the
customer is a guest

string\ email_sent\ Defines whether the
email notification is
sent

string\ order_id\ Order ID

string\ gift_message_id\ Gift message ID

string\ gift_message\ Gift message

---

### [Examples](#sales_order.list-Examples)

### [Request Example SOAP V1](#sales_order.list-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'order.list');
var_dump ($result);
```

</div>



### [Request Example SOAP V2](#sales_order.list-RequestExampleSOAPV2)

<div>

```
$params = array(array(
            'filter' => array(
                array(
                    'key' => 'status',
                    'value' => 'pending'
                ),
                array(
                    'key' => 'created_at',
                    'value' => '2001-11-25 12:12:07',
                )
            ),
            'complex_filter' => array(
                array(
                    'key' => 'order_id',
                    'value' => array(
                        'key' => 'in',
                        'value' => '12,23'
                    ),
                ),
                array(
                    'key' => 'protect_code',
                    'value' => array(
                        'key' => 'eq',
                        'value' => 'ebb2a0'
                    ),
                ),
            )
        ));

$result = $client->__call('salesOrderList', $params);
```

</div>



## [Request Example SOAP V2 (Simple Filter)](#sales_order.list-RequestExampleSOAPV2%28SimpleFilter%29)

<div>

```
$client = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

// If some stuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');
$filter = array('filter' => array(array('key' => 'status', 'value' => 'closed')));
$result = $client->salesOrderList($session, $filter);

var_dump ($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#sales_order.list-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->salesOrderList((object)array('sessionId' => $sessionId->result, 'filters' => null));
var_dump($result->result);
```

</div>



### [XML SOAP V2 Example](#sales_order.list-XMLSOAPV2Example)

**SOAP \"v2\" XML Request**


<div>

```
<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="urn:Magento"
                   xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                   xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/"
                   SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
    <SOAP-ENV:Body>
        <ns1:salesOrderList>
            <sessionId xsi:type="xsd:string">abbc417256a3ffb93d130a77a2fd3665</sessionId>
            <filters xsi:type="ns1:filters">
                <filter SOAP-ENC:arrayType="ns1:associativeEntity[2]" xsi:type="ns1:associativeArray">
                    <item xsi:type="ns1:associativeEntity">
                        <key xsi:type="xsd:string">status</key>
                        <value xsi:type="xsd:string">pending</value>
                    </item>
                    <item xsi:type="ns1:associativeEntity">
                        <key xsi:type="xsd:string">created_at</key>
                        <value xsi:type="xsd:string">2011-11-29 15:41:11</value>
                    </item>
                </filter>
                <complex_filter SOAP-ENC:arrayType="ns1:complexFilter[2]" xsi:type="ns1:complexFilterArray">
                    <item xsi:type="ns1:complexFilter">
                        <key xsi:type="xsd:string">order_id</key>
                        <value xsi:type="ns1:associativeEntity">
                            <key xsi:type="xsd:string">in</key>
                            <value xsi:type="xsd:string">Array</value>
                        </value>
                    </item>
                    <item xsi:type="ns1:complexFilter">
                        <key xsi:type="xsd:string">protect_code</key>
                        <value xsi:type="ns1:associativeEntity">
                            <key xsi:type="xsd:string">in</key>
                            <value xsi:type="xsd:string">a4ffa8</value>
                        </value>
                    </item>
                </complex_filter>
            </filters>
        </ns1:salesOrderList>
    </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
```

</div>


