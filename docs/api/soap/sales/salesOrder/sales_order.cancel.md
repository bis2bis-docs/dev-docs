

## [Module: Mage_Sales](#sales_order.cancel-Module:MageSales)

## [Resource: sales_order](#sales_order.cancel-Resource:salesorder)

**Aliases**:

-   order

## [Method:](#sales_order.cancel-Method:)

-   sales_order.cancel (SOAP V1)
-   salesOrderCancel (SOAP V2)

Allows you to cancel the required order.

**Aliases**:

-   order.cancel

**Arguments**:

---

Type Name Description

---

string sessionId\ Session ID

string orderIncrementId\ Order increment ID

---

**Returns**:

Type Description

---

boolean True if the order is canceled

### [Examples](#sales_order.cancel-Examples)

### [Request Example SOAP V1](#sales_order.cancel-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'sales_order.cancel', '200000004');
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#sales_order.cancel-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->salesOrderCancel($sessionId, '200000004');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#sales_order.cancel-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->salesOrderCancel((object)array('sessionId' => $sessionId->result, 'orderIncrementId' => '200000004'));
var_dump($result->result);
```

</div>


