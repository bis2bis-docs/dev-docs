

## [Module: Mage_Sales](#sales_order.hold-Module:MageSales)

## [Resource: sales_order](#sales_order.hold-Resource:salesorder)

**Aliases**:

-   order

## [Method:](#sales_order.hold-Method:)

-   sales_order.hold (SOAP V1)
-   salesOrderHold (SOAP V2)

Allows you to place the required order on hold.

**Aliases**:

-   order.hold

**Arguments**:

---

Type Name Description

---

string sessionId\ Session ID

string orderIncrementId\ Order increment ID

---

**Returns**:

Type Description

---

boolean\\int True (1) if the order is placed on hold

### [Examples](#sales_order.hold-Examples)

### [Request Example SOAP V1](#sales_order.hold-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'sales_order.hold', '200000006');
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#sales_order.hold-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->salesOrderHold($sessionId, '200000006');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#sales_order.hold-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->salesOrderHold((object)array('sessionId' => $sessionId->result, 'orderIncrementId' => '200000006'));
var_dump($result->result);
```

</div>


