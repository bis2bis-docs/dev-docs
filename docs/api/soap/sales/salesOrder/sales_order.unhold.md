

## [Module: Mage_Sales](#sales_order.unhold-Module:MageSales)

## [Resource: sales_order](#sales_order.unhold-Resource:salesorder)

**Aliases**:

-   order

## [Method:](#sales_order.unhold-Method:)

-   sales_order.unhold (SOAP V1)
-   salesOrderUnhold (SOAP V2)

Allows you to unhold the required order.

**Aliases**:

-   order.unhold

**Arguments**:

---

Type Name Description

---

string sessionId\ Session ID

string orderIncrementId\ Order increment ID

---

**Returns**:

Type Description

---

boolean\\int True (1) if the order is unheld

### [Examples](#sales_order.unhold-Examples)

### [Request Example SOAP V1](#sales_order.unhold-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'sales_order.unhold', '200000006');
var_dump($result);
```

</div>



### [Request Example SOAP V2](#sales_order.unhold-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$result = $proxy->salesOrderUnhold($sessionId, '200000006');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#sales_order.unhold-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->salesOrderUnhold((object)array('sessionId' => $sessionId->result, 'orderIncrementId' => '200000006'));
var_dump($result->result);
```

</div>


