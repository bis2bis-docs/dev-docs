

### [Module: Order Credit Memo API](#order_creditmemo.list-Module:OrderCreditMemoAPI)

## [Resource: sales_order_creditmemo](#order_creditmemo.list-Resource:salesordercreditmemo)

## [Aliases: order_creditmemo](#order_creditmemo.list-Aliases:ordercreditmemo)

## [Method:](#order_creditmemo.list-Method:)

-   order_creditmemo.list (SOAP V1)
-   salesOrderCreditmemoList (SOAP V2)

Allows you to retrieve the list of credit memos by filters.

**Arguments:**

Type Name Description

---

string sessionId Session ID
associative array filters Filters (optional)

**Return:**

Type Name Description

---

array result Array of salesOrderCreditmemoEntity

The **salesOrderCreditmemoEntity** content is as follows:

---

Type Name Description

---

string updated_at Date of updating

string\ created_at\ Date of creation

string\ increment_id\ Increment ID

string\ transaction_id\ Transaction ID

string\ global_currency_code\ Global currency code

string\ base_currency_code\ Base currency code

string\ order_currency_code\ Order currency code

string\ store_currency_code\ Store currency code

string\ cybersource_token\ Cybersource token

string\ invoice_id\ ID of the invoice for which the
credit memo was created

string\ billing_address_id\ Billing address ID

string\ shipping_address_id\ Shipping address ID

string\ state\ State

string\ creditmemo_status\ Credit memo status

string\ email_sent\ Defines whether the email is sent

string\ order_id\ ID of the order for which the
credit memo was created

string\ tax_amount\ Tax amount

string\ shipping_tax_amount\ Shipping tax amount

string\ base_tax_amount\ Base tax amount

string\ base_adjustment_positive\ Adjustment refund amount (using
base currency)

string\ base_grand_total\ Base grand total

string\ adjustment\ Adjustment

string\ subtotal\ Subtotal

string\ discount_amount\ Discount amount

string\ base_subtotal\ Base subtotal

string\ base_adjustment\ Base adjustment

string\ base_to_global_rate\ Base to global rate

string\ store_to_base_rate\ Store to base rate

string\ base_shipping_amount\ Base shipping amount

string\ adjustment_negative\ Adjustment fee amount

string\ subtotal_incl_tax\ Subtotal including tax

string\ shipping_amount\ Shipping amount

string\ base_subtotal_incl_tax\ Base subtotal including tax

string\ base_adjustment_negative\ Adjustment fee amount (using base
currency)

string\ grand_total\ Grand total

string\ base_discount_amount\ Base discount amount

string\ base_to_order_rate\ Base to order rate

string\ store_to_order_rate\ Store to order rate

string\ base_shipping_tax_amount\ Base shipping tax amount

string\ adjustment_positive\ Adjustment refund amount

string\ store_id\ Store ID

string\ hidden_tax_amount\ Hidden tax amount

string\ base_hidden_tax_amount\ Base hidden tax amount

string\ shipping_hidden_tax_amount\ Shipping hidden tax amount

string\ base_shipping_hidden_tax_amnt\ Base shipping hidden tax amount

string\ shipping_incl_tax\ Shipping including tax

string\ base_shipping_incl_tax\ Base shipping including tax

string\ base_customer_balance_amount\ Base customer balance amount

string\ customer_balance_amount\ Customer balance amount

string\ bs_customer_bal_total_refunded\ Refunded base customer balance
amount

string\ customer_bal_total_refunded\ Customer balance total refunded

string\ base_gift_cards_amount\ Base gift cards amount

string\ gift_cards_amount\ Gift cards amount

string\ gw_base_price\ Gift wrapping price refunded amount
(using base currency)

string\ gw_price\ Gift wrapping price refunded
amount

string\ gw_items_base_price\ Gift wrapping items base price

string\ gw_items_price\ Gift wrapping items price

string\ gw_card_base_price\ Gift wrapping card base price

string\ gw_card_price\ Gift wrapping card price

string\ gw_base_tax_amount\ Gift wrapping tax amount refunded
(using base currency)

string\ gw_tax_amount\ Gift wrapping tax amount refunded

string\ gw_items_base_tax_amount\ Gift wrapping items base tax amount

string\ gw_items_tax_amount\ Gift wrapping items tax amount

string\ gw_card_base_tax_amount\ Gift wrapping card base tax amount

string\ gw_card_tax_amount\ Gift wrapping card tax amount

string\ base_reward_currency_amount\ Base reward currency amount

string\ reward_currency_amount\ Reward currency amount

string\ reward_points_balance\ Reward points balance

string\ reward_points_balance_refund\ Reward points balance refund

string\ creditmemo_id\ Credit memo ID

array items\ Array of
salesOrderCreditmemoItemEntity

array comments\ Array of
salesOrderCreditmemoCommentEntity

---

The **salesOrderCreditmemoItemEntity** content is as follows:

---

Type Name Description

---

string item_id Credit memo item ID

string\ parent_id\ Parent ID

string\ weee_tax_applied_row_amount\ Applied fixed product
tax row amount

string\ base_price\ Base price

string\ base_weee_tax_row_disposition\ Fixed product tax row
disposition (in base
currency)

string\ tax_amount\ Tax amount

string\ base_weee_tax_applied_amount\ Applied fixed product
tax amount (in base
currency)

string\ weee_tax_row_disposition\ Fixed product tax row
disposition

string\ base_row_total\ Base row total

string\ discount_amount\ Discount amount

string\ row_total\ Row total

string\ weee_tax_applied_amount\ Applied fixed product
tax amount

string\ base_discount_amount\ Base discount amount

string\ base_weee_tax_disposition\ Fixed product tax
disposition (in base
currency)

string\ price_incl_tax\ Price including tax

string\ base_tax_amount\ Base tax amount

string\ weee_tax_disposition\ Fixed product tax
disposition

string\ base_price_incl_tax\ Base price including
tax

string\ qty\ Quantity

string\ base_cost\ Base cost

string\ base_weee_tax_applied_row_amount\ Applied fixed product
tax row amount (in base
currency)

string\ price\ Price

string\ base_row_total_incl_tax\ Base row total
including tax

string\ row_total_incl_tax\ Row total including tax

string\ product_id\ Product ID

string\ order_item_id\ Order item ID

string\ additional_data\ Additional data

string\ description\ Description

string\ weee_tax_applied\ Applied fixed product
tax

string\ sku\ Item SKU

string\ name\ Name

string\ hidden_tax_amount\ Hidden tax amount

string\ base_hidden_tax_amount\ Base hidden tax amount

---

The **salesOrderCreditmemoCommentEntity** content is as follows:

---

Type Name Description

---

string parent_id\ Parent ID

string\ created_at\ Date of creation

string\ comment\ Comment data

string\ is_customer_notified\ Defines whether the
customer is notified

string\ comment_id\ Comment ID

string\ is_visible_on_front\ Defines whether the
comment is visible on
the frontend

---

**Faults:**

Fault Code Fault Description

---

101 Invalid filter given. Details in error message.

### [Examples](#order_creditmemo.list-Examples)

### [Request Example SOAP V1](#order_creditmemo.list-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');
$filter = array(
    'order_id' => array(
        'or' => array(
            array(
                'from' => '673',
                'to' => '674',
            ),
            array(
                'from' => '677',
                'to' => NULL,
            )
        )
    ),
    'increment_id' => array(
        'or' => array(
            array(
                'from' => '100000617',
                'to' => '100000619',
            ),
            array(
                'from' => '100000619',
                'to' => NULL,
            )
        )
    )
);
$creditmemoList = $proxy->call($sessionId, 'order_creditmemo.list', array($filter));
```

</div>



### [Request Example SOAP V2](#order_creditmemo.list-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->salesOrderCreditmemoList($sessionId, '200000001');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (Complex Filter)](#order_creditmemo.list-RequestExampleSOAPV2%28ComplexFilter%29)

<div>

```
$client = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

// If some stuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');
$complexFilter = array(
    'complex_filter' => array(
        array(
            'key' => 'state',
            'value' => array('key' => 'in', 'value' => '2,3')
        )
    )
);
$result = $client->salesOrderCreditmemoList($session, $complexFilter);

var_dump ($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#order_creditmemo.list-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->salesOrderCreditmemoList((object)array('sessionId' => $sessionId->result));
var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#order_creditmemo.list-ResponseExampleSOAPV1)

<div>

```
array(
    0 => array(
        'store_id' => '1',
        'adjustment_positive' => NULL,
        'base_shipping_tax_amount' => '0.0000',
        'store_to_order_rate' => '1.0000',
        'base_discount_amount' => '0.0000',
        'base_to_order_rate' => '1.0000',
        'grand_total' => '60.0000',
        'base_adjustment_negative' => NULL,
        'base_subtotal_incl_tax' => '55.0000',
        'shipping_amount' => '5.0000',
        'subtotal_incl_tax' => '55.0000',
        'adjustment_negative' => NULL,
        'base_shipping_amount' => '5.0000',
        'store_to_base_rate' => '1.0000',
        'base_to_global_rate' => '1.0000',
        'base_adjustment' => '0.0000',
        'base_subtotal' => '55.0000',
        'discount_amount' => '0.0000',
        'subtotal' => '55.0000',
        'adjustment' => '0.0000',
        'base_grand_total' => '60.0000',
        'base_adjustment_positive' => NULL,
        'base_tax_amount' => '0.0000',
        'shipping_tax_amount' => '0.0000',
        'tax_amount' => '0.0000',
        'order_id' => '674',
        'email_sent' => NULL,
        'creditmemo_status' => NULL,
        'state' => '2',
        'shipping_address_id' => '1348',
        'billing_address_id' => '1347',
        'invoice_id' => NULL,
        'cybersource_token' => NULL,
        'store_currency_code' => 'USD',
        'order_currency_code' => 'USD',
        'base_currency_code' => 'USD',
        'global_currency_code' => 'USD',
        'transaction_id' => NULL,
        'increment_id' => '100000617',
        'created_at' => '2011-05-26 10:49:45',
        'updated_at' => '2011-05-26 10:49:45',
        'hidden_tax_amount' => '0.0000',
        'base_hidden_tax_amount' => '0.0000',
        'shipping_hidden_tax_amount' => NULL,
        'base_shipping_hidden_tax_amnt' => NULL,
        'shipping_incl_tax' => '5.0000',
        'base_shipping_incl_tax' => '5.0000',
        'base_customer_balance_amount' => NULL,
        'customer_balance_amount' => NULL,
        'bs_customer_bal_total_refunded' => '0.0000',
        'customer_bal_total_refunded' => '0.0000',
        'base_gift_cards_amount' => NULL,
        'gift_cards_amount' => NULL,
        'gw_base_price' => NULL,
        'gw_price' => NULL,
        'gw_items_base_price' => NULL,
        'gw_items_price' => NULL,
        'gw_card_base_price' => NULL,
        'gw_card_price' => NULL,
        'gw_base_tax_amount' => NULL,
        'gw_tax_amount' => NULL,
        'gw_items_base_tax_amount' => NULL,
        'gw_items_tax_amount' => NULL,
        'gw_card_base_tax_amount' => NULL,
        'gw_card_tax_amount' => NULL,
        'base_reward_currency_amount' => NULL,
        'reward_currency_amount' => NULL,
        'reward_points_balance' => NULL,
        'reward_points_balance_refund' => NULL,
        'creditmemo_id' => '617',
    ),
    1 => array(
        'store_id' => '1',
        'adjustment_positive' => NULL,
        .......................................
        'creditmemo_id' => '620',
    )
);
```

</div>


