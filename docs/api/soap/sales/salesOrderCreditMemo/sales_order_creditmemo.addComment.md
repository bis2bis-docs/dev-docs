

### [Module: Order Credit Memo API](#order_creditmemo.addComment-Module:OrderCreditMemoAPI)

## [Resource: sales_order_creditmemo](#order_creditmemo.addComment-Resource:salesordercreditmemo)

**Aliases**: order_creditmemo

## [Method:](#order_creditmemo.addComment-Method:)

-   order_creditmemo.addComment (SOAP V1)
-   salesOrderCreditmemoAddComment (SOAP V2)

Allows you to add a new comment to an existing credit memo. Email
notification can be sent to the user email.

**Arguments:**

---

Type Name Description

---

string sessionId\ Session ID

string creditmemoIncrementId Credit memo increment
ID

string comment Comment text (optional)

int notifyCustomer Notify customer by
email flag (optional)

int includeComment Include comment text
into the email
notification (optional)

---

**Return:**

Type Description

---

boolean\\int True (1) if the comment is added to the credit memo

**Faults:**

Fault Code Fault Message

---

100 Requested credit memo does not exist.
102 Invalid data given. Details in error message.

### [Examples](#order_creditmemo.addComment-Examples)

### [Request Example SOAP V1](#order_creditmemo.addComment-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');
$creditmemoIncrementId = '200000001'; //increment id of existing credit memo
$commentText = "Credit memo comment successfully added";

$isCommentAdded = $proxy->call($sessionId, 'order_creditmemo.addComment', array($creditmemoIncrementId, $commentText, true));
```

</div>



### [Request Example SOAP V2](#order_creditmemo.addComment-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->salesOrderCreditmemoAddComment($sessionId, '200000001');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#order_creditmemo.addComment-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->salesOrderCreditmemoAddComment((object)array('sessionId' => $sessionId->result, 'creditmemoIncrementId' => '200000001', 'comment' => 'credit memo comment', 'notifyCustomer' => 1, 'includeComment' => 1));
var_dump($result->result);
```

</div>


