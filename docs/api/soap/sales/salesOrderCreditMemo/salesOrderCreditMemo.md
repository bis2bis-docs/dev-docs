

## [Module: Sales_Order_Creditmemo](#MAPI-OrderCreditMemo-Module:SalesOrderCreditmemo)

Allows you to operate with credit memos for orders.

## [Resource: sales_order_creditmemo](#MAPI-OrderCreditMemo-Resource:salesordercreditmemo)

**Aliases**: order_creditmemo

## [Methods:](#MAPI-OrderCreditMemo-Methods:)

-   [sales_order_creditmemo.list](sales_order_creditmemo.list.html "sales_order_creditmemo.list") -
    Retrieve the list of credit memos by filters
-   [sales_order_creditmemo.info](sales_order_creditmemo.info.html "sales_order_creditmemo.info") -
    Retrieve the credit memo information
-   [sales_order_creditmemo.create](sales_order_creditmemo.create.html "sales_order_creditmemo.create") -
    Create a new credit memo for order
-   [sales_order_creditmemo.addComment](sales_order_creditmemo.addComment.html "sales_order_creditmemo.addComment") -
    Add a new comment to the credit memo
-   [sales_order_creditmemo.cancel](sales_order_creditmemo.cancel.html "sales_order_creditmemo.cancel") -
    Cancel the credit memo

## [Faults:](#MAPI-OrderCreditMemo-Faults:)

Fault Code Fault Message

---

100 Requested credit memo does not exist.
101 Invalid filter given. Details in error message.
102 Invalid data given. Details in error message.
103 Requested order does not exist.
104 Credit memo status not changed.
105 Money can not be refunded to the store credit account as order was created by guest.
106 Credit memo for requested order can not be created.

## [Example:](#MAPI-OrderCreditMemo-Example:)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

// Create creditmemo
$orderIncrementId = '100000683'; //increment id of the invoiced order
$data = array(
    'qtys' => array(
        '712' => 1
    ),
    'shipping_amount' => 3,
    'adjustment_positive' => 0.7,
    'adjustment_negative' => 0.06
);
$creditmemoIncrementId = $proxy->call($sessionId, 'order_creditmemo.create', array($orderIncrementId, $data));
echo $creditmemoIncrementId . "<br />";

// Add comment to created creditmemo
$commentText = "Credit memo comment successfully added";
$isCommentAdded = $proxy->call($sessionId, 'order_creditmemo.addComment', array($creditmemoIncrementId, $commentText, true));

// Retrieve information about created creditmemo
$creditmemoInfo = $proxy->call($sessionId, 'order_creditmemo.info', array($creditmemoIncrementId));
print_r($creditmemoInfo);

// Retrieve list of creditmemos by filter
$filter = array(
    'increment_id' => array(
        'or' => array(
            array(
                'from' => '100000617',
                'to' => '100000619',
            ),
            array(
                'from' => $creditmemoIncrementId,
                'to' => NULL,
            )
        )
    )
);
$creditmemoList = $proxy->call($sessionId, 'order_creditmemo.list', array($filter));
print_r($creditmemoList);
```

</div>


