

### [Module: Order Credit Memo API](#order_creditmemo.cancel-Module:OrderCreditMemoAPI)

## [Resource: sales_order_creditmemo](#order_creditmemo.cancel-Resource:salesordercreditmemo)

## [Aliases: order_creditmemo](#order_creditmemo.cancel-Aliases:ordercreditmemo)

## [Method:](#order_creditmemo.cancel-Method:)

-   order_creditmemo.cancel (SOAP V1)
-   salesOrderCreditmemoCancel (SOAP V2)

Allows you to cancel an existing credit memo.

**Arguments:**

Type Name Description

---

string sessionId Session ID
string creditmemoIncrementId Credit memo increment ID

**Return:**

Type Name Description

---

string result Result of canceling the credit memo

**Faults:**

Fault Code Fault Message

---

100 Requested credit memo does not exist.
104 Credit memo status not changed.

### [Examples](#order_creditmemo.cancel-Examples)

### [Request Example SOAP V1](#order_creditmemo.cancel-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');
$creditmemoIncrementId = '100000637'; //increment id of existing credit memo

$isCreditMemoCanceled = $proxy->call($sessionId, 'order_creditmemo.cancel', array($creditmemoIncrementId));
```

</div>



Create the Magento file system owner
