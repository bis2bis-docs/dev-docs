

### [Module: Order Credit Memo API](#order_creditmemo.create-Module:OrderCreditMemoAPI)

## [Resource: sales_order_creditmemo](#order_creditmemo.create-Resource:salesordercreditmemo)

## [Aliases: order_creditmemo](#order_creditmemo.create-Aliases:ordercreditmemo)

## [Method:](#order_creditmemo.create-Method:)

-   order_creditmemo.create (SOAP V1)
-   salesOrderCreditmemoCreate (SOAP V2)

Allows you to create a new credit memo for the invoiced order. Comments
can be added and an email notification can be sent to the user email.

**Arguments:**

Type Name Description

---

string sessionId Session ID
string orderIncrementId Order increment ID
array creditmemoData Array of salesOrderCreditmemoData (optional)
string comment Comment text (optional)
int notifyCustomer Notify customer by email flag (optional)
int includeComment Include comment text into an email notification (optional)
string refundToStoreCreditAmount Payment amount to be refunded to the customer store credit (optional)

**Return:**

Type Name Description

---

string result Created credit memo increment ID

The **salesOrderCreditmemoData** content is as follows:

---

Type Name Description

---

array qtys\ Array of orderItemIdQty

double shipping_amount\ Refund shipping amount
(optional)

double adjustment_positive\ Adjustment refund
amount (optional)

double adjustment_negative\ Adjustment fee amount
(optional)

---

The **orderItemIdQty** content is as follows:

---

Type Name Description

---

int order_item_id\ Order item ID to be
refunded

double qty\ Items quantity to be
refunded

---

**Faults:**

Fault Code Fault Message

---

102 Invalid data given. Details in error message.
103 Requested order does not exist.
105 Money can not be refunded to the store credit account as order was created by guest.
106 Credit memo for requested order can not be created.

### [Examples](#order_creditmemo.create-Examples)

### [Request Example SOAP V1](#order_creditmemo.create-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'order_creditmemo.create', '200000010');
var_dump ($result);
```

</div>



### [Request Example SOAP V2](#order_creditmemo.create-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->salesOrderCreditmemoCreate($sessionId, '200000010');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#order_creditmemo.create-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->salesOrderCreditmemoCreate((object)array('sessionId' => $sessionId->result, 'creditmemoIncrementId' => '200000010',
'creditmemoData' => array(
'qtys' => array(
'order_item_id' => 3,
'qty' => '1'),
'shipping_amount' => null,
'adjustment_positive' => '0',
'adjustment_negative' => null),
'comment' => 'comment for credit memo',
'notifyCustomer' => null,
'includeComment' => 1,
'refundToStoreCreditAmount' => '1'
));
var_dump($result->result);
```

</div>


