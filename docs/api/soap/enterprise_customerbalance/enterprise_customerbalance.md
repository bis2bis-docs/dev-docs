

# [](#MAPI-StoreCredit-General-StoreCreditAPI)Store Credit API

Allows you to operate with customer virtual balance.

### [Module: Enterprise_CustomerBalance](#MAPI-StoreCredit-General-Module:EnterpriseCustomerBalance)

## [Resource: enterprise_customerbalance](#MAPI-StoreCredit-General-Resource:enterprisecustomerbalance)

**Aliases**: storecredit

## [Methods:](#MAPI-StoreCredit-General-Resource:enterprisecustomerbalance-Methods)

-   [storecredit.balance](customerBalance/storecredit.balance.html "storecredit.balance") -
    Retrieve the customer store credit balance information
-   [storecredit.history](customerBalance/storecredit.history.html "storecredit.history") -
    Retrieve the customer store credit history information

### [Faults:](#MAPI-StoreCredit-General-Resource:enterprisecustomerbalance-Faults)

Fault Code Fault Message

---

100 Provided data is invalid.
101 No balance found with requested parameters.
102 No history found with requested parameters.

## [Resource: enterprise_customerbalance_quote](#MAPI-StoreCredit-General-Resource:enterprisecustomerbalancequote)

**Aliases**: storecredit_quote

## [Methods:](#MAPI-StoreCredit-General-Resource:enterprisecustomerbalancequote-Methods)

-   [storecredit_quote.setAmount](shoppingCartCustomerBalance/storecredit_quote.setAmount.html "storecredit_quote.setAmount") -
    Set amount from the customer store credit into a shopping cart
    (quote)
-   [storecredit_quote.removeAmount](shoppingCartCustomerBalance/storecredit_quote.removeAmount.html "storecredit_quote.removeAmount") -
    Remove amount from a shopping cart (quote) and increase the customer
    store credit

### [Faults:](#MAPI-StoreCredit-General-Resource:enterprisecustomerbalancequote-Faults)

Fault Code Fault Message

---

100 Provided data is invalid.
103 No quote found with requested id.
104 Store credit can not be used for quote created by guest.
105 No store found with requested id or code.

## [Example:](#MAPI-StoreCredit-General-Example:)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$customerId = 10002;
$websiteId = 1;
// Get customer balance amount
$balanceAmount = $proxy->call($sessionId, 'storecredit.balance', array($customerId, $websiteId));
echo $balanceAmount . "<br />";
// Get store credit history for specified customer
$balanceHistory = $proxy->call($sessionId, 'storecredit.history', array($customerId));
print_r($balanceHistory);

$quoteId = 2;
$store = 'default';
// Set customer balance use in shopping cart (quote)
$balanceAmountUsedInQuote = $proxy->call($sessionId, 'storecredit_quote.setAmount', array($quoteId, $store));
echo $balanceAmountUsedInQuote . "<br />";
// Unset customer balance use in shopping cart (quote)
$balanceAmountUsedInQuote = $proxy->call($sessionId, 'storecredit_quote.removeAmount', array($quoteId));
echo $balanceAmountUsedInQuote;
```

</div>



Create the Magento file system owner
