\-\-- layout: m1x\_soap title: Shopping Cart Customer Balance \-\--

[]{#ShoppingCartCustomerBalance-Module:EnterpriseCustomerBalance}Module: Enterprise\_CustomerBalance
------------------------------------------------------------------------------------------------------

Allows you to operate with customer virtual balance.

## [Resource: enterprise](#ShoppingCartCustomerBalance-Resource:enterprisecustomerbalancequote)\_customerbalance\_quote

**Aliases**: storecredit\_quote

## [Methods:](#ShoppingCartCustomerBalance-Methods:)

-   [storecredit\_quote.setAmount](storecredit_quote.setAmount.html "storecredit_quote.setAmount") -
    Set amount from the customer store credit into shopping cart (quote)
-   [storecredit\_quote.removeAmount](storecredit_quote.removeAmount.html "storecredit_quote.removeAmount") -
    Remove amount from the shopping cart (quote) and increase customer
    store credit

## [Faults:](#ShoppingCartCustomerBalance-Faults:)

  Fault Code   Fault Message
  ------------ ----------------------------------------------------------
  100          Provided data is invalid.
  103          No quote found with requested id.
  104          Store credit can not be used for quote created by guest.
  105          No store found with requested id or code.

Create the Magento file system owner
