

### [Module: Customer Balance API](#storecredit.history-Module:CustomerBalanceAPI)

## [Resource: enterprise_customerbalance](#storecredit.history-Resource:enterprisecustomerbalance)

## [Aliases: storecredit](#storecredit.history-Aliases:storecredit)

## [Method:](#storecredit.history-Method:)

-   enterprise_customerbalance.history (SOAP V1)
-   enterpriseCustomerbalanceHistory (SOAP V2)

Allows you to retrieve the customer store credit history information.

**Arguments:**

Type Name Description

---

string sessionId Session ID
string customerId Customer ID
string websiteId Website ID (optional)

**Return:**

Type Name Description

---

array result Array of enterpriseCustomerbalanceHistoryItemEntity

The **enterpriseCustomerbalanceHistoryItemEntity** content is as
follows:

---

Type Name Description

---

string history_id History item ID

string\ balance_id\ Customer balance ID

string\ updated_at\ Date when the balance
was updated

string\ action\ Code of the reason for
balance change

string\ balance_amount\ Balance amount after
changing

string\ balance_delta\ Balance change amount

string\ additional_info\ Comment text

string\ is_customer_notified\ Defines whether the
customer is notified

string\ customer_id\ Balance owner ID

string\ website_id\ Website ID

string\ base_currency_code\ Currency code for the
customer balance

---

**Faults:**

Fault Code Fault Message

---

100 Provided data is invalid.
102 No history found with requested parameters.

### [Examples](#storecredit.history-Examples)

### [Request Example SOAP V1](#storecredit.history-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');
$customerId = 4;

$balanceHistory = $proxy->call($sessionId, 'storecredit.history', array($customerId));
```

</div>



### [Request Example SOAP V2](#storecredit.history-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->enterpriseCustomerbalanceHistory($sessionId, '4');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#storecredit.history-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->enterpriseCustomerbalanceHistory((object)array('sessionId' => $sessionId->result, 'customerId' => '4', 'websiteId' => '2'));
var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#storecredit.history-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
    array
      'history_id' => string '2' (length=1)
      'balance_id' => string '1' (length=1)
      'updated_at' => string '2012-04-06 13:50:33' (length=19)
      'action' => string '1' (length=1)
      'balance_amount' => string '300.0000' (length=8)
      'balance_delta' => string '-200.0000' (length=9)
      'additional_info' => string 'By admin: john@example.com. (update balance)' (length=44)
      'is_customer_notified' => string '0' (length=1)
      'customer_id' => string '4' (length=1)
      'website_id' => string '2' (length=1)
      'base_currency_code' => null
  1 =>
    array
      'history_id' => string '1' (length=1)
      'balance_id' => string '1' (length=1)
      'updated_at' => string '2012-04-06 13:44:58' (length=19)
      'action' => string '2' (length=1)
      'balance_amount' => string '500.0000' (length=8)
      'balance_delta' => string '500.0000' (length=8)
      'additional_info' => string 'By admin: john@example.com. (new balance)' (length=41)
      'is_customer_notified' => string '0' (length=1)
      'customer_id' => string '4' (length=1)
      'website_id' => string '2' (length=1)
      'base_currency_code' => null
```

</div>


