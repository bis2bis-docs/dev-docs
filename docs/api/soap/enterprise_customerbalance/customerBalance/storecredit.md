\-\-- layout: m1x\_soap title: Customer Balance \-\--

[]{#CustomerBalance-Module:EnterpriseCustomerBalance}Module: Enterprise\_CustomerBalance
------------------------------------------------------------------------------------------

Allows you to operate with customer virtual balance.

## [Resource:enterprise](#CustomerBalance-Resource:enterprisecustomerbalance)\_customerbalance

**Aliases**: storecredit

## [Methods:](#CustomerBalance-Methods:)

-   [storecredit.balance](storecredit.balance.html "storecredit.balance") -
    Retrieve the customer store credit balance information
-   [storecredit.history](storecredit.history.html "storecredit.history") -
    Retrieve the customer store credit history information

## [Faults:](#CustomerBalance-Faults:)

  Fault Code   Fault Message
  ------------ ---------------------------------------------
  100          Provided data is invalid.
  101          No balance found with requested parameters.
  102          No history found with requested parameters.

Create the Magento file system owner
