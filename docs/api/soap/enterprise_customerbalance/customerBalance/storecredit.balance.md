

### [Module: Customer Balance API](#storecredit.balance-Module:CustomerBalanceAPI)

## [Resource: enterprise_customerbalance](#storecredit.balance-Resource:enterprisecustomerbalance)

## [Aliases: storecredit](#storecredit.balance-Aliases:storecredit)

## [Method:](#storecredit.balance-Method:)

-   enterprise_customerbalance.balance (SOAP V1)
-   enterpriseCustomerbalanceBalance (SOAP V2)

Allows you to retrieve the customer store credit balance amount.

**Arguments:**

Type Name Description

---

string sessionId Session ID
string customerId Customer ID
string websiteId Website ID

**Return:**

Type Description

---

string/float Virtual balance amount

**Faults:**

Fault Code Fault Message

---

100 Provided data is invalid.
101 No balance found with requested parameters.

### [Examples](#storecredit.balance-Examples)

### [Request Example SOAP V1](#storecredit.balance-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');
$customerId = 4;
$websiteId = 2;

$balanceAmount = $proxy->call($sessionId, 'storecredit.balance', array($customerId, $websiteId));
```

</div>



### [Request Example SOAP V2](#storecredit.balance-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->enterpriseCustomerbalanceBalance($sessionId, '4', '2');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#storecredit.balance-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->enterpriseCustomerbalanceBalance((object)array('sessionId' => $sessionId->result, 'customerId' => '4', 'websiteId' => '2'));
var_dump($result->result);
```

</div>


