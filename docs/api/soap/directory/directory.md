

# [Module: Mage_Directory](#MAPI-Directory-Module:MageDirectory)

The Mage_Directory module allows you to retrieve country and region
data.

### [Country](#MAPI-Directory-Country)

Allows you to retrieve a list of countries.

**Resource Name**: directory_country

**Aliases**:

-   country

**Methods**:

-   [directory_country.list](directory_country.list.html "directory_country.list") -
    Retrieve a list of countries

### [Region](#MAPI-Directory-Region)

Allows you to retrieve a list of regions within a country.

**Resource Name**: directory_region

**Aliases**:

-   region

**Methods**:

-   [directory_region.list](directory_region.list.html "directory_region.list") -
    Retrieve a list of regions in a specified country
