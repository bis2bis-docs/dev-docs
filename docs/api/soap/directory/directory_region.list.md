

## [Region API](#directory_region.list-RegionAPI)

Allows you to export the list of regions from Magento

## [Module: Mage_Directory](#directory_region.list-Module:MageDirectory)

### [Resource: directory_region](#directory_region.list-Resource:directoryregion)

**Aliases:**

-   region

## [Method:](#directory_region.list-Method:)

-   directory_region.list (SOAP V1)
-   directoryRegionList (SOAP V2)

Retrieve the list of regions in the specified country.

**Aliases:**

-   region.list

**Arguments:**

Type Name Description

---

string sessionId Session ID
string country Country code in ISO2 or ISO3

**Returns:**

---

Type Name Description

---

array directoryRegionEntityArray\ An array of
directoryRegionEntity

---

The **directoryRegionEntity** content is as follows:

Type Name Description

---

string region_id ID of the region
string code Region code
string name Name of the region

**Faults:**

Fault Code Fault Message

---

101 Country not exists.

### [Examples](#directory_region.list-Examples)

### [Request Example SOAP V1](#directory_region.list-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');
$regions = $proxy->call($sessionId, 'region.list', 'US');

var_dump($regions); // Region list for USA.
```

</div>



### [Request Example SOAP V2](#directory_region.list-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$result = $proxy->directoryRegionList($sessionId,'US');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#directory_region.list-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->directoryRegionList((object)array('sessionId' => $sessionId->result, 'country' => 'US'));
var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#directory_region.list-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
    array
      'region_id' => string '1' (length=1)
      'code' => string 'AL' (length=2)
      'name' => string 'Alabama' (length=7)
  1 =>
    array
      'region_id' => string '2' (length=1)
      'code' => string 'AK' (length=2)
      'name' => string 'Alaska' (length=6)
  2 =>
    array
      'region_id' => string '3' (length=1)
      'code' => string 'AS' (length=2)
      'name' => string 'American Samoa' (length=14)
  3 =>
    array
      'region_id' => string '4' (length=1)
      'code' => string 'AZ' (length=2)
      'name' => string 'Arizona' (length=7)
```

</div>


