

## [Country API](#directory_country.list-CountryAPI)

Allows you to retrieve the list of countries from Magento.

**Module: Mage_Directory**

**Resource: directory_country**

**Aliases**:

-   country

## [Method:](#directory_country.list-Method:)

-   directory_country.list (SOAP V1)
-   directoryCountryList (SOAP V2)

Retrieve the list of countries from Magento.

**Aliases**:

-   country.list

## [Arguments:](#directory_country.list-Arguments:)

Type Name Description

---

string sessionId Session ID

## [Returns:](#directory_country.list-Returns:)

Type Name Description

---

array countries An array of directoryCountryEntity

The **directoryCountryEntity** content is as follows:

Type Name Description

---

string country_id ID of the retrieved country
string iso2_code ISO 2-alpha code
string iso3_code ISO 3-alpha code
string name Name of the retrieved country

### [Examples](#directory_country.list-Examples)

### [Request Example SOAP V1](#directory_country.list-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');
$countries = $proxy->call($sessionId, 'country.list');
var_dump($countries); // Countries list.
```

</div>



### [Request Example SOAP V2](#directory_country.list-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->directoryCountryList($sessionId);
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#directory_country.list-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->directoryCountryList((object)array('sessionId' => $sessionId->result));
var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#directory_country.list-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
    array
      'country_id' => string 'AD' (length=2)
      'iso2_code' => string 'AD' (length=2)
      'iso3_code' => string 'AND' (length=3)
      'name' => string 'Andorra' (length=7)
  1 =>
    array
      'country_id' => string 'AE' (length=2)
      'iso2_code' => string 'AE' (length=2)
      'iso3_code' => string 'ARE' (length=3)
      'name' => string 'United Arab Emirates' (length=20)
  2 =>
    array
      'country_id' => string 'AF' (length=2)
      'iso2_code' => string 'AF' (length=2)
      'iso3_code' => string 'AFG' (length=3)
      'name' => string 'Afghanistan' (length=11)
```

</div>


