

## [Magento Info API](#magento.info-MagentoInfoAPI)

Allows you to get information about the current Magento installation.

### [Module: Mage_Core](#magento.info-Module:MageCore)

## [Resource: core_magento](#magento.info-Resource:coremagento)

**Aliases:** magento

## [Method:](#magento.info-Method:)

-   core_magento.info (SOAP V1)
-   magentoInfo (SOAP V2)

Allows you to retrieve information about Magento version and edition.

**Aliases**: magento.info

**Arguments**:

---

Type\ Name\ Description

---

string\ sessionId\ Session ID

---

**Returns**:

---

Type\ Name\ Description

---

string magento_version Magento version

string magento_edition Magento edition
(Community,
Professional,
Enterprise)

---

### [Examples](#magento.info-Examples)

### [Request Example SOAP V1](#magento.info-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');
$magentoInfo = $proxy->call(
    $sessionId,
    'magento.info'
);
```

</div>



### [Request Example SOAP V2](#magento.info-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->magentoInfo($sessionId);
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#magento.info-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->magentoInfo((object)array('sessionId' => $sessionId->result));

var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#magento.info-ResponseExampleSOAPV1)

<div>

```
array
  'magento_edition' => string 'Community' (length=9)
  'magento_version' => string '1.4.2.0-rc1' (length=11)
```

</div>


