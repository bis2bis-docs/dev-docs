

### [Module: Store View API](#store.list-Module:StoreViewAPI)

## [Resource: store](#store.list-Resource:store)

## [Method:](#store.list-Method:)

-   store.list (SOAP V1)
-   storeList (SOAP V2)

Allows you to retrieve the list of store views.

**Arguments:**

Type Name Description

---

string sessionId Session ID

**Return:**

Type Name Description

---

array result Array of storeEntity

The **storeEntity** content is as follows:

---

Type Name Description

---

int store_id\ Store view ID

string code\ Store view code

int website_id\ Website ID

int group_id\ Group ID

string name\ Store view name

int sort_order\ Store view sort order

int is_active\ Defines whether the
store is active

---

**Faults:**
_No Faults_

### [Examples](#store.list-Examples)

### [Request Example SOAP V1](#store.list-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'store.list');
var_dump ($result);
```

</div>



### [Request Example SOAP V2](#store.list-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->storeList($sessionId);
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#store.list-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->storeList((object)array('sessionId' => $sessionId->result));

var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#store.list-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
    array
      'store_id' => string '1' (length=1)
      'code' => string 'default' (length=7)
      'website_id' => string '1' (length=1)
      'group_id' => string '1' (length=1)
      'name' => string 'Default Store View' (length=18)
      'sort_order' => string '0' (length=1)
      'is_active' => string '1' (length=1)
  1 =>
    array
      'store_id' => string '2' (length=1)
      'code' => string 'english' (length=7)
      'website_id' => string '2' (length=1)
      'group_id' => string '2' (length=1)
      'name' => string 'English' (length=7)
      'sort_order' => string '0' (length=1)
      'is_active' => string '1' (length=1)
```

</div>


