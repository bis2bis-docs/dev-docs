\-\-- layout: m1x\_soap title: Miscellaneous \-\--

The following APIs allow you to access additional Magento information.

[]{#MiscellaneousAPIs-Module:StoreViewAPI}Module: Store View API
------------------------------------------------------------------

Allows you to retrieve information on the store view.

**Resource Name**: Store

## [Methods:](#MiscellaneousAPIs-Methods:)

-   [store.info](store.info.html "store.info") - Get information about a
    store view
-   [store.list](store.list.html "store.list") - Get the list of store
    views

[]{#MiscellaneousAPIs-Module:MageCoreAPI}Module: Mage\_Core API
-----------------------------------------------------------------

Allows you to get information about the current Magento installation.

**Resource Name**: core\_magento

**Aliases:** magento

## [Method:](#MiscellaneousAPIs-Method:)

-   [core\_magento.info](magento.info.html "magento.info")

Create the Magento file system owner
