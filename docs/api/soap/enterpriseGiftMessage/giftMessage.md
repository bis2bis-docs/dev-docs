

## [Module: Mage_GiftMessage](#MAPI-GiftMessage-Module:MageGiftMessage)

This module allows you to set a gift message for a quote or for a
product in a quote.

## [Resource: giftmessage](#MAPI-GiftMessage-Resource:giftmessage)

## [Methods:](#MAPI-GiftMessage-Methods:)

-   [giftmessage.setForQuote](giftmessage.setForQuote.html "giftmessage.setForQuote") -
    Set a gift message for the shopping cart (quote)
-   [giftmessage.setForQuoteItem](giftmessage.setForQuoteItem.html "giftmessage.setForQuoteItem") -
    Set a gift message for an item in the shopping cart (quote)
-   [giftmessage.setForQuoteProduct](giftmessage.setForQuoteProduct.html "giftmessage.setForQuoteProduct") -
    Set a gift message for a product in the shopping cart (quote)

## [Faults:](#MAPI-GiftMessage-Faults:)

Fault Code Fault Message

---

1001 Can not make operation because store is not exists
1002 Can not make operation because quote is not exists
1101 Gift message data is not valid.
1102 Product\'s data is not valid.
1103 Quote item is not exists

## [Example:](#MAPI-GiftMessage-Example:)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

// Create a quote, get quote identifier
$shoppingCartId = $proxy->call( $sessionId, 'cart.create', array( 'magento_store' ) );

// set gift message for quote
$giftMessage = array (
  'from' => 'test',
  'to' => 'test',
  'message' => 'test'
);

$giftMessageSetForQuoteResult = $proxy->call($sessionId,"giftmessage.setForQuote", array($shoppingCartId, $giftMessage, 'magento_store'));
print_r( $giftMessageSetForQuoteResult );


// add products into shopping cart
$arrProducts = array(
    array(
        'product_id' => '1',
        'qty' => 2
    ),
    array(
        'sku' => 'testSKU',
        'qty' => 4
    )
);
$resultCartProductAdd = $proxy->call($sessionId, 'cart_product.add', array($shoppingCartId, $arrProducts));

// set gift message for product
$giftMessage = array (
  'from' => 'test for Product',
  'to' => 'test for Product',
  'message' => 'test for Product'
);

/**
* Note:
* array for product should be the same as for cart_product.add
*/
$giftMessageSetForProductResult = $proxy->call(
  $sessionId,
  "giftmessage.setForQuoteProduct",
  array(
    $shoppingCartId,
    array(
        array(
            'product' => $arrProducts[0],
            'giftmessage' => $giftMessage,
        ),
        array(
            'product' => $arrProducts[1],
            'giftmessage' => $giftMessage,
        )
    ),
    'magento_store'
    )
);

print_r( $giftMessageSetForProductResult );
```

</div>



Create the Magento file system owner
