

### [Module: GiftMessage API](#giftmessage.setForQuote-Module:GiftMessageAPI)

## [Resource: giftmessage](#giftmessage.setForQuote-Resource:giftmessage)

## [Method:](#giftmessage.setForQuote-Method:)

-   giftmessage.setForQuote (SOAP V1)
-   giftMessageSetForQuote (SOAP V2)

Allows you to set a global gift message for the shopping cart (quote).

**Arguments:**

Type Name Description

---

string sessionId Session ID
string quoteId Shopping cart ID (quote ID)
array giftMessage Array of giftMessageEntity
string store Store view ID or code (optional)

**Return:**

Type Name Description

---

array result giftMessageResponse

The **giftMessageEntity** content is as follows:

---

Type Name Description

---

string from\ Gift message sender

string\ to\ Gift message recipient

string\ message\ Gift message text

---

The **giftMessageResponse** content is as follows:

---

Type Name Description

---

string entityId\ Entity ID

boolean result\ Result of adding a
message

string error\ Error

---

### [Examples](#giftmessage.setForQuote-Examples)

### [Request Example SOAP V1](#giftmessage.setForQuote-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'giftmessage.setForQuote', array('quoteId' => '10', 'giftMessage' => array('from' => 'John', 'to' => 'Sara', 'message' => 'Gift message text')));
var_dump ($result);
```

</div>



### [Request Example SOAP V2](#giftmessage.setForQuote-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); 

$sessionId = $proxy->login('apiUser', 'apiKey'); 
 
$result = $proxy->giftMessageSetForQuote($sessionId, 10, array(
'from' => 'John',
'to' => 'Sara',
'message' => 'Gift message text'
));  
 
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#giftmessage.setForQuote-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->giftMessageSetForQuote((object)array('sessionId' => $sessionId->result, 'quoteId' => '10', 'giftMessage' => array(
'from' => 'John',
'to' => 'Sara',
'message' => 'Gift message text'
)));

var_dump($result);
```

</div>



### [Response Example SOAP V1](#giftmessage.setForQuote-ResponseExampleSOAPV1)

<div>

```
array
  'entityId' => string '15' (length=2)
  'result' => boolean true
  'error' => string '' (length=0)
```

</div>


