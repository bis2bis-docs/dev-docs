

### [Module: GiftMessage API](#giftmessage.setForQuoteProduct-Module:GiftMessageAPI)

## [Resource: giftmessage](#giftmessage.setForQuoteProduct-Resource:giftmessage)

## [Method:](#giftmessage.setForQuoteProduct-Method:)

-   giftmessage.setForQuoteProduct (SOAP V1)
-   giftMessageSetForQuoteProduct (SOAP V2)

Allows you to set a gift message for a product in the shopping cart
(quote).

**Arguments:**

---

Type Name Description

---

string sessionId Session ID

string quoteId Shopping cart ID (quote ID)

array productsAndMessages\ Array of
giftMessageAssociativeProductsEntity

string store Store view ID or code (optional)

---

**Return:**

Type Name Description

---

array result Array of giftMessageResponse

The **giftMessageAssociativeProductsEntity** content is as follows:

---

Type Name Description

---

array product Array of
shoppingCartProductEntity

array message\ Array of giftMessageEntity

---

The **giftMessageResponse** content is as follows:

---

Type Name Description

---

string entityId\ Entity ID

boolean result\ Result

string error\ Error

---

The **shoppingCartProductEntity** content is as follows:

---

Type Name Description

---

string product_id\ ID of the product

string sku\ SKU of the product

double qty\ Number of products

associativeArray\ options\ An array of options

associativeArray\ bundle_option\ An array of bundle item
options

associativeArray\ bundle_option_qty\ An array of bundle
items quantity

ArrayOfString links\ An array of links

---

The **giftMessageEntity** content is as follows:

---

Type Name Description

---

string from\ Gift message sender

string to\ Gift message recipient

string message\ Gift message

---

**Faults:**
_No Faults._

### [Examples](#giftmessage.setForQuoteProduct-Examples)

### [Request Example SOAP V1](#giftmessage.setForQuoteProduct-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$result = $proxy->call($sessionId, 'giftmessage.setForQuoteProduct', array(10, array(
'product' => array(
 'product_id' => '5',
 'sku' => 'sku',
 'qty' => '1',
 'options' => null,
 'bundle_option' => null,
 'bundle_option_qty' => null,
 'links' => null),
'message' => array(
 'from' => 'Sara', 'to' => 'John', 'message' => 'Gift message text')
)));


var_dump($result);
```

</div>



### [Request Example SOAP V2](#giftmessage.setForQuoteProduct-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login('apiUser', 'apiKey');

$result = $proxy->giftMessageSetForQuoteProduct($sessionId, '10', array(array(
'product' => array(
 'product_id' => '5',
 'sku' => 'sku',
 'qty' => '1',
 'options' => null,
 'bundle_option' => null,
 'bundle_option_qty' => null,
 'links' => null),
'message' => array(
 'from' => 'Sara', 'to' => 'John', 'message' => 'Gift message text')
)));

var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#giftmessage.setForQuoteProduct-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->giftMessageSetForQuoteProduct((object)array('sessionId' => $sessionId->result, 'quoteId' => '10', 'productsAndMessages' => array(array(
'product' => array(
 'product_id' => '5',
 'sku' => 'sku',
 'qty' => '1',
 'options' => null,
 'bundle_option' => null,
 'bundle_option_qty' => null,
 'links' => null),
'message' => array(
 'from' => 'Sara', 'to' => 'John', 'message' => 'Gift message text')
))));

var_dump($result->result);
```

</div>


