

### [Module: GiftMessage API](#giftmessage.setForQuoteItem-Module:GiftMessageAPI)

## [Resource: giftmessage](#giftmessage.setForQuoteItem-Resource:giftmessage)

## [Method:](#giftmessage.setForQuoteItem-Method:)

-   giftmessage.setForQuoteItem (SOAP V1)
-   giftMessageSetForQuoteItem (SOAP V2)

Allows you to set a gift message for an item in the shopping cart
(quote).

**Arguments:**

Type Name Description

---

string sessionId Session ID
string quoteItemId Shopping cart (quote) item ID
array giftMessage Array of giftMessageEntity
string store Store view ID or code (optional)

**Return:**

Type Name Description

---

array result giftMessageResponse

The **giftMessageEntity** content is as follows:

---

Type Name Description

---

string from\ Gift message sender

string to\ Gift message recipient

string message\ Gift message

---

The **giftMessageResponse** content is as follows:

---

Type Name Description

---

string entityId\ Entity ID

boolean result\ Result of adding a
message

string error\ Error

---

### [Examples](#giftmessage.setForQuoteItem-Examples)

### [Request Example SOAP V1](#giftmessage.setForQuoteItem-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'giftmessage.setForQuoteItem', array('quoteItemId' => '1', 'giftMessage' => array('from' => 'John', 'to' => 'Sara', 'message' => 'Gift message text')));
var_dump ($result);
```

</div>



### [Request Example SOAP V2](#giftmessage.setForQuoteItem-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login('apiUser', 'apiKey');

$result = $proxy->giftMessageSetForQuoteItem($sessionId, '1', array(
'from' => 'John',
'to' => 'Sara',
'message' => 'Gift message text'
));

var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#giftmessage.setForQuoteItem-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); 

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey')); 
 
$result = $proxy->giftMessageSetForQuoteItem((object)array('sessionId' => $sessionId->result, 'quoteItemId' => '1', 'giftMessage' => array(
'from' => 'John',
'to' => 'Sara',
'message' => 'Gift message text'
)));  
 
var_dump($result);
```

</div>



### [Response Example SOAP V1](#giftmessage.setForQuoteItem-ResponseExampleSOAPV1)

<div>

```
array
  'entityId' => string '1' (length=1)
  'result' => boolean true
  'error' => string '' (length=0)
```

</div>


