

## [Module: Mage_CatalogInventory](#MAPI-Inventory-Module:MageCatalogInventory)

The Mage_CatalogInventory module allows you to retrieve and update
stock attributes, such as status and quantity.

### [Inventory](#MAPI-Inventory-Inventory)

Allows you to retrieve and update the stock data.

**Resource Name**: cataloginventory_stock_item

**Aliases**:

-   product_stock

**Methods**:

-   [cataloginventory_stock_item.list](cataloginventory_stock_item.list.html "cataloginventory_stock_item.list") -
    Retrieve the list of stock data by product IDs
-   [cataloginventory_stock_item.update](cataloginventory_stock_item.update.html "cataloginventory_stock_item.update") -
    Update the stock data for a list of products

### [Faults](#MAPI-Inventory-Faults)

Fault Code Fault Message

---

101 Product not exists.
102 Product inventory not updated. Details in error message.

### [Examples](#MAPI-Inventory-Examples)

Change manage_stock setting to 'off' in the inventory area.

\$attributeSets = \$client-\>call(\$session, 'product_stock.update',
array('SKU',array('manage_stock'=\>\'0','use_config_manage_stock'=\>\'0')));

The use_config_manage_stock unchecks the 'Use Config Settings' box
which allows you to make changes to this product and not to use the
global settings that are set by default.

### [Example 1. Working with stock update](#MAPI-Inventory-Example1.Workingwithstockupdate)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

// Get stock info
var_dump($proxy->call($sessionId, 'product_stock.list', 'Sku'));

// Update stock info
$proxy->call($sessionId, 'product_stock.update', array('Sku', array('qty'=>50, 'is_in_stock'=>1)));

var_dump($proxy->call($sessionId, 'product_stock.list', 'Sku'));
```

</div>



## [Example 2](#MAPI-Inventory-Example2.ListingmultipleSKUs%27data). Listing multiple SKUs\' data

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');
$sid = $client->login('apiUser', 'apiKey');

print_r(
    $client->call(
        $sid,
        'product_stock.list',
        array(
            array( // Notice the nested array
                'sku1',
                'sku2',
                'sku3',
                ...
                'skuN'
            )
        )
    )
);
```

</div>



Create the Magento file system owner
