

## [Module: Mage_CatalogInventory](#cataloginventory_stock_item.update-Module:MageCatalogInventory)

## [Resource: cataloginventory_stock_item](#cataloginventory_stock_item.update-Resource:cataloginventorystockitem)

**Aliases**:

-   product_stock

## [Method:](#cataloginventory_stock_item.update-Method:)

-   cataloginventory_stock_item.update (SOAP V1)
-   catalogInventoryStockItemUpdate (SOAP V2)

Allows you to update the required product stock data.

**Aliases**:

-   product_stock.update

**Arguments**:

---

Type Name Description

---

string sessionId\ Session ID

string product\\productId\ Product ID or SKU

array data\ Array of
catalogInventoryStockItemUpdateEntity

---

**Returns**:

Type Name Description

---

int result Result of stock item updating

The **catalogInventoryStockItemUpdateEntity** content is as follows:

---

Type Name Description

---

string qty\ Quantity of items to be
updated

int is_in_stock\ Defines whether the
item is in stock

int\ manage_stock\ Manage stock flag

int\ use_config_manage_stock\ Use config manage stock

int\ min_qty\ Minimum quantity for
items to be in stock

int\ use_config_min_qty\ Use config settings
flag (value defined in
the Inventory System
Configuration)

int\ min_sale_qty\ Minimum quantity
allowed in the shopping
cart

int\ use_config_min_sale_qty\ Use config settings
flag

int\ max_sale_qty\ Maximum quantity
allowed in the shopping
cart

int\ use_config_max_sale_qty\ Use config settings
flag

int\ is_qty_decimal\ Defines whether the
quantity is decimal

int\ backorders\ Backorders status

int\ use_config_backorders\ Use config settings
flag (for backorders)

int\ notify_stock_qty\ Stock quantity below
which a notification
will appear

int\ use_config_notify_stock_qty\ Use config settings
flag (for stock
quantity)

---

### [Examples](#cataloginventory_stock_item.update-Examples)

### [Request Example SOAP V1](#cataloginventory_stock_item.update-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$productId = '1';
$stockItemData = array(
    'qty' => '100',
    'is_in_stock ' => 1,
    'manage_stock ' => 1,
    'use_config_manage_stock' => 0,
    'min_qty' => 2,
    'use_config_min_qty ' => 0,
    'min_sale_qty' => 1,
    'use_config_min_sale_qty' => 0,
    'max_sale_qty' => 10,
    'use_config_max_sale_qty' => 0,
    'is_qty_decimal' => 0,
    'backorders' => 1,
    'use_config_backorders' => 0,
    'notify_stock_qty' => 10,
    'use_config_notify_stock_qty' => 0
);

$result = $client->call(
    $session,
    'product_stock.update',
    array(
        $productId,
        $stockItemData
    )
);
```

</div>



### [Request Example SOAP V2](#cataloginventory_stock_item.update-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');
 
$result = $proxy->catalogInventoryStockItemUpdate($sessionId, 1, array(
'qty' => '49',
'is_in_stock' => 1
));

var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#cataloginventory_stock_item.update-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogInventoryStockItemUpdate((object)array('sessionId' => $sessionId->result, 'productId' => '1', 'data' => array(
'qty' => '49',
'is_in_stock' => 1
)));

var_dump($result->result);
```

</div>


