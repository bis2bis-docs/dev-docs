

## [Module: Mage_CatalogInventory](#cataloginventory_stock_item.list-Module:MageCatalogInventory)

## [Resource: cataloginventory_stock_item](#cataloginventory_stock_item.list-Resource:cataloginventorystockitem)

**Aliases**:

-   product_stock

## [Method:](#cataloginventory_stock_item.list-Method:)

-   cataloginventory_stock_item.list (SOAP V1)
-   catalogInventoryStockItemList (SOAP V2)

Allows you to retrieve the list of stock data by product IDs.

**Aliases**:

-   product_stock.list

**Arguments**:

---

Type Name Description

---

string sessionId\ Session ID

ArrayOfString\ products/productIds List of product IDs or
(for WS-I mode)\ SKUs

---

**Returns**:

Type Name Description

---

array result Array of catalogInventoryStockItemEntity

The **catalogInventoryStockItemEntity** content is as follows:

---

Type Name Description

---

string product_id\ Product ID

string sku\ Product SKU

string qty\ Product quantity

string is_in_stock\ Defines whether the
product is in stock

---

### [Examples](#cataloginventory_stock_item.list-Examples)

### [Request Example SOAP V1](#cataloginventory_stock_item.list-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'cataloginventory_stock_item.list', '1');
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#cataloginventory_stock_item.list-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogInventoryStockItemList($sessionId, array('1', '2'));
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#cataloginventory_stock_item.list-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogInventoryStockItemList((object)array('sessionId' => $sessionId->result, 'productIds' => array(1,2)));
var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#cataloginventory_stock_item.list-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
    array
      'product_id' => string '1' (length=1)
      'sku' => string 'n2610' (length=5)
      'qty' => string '98.0000' (length=7)
      'is_in_stock' => string '1' (length=1)
```

</div>


