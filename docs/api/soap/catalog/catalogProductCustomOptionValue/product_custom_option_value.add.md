

### [Module: Complex Product API](#product_custom_option_value.add-Module:ComplexProductAPI)

## [Resource: product_custom_option_value](#product_custom_option_value.add-Resource:productcustomoptionvalue)

## [Method:](#product_custom_option_value.add-Method:)

-   product_custom_option_value.add (SOAP V1)
-   catalogProductCustomOptionValueAdd (SOAP V2)

Allows you to add a new custom option value to a custom option. Note
that the custom option value can be added only to the option with the
**Select** Input Type.

**Arguments:**

---

Type Name Description

---

string sessionId Session ID

string optionId\ Option ID

array data Array of
catalogProductCustomOptionValueAdd

string store Store view ID or code (optional)

---

**Return:**

Type Description

---

boolean True if the custom option value is added

The **catalogProductCustomOptionValueAdd** content is as follows:

---

Type Name Description

---

string title\ Custom option value
title

string\ price\ Custom option value
price

string\ price_type\ Type of the custom
option value price. Can
have one of the
following
values: \"fixed\" or
\"percent\"

string\ sku\ Custom option value row
SKU

string\ sort_order\ Custom option value
sort order

---

**Faults:**

Fault Code Fault Message

---

101 Option value with requested id does not exist.
102 Error while adding an option value. Details are in the error message.
104 Invalid option type.

### [Examples](#product_custom_option_value.add-Examples)

### [Request Example SOAP V1](#product_custom_option_value.add-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');
$productOptionId = 1;// Existing product option ID

// Add custom option value
$customOptionValue = array(
    "title" => "Some value text 1",
    "price" => 10.00,
    "price_type" => "fixed",
    "sku" => "custom_text_option_sku",
    "sort_order" => 0
);
$resultCustomOptionValueAdd = $proxy->call(
    $sessionId,
    "product_custom_option_value.add",
    array(
        $productOptionId,
        array($customOptionValue)
    )
);
```

</div>



### [Request Example SOAP V2](#product_custom_option_value.add-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login('apiUser', 'apiKey');

$result = $proxy->catalogProductCustomOptionValueAdd($sessionId, '10', array(array(
'title' => 'value',
'price' => '99.99',
'price_type' => 'fixed',
'sku' => 'sku',
'sort_order' => '1'
)));
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#product_custom_option_value.add-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductCustomOptionValueAdd((object)array('sessionId' => $sessionId->result, 'optionId' => '10', 'data' => array(array(
'title' => 'value',
'price' => '99.99',
'price_type' => 'fixed',
'sku' => 'sku',
'sort_order' => '1'
))));
var_dump($result->result);
```

</div>


