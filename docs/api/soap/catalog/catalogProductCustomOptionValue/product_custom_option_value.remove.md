

### [Module: Complex Product API](#product_custom_option_value.remove-Module:ComplexProductAPI)

## [Resource: product_custom_option_value](#product_custom_option_value.remove-Resource:productcustomoptionvalue)

## [Method:](#product_custom_option_value.remove-Method:)

-   product_custom_option_value.remove (SOAP V1)
-   catalogProductCustomOptionValueRemove (SOAP V2)

Allows you to remove the custom option value from a product.

**Arguments:**

---

Type Name Description

---

string sessionId\ Session ID

string\ valueId Custom option value ID

---

**Return:**

Type Description

---

boolean\\int True (1) if the custom option value is removed

**Faults:**

Fault Code Fault Message

---

103 Option with requested id does not exist.
106 Can not delete option.
109 Option should have at least one value. Can not delete last value.

### [Examples](#product_custom_option_value.remove-Examples)

### [Request Example SOAP V1](#product_custom_option_value.remove-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$productOptionId = 4;// Existing option ID
// Get last value from option values list
$optionValues = $proxy->call($sessionId, "product_custom_option_value.list", array($productOptionId));
$optionValue = reset($optionValues);
$valueId = $optionValue['value_id'];

$result = $proxy->call(
    $sessionId,
    "product_custom_option_value.remove",
    array(
         $valueId
    )
);
```

</div>



### [Request Example SOAP V2](#product_custom_option_value.remove-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductCustomOptionValueRemove($sessionId, '4');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#product_custom_option_value.remove-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductCustomOptionValueRemove((object)array('sessionId' => $sessionId->result, 'valueId' => '4'));
var_dump($result->result);
```

</div>


