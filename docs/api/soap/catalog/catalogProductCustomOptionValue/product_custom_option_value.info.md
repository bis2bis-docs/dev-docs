

### [**Module: Complex Product API**](#product_custom_option_value.info-Module:ComplexProductAPI)

## [**Resource: product_custom_option_value**](#product_custom_option_value.info-Resource:productcustomoptionvalue)

## [Method:](#product_custom_option_value.info-Method:)

-   product_custom_option_value.info (SOAP V1)
-   catalogProductCustomOptionValueInfo (SOAP V2)

Allows you to retrieve full information about the specified product
custom option value.

**Arguments:**

Type Name Description

---

string sessionId Session ID
string valueId Value ID
string store Store view ID or code (optional)

**Return:**

---

Type Name Description

---

array\ result Array of
catalogProductCustomOptionValueInfoEntity

---

The **catalogProductCustomOptionValueInfoEntity** content is as follows:

---

Type Name Description

---

string value_id\ Option value ID

string\ option_id\ Option ID

string\ sku\ Custom option value row
SKU

string\ sort_order\ Option value sort order

string\ default_price\ Option value default
price

string\ default_price_type\ Default price type.
Possible values are as
follows: \"fixed\" or
\"percent\"

string\ store_price\ Option value store
price

string\ store_price_type\ Store price type.
Possible values are as
follows: \"fixed\" or
\"percent\"

string\ price\ Option value price

string\ price_type\ Price type. Possible
values are as follows:
\"fixed\" or
\"percent\"

string\ default_title\ Option value default
title

string\ store_title\ Option value store
title

string\ title\ Option value title

---

**Faults:**

---

Fault Code Fault Message

---

101 Option value with requested id does
not exist.

---

### [Examples](#product_custom_option_value.info-Examples)

### [Request Example SOAP V1](#product_custom_option_value.info-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'product_custom_option_value.info', '5');
var_dump($result);
```

</div>



### [Request Example SOAP V2](#product_custom_option_value.info-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductCustomOptionValueInfo($sessionId, '5');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#product_custom_option_value.info-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductCustomOptionValueInfo((object)array('sessionId' => $sessionId->result, 'valueId' => '5'));

var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#product_custom_option_value.info-ResponseExampleSOAPV1)

<div>

```
array
  'option_id' => string '5' (length=1)
  'sku' => string 'slider' (length=6)
  'sort_order' => string '0' (length=1)
  'default_title' => string 'slider' (length=6)
  'store_title' => string 'slider' (length=6)
  'title' => string 'slider' (length=6)
  'default_price' => string '239.9900' (length=8)
  'default_price_type' => string 'fixed' (length=5)
  'store_price' => string '239.9900' (length=8)
  'store_price_type' => string 'fixed' (length=5)
  'price' => string '239.9900' (length=8)
  'price_type' => string 'fixed' (length=5)
  'value_id' => string '2' (length=1)
```

</div>


