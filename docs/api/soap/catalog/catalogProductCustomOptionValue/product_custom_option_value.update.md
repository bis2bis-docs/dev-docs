

### [Module: Complex Product API](#product_custom_option_value.update-Module:ComplexProductAPI)

## [Resource: product_custom_option_value](#product_custom_option_value.update-Resource:productcustomoptionvalue)

## [Method:](#product_custom_option_value.update-Method:)

-   product_custom_option_value.update (SOAP V1)
-   catalogProductCustomOptionValueUpdate (SOAP V2)

Allows you to update the product custom option value.

**Arguments:**

---

Type Name Description

---

string sessionId Session ID

string\ valueId Value ID

array data Array of
catalogProductCustomOptionValueUpdateEntity

string storeId Store view ID or code (optional)

---

**Return:**

Type Description

---

boolean True if the custom option value is updated

The **catalogProductCustomOptionValueUpdateEntity** content is as
follows:

---

Type Name Description

---

string title\ Option value title

string\ price\ Option value price

string\ price_type\ Price type. Possible
values are as follows:
\"fixed\" or
\"percent\"

string\ sku\ Custom option value row
SKU

string\ sort_order\ Custom option value
sort order

---

**Faults:**

Fault Code Fault Message

---

101 Option value with requested id does not exist.
103 Option with requested id does not exist.
104 Invalid option type.
107 Error while updating an option value. Details are in the error message.
108 Title field is required.

### [Examples](#product_custom_option_value.update-Examples)

### [Request Example SOAP V1](#product_custom_option_value.update-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$productOptionId = 1;// Existing option ID

// Get last value from option values list
$optionValues = $proxy->call($sessionId, "product_custom_option_value.list", array($productOptionId));
$optionValue = reset($optionValues);
$valueId = $optionValue['value_id'];
// Update custom option value
$customOptionValue = array(
    "title" => "new title",
    "price" => 12.00,
    "price_type" => "percent",
    "sku" => "custom_text_option_2",
    "sort_order" => 2
);
$resultCustomOptionValueUpdate = $proxy->call(
    $sessionId,
    "product_custom_option_value.update",
    array(
         $valueId,
         $customOptionValue
    )
);
```

</div>



### [Request Example SOAP V2](#product_custom_option_value.update-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login('apiUser', 'apiKey');

$result = $proxy->catalogProductCustomOptionValueUpdate($sessionId, '2', array(
'title' => 'value',
'price' => '20',
'price_type' => 'fixed',
'sku' => 'sku'
));

var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#product_custom_option_value.update-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductCustomOptionValueUpdate((object)array('sessionId' => $sessionId->result, 'valueId' => '2', 'data' => ((object)array(
'title' => 'value',
'sku' => 'sku',
'price' => '199',
'price_type' => 'percent'
))));

var_dump($result->result);
```

</div>


