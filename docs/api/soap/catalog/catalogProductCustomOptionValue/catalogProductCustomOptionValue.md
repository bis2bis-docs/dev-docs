\-\-- layout: m1x\_soap title: Catalog Product Custom Option Value \-\--

[]{#MAPI-ProductCustomOptionValue-Module:MageCatalog}Module: Mage\_Catalog
----------------------------------------------------------------------------

## [Resource: catalog](#MAPI-ProductCustomOptionValue-Resource:catalogproductcustomoptionvalue)\_product\_custom\_option\_value

**Aliases**: product\_custom\_option\_value

## [Methods:](#MAPI-ProductCustomOptionValue-Methods:)

-   [product\_custom\_option\_value.add](product_custom_option_value.add.html "product_custom_option_value.add")
    -  Add a new custom option value to a selectable custom option
-   [product\_custom\_option\_value.list](product_custom_option_value.list.html "product_custom_option_value.list")
    -  Retrieve the list of product custom option values
-   [product\_custom\_option\_value.info](product_custom_option_value.info.html "product_custom_option_value.info")
    -  Retrieve full information about the specified product custom
    option value
-   [product\_custom\_option\_value.update](product_custom_option_value.update.html "product_custom_option_value.update")
    -  Update the custom option value
-   [product\_custom\_option\_value.remove](product_custom_option_value.remove.html "product_custom_option_value.remove")
    -  Remove the custom option value

## [Faults:](#MAPI-ProductCustomOptionValue-Faults:)

  Fault Code   Fault Message
  ------------ -------------------------------------------------------------------------
  101          Option value with requested id does not exist.
  102          Error while adding an option value. Details are in the error message.
  103          Option with requested id does not exist.
  104          Invalid option type.
  105          Store with requested code/id does not exist.
  106          Can not delete option.
  107          Error while updating an option value. Details are in the error message.
  108          Title field is required.
  109          Option should have at least one value. Can not delete last value.

Create the Magento file system owner
