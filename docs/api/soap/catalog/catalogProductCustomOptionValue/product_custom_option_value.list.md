

### [Module: Complex Product API](#product_custom_option_value.list-Module:ComplexProductAPI)

## [Resource: product_custom_option_value](#product_custom_option_value.list-Resource:productcustomoptionvalue)

## [Method:](#product_custom_option_value.list-Method:)

-   product_custom_option_value.list (SOAP V1)
-   catalogProductCustomOptionValueList (SOAP V2)

Allows you to retrieve the list of product custom option values. Note
that the method is available only for the option **Select** Input Type.

**Arguments:**

---

Type Name Description

---

string sessionId Session ID

string\ optionId Option ID

string store Store view ID or code
(optional)

---

**Returns**:

---

Type Name Description

---

array\ result Array of
catalogProductCustomOptionValueList

---

The **catalogProductCustomOptionValueListEntity** content is as follows:

---

Type Name Description

---

string value_id Custom option value ID

string\ title\ Custom option value
title

string\ price\ Option value price

string\ price_type\ Price type. Possible
values: \"fixed\" or
\"percent\"

string\ sku\ Custom option value
SKU

string\ sort_order\ Option value sort order
(optional)

---

**Faults**:

Fault Code Fault Message

---

101 Provided data is invalid.
102 Error while adding an option value. Details are in the error message.

### [Examples](#product_custom_option_value.list-Examples)

### [Request Example SOAP V1](#product_custom_option_value.list-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'product_custom_option_value.list', '3');
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#product_custom_option_value.list-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductCustomOptionValueList($sessionId, '3');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#product_custom_option_value.list-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductCustomOptionValueList((object)array('sessionId' => $sessionId->result, 'optionId' => '3'));

var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#product_custom_option_value.list-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
    array
      'value_id' => string '1' (length=1)
      'title' => string 'monoblock' (length=9)
      'price' => string '139.9900' (length=8)
      'price_type' => string 'fixed' (length=5)
      'sku' => string 'monoblock' (length=9)
      'sort_order' => string '0' (length=1)
  1 =>
    array
      'value_id' => string '2' (length=1)
      'title' => string 'slider' (length=6)
      'price' => string '239.9900' (length=8)
      'price_type' => string 'fixed' (length=5)
      'sku' => string 'slider' (length=6)
      'sort_order' => string '0' (length=1)
```

</div>


