

### [Module: Product Attribute Set API](#product_attribute_set.remove-Module:ProductAttributeSetAPI)

## [Resource: product_attribute_set](#product_attribute_set.remove-Resource:productattributeset)

## [Method:](#product_attribute_set.remove-Method:)

-   product_attribute_set.remove (SOAP V1)
-   catalogProductAttributeSetRemove (SOAP V2)

Allows you to remove an existing attribute set.

**Arguments:**

---

Type Name Description

---

string sessionId\ Session ID

string\ attributeSetId Attribute set ID

string\ forceProductsRemove Force product remove
flag (optional)

---

**Return:**

Type Name Description

---

boolean\\int isRemoved True (1) if the attribute set is removed

**Faults:**

Fault Code Fault Message

---

103 Error while removing attribute set. Details in error message.
104 Attribute set with requested id does not exist.
105 Unable to remove attribute set as it has related goods. Use forceProductsRemove parameter to remove attribute set with all goods.

### [Examples](#product_attribute_set.remove-Examples)

### [Request Example SOAP V1](#product_attribute_set.remove-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$setId = 5;

$result = $proxy->call(
    $sessionId,
    "product_attribute_set.remove",
    array(
         $setId
    )
);
```

</div>



### [Request Example SOAP V2](#product_attribute_set.remove-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductAttributeSetRemove($sessionId, '5');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#product_attribute_set.remove-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductAttributeSetRemove((object)array('sessionId' => $sessionId->result, 'attributeSetId' => '5'));
var_dump($result->result);
```

</div>


