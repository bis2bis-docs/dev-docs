

### [Module: Product Attribute Set API](#product_attribute_set.groupAdd-Module:ProductAttributeSetAPI)

## [Resource: product_attribute_set](#product_attribute_set.groupAdd-Resource:productattributeset)

## [Method:](#product_attribute_set.groupAdd-Method:)

-   product_attribute_set.groupAdd (SOAP V1)
-   catalogProductAttributeSetGroupAdd (SOAP V2)

Allows you to add a new group for attributes to the attribute set.

**Arguments:**

---

Type Name Description

---

string sessionId\ Session ID

string\ attributeSetId Attribute set ID

string groupName Group name

---

**Return:**

Type Name Description

---

int result ID of the created group

**Faults:**

Fault Code Fault Message

---

112 Requested group exist already in requested attribute set.
113 Error while adding group to attribute set. Details in error message.

### [Examples](#product_attribute_set.groupAdd-Examples)

### [Request Example SOAP V1](#product_attribute_set.groupAdd-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'product_attribute_set.groupAdd', array('attributeSetId' => '9', 'groupName' => 'new_group'));
var_dump ($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#product_attribute_set.groupAdd-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductAttributeSetGroupAdd($sessionId, '9', 'new_group');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#product_attribute_set.groupAdd-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://maentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductAttributeSetGroupAdd((object)array('sessionId' => $sessionId->result, 'attributeSetId' => '9', 'groupName' => 'new_group'));
var_dump($result->result);
```

</div>


