

### [Module: Product Attribute Set API](#product_attribute_set.groupRemove-Module:ProductAttributeSetAPI)

## [Resource: product_attribute_set](#product_attribute_set.groupRemove-Resource:productattributeset)

## [Method:](#product_attribute_set.groupRemove-Method:)

-   product_attribute_set.groupRemove (SOAP V1)
-   catalogProductAttributeSetGroupRemove (SOAP V2)

Allows you to remove a group from an attribute set.

**Arguments:**

---

Type Name Description

---

string sessionId\ Session ID

string\ attributeGroupId Group ID

---

**Return:**

Type Description

---

boolean\\int True (1) if the group is removed

**Faults:**

Fault Code Fault Message

---

108 Attribute group with requested id does not exist.
115 Error while removing group from attribute set. Details in error message.
116 Group can not be removed as it contains system attributes.
117 Group can not be removed as it contains attributes, used in configurable products.

### [Examples](#product_attribute_set.groupRemove-Examples)

### [Request Example SOAP V1](#product_attribute_set.groupRemove-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$groupId = 70;

$result = $proxy->call(
    $sessionId,
    "product_attribute_set.groupRemove",
    array(
         $groupId
    )
);
```

</div>



### [Request Example SOAP V2](#product_attribute_set.groupRemove-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductAttributeSetGroupRemove($sessionId, '70');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#product_attribute_set.groupRemove-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductAttributeSetGroupRemove((object)array('sessionId' => $sessionId->result, 'attributeGroupId' => '70'));
var_dump($result->result);
```

</div>


