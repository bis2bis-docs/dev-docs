

### [Module: Product Attribute Set API](#product_attribute_set.attributeAdd-Module:ProductAttributeSetAPI)

## [Resource: product_attribute_set](#product_attribute_set.attributeAdd-Resource:productattributeset)

## [Method:](#product_attribute_set.attributeAdd-Method:)

-   product_attribute_set.attributeAdd (SOAP V1)
-   catalogProductAttributeSetAttributeAdd (SOAP V2)

Allows you to add an existing attribute to an attribute set.

**Arguments:**

---

Type Name Description

---

string sessionId\ Session ID

string\ attributeId Attribute ID

string\ attributeSetId Attribute set ID

string\ attributeGroupId Group ID (optional)

string\ sortOrder Sort order (optional)

---

**Note**: If the _attributeGroupId_ parameter is not passed, the
attribute is added to the _General_ group by default.

**Returns:**

Type Name Description

---

boolean isAdded True if the attribute is added to an attribute set

**Faults:**

Fault Code Fault Message

---

104 Attribute set with requested id does not exist.
106 Attribute with requested id does not exist.
107 Error while adding attribute to attribute set. Details in error message.
108 Attribute group with requested id does not exist.
109 Requested attribute is already in requested attribute set.

### [Examples](#product_attribute_set.attributeAdd-Examples)

### [Request Example SOAP V1](#product_attribute_set.attributeAdd-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$setId = 5;
$attributeId = 83;

$result = $proxy->call(
    $sessionId,
    "product_attribute_set.attributeAdd",
    array(
         $attributeId,
         $setId
    )
);
```

</div>



### [Request Example SOAP V2](#product_attribute_set.attributeAdd-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$setId = 5;
$attributeId = 83;

$result = $proxy->catalogProductAttributeSetAttributeAdd(
    $sessionId,
    $attributeId,
    $setId
);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#product_attribute_set.attributeAdd-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductAttributeSetAttributeAdd((object)array('sessionId' => $sessionId->result, 'attributeId' => '5', 'attributeSetId' => '83'));
var_dump($result->result);
```

</div>


