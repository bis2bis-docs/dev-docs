


### [Module: Product Attribute Set API](#product_attribute_set.attributeRemove-Module:ProductAttributeSetAPI)

## [Resource: product_attribute_set](#product_attribute_set.attributeRemove-Resource:productattributeset)

## [Method:](#product_attribute_set.attributeRemove-Method:)

-   product_attribute_set.attributeRemove (SOAP V1)
-   catalogProductAttributeSetAttributeRemove (SOAP V2)

Allows you to remove an existing attribute from an attribute set.

**Arguments:**

---

Type Name Description

---

string sessionId\ Session ID

string\ attributeId Attribute ID

string\ attributeSetId Attribute set ID

---

**Returns:**

Type Name Description

---

boolean isRemoved True if the attribute is removed from an attribute set

**Faults:**

Fault Code Fault Message

---

104 Attribute set with requested id does not exist.
106 Attribute with requested id does not exist.
110 Error while removing attribute from attribute set. Details in error message.
111 Requested attribute is not in requested attribute set.

### [Examples](#product_attribute_set.attributeRemove-Examples)

### [Request Example SOAP V1](#product_attribute_set.attributeRemove-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$setId = 5;
$attributeId = 83;

$result = $proxy->call(
    $sessionId,
    "product_attribute_set.attributeRemove",
    array(
         $attributeId,
         $setId
    )
);
```

</div>



### [Request Example SOAP V2](#product_attribute_set.attributeRemove-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductAttributeSetAttributeRemove($sessionId, '5', '83');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#product_attribute_set.attributeRemove-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductAttributeSetAttributeRemove((object)array('sessionId' => $sessionId->result, 'attributeId' => '5', 'attributeSetId' => '83'));
var_dump($result->result);
```

</div>


