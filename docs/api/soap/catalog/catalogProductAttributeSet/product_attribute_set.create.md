

### [Module: Product Attribute Set API](#product_attribute_set.create-Module:ProductAttributeSetAPI)

## [Resource: product_attribute_set](#product_attribute_set.create-Resource:productattributeset)

## [Method:](#product_attribute_set.create-Method:)

-   product_attribute_set.create (SOAP V1)
-   catalogProductAttributeSetCreate (SOAP V2)

Allows you to create a new attribute set based on another attribute set.

**Arguments:**

---

Type Name Description

---

string sessionId\ Session ID

string attributeSetName Attribute set name

string\ skeletonSetId Attribute set ID basing
on which the new
attribute set will be
created

---

**Return:**

Type Name Description

---

int setId ID of the created attribute set

**Faults:**

Fault Code Fault Message

---

100 Attribute set with requested id does not exist.
101 Invalid data given.
102 Error while creating attribute set. Details in error message.

### [Examples](#product_attribute_set.create-Examples)

### [Request Example SOAP V1](#product_attribute_set.create-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$setName = "New Attribute Set";
$skeletonId = 4;

$newSetId = $proxy->call(
    $sessionId,
    "product_attribute_set.create",
    array(
         $setName,
         $skeletonId
    )
);
```

</div>



### [Request Example SOAP V2](#product_attribute_set.create-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$attributeSetName = 'New Attribute Set';
$skeletonId = 4;

$result = $client->catalogProductAttributeSetCreate(
    $sessionId,
    $attributeSetName,
    $skeletonId
);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#product_attribute_set.create-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductAttributeSetCreate((object)array('sessionId' => $sessionId->result, 'attributeSetName' => 'New Attribute Set', 'skeletonSetId' => '4'));
var_dump($result->result);
```

</div>


