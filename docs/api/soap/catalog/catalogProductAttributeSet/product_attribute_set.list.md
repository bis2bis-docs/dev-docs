

## [Module:Mage_Catalog](#catalog_product_attribute_set.list-Module:MageCatalog)

## [Resource:catalog_product_attribute_set](#catalog_product_attribute_set.list-Resource:catalogproductattributeset)

**Aliases:**

-   product_attribute_set

## [Method:](#catalog_product_attribute_set.list-Method:)

-   catalog_product_attribute_set.list (SOAP V1)
-   catalogProductAttributeSetList (SOAP V2)

Allows you to retrieve the list of product attribute sets.

**Aliases:**

-   product_attribute_set.list

**Arguments:**

Type Name Description

---

string sessionId Session ID

**Returns**:

Type Name Description

---

array result Array of catalogProductAttributeSetEntity

The **catalogProductAttributeSetEntity** content is as follows:

Type Name Description

---

int set_id ID of the attribute set
string name Attribute set name

**Faults:**

_No faults._

### [Examples](#catalog_product_attribute_set.list-Examples)

### [Request Example SOAP V1](#catalog_product_attribute_set.list-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_product_attribute_set.list');
var_dump ($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#catalog_product_attribute_set.list-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductAttributeSetList($sessionId);
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_product_attribute_set.list-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductAttributeSetList((object)array('sessionId' => $sessionId->result));
var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#catalog_product_attribute_set.list-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
    array
      'set_id' => string '4' (length=1)
      'name' => string 'Default' (length=7)
  1 =>
    array
      'set_id' => string '9' (length=1)
      'name' => string 'products_set' (length=12)
```

</div>


