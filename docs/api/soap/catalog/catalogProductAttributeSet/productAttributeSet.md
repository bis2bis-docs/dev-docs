

## [Module: Mage_Catalog](#MAPI-ProductAttributeSets-Module:MageCatalog)

The Mage_Catalog module allows you to manage categories and products.

### [Product Attribute Sets](#MAPI-ProductAttributeSets-ProductAttributeSets)

Allows you to retrieve product attribute sets.

**Resource Name**: catalog_product_attribute_set

**Aliases**:

-   product_attribute_set

**Methods**:

-   [product_attribute_set.list](product_attribute_set.list.html "product_attribute_set.list") -
    Retrieve the list of product attribute sets
-   [product_attribute_set.attributeAdd](product_attribute_set.attributeAdd.html "product_attribute_set.attributeAdd") -
    Add an attribute to the attribute set
-   [product_attribute_set.attributeRemove](product_attribute_set.attributeRemove.html "product_attribute_set.attributeRemove") -
    Remove an attribute from an attribute set
-   [product_attribute_set.create](product_attribute_set.create.html "product_attribute_set.create") -
    Create a new attribute set
-   [product_attribute_set.groupAdd](product_attribute_set.groupAdd.html "product_attribute_set.groupAdd") -
    Add a new group for attributes in the attribute set
-   [product_attribute_set.groupRemove](product_attribute_set.groupRemove.html "product_attribute_set.groupRemove") -
    Remove a group of attributes from an attribute set
-   [product_attribute_set.groupRename](product_attribute_set.groupRename.html "product_attribute_set.groupRename") -
    Rename a group of attributes in an attribute set
-   [product_attribute_set.remove](product_attribute_set.remove.html "product_attribute_set.remove") -
    Remove an attribute set

## [Faults:](#MAPI-ProductAttributeSets-Faults:)

Fault Code Fault Message

---

100 Attribute set with requested id does not exist.
101 Invalid data given.
102 Error while creating attribute set. Details in error message.
103 Error while removing attribute set. Details in error message.
104 Attribute set with requested id does not exist.
105 Unable to remove attribute set as it has related goods. Use forceProductsRemove parameter to remove attribute set with all goods.
106 Attribute with requested id does not exist.
107 Error while adding attribute to attribute set. Details in error message.
108 Attribute group with requested id does not exist.
109 Requested attribute is already in requested attribute set.
110 Error while removing attribute from attribute set. Details in error message.
111 Requested attribute is not in requested attribute set.
112 Requested group exist already in requested attribute set.
113 Error while adding group to attribute set. Details in error message.
114 Error while renaming group. Details in error message.
115 Error while removing group from attribute set. Details in error message.
116 Group can not be removed as it contains system attributes.
117 Group can not be removed as it contains attributes, used in configurable products.

## [Example:](#MAPI-ProductAttributeSets-Example:)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

echo "<pre>";
// create new set
$setName = "New Test Set";
$skeletonId = 4;

$setId = $proxy->call(
    $sessionId,
    "product_attribute_set.create",
    array(
         $setName,
         $skeletonId
    )
);

// Get list
$setList = $proxy->call(
    $sessionId,
    "product_attribute_set.list"
);
echo "Set list:\n";
print_r($setList);

// create group
$groupName = "Test Group";
$groupId = $proxy->call(
    $sessionId,
    "product_attribute_set.groupAdd",
    array(
         $setId,
         $groupName
    )
);

// rename group
$newGroupName = "New Test Group";
$result = $proxy->call(
    $sessionId,
    "product_attribute_set.groupRename",
    array(
         $groupId,
         $newGroupName
    )
);

// add attribute
$attributeId = 83;
$result = $proxy->call(
    $sessionId,
    "product_attribute_set.attributeAdd",
    array(
         $attributeId,
         $setId
    )
);

//remove attribute
$result = $proxy->call(
    $sessionId,
    "product_attribute_set.attributeRemove",
    array(
         $attributeId,
         $setId
    )
);

// remove group
$result = $proxy->call(
    $sessionId,
    "product_attribute_set.groupRemove",
    array(
         $groupId
    )
);

// remove set
$result = $proxy->call(
    $sessionId,
    "product_attribute_set.remove",
    array(
         $setId
    )
);
```

</div>



Create the Magento file system owner
