

### [Module: Product Attribute Set API](#product_attribute_set.groupRename-Module:ProductAttributeSetAPI)

## [Resource: product_attribute_set](#product_attribute_set.groupRename-Resource:productattributeset)

## [Method:](#product_attribute_set.groupRename-Method:)

-   product_attribute_set.groupRename (SOAP V1)
-   catalogProductAttributeSetGroupRename (SOAP V2)

Allows you to rename a group in the attribute set.

**Arguments:**

---

Type Name Description

---

string sessionId\ Session ID

string\ groupId ID of the group that
will be renamed

string groupName New name for the group

---

**Return:**

Type Description

---

boolean\\int True (1) if the group is renamed

**Faults:**

Fault Code Fault Message

---

108 Attribute group with requested id does not exist.
114 Error while renaming group. Details in error message.

### [Examples](#product_attribute_set.groupRename-Examples)

### [Request Example SOAP V1](#product_attribute_set.groupRename-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$groupId = 100;
$groupName = "New Group";

echo "Renaming group...";
$result = $proxy->call(
    $sessionId,
    "product_attribute_set.groupRename",
    array(
         $groupId,
         $groupName
    )
);
```

</div>



### [Request Example SOAP V2](#product_attribute_set.groupRename-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductAttributeSetGroupRename($sessionId, '100', 'New Group');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#product_attribute_set.groupRename-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductAttributeSetGroupRename((object)array('sessionId' => $sessionId->result, 'groupId' => '100', 'groupName' => 'New Group'));
var_dump($result->result);
```

</div>


