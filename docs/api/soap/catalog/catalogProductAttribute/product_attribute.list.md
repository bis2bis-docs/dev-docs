

## [Module: Mage_Catalog](#catalog_product_attribute.list-Module:MageCatalog)

## [Resource: catalog_product_attribute](#catalog_product_attribute.list-Resource:catalogproductattribute)

**Aliases:**

-   product_attribute

## [Method:](#catalog_product_attribute.list-Method:)

-   catalog_product_attribute.list (SOAP V1)
-   catalogProductAttributeList (SOAP V2)

Allows you to retrieve the list of product attributes.

**Aliases:**

-   product_attribute.list

**Arguments:**

---

Type Name Description

---

string sessionId\ Session ID

int setId\ ID of the attribute set

---

**Returns**:

Type Name Description

---

array result Array of catalogAttributeEntity

The **catalogAttributeEntity** content is as follows:

---

Type Name Description

---

int attribute_id\ Attribute ID

string code\ Attribute code

string\ type\ Attribute type

string\ required\ Defines whether the
attribute is required

string\ scope\ Attribute scope.
Possible values:
\'store\', \'website\',
or \'global\'

---

**Faults:**

_No faults_

### [Examples](#catalog_product_attribute.list-Examples)

### [Request Example SOAP V1](#catalog_product_attribute.list-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$setid = 4; // Existing attribute set id

$result = $proxy->call(
    $sessionId,
    "product_attribute.list",
    array(
         $setId
    )
);
echo "<pre>";
var_dump($result);
```

</div>



### [Request Example SOAP V2](#catalog_product_attribute.list-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductAttributeList($sessionId, '4');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_product_attribute.list-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductAttributeList((object)array('sessionId' => $sessionId->result, 'setId' => '4'));

var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#catalog_product_attribute.list-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
    array
      'attribute_id' => string '71' (length=2)
      'code' => string 'name' (length=4)
      'type' => string 'text' (length=4)
      'required' => string '1' (length=1)
      'scope' => string 'store' (length=5)
  1 =>
    array
      'attribute_id' => string '72' (length=2)
      'code' => string 'description' (length=11)
      'type' => string 'textarea' (length=8)
      'required' => string '1' (length=1)
      'scope' => string 'store' (length=5)
```

</div>


