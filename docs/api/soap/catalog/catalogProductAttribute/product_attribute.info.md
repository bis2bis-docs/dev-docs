

### [Module: Product Attributes API](#product_attribute.info-Module:ProductAttributesAPI)

## [Resource: product_attribute](#product_attribute.info-Resource:productattribute)

## [Method:](#product_attribute.info-Method:)

-   product_attribute.info (SOAP V1)
-   catalogProductAttributeInfo (SOAP V2)

Allows you to get full information about a required attribute with the
list of options.

**Arguments:**

Type Name Description

---

string sessionId Session ID
string attribute Attribute code or ID

**Return:**

Type Name Description

---

array result Array of catalogProductAttributeEntity

The **catalogProductAttributeEntity** content is as follows:

---

Type Name Description

---

string attribute_id Attribute ID

string\ attribute_code\ Attribute code

string\ frontend_input\ Attribute type

string\ scope\ Attribute scope

string\ default_value\ Attribute default value

int is_unique\ Defines whether the attribute is
unique

int is_required\ Defines whether the attribute is
required

ArrayOfString apply_to\ Apply to. Empty for \"Apply to all\"
or array of the following possible
values: \'simple\', \'grouped\',
\'configurable\', \'virtual\',
\'bundle\', \'downloadable\',
\'giftcard\'

int is_configurable\ Defines whether the attribute can be
used for configurable products

int is_searchable\ Defines whether the attribute can be
used in Quick Search

int is_visible_in_advanced_search\ Defines whether the attribute can be
used in Advanced Search

int is_comparable\ Defines whether the attribute can be
compared on the frontend

int is_used_for_promo_rules\ Defines whether the attribute can be
used for promo rules

int is_visible_on_front\ Defines whether the attribute is
visible on the frontend

int used_in_product_listing\ Defines whether the attribute can be
used in product listing

associativeArray\ additional_fields\ Array of additional fields

array options\ Array of catalogAttributeOptionEntity

array frontend_label\ Array of
catalogProductAttributeFrontendLabel

---

The **catalogAttributeOptionEntity** content is as follows:

---

Type Name Description

---

string label\ Text label

string value\ Option ID

---

The **catalogProductAttributeFrontendLabelEntity** content is as
follows:

---

Type Name Description

---

string store_id\ Store ID

string label\ Text label

---

The **AdditionaFieldsEntity** array of additional fields for the
**text** type is as follows:

Type Name Description

---

string frontend_class Input Validation for Store Owner. Possible values: \'validate-number\' (Decimal Number), \'validate-digits\' (Integer Number), \'validate-email\', \'validate-url\', \'validate-alpha\' (Letters), \'validate-alphanum\' (Letters (a-z, A-Z), or Numbers (0-9))
boolean is_html_allowed_on_front Defines whether the HTML tags are allowed on the frontend
boolean used_for_sort_by Defines whether it is used for sorting in product listing

The **AdditionaFieldsEntity** array of additional fields for the **text
area** type is as follows:

---

Type Name Description

---

boolean is_wysiwyg_enabled Enable WYSIWYG flag

boolean is_html_allowed_on_front Defines whether the
HTML tags are allowed
on the frontend

---

The **AdditionaFieldsEntity** array of additional fields for the
**date** and **boolean** types is as follows:

---

Type Name Description

---

boolean used_for_sort_by Defines whether it is
used for sorting in
product listing

---

The **AdditionaFieldsEntity** array of additional fields for the
**multiselect** type is as follows:

Type Name Description

---

boolean is_filterable Defines whether it used in layered navigation
boolean is_filterable_in_search Defines whether it is used in search results layered navigation
int position Position

The **AdditionaFieldsEntity** array of additional fields for the
**select** and **price** types is as follows:

---

Type Name Description

---

boolean is_filterable Defines whether it used
in layered navigation

boolean is_filterable_in_search Defines whether it is
used in search results
layered navigation

int position Position

boolean used_for_sort_by Defines whether it is
used for sorting in
product listing

---

**Faults:**

Fault Code Fault Message

---

101 Requested attribute not found.

### [Examples](#product_attribute.info-Examples)

### [Request Example SOAP V1](#product_attribute.info-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'product_attribute.info', '11');
var_dump ($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#product_attribute.info-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductAttributeInfo($sessionId, '11');
var_dump($result);
```

</div>



### [Response Example SOAP V1](#product_attribute.info-ResponseExampleSOAPV1)

<div>

```
array
  'attribute_id' => string '11' (length=3)
  'attribute_code' => string 'new_special_price' (length=17)
  'frontend_input' => string 'text' (length=4)
  'default_value' => null
  'is_unique' => string '0' (length=1)
  'is_required' => string '0' (length=1)
  'apply_to' =>
    array
      empty
  'is_configurable' => string '0' (length=1)
  'is_searchable' => string '0' (length=1)
  'is_visible_in_advanced_search' => string '0' (length=1)
  'is_comparable' => string '0' (length=1)
  'is_used_for_promo_rules' => string '0' (length=1)
  'is_visible_on_front' => string '0' (length=1)
  'used_in_product_listing' => string '0' (length=1)
  'frontend_label' =>
    array
      0 =>
        array
          'store_id' => int 0
          'label' => string 'special price' (length=13)
      1 =>
        array
          'store_id' => int 2
          'label' => string 'special price' (length=13)
  'scope' => string 'store' (length=5)
  'additional_fields' =>
    array
      'frontend_class' => null
      'is_html_allowed_on_front' => string '1' (length=1)
      'used_for_sort_by' => string '0' (length=1)
```

</div>



Create the Magento file system owner
