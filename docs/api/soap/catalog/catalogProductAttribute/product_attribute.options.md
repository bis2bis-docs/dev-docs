

## [Module: Mage_Catalog](#catalog_product_attribute.options-Module:MageCatalog)

## [Resource: catalog_product_attribute](#catalog_product_attribute.options-Resource:catalogproductattribute)

**Aliases:**

-   product_attribute

## [Method:](#catalog_product_attribute.options-Method:)

-   catalog_product_attribute.options (SOAP V1)
-   catalogProductAttributeOptions (SOAP V2)

Allows you to retrieve the product attribute options.

**Aliases:**

-   product_attribute.options

**Arguments:**

---

Type Name Description

---

string sessionId\ Session ID

string\ attributeId\ Attribute ID or code

string\ storeView\ Store view ID or code
(optional)

---

**Returns**:

Type Name Description

---

array result Array of catalogAttributeOptionEntity

The **catalogAttributeOptionEntity** content is as follows:

Type Name Description

---

string label Option label
string value Option value

**Faults:**

Fault Code Fault Message

---

101 Requested attribute not found.

### [Examples](#catalog_product_attribute.options-Examples)

### [Request Example SOAP V1](#catalog_product_attribute.options-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$attributeId = 11; // Existing selectable attribute ID

$result = $proxy->call(
    $sessionId,
    "product_attribute.options",
    array(
         $attributeId
    )
);
echo "<pre>";
var_dump($result);
```

</div>



### [Request Example SOAP V2](#catalog_product_attribute.options-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductAttributeOptions($sessionId, '11');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_product_attribute.options-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductAttributeOptions((object)array('sessionId' => $sessionId->result, 'attributeId' => '11'));

var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#catalog_product_attribute.options-ResponseExampleSOAPV1)

<div>

```
array
  1 =>
    array
      'value' => string '5' (length=1)
      'label' => string 'blue' (length=4)
  2 =>
    array
      'value' => string '4' (length=1)
      'label' => string 'green' (length=5)
  3 =>
    array
      'value' => string '3' (length=1)
      'label' => string 'yellow' (length=6)
```

</div>


