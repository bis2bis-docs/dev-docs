

### [Module: Product Attributes API](#product_attribute.update-Module:ProductAttributesAPI)

## [Resource: product_attribute](#product_attribute.update-Resource:productattribute)

## [Method:](#product_attribute.update-Method:)

-   product_attribute.update (SOAP V1)
-   catalogProductAttributeUpdate (SOAP V2)

Allows you to update the required attribute.

**Arguments:**

---

Type Name Description

---

string sessionId Session ID

string attribute\ Attribute code or ID

array data Array of
catalogProductAttributeEntityToUpdate

---

**Returns:**

Type Description

---

boolean True if the attribute is updated

The **catalogProductAttributeEntityToUpdate** content is as follows:

---

Type Name Description

---

string scope\ Attribute scope. Possible values are
as follows: \'store\', \'website\', or
\'global\'

string default_value\ Attribute default value

int is_unique\ Defines whether the attribute is
unique

int is_required\ Defines whether the attribute is
required

ArrayOfString apply_to\ Apply to. Empty for \"Apply to all\"
or array of the following possible
values: \'simple\', \'grouped\',
\'configurable\', \'virtual\',
\'bundle\', \'downloadable\',
\'giftcard\'

int is_configurable\ Defines whether the attribute can be
used for configurable products

int\ is_searchable\ Defines whether the attribute can be
used in Quick Search

int\ is_visible_in_advanced_search\ Defines whether the attribute can be
used in Advanced Search

int\ is_comparable\ Defines whether the attribute can be
compared on the frontend

int\ is_used_for_promo_rules\ Defines whether the attribute can be
used for promo rules

int\ is_visible_on_front\ Defines whether the attribute can be
visible on the frontend

int\ used_in_product_listing\ Defines whether the attribute can be
used in product listing

associativeArray additional_fields\ Array of additional fields

array frontend_label\ Array of
catalogProductAttributeFrontendLabel

---

The **AdditionaFieldsEntity** array of additional fields for the
**text** type is as follows:

---

Type Name Description

---

string frontend_class Input Validation for
Store Owner. Possible
values:
\'validate-number\'
(Decimal Number),
\'validate-digits\'
(Integer Number),
\'validate-email\',
\'validate-url\',
\'validate-alpha\'
(Letters),
\'validate-alphanum\'
(Letters (a-z, A-Z), or
Numbers (0-9))

boolean is_html_allowed_on_front Defines whether the
HTML tags are allowed
on the frontend

boolean used_for_sort_by Defines whether it is
used for sorting in
product listing

---

The **AdditionaFieldsEntity** array of additional fields for the **text
area** type is as follows:

---

Type Name Description

---

boolean is_wysiwyg_enabled Enable WYSIWYG flag

boolean is_html_allowed_on_front Defines whether the
HTML tags are allowed
on the frontend

---

The **AdditionaFieldsEntity** array of additional fields for the
**date** and **boolean** types is as follows:

---

Type Name Description

---

boolean used_for_sort_by Defines whether it is
used for sorting in
product listing

---

The **AdditionaFieldsEntity** array of additional fields for the
**multiselect** type is as follows:

---

Type Name Description

---

boolean is_filterable Defines whether it used
in layered navigation

boolean is_filterable_in_search Defines whether it is
used in search results
layered navigation

integer position Position

---

The **AdditionaFieldsEntity** array of additional fields for the
**select** and **price** types is as follows:

---

Type Name Description

---

boolean is_filterable Defines whether it used
in layered navigation

boolean is_filterable_in_search Defines whether it is
used in search results
layered navigation

integer position Position

boolean used_for_sort_by Defines whether it is
used for sorting in
product listing

---

The **catalogProductAttributeFrontendLabel** content is as follows:

Type Name Description

---

string store_id Store ID
string label Text label

**Faults:**

Fault Code Fault Message

---

101 Requested attribute not found.
105 Unable to save attribute.
107 This attribute cannot be edited.

### [Examples](#product_attribute.update-Examples)

### [Request Example SOAP V1](#product_attribute.update-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$attributeToUpdate = array(
    "scope" => "global",
    "is_unique" => 1,
    "is_required" => 1,
    "is_configurable" => 1,
    "is_searchable" => 1,
    "is_visible_in_advanced_search" => 0,
    "used_in_product_listing" => 0,
    "additional_fields" => array(
        "is_filterable" => 1,
        "is_filterable_in_search" => 0,
        "position" => 2,
        "used_for_sort_by" => 0
    ),
    "frontend_label" => array(
        array(
            "store_id" => 0,
            "label" => "A Test Attribute"
        )
    )
);

$result = $proxy->call(
    $sessionId,
    "product_attribute.update",
    array(
        $attributeToUpdate
    )
);
```

</div>



### [Request Example SOAP V2](#product_attribute.update-RequestExampleSOAPV2)

<div>

```
<?php
//ini_set("soap.wsdl_cache_enabled", 0);

$client = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

//V2
$session = $client->login('apiUser', 'apiKey');

// V2 WS-I Mode
//$response = $client->login(array('username' => 'apiUser', 'apiKey' => 'apiKey'));
//$session = $response->result;


//v2
$attributeCode = "code1";
$data = array(
   "frontend_input" => "text",
   "scope" => "1",
   "default_value" => "1",
   "is_unique" => 0,
   "is_required" => 0,
   "apply_to" => array("simple"),
   "is_configurable" => 0,
   "is_searchable" => 0,
   "is_visible_in_advanced_search" => 0,
   "is_comparable" => 0,
   "is_used_for_promo_rules" => 0,
   "is_visible_on_front" => 0,
   "used_in_product_listing" => 0,
   "additional_fields" => array(),
   "frontend_label" => array(array("store_id" => "0", "label" => "some random label updated"))
  );

$orders = $client->catalogProductAttributeUpdate($session, $attributeCode, $data);



//V2 WSI
//WSDL WSI Sample is not complete
//$result = $client->catalogProductAttributeCreate(array("sessionId" => $session, "data" => $data));
//$orders = $result->result->complexObjectArray;

echo 'Number of results: ' . count($orders) . '<br/>';
var_dump ($orders);
?>
```

</div>



Create the Magento file system owner
