

## [Module: Mage_Catalog](#catalog_product_attribute.currentStore-Module:MageCatalog)

## [Resource: catalog_product_attribute](#catalog_product_attribute.currentStore-Resource:catalogproductattribute)

**Aliases:**

-   product_attribute

## [Method:](#catalog_product_attribute.currentStore-Method:)

-   catalog_product_attribute.currentStore (SOAP V1)
-   catalogProductAttributeCurrentStore (SOAP V2)

Allows you to set/get the current store view.

**Aliases:**

-   product_attribute.currentStore

**Arguments:**

---

Type Name Description

---

string\ sessionId\ Session ID

string\ storeView\ Store view ID or code
(optional)

---

**Returns**:

Type Name Description

---

int storeView Store view ID

### [Examples](#catalog_product_attribute.currentStore-Examples)

### [Request Example SOAP V1](#catalog_product_attribute.currentStore-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_product_attribute.currentStore', 'english');
var_dump ($result);
```

</div>



### [Request Example SOAP V2](#catalog_product_attribute.currentStore-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductAttributeCurrentStore($sessionId, 'english');
var_dump($result);
```

</div>



Create the Magento file system owner
