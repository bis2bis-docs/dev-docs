

## [Module: Mage_Catalog](#MAPI-ProductAttributes-Module:MageCatalog)

The Mage_Catalog module allows you to manage categories and products.

### [Product Attributes](#MAPI-ProductAttributes-ProductAttributes)

Allows you to retrieve product attributes and options.

**Resource Name**: catalog_product_attribute

**Aliases**:

-   product_attribute

**Methods**:

-   [product_attribute.currentStore](product_attribute.currentStore.html "product_attribute.currentStore") -
    Set/Get the current store view
-   [product_attribute.list](product_attribute.list.html "product_attribute.list") -
    Retrieve the attribute list
-   [product_attribute.options](product_attribute.options.html "product_attribute.options") -
    Retrieve the attribute options
-   [product_attribute.addOption](product_attribute.addOption.html "product_attribute.addOption") -
    Add a new option for attributes with selectable fields
-   [product_attribute.create](product_attribute.create.html "product_attribute.create") -
    Create a new attribute
-   [product_attribute.info](product_attribute.info.html "product_attribute.info") -
    Get full information about an attribute with the list of options
-   [product_attribute.remove](product_attribute.remove.html "product_attribute.remove") -
    Remove the required attribute
-   [product_attribute.removeOption](product_attribute.removeOption.html "product_attribute.removeOption") -
    Remove an option for attributes with selectable fields
-   [product_attribute.types](product_attribute.types.html "product_attribute.types") -
    Get the list of possible attribute types
-   [product_attribute.update](product_attribute.update.html "product_attribute.update") -
    Update the required attribute

## [Faults:](#MAPI-ProductAttributes-Faults:)

Fault Code Fault Message

---

100 Requested store view not found.
101 Requested attribute not found.
102 Invalid request parameters.
103 Attribute code is invalid. Please use only letters (a-z), numbers (0-9) or underscore (\_) in this field, first character should be a letter.
104 Incorrect attribute type.
105 Unable to save attribute.
106 This attribute cannot be deleted.
107 This attribute cannot be edited.
108 Unable to add option.
109 Unable to remove option.

## [Example:](#MAPI-ProductAttributes-Example:)

<div>

```
<pre>
<?php
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

echo "<pre>";
// Create new attribute
$attributeToCreate = array(
    "attribute_code" => "new_attribute",
    "scope" => "store",
    "frontend_input" => "select",
    "is_unique" => 0,
    "is_required" => 0,
    "is_configurable" => 0,
    "is_searchable" => 0,
    "is_visible_in_advanced_search" => 0,
    "used_in_product_listing" => 0,
    "additional_fields" => array(
        "is_filterable" => 1,
        "is_filterable_in_search" => 1,
        "position" => 1,
        "used_for_sort_by" => 1
    ),
    "frontend_label" => array(
        array(
            "store_id" => 0,
            "label" => "A new attribute"
        )
    )
);

$attributeId = $proxy->call(
    $sessionId,
    "product_attribute.create",
    array(
        $attributeToCreate
    )
);

// Update attribute
$attributeToUpdate = array(
    "scope" => "global",
    "is_unique" => 1,
    "is_required" => 1,
    "is_configurable" => 1,
    "is_searchable" => 1,
    "is_visible_in_advanced_search" => 0,
    "used_in_product_listing" => 0,
    "additional_fields" => array(
        "is_filterable" => 01,
        "is_filterable_in_search" => 0,
        "position" => 2,
        "used_for_sort_by" => 0
    ),
    "frontend_label" => array(
        array(
            "store_id" => 0,
            "label" => "A Test Attribute"
        )
    )
);
$proxy->call(
    $sessionId,
    "product_attribute.update",
    array(
         "new_attribute",
         $attributeToUpdate
    )
);

// Add option
$optionToAdd = array(
    "label" => array(
        array(
            "store_id" => 0,
            "value" => "New Option"
        )
    ),
    "order" => 0,
    "is_default" => 0
);

$proxy->call(
    $sessionId,
    "product_attribute.addOption",
    array(
         $attributeId,
         $optionToAdd
    )
);

// Get info
$resultInfo = $proxy->call(
    $sessionId,
    "product_attribute.info",
    array(
         $attributeId
    )
);
echo "info result:\n";
print_r($resultInfo);

// List options
$resultListOptions = $proxy->call(
    $sessionId,
    "product_attribute.options",
    array(
         $attributeId
    )
);
echo "\n options result:\n";
print_r($resultListOptions);

// Remove option
$result = $proxy->call(
    $sessionId,
    "product_attribute.removeOption",
    array(
         $attributeId,
         $resultInfo['options'][0]['value']
    )
);

// remove attribute
$result = $proxy->call(
    $sessionId,
    "product_attribute.remove",
    array(
         $attributeId
    )
);
```

</div>



Create the Magento file system owner
