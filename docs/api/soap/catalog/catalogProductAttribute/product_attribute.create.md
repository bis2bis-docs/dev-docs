

### [Module: Product Attributes API](#product_attribute.create-Module:ProductAttributesAPI)

## [Resource: product_attribute](#product_attribute.create-Resource:productattribute)

## [Method:](#product_attribute.create-Method:)

-   product_attribute.create (SOAP V1)
-   catalogProductAttributeCreate (SOAP V2)

Allows you to create a new product attribute.

**Arguments:**

Type Name Description

---

string sessionId Session ID
array data Array of catalogProductAttributeEntityToCreate

**Returns:**

Type Name Description

---

int result ID of the created attribute

The **catalogProductAttributeEntityToCreate** content is as follows:

---

Type Name Description

---

string attribute_code\ Attribute code

string\ frontend_input\ Attribute type

string\ scope\ Attribute scope. Possible values are
as follows: \'store\', \'website\', or
\'global\'

string\ default_value\ Attribute default value

int is_unique\ Defines whether the attribute is
unique

int is_required\ Defines whether the attribute is
required

ArrayOfString apply_to\ Apply to. Empty for \"Apply to all\"
or array of the following possible
values: \'simple\', \'grouped\',
\'configurable\', \'virtual\',
\'bundle\', \'downloadable\',
\'giftcard\'

int is_configurable\ Defines whether the attribute can be
used for configurable products

int\ is_searchable\ Defines whether the attribute can be
used in Quick Search

int\ is_visible_in_advanced_search\ Defines whether the attribute can be
used in Advanced Search

int\ is_comparable\ Defines whether the attribute can be
compared on the frontend

int\ is_used_for_promo_rules\ Defines whether the attribute can be
used for promo rules

int\ is_visible_on_front\ Defines whether the attribute is
visible on the frontend

int\ used_in_product_listing\ Defines whether the attribute can be
used in product listing

associativeArray additional_fields\ Array of additional fields

array frontend_label\ Array of
catalogProductAttributeFrontendLabel

---

The **catalogProductAttributeFrontendLabelEntity** content is as
follows:

---

Type Name Description

---

string store_id\ Store ID

string label\ Text label

---

**Notes**: The \"label\" value for the \"store_id\" value set to 0 must
be specified. An attribute cannot be created without specifying the
label for store_id=0.

The **AdditionaFieldsEntity** array of additional fields for the
**text** type is as follows:

Type Name Description

---

string frontend_class Input Validation for Store Owner. Possible values are as follows: \'validate-number\' (Decimal Number), \'validate-digits\' (Integer Number), \'validate-email\', \'validate-url\', \'validate-alpha\' (Letters), \'validate-alphanum\' (Letters (a-z, A-Z), or Numbers (0-9))
boolean is_html_allowed_on_front Defines whether the HTML tags are allowed on the frontend
boolean used_for_sort_by Defines whether it is used for sorting in product listing

The **AdditionaFieldsEntity** array of additional fields for the **text
area** type is as follows:

---

Type Name Description

---

boolean is_wysiwyg_enabled Enable WYSIWYG flag

boolean is_html_allowed_on_front Defines whether the
HTML tags are allowed
on the frontend

---

The **AdditionaFieldsEntity** array of additional fields for the
**date** and **boolean** types is as follows:

---

Type Name Description

---

boolean used_for_sort_by Defines whether it is
used for sorting in
product listing

---

The **AdditionaFieldsEntity** array of additional fields for the
**multiselect** type is as follows:

Type Name Description

---

boolean is_filterable Defines whether it is used in layered navigation
boolean is_filterable_in_search Defines whether it is used in search results layered navigation
int position Position

The **AdditionaFieldsEntity** array of additional fields for the
**select** and **price** types is as follows:

---

Type Name Description

---

boolean is_filterable Defines whether it is
used in layered
navigation

boolean is_filterable_in_search Defines whether it is
used in search results
layered navigation

int position Position

boolean used_for_sort_by Defines whether it is
used for sorting in
product listing

---

**Faults:**

Fault Code Fault Message

---

102 Invalid request parameters.
103 Attribute code is invalid. Please use only letters (a-z), numbers (0-9) or underscore (\_) in this field, first character should be a letter.
104 Incorrect attribute type.
105 Unable to save attribute.

### [Examples](#product_attribute.create-Examples)

### [Request Example SOAP V1](#product_attribute.create-RequestExampleSOAPV1)

<div>

```
<?php

$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If some stuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$attributeToUpdate = array(
    "scope" => "global",
 "default_value" => "100",
    "frontend_input" => "text",
    "is_unique" => 0,
    "is_required" => 0,
    "is_configurable" => 0,
    "is_searchable" => 0,
    "is_visible_in_advanced_search" => 0,
    "used_in_product_listing" => 0,
    "additional_fields" => array(
        "is_filterable" => 1,
        "is_filterable_in_search" => 1,
        "position" => 1,
        "used_for_sort_by" => 1
    ),
    "frontend_label" => array(
        array(
            "store_id" => 0,
            "label" => "Updated attribute"
        )
    )
);


$attributeCode = 'code1';

$result = $client->call($session, 'product_attribute.update', array($attributeCode, $attributeToUpdate));
var_dump ($result);

// If you don't need the session anymore
//$client->endSession($session);

?>
```

</div>



### [Request Example SOAP V2](#product_attribute.create-RequestExampleSOAPV2)

<div>

```
<?php
//ini_set("soap.wsdl_cache_enabled", 0);

$client = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

//V2
$session = $client->login('apiUser', 'apiKey');

// V2 WS-I Mode
//$response = $client->login(array('username' => 'apiUser', 'apiKey' => 'apiKey'));
//$session = $response->result;


//v2

$data = array(
   "attribute_code" => "test_attribute",
   "frontend_input" => "text",
   "scope" => "1",
   "default_value" => "1",
   "is_unique" => 0,
   "is_required" => 0,
   "apply_to" => array("simple"),
   "is_configurable" => 0,
   "is_searchable" => 0,
   "is_visible_in_advanced_search" => 0,
   "is_comparable" => 0,
   "is_used_for_promo_rules" => 0,
   "is_visible_on_front" => 0,
   "used_in_product_listing" => 0,
   "additional_fields" => array(),
   "frontend_label" => array(array("store_id" => "0", "label" => "some label"))
  );

$orders = $client->catalogProductAttributeCreate($session, $data);



//V2 WSI
//WSDL WSI Sample is not complete
//$result = $client->catalogProductAttributeCreate(array("sessionId" => $session, "data" => $data));
//$orders = $result->result->complexObjectArray;

echo 'Number of results: ' . count($orders) . '<br/>';
var_dump ($orders);
?>
```

</div>



Create the Magento file system owner
