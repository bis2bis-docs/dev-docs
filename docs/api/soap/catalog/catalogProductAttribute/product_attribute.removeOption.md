

### [Module: Product Attributes API](#product_attribute.removeOption-Module:ProductAttributesAPI)

## [Resource: product_attribute](#product_attribute.removeOption-Resource:productattribute)

## [Method:](#product_attribute.removeOption-Method:)

-   product_attribute.removeOption (SOAP V1)
-   catalogProductAttributeRemoveOption (SOAP V2)

Allows you to remove the option for an attribute.

**Arguments:**

---

Type Name Description

---

string sessionId\ Session ID

string attribute Attribute code or ID

string\ optionId Option ID

---

**Return:**

Type Description

---

boolean True if the option is removed

**Faults:**

Fault Code Fault Message

---

101 Requested attribute not found.
104 Incorrect attribute type.
109 Unable to remove option.

### [Examples](#product_attribute.removeOption-Examples)

### [Request example SOAP V1](#product_attribute.removeOption-RequestexampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$attributeCode = "2";
$optionId = 11; // Existing option ID

$result = $proxy->call(
    $sessionId,
    "product_attribute.removeOption",
    array(
         $attributeCode,
         $optionId
    )
);
```

</div>



### [Request example SOAP V2](#product_attribute.removeOption-RequestexampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductAttributeRemoveOption($sessionId, '2', '11');
var_dump($result);
```

</div>



Create the Magento file system owner
