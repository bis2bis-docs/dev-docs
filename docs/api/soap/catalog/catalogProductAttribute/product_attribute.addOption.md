

### [Module: Product Attributes API](#product_attribute.addOption-Module:ProductAttributesAPI)

## [Resource: product_attribute](#product_attribute.addOption-Resource:productattribute)

## [Method:](#product_attribute.addOption-Method:)

-   product_attribute.addOption (SOAP V1)
-   catalogProductAttributeAddOption (SOAP V2)

Allows you to add a new option for attributes with selectable fields.

**Arguments:**

---

Type Name Description

---

string sessionId Session ID

string attribute\ Attribute code or ID

array data Array of
catalogProductAttributeOptionEntityToAdd

---

**Return:**

Type Name Description

---

boolean result True on success

The **catalogProductAttributeOptionEntityToAdd** content is as follows:

Type Name Description

---

array label Array of catalogProductAttributeOptionLabel
int order Option order
int is_default Defines whether the option is a default one

The **catalogProductAttributeOptionLabel** content is as follows:

---

Type Name Description

---

ArrayOfString store_id\ Array of store view IDs

string value\ Text label

---

**Faults:**

Fault Code Fault Message

---

101 Requested attribute not found.
104 Incorrect attribute type.
108 Unable to add option.

### [Examples](#product_attribute.addOption-Examples)

### [Request Example SOAP V1](#product_attribute.addOption-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$attributeCode = "new_attribute";
$optionToAdd = array(
    "label" => array(
        array(
            "store_id" => 0,
            "value" => "New Option"
        )
    ),
    "order" => 0,
    "is_default" => 0
);

$result = $proxy->call(
    $sessionId,
    "product_attribute.addOption",
    array(
         $attributeCode,
         $optionToAdd
    )
);
```

</div>



### [Request Example SOAP V2](#product_attribute.addOption-RequestExampleSOAPV2)

<div>

```
<?php
$client = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

//V2
$session = $client->login('apiUser', 'apiKey');

// V2 WS-I Mode
//$response = $client->login(array('username' => 'apiUser', 'apiKey' => 'apiKey'));
//$session = $response->result;

$attributeCode = "new_attribute";

//v2

$label = array (
   array(
    "store_id" => array("0"),
    "value" => "some random data"
   )
  );

$data = array(
   "label" => $label,
   "order" => "10",
   "is_default" => "1"
  );

$orders = $client->catalogProductAttributeAddOption($session, $attributeCode, $data);



//V2 WSI
//WSDL WSI does not describe this method
//$result = $client->catalogProductAttributeAddOption(...);
//$orders = $result->result->complexObjectArray;

var_dump ($orders);
?>
```

</div>



Create the Magento file system owner
