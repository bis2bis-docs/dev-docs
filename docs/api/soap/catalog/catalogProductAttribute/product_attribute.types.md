

### [Module: Product Attributes API](#product_attribute.types-Module:ProductAttributesAPI)

## [Resource: product_attribute](#product_attribute.types-Resource:productattribute)

## [Method:](#product_attribute.types-Method:)

-   product_attribute.types (SOAP V1)
-   catalogProductAttributeTypes (SOAP V2)

Allows you to retrieve the list of possible attribute types.

**Arguments:**

Type Name Description

---

string sessionId Session ID

**Return:**

Type Name Description

---

array result Array of catalogAttributeOptionEntity

The **catalogAttributeOptionEntity** content is as follows:

---

Type Name Description

---

string label Option label

string\ value\ Option value

---

**Faults:**

_No Faults._

### [Examples](#product_attribute.types-Examples)

### [Request Example SOAP V1](#product_attribute.types-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$result = $proxy->call(
    $sessionId,
    "product_attribute.types"
);

echo "<pre>";
var_dump($result);
```

</div>



### [Request Example SOAP V2](#product_attribute.types-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductAttributeTypes($sessionId);
var_dump($result);
```

</div>



### [Response Example SOAP V1](#product_attribute.types-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
      'value' => 'text'
      'label' => 'Text Field'
  1 =>
      'value' => 'textarea'
      'label' => 'Text Area'
  2 =>
      'value' => 'date'
      'label' => 'Date'
  3 =>
      'value' => 'boolean'
      'label' => 'Yes/No'
  4 =>
      'value' => 'multiselect'
      'label' => 'Multiple Select'
  5 =>
      'value' => 'select'
      'label' => 'Dropdown'
  6 =>
      'value' => 'price'
      'label' => 'Price'
  7 =>
      'value' => 'media_image'
      'label' => 'Media Image'
```

</div>



Create the Magento file system owner
