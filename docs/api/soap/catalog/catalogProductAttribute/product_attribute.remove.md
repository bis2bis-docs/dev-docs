

### [Module: Product Attributes API](#product_attribute.remove-Module:ProductAttributesAPI)

## [Resource: product_attribute](#product_attribute.remove-Resource:productattribute)

## [Method:](#product_attribute.remove-Method:)

-   product_attribute.remove (SOAP V1)
-   catalogProductAttributeRemove (SOAP V2)

Allows you to remove the required attribute from a product.

**Arguments:**

---

Type Name Description

---

string sessionId\ Session ID

string attribute Attribute code or ID

---

**Return:**

Type Description

---

boolean True if the attribute is removed

**Faults:**

Fault Code Fault Message

---

101 Requested attribute not found.
106 This attribute cannot be deleted.

### [Examples](#product_attribute.remove-Examples)

### [Request Example SOAP V1](#product_attribute.remove-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$attributeCode = "11";

$result = $proxy->call(
    $sessionId,
    "product_attribute.remove",
    array(
         $attributeCode
    )
);
```

</div>



### [Request Example SOAP V2](#product_attribute.remove-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductAttributeRemove($sessionId, '11');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#product_attribute.remove-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$client = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$session = $client->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $client->catalogProductAttributeRemove((object)array('sessionId' => $session->result, 'attribute' => '11'));

var_dump ($result);
```

</div>


