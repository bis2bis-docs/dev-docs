

## [Module: Mage_Catalog](#catalog_category.create-Module:MageCatalog)

The Mage_Catalog module allows you to manage categories and products.

## [**Resource Name:** catalog_category](#catalog_category.create-ResourceName:catalogcategory)

**Aliases:**

-   category

## [Method:](#catalog_category.create-Method:)

-   catalog_category.create (SOAP V1)
-   catalogCategoryCreate (SOAP V2)

Create a new category and return its ID.

**Aliases:**

-   category.create

**Arguments:**

Type Name Description

---

string sessionId Session ID
int parentId Parent category ID
array categoryData Array of catalogCategoryEntityCreate
string storeView Store view ID or code (optional)

**Returns**:

---

Type Name Description

---

int attribute_id\ ID of the created
category

---

The **categoryData** content is as follows:

---

Type Name Description

---

string name Name of the created
category

int is_active Defines whether the
category will be
visible in the frontend

int position Position of the created
category (optional)

ArrayOfString available_sort_by All available options
by which products in
the category can be
sorted

string custom_design The custom design for
the category (optional)

int custom_apply_to_products\ Apply the custom design
to all products
assigned to the
category (optional)

string custom_design_from Date starting from
which the custom design
will be applied to the
category (optional)

string custom_design_to Date till which the
custom design will be
applied to the category
(optional)

string custom_layout_update Custom layout update
(optional)

string default_sort_by The default option by
which products in the
category are sorted

string description Category description
(optional)

string display_mode Content that will be
displayed on the
category view page
(optional)

int is_anchor Defines whether the
category will be
anchored (optional)

int landing_page Landing page (optional)

string meta_description Category meta
description (optional)

string meta_keywords Category meta keywords
(optional)

string meta_title Category meta title
(optional)

string page_layout Type of page layout
that the category
should use (optional)

string url_key A relative URL path
which can be entered in
place of the standard
target path (optional)

int include_in_menu Defines whether the
category is visible on
the top menu bar

string filter_price_range Price range of each
price level displayed
in the layered
navigation block
(optional)

int custom_use_parent_settings\ Defines whether the
category will inherit
custom design settings
of the category to
which it is assigned. 1 - Yes, 0 - No
(optional)

---

**Notes**: The **position** parameter is deprecated, the category will
be positioned anyway in the end of the list and you can not set the
position directly. You should use the catalog_category.move method
instead. You cannot also assign a root category to the specified store.

### [Examples](#catalog_category.create-Examples)

### [Request Example SOAP V1](#catalog_category.create-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If some stuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');
$result = $client->call($session, 'catalog_category.create', array(2, array(
    'name' => 'Category name',
    'is_active' => 1,
    'position' => 1,
    //<!-- position parameter is deprecated, category anyway will be positioned in the end of list
    //and you can not set position directly, use catalog_category.move instead -->
    'available_sort_by' => 'position',
    'custom_design' => null,
    'custom_apply_to_products' => null,
    'custom_design_from' => null,
    'custom_design_to' => null,
    'custom_layout_update' => null,
    'default_sort_by' => 'position',
    'description' => 'Category description',
    'display_mode' => null,
    'is_anchor' => 0,
    'landing_page' => null,
    'meta_description' => 'Category meta description',
    'meta_keywords' => 'Category meta keywords',
    'meta_title' => 'Category meta title',
    'page_layout' => 'two_columns_left',
    'url_key' => 'url-key',
    'include_in_menu' => 1,
)));

var_dump ($result);
```

</div>



### [Request Example SOAP V2](#catalog_category.create-RequestExampleSOAPV2)

<div>

```
$client = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

// If some stuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');
$result = $client->catalogCategoryCreate($session, 2, array(
    'name' => 'Category name 2',
    'is_active' => 1,
    'position' => 1,
    //<!-- position parameter is deprecated, category anyway will be positioned in the end of list
    //and you can not set position directly, use catalog_category.move instead -->
    'available_sort_by' => array('position'),
    'custom_design' => null,
    'custom_apply_to_products' => null,
    'custom_design_from' => null,
    'custom_design_to' => null,
    'custom_layout_update' => null,
    'default_sort_by' => 'position',
    'description' => 'Category description',
    'display_mode' => null,
    'is_anchor' => 0,
    'landing_page' => null,
    'meta_description' => 'Category meta description',
    'meta_keywords' => 'Category meta keywords',
    'meta_title' => 'Category meta title',
    'page_layout' => 'two_columns_left',
    'url_key' => 'url-key',
    'include_in_menu' => 1,
));

var_dump ($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_category.create-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogCategoryCreate((object)array('sessionId' => $sessionId->result, 'parentId' => '5', 'categoryData' => ((object)array(
    'name' => 'category',
    'is_active' => '1',
    'position' => '1',
    'available_sort_by' => array('position'),
    'default_sort_by' => 'position',
    'description' => 'Category description',
    'is_anchor' => '1',
    'include_in_menu' => '1'
))));
var_dump($result->result);
```

</div>


