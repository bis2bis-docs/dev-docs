

## [Module: Mage_Catalog](#catalog_category.currentStore-Module:MageCatalog)

The Mage_Catalog module allows you to manage categories and products.

## [Resource Name: catalog_category](#catalog_category.currentStore-ResourceName:catalogcategory)

**Aliases:**

-   category

## [Method:](#catalog_category.currentStore-Method:)

-   catalog_category.currentStore (SOAP V1)
-   catalogCategoryCurrentStore (SOAP V2)

Allows you to set/get the current store view.

**Aliases:**

-   category.currentStore

**Arguments:**

Type Name Description

---

string sessionId Session ID
string storeView Store view ID or code

**Returns**:

Type Name Description

---

int storeView Store view ID

### [Examples](#catalog_category.currentStore-Examples)

### [Request Example SOAP V1](#catalog_category.currentStore-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'category.currentStore', '1');
var_dump ($result);
```

</div>



### [Request Example SOAP V2](#catalog_category.currentStore-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogCategoryCurrentStore($sessionId, '1');
var_dump($result);
```

</div>


