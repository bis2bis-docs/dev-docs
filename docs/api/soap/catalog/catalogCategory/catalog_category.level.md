

## [Module: Mage_Catalog](#catalog_category.level-Module:MageCatalog)

The Mage_Catalog module allows you to manage categories and products.

## [Resource Name: catalog_category](#catalog_category.level-ResourceName:catalogcategory)

**Aliases:**

-   category

## [Method:](#catalog_category.level-Method:)

-   catalog_category.level (SOAP V1)
-   catalogCategoryLevel (SOAP V2)

Allows you to retrieve one level of categories by a website, a store
view, or a parent category.

**Aliases:**

-   category.level

**Arguments:**

Type Name Description

---

string sessionId Session ID
string website Website ID or code (optional)
string storeView Store view ID or code (optional)
string parentCategory Parent category ID (optional)

**Returns**:

Type Name Description

---

array tree Array of CatalogCategoryEntitiesNoChildren

The **CatalogCategoryEntitityNoChildren** content is as follows:

---

Type Name Description

---

int category_id\ Category ID

int parent_id\ Parent category ID

string name\ Category name

int is_active\ Defines whether the
category is active

int position\ Category position

int level\ Category level

---

### [Examples](#catalog_category.level-Examples)

### [Request Example SOAP V1](#catalog_category.level-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_category.level');
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#catalog_category.level-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogCategoryLevel($sessionId);
var_dump($result);
```

</div>



### [Response Example SOAP V1](#catalog_category.level-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
    array
      'category_id' => string '2' (length=1)
      'parent_id' => int 1
      'name' => string 'Default Category' (length=16)
      'is_active' => string '1' (length=1)
      'position' => string '1' (length=1)
      'level' => string '1' (length=1)
  1 =>
    array
      'category_id' => string '3' (length=1)
      'parent_id' => int 1
      'name' => string 'root_category' (length=13)
      'is_active' => string '1' (length=1)
      'position' => string '2' (length=1)
      'level' => string '1' (length=1)
```

</div>


