

## [Module: Mage_Catalog](#MAPI-Category-Module:MageCatalog)

The Mage_Catalog module allows you to manage categories and products.

### [Category](#MAPI-Category-Category)

Allows you to manage categories and how products are assigned to
categories.

**Resource Name**: catalog_category

**Aliases**:

-   category

**Methods**:

-   [catalog_category.currentStore](catalog_category.currentStore.html "catalog_category.currentStore") -
    Set/Get the current store view
-   [catalog_category.tree](catalog_category.tree.html "catalog_category.tree") -
    Retrieve the hierarchical category tree
-   [catalog_category.level](catalog_category.level.html "catalog_category.level") -
    Retrieve one level of categories by a website, store view, or parent
    category
-   [catalog_category.info](catalog_category.info.html "catalog_category.info") -
    Retrieve the category data
-   [catalog_category.create](catalog_category.create.html "catalog_category.create") -
    Create a new category
-   [catalog_category.update](catalog_category.update.html "catalog_category.update") -
    Update a category
-   [catalog_category.move](catalog_category.move.html "catalog_category.move") -
    Move a category in its tree
-   [catalog_category.delete](catalog_category.delete.html "catalog_category.delete") -
    Delete a category
-   [catalog_category.assignedProducts](catalog_category.assignedProducts.html "catalog_category.assignedProducts") -
    Retrieve a list of products assigned to a category
-   [catalog_category.assignProduct](catalog_category.assignProduct.html "catalog_category.assignProduct") -
    Assign product to a category
-   [catalog_category.updateProduct](catalog_category.updateProduct.html "catalog_category.updateProduct") -
    Update an assigned product
-   [catalog_category.removeProduct](catalog_category.removeProduct.html "catalog_category.removeProduct") -
    Remove a product assignment

### [Faults](#MAPI-Category-Faults)

Fault Code Fault Message

---

100 Requested store view not found.
101 Requested website not found.
102 Category not exists.
103 Invalid data given. Details in error message.
104 Category not moved. Details in error message.
105 Category not deleted. Details in error message.
106 Requested product is not assigned to category.

### [Example 1. Working with categories](#MAPI-Category-Example1.Workingwithcategories)

<div>

```
function getSomeRandomCategory(&$categories, $targetLevel, $currentLevel = 0) {
    if (count($categories)==0) {
        return false;
    }
    if ($targetLevel == $currentLevel) {
        return $categories[array_rand($categories)];
    } else {
        return getSomeRandomCategory($categories[array_rand($categories)]['children'], $targetLevel + 1);
    }
}

$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');
$allCategories = $proxy->call($sessionId, 'category.tree'); // Get all categories.

// select random category from tree
while (($selectedCategory = getSomeRandomCategory($allCategories, 3)) === false) 

// create new category
$newCategoryId = $proxy->call(
    $sessionId,
    'category.create',
    array(
        $selectedCategory['category_id'],
         array(
                'name'=>'Newopenerp',
                'is_active'=>1,
                'include_in_menu'=>2,
                'available_sort_by'=>'position',
                'default_sort_by'=>'position'
               )
    )
);

$newData = array('is_active'=>1);
// update created category on German store view
$proxy->call($sessionId, 'category.update', array($newCategoryId, $newData, 'german'));

$firstLevel = $proxy->call($sessionId, 'category.level', array(null, 'german', $selectedCategory['category_id']));

var_dump($firstLevel);

// If you wish remove category, uncomment next line
//$proxy->call($sessionId, 'category.delete', $newCategoryId);
```

</div>



### [Example 2. Working with assigned products](#MAPI-Category-Example2.Workingwithassignedproducts)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$categoryId = 5; // Put here your category id
$storeId = 1; // You can add store level

$assignedProducts = $proxy->call($sessionId, 'category.assignedProducts', array($categoryId, $storeId));
var_dump($assignedProducts); // Will output assigned products.

// Assign product
$proxy->call($sessionId, 'category.assignProduct', array($categoryId, 'someProductSku', 5));

// Update product assignment position
$proxy->call($sessionId, 'category.updateProduct', array($categoryId, 'someProductSku', 25));

// Remove product assignment
$proxy->call($sessionId, 'category.removeProduct', array($categoryId, 'someProductSku'));
```

</div>


