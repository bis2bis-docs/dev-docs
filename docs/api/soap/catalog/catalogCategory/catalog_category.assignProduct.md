

## [Module: Mage_Catalog](#catalog_category.assignProduct-Module:MageCatalog)

The Mage_Catalog module allows you to manage categories and products.

## [Resource Name: catalog_category](#catalog_category.assignProduct-ResourceName:catalogcategory)

**Aliases:**

-   category

## [Method:](#catalog_category.assignProduct-Method:)

-   catalog_category.assignProduct (SOAP V1)
-   catalogCategoryAssignProduct (SOAP V2)

Assign a product to the required category.

**Aliases:**

-   category.assignProduct

**Arguments:**

Type Name Description

---

string sessionId Session ID
int categoryId ID of the category
string product/productId ID or SKU of the product to be assigned to the category
string position Position of the assigned product in the category (optional)
string identifierType Defines whether the product ID or SKU is passed in the \'product\' argument

**Returns**:

Type Description

---

boolean True if the product is assigned to the specified category

### [Examples](#catalog_category.assignProduct-Examples)

### [Request Example SOAP V1](#catalog_category.assignProduct-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_category.assignProduct', array('categoryId' => '4', 'product' => '1'));
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#catalog_category.assignProduct-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogCategoryAssignProduct($sessionId, '4', '3');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_category.assignProduct-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogCategoryAssignProduct((object)array('sessionId' => $sessionId->result, 'categoryId' => '5', 'productId' => '1', 'position' => '5'));
var_dump($result->result);
```

</div>


