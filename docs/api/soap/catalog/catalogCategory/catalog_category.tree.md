

## [Module: Mage_Catalog](#catalog_category.tree-Module:MageCatalog)

The Mage_Catalog module allows you to manage categories and products.

## [Resource Name: catalog_category](#catalog_category.tree-ResourceName:catalogcategory)

**Aliases:**

-   category

## [Method:](#catalog_category.tree-Method:)

-   catalog_category.tree (SOAP V1)
-   catalogCategoryTree (SOAP V2)

Allows you to retrieve the hierarchical tree of categories.

**Aliases:**

-   category.tree

**Arguments:**

Type Name Description

---

string sessionId Session ID
string parentId ID of the parent category (optional)
string storeView Store view (optional)

**Returns:**

Type Name Description

---

array tree Array of catalogCategoryTree

The **catalogCategoryTree** content is as follows:

---

Type Name Description

---

int category_id Category ID

int parent_id\ Parent category ID

string name\ Category name

int position\ Category position

int level\ Category level

array children\ Array of
CatalogCategoryEntities

---

The **catalogCategoryEntity** content is as follows:

---

Type Name Description

---

int category_id\ Category ID

int parent_id\ Parent category ID

string name\ Category name

int is_active\ defines whether the
category is active

int position\ Category position

int level\ Category level

array children\ Array of
CatalogCategoryEntities

---

### [Examples](#catalog_category.tree-Examples)

### [Request Example SOAP V1](#catalog_category.tree-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_category.tree');
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#catalog_category.tree-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogCategoryTree($sessionId);
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_category.tree-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogCategoryTree((object)array('sessionId' => $sessionId->result, 'parentId' => '15'));
var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#catalog_category.tree-ResponseExampleSOAPV1)

<div>

```
array
  'category_id' => string '1' (length=1)
  'parent_id' => string '0' (length=1)
  'name' => string 'Root Catalog' (length=12)
  'is_active' => null
  'position' => string '0' (length=1)
  'level' => string '0' (length=1)
  'children' =>
    array
      0 =>
        array
          'category_id' => string '2' (length=1)
          'parent_id' => string '1' (length=1)
          'name' => string 'Default Category' (length=16)
          'is_active' => string '1' (length=1)
          'position' => string '1' (length=1)
          'level' => string '1' (length=1)
          'children' =>
            array
              ...
      1 =>
        array
          'category_id' => string '3' (length=1)
          'parent_id' => string '1' (length=1)
          'name' => string 'root_category' (length=13)
          'is_active' => string '1' (length=1)
          'position' => string '2' (length=1)
          'level' => string '1' (length=1)
          'children' =>
            array
              ...
```

</div>


