

## [Module: Mage_Catalog](#catalog_category.updateProduct-Module:MageCatalog)

The Mage_Catalog module allows you to manage categories and products.

## [Resource Name: catalog_category](#catalog_category.updateProduct-ResourceName:catalogcategory)

**Aliases:**

-   category

## [Method:](#catalog_category.updateProduct-Method:)

-   catalog_category.updateProduct (SOAP V1)
-   catalogCategoryUpdateProduct (SOAP V2)

Allows you to update the product assigned to a category. The product
position is updated.

**Aliases:**

-   category.updateProduct

**Arguments:**

Type Name Description

---

string sessionId Session ID
int categoryId ID of the category to which the product is assigned
string productId ID or SKU of the product to be updated
string position Position of the product in the category (optional)
string identifierType Defines whether the product ID or SKU is passed in the \'product\' parameter

**Returns**:

Type Description

---

boolean True if the product is updated in the category

### [Examples](#catalog_category.updateProduct-Examples)

### [Request Example SOAP V1](#catalog_category.updateProduct-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_category.updateProduct', array('categoryId' => '4', 'product' => '1', 'position' => '3'));
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#catalog_category.updateProduct-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogCategoryUpdateProduct($sessionId, '4', '1', '3');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_category.updateProduct-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogCategoryUpdateProduct((object)array('sessionId' => $sessionId->result, 'categoryId' => '4', 'productId' => '1', 'position' => '3'));
var_dump($result->result);
```

</div>


