

## [Module: Mage_Catalog](#catalog_category.delete-Module:MageCatalog)

The Mage_Catalog module allows you to manage categories and products.

## [Resource Name: catalog_category](#catalog_category.delete-ResourceName:catalogcategory)

**Aliases:**

-   category

## [Method:](#catalog_category.delete-Method:)

-   catalog_category.delete (SOAP V1)
-   catalogCategoryDelete (SOAP V2)

Allows you to delete the required category.

**Aliases:**

-   category.delete

**Arguments:**

Type Name Description

---

string sessionId Session ID
int categoryId ID of the category to be deleted

**Returns**:

Type Description

---

boolean True if the category is deleted

### [Examples](#catalog_category.delete-Examples)

### [Request Example SOAP V1](#catalog_category.delete-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_category.delete', '7');
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#catalog_category.delete-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogCategoryDelete($sessionId, '7');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_category.delete-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogCategoryDelete((object)array('sessionId' => $sessionId->result, 'categoryId' => '7'));
var_dump($result->result);
```

</div>


