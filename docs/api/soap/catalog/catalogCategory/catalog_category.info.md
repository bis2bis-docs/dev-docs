

## [Module: Mage_Catalog](#catalog_category.info-Module:MageCatalog)

The Mage_Catalog module allows you to manage categories and products.

## [Resource Name: catalog_category](#catalog_category.info-ResourceName:catalogcategory)

**Aliases:**

-   category

## [Method:](#catalog_category.info-Method:)

-   catalog_category.info (SOAP V1)
-   catalogCategoryInfo (SOAP V2)

Allows you to retrieve information about the required category.

**Aliases:**

-   category.info

**Arguments:**

Type Name Description

---

string sessionId Session ID
int categoryId Category ID
string storeView Store view ID or code (optional)
ArrayOfString attributes Array of attributes (optional)

**Returns**:

Type Name Description

---

array info Array of catalogCategoryInfo

The **catalogCategoryInfo** content is as follows:

---

Type Name Description

---

string category_id\ Category ID

int is_active\ Defines whether the
category is active

string\ position\ Category position

string\ level\ Category level

string\ parent_id\ Parent category ID

string\ all_children\ All child categories of
the current category

string\ children\ Names of direct child
categories

string\ created_at\ Date when the category
was created

string\ updated_at\ Date when the category
was updated

string\ name\ Category name

string\ url_key\ A relative URL path
which can be entered in
place of the standard
target path (optional)

string\ description\ Category description

string\ meta_title\ Category meta title

string\ meta_keywords\ Category meta keywords

string\ meta_description\ Category meta
description

string\ path\ Path

string\ url_path\ URL path

int children_count\ Number of child
categories

string\ display_mode\ Content that will be
displayed on the
category view page
(optional)

int is_anchor\ Defines whether the
category is anchored

ArrayOfString available_sort_by\ All available options
by which products in
the category can be
sorted

string\ custom_design\ The custom design for
the category
(optional)

string\ custom_apply_to_products \ Apply the custom design
to all products
assigned to the
category (optional)

string\ custom_design_from\ Date starting from
which the custom design
will be applied to the
category (optional)

string\ custom_design_to\ Date till which the
custom design will be
applied to the category
(optional)

string\ page_layout\ Type of page layout
that the category
should use (optional)

string\ custom_layout_update\ Custom layout update
(optional)

string\ default_sort_by\ The default option by
which products in the
category are sorted

int landing_page\ Landing page
(optional)

int include_in_menu\ Defines whether the
category is available
on the Magento top menu
bar

string filter_price_range\ Price range of each
price level displayed
in the layered
navigation block

int custom_use_parent_settings\ Defines whether the
category will inherit
custom design settings
of the category to
which it is assigned. 1 - Yes, 0 - No

---

### [Examples](#catalog_category.info-Examples)

### [Request Example SOAP V1](#catalog_category.info-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_category.info', '5');
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#catalog_category.info-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogCategoryInfo($sessionId, '5');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_category.info-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogCategoryInfo((object)array('sessionId' => $sessionId->result, 'categoryId' => '5'));
var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#catalog_category.info-ResponseExampleSOAPV1)

<div>

```
array
  'category_id' => string '5' (length=1)
  'is_active' => string '1' (length=1)
  'position' => string '1' (length=1)
  'level' => string '2' (length=1)
  'parent_id' => int 3
  'increment_id' => null
  'created_at' => string '2012-03-29 12:30:51' (length=19)
  'updated_at' => string '2012-03-29 14:25:08' (length=19)
  'name' => string 'Mobile Phones' (length=13)
  'url_key' => string 'mobile-phones' (length=13)
  'thumbnail' => null
  'description' => string 'Category for cell phones' (length=24)
  'image' => null
  'meta_title' => string 'Cell Phones' (length=11)
  'meta_keywords' => string 'cell, phone' (length=11)
  'meta_description' => null
  'include_in_menu' => string '1' (length=1)
  'path' => string '1/3/4' (length=5)
  'all_children' => string '4' (length=1)
  'path_in_store' => null
  'children' => string '' (length=0)
  'url_path' => string 'mobile-phones.html' (length=18)
  'children_count' => string '0' (length=1)
  'display_mode' => string 'PRODUCTS' (length=8)
  'landing_page' => null
  'is_anchor' => string '1' (length=1)
  'available_sort_by' => null
  'default_sort_by' => null
  'filter_price_range' => null
  'custom_use_parent_settings' => string '1' (length=1)
  'custom_apply_to_products' => null
  'custom_design' => null
  'custom_design_from' => null
  'custom_design_to' => null
  'page_layout' => null
  'custom_layout_update' => null
```

</div>


