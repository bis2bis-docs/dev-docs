---
sidebar: auto
---



# [Module: Mage_Catalog](#catalog_category.assignedProducts-Module:MageCatalog)

The Mage_Catalog module allows you to manage categories and products.

## [Resource Name: catalog_category](#catalog_category.assignedProducts-ResourceName:catalogcategory)

**Aliases:**

-   category

## [Method:](#catalog_category.assignedProducts-Method:)

-   catalog_category.assignedProducts (SOAP V1)
-   catalogCategoryAssignedProducts (SOAP V2)

Retrieve the list of products assigned to a required category.

**Aliases:**

-   category.assignedProducts

**Arguments:**

Type Name Description

---

string sessionId Session ID
int categoryId ID of the required category

**Returns**:

Type Name Description

---

array result Array of catalogAssignedProduct

The **catalogAssignedProduct** content is as follows:

Type Name Description

---

int product_id ID of the assigned product
string type Product type
int set Attribute set ID
string sku Product SKU
int position Position of the assigned product

### [Examples](#catalog_category.assignedProducts-Examples)

### [Request Example SOAP V1](#catalog_category.assignedProducts-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_category.assignedProducts', '4');
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#catalog_category.assignedProducts-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogCategoryAssignedProducts($sessionId, '4');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_category.assignedProducts-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogCategoryAssignedProducts((object)array('sessionId' => $sessionId->result, 'categoryId' => '4'));
var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#catalog_category.assignedProducts-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
    array
      'product_id' => string '1' (length=1)
      'type' => string 'simple' (length=6)
      'set' => string '4' (length=1)
      'sku' => string 'n2610' (length=5)
      'position' => string '1' (length=1)
  1 =>
    array
      'product_id' => string '2' (length=1)
      'type' => string 'simple' (length=6)
      'set' => string '4' (length=1)
      'sku' => string 'b8100' (length=5)
      'position' => string '1' (length=1)
```

</div>


