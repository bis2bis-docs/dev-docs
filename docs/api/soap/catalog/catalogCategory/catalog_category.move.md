

## [Module: Mage_Catalog](#catalog_category.move-Module:MageCatalog)

The Mage_Catalog module allows you to manage categories and products.

## [Resource Name: catalog_category](#catalog_category.move-ResourceName:catalogcategory)

**Aliases:**

-   category

## [Method:](#catalog_category.move-Method:)

-   catalog_category.move (SOAP V1)
-   catalogCategoryMove (SOAP V2)

Allows you to move the required category in the category tree.

**Aliases:**

-   category.move

**Arguments:**

Type Name Description

---

string sessionId Session ID
int categoryId ID of the category to be moved
int parentId ID of the new parent category
string afterId ID of the category after which the required category will be moved (optional for V1 and V2)

**Returns**:

Type Name Description

---

boolean id True if the category is moved

### [Examples](#catalog_category.move-Examples)

### [Request Example SOAP V1](#catalog_category.move-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_category.move', array('categoryId' => '4', 'parentId' => '3'));
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#catalog_category.move-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogCategoryMove($sessionId, '4', '3');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_category.move-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogCategoryMove((object)array('sessionId' => $sessionId->result, 'categoryId' => '19', 'parentId' => '8', 'afterId' => '4'));
var_dump($result->result);
```

</div>



**Note**: Please make sure that you are not moving the category to any
of its own children. There are no extra checks to prevent doing it
through API, and you won't be able to fix this from the admin interface
later.
