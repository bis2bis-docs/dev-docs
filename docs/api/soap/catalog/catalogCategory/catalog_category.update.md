

## [Module: Mage_Catalog](#catalog_category.update-Module:MageCatalog)

The Mage_Catalog module allows you to manage categories and products.

## [**Resource Name:** catalog_category](#catalog_category.update-ResourceName:catalogcategory)

**Aliases:**

-   category

## [Method:](#catalog_category.update-Method:)

-   catalog_category.update (SOAP V1)
-   catalogCategoryUpdate (SOAP V2)

Update the required category. Note that you should specify only those
parameters which you want to be updated.

**Aliases:**

-   category.update

**Arguments**:

Type Name Description

---

string sessionId Session ID
int categoryId ID of the category to be updated
array categoryData An array of catalogCategoryEntityCreate
string storeView Store view ID or code (optional)

**Returns**:

Type Description

---

boolean True if the category is updated

The **catalogCategoryEntityCreate** content is as follows:

---

Type Name Description

---

string name Name of the category to
be updated

int is_active Defines whether the
category is visible in
the frontend

int position Position of the
category to be updated

arrayOfString\ available_sort_by\ All available options
by which products in
the category can be
sorted

string\ custom_design\ The custom design for
the category

int\ custom_apply_to_products \ Apply the custom design
to all products
assigned to the
category

string\ custom_design_from\ Date starting from
which the custom design
will be applied to the
category

string\ custom_design_to\ Date till which the
custom design will be
applied to the
category

string\ custom_layout_update\ Custom layout update

string\ default_sort_by\ The default option by
which products in the
category are sorted

string\ description\ Category description

string\ display_mode\ Content that will be
displayed on the
category view page

int\ is_anchor\ Defines whether the
category will be
anchored

int\ landing_page\ Landing page

string\ meta_description\ Category meta
description

string\ meta_keywords\ Category meta keywords

string\ meta_title\ Category meta title

string\ page_layout\ Type of page layout
that the category
should use

string\ url_key\ A relative URL path
which can be entered in
place of the standard
target path

int\ include_in_menu\ Defines whether the
category is visible on
the top menu bar in the
frontend

string filter_price_range\ Price range of each
price level displayed
in the layered
navigation block

int custom_use_parent_settings\ Defines whether the
category will inherit
custom design settings
of the category to
which it is assigned. 1 - Yes, 0 - No

---

**Faults**:
_No Faults_

### [Examples](#catalog_category.update-Examples)

### [Request Example SOAP V1](#catalog_category.update-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If some stuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');
$result = $client->call($session, 'catalog_category.update', array(23, array(
    'name' => 'Category name',
    'is_active' => 1,
    'position' => 1,
    //<!-- position parameter is deprecated, category anyway will be positioned in the end of list
    //and you can not set position directly, use catalog_category.move instead -->
    'available_sort_by' => 'position',
    'custom_design' => null,
    'custom_apply_to_products' => null,
    'custom_design_from' => null,
    'custom_design_to' => null,
    'custom_layout_update' => null,
    'default_sort_by' => 'position',
    'description' => 'Category description',
    'display_mode' => null,
    'is_anchor' => 0,
    'landing_page' => null,
    'meta_description' => 'Category meta description',
    'meta_keywords' => 'Category meta keywords',
    'meta_title' => 'Category meta title',
    'page_layout' => 'two_columns_left',
    'url_key' => 'url-key',
    'include_in_menu' => 1,
)));

var_dump ($result);
```

</div>



### [Request Example SOAP V2](#catalog_category.update-RequestExampleSOAPV2)

<div>

```
$client = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

// If some stuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');
$result = $client->catalogCategoryUpdate($session, 23, array(
    'name' => 'Category name 2',
    'is_active' => 1,
    'position' => 1,
    //<!-- position parameter is deprecated, category anyway will be positioned in the end of list
    //and you can not set position directly, use catalog_category.move instead -->
    'available_sort_by' => array('position'),
    'custom_design' => null,
    'custom_apply_to_products' => null,
    'custom_design_from' => null,
    'custom_design_to' => null,
    'custom_layout_update' => null,
    'default_sort_by' => 'position',
    'description' => 'Category description',
    'display_mode' => null,
    'is_anchor' => 0,
    'landing_page' => null,
    'meta_description' => 'Category meta description',
    'meta_keywords' => 'Category meta keywords',
    'meta_title' => 'Category meta title',
    'page_layout' => 'two_columns_left',
    'url_key' => 'url-key',
    'include_in_menu' => 1,
));

var_dump ($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_category.update-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogCategoryUpdate((object)array('sessionId' => $sessionId->result, 'categoryId' => '23', 'categoryData' => ((object)array(
    'name' => 'Category Name Updated',
    'is_active' => '1',
    'position' => '1',
    'available_sort_by' => array('name'),
    'default_sort_by' => 'name',
    'description' => 'Category description',
    'is_anchor' => '1',
    'include_in_menu' => '1'
))));
var_dump($result->result);
```

</div>


