

## [Module: Mage_Catalog](#catalog_category.removeProduct-Module:MageCatalog)

The Mage_Catalog module allows you to manage categories and products.

## [Resource Name: catalog_category](#catalog_category.removeProduct-ResourceName:catalogcategory)

**Aliases:**

-   category

## [Method:](#catalog_category.removeProduct-Method:)

-   catalog_category.removeProduct (SOAP V1)
-   catalogCategoryRemoveProduct (SOAP V2)

Allows you to remove the product assignment from the category.

**Aliases:**

-   category.removeProduct

**Arguments:**

Type Name Description

---

string sessionId Session ID
int categoryId Category ID
string productId ID or SKU of the product to be removed from the category
string identifierType Defines whether the product ID or SKU is passed in the \'product\' parameter

**Returns**:

Type Description

---

boolean True if the product is removed from the category

### [Examples](#catalog_category.removeProduct-Examples)

### [Request Example SOAP V1](#catalog_category.removeProduct-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_category.removeProduct', array('categoryId' => '4', 'product' => '3'));
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#catalog_category.removeProduct-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogCategoryRemoveProduct($sessionId, '4', '3');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_category.removeProduct-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogCategoryRemoveProduct((object)array('sessionId' => $sessionId->result, 'categoryId' => '4', 'productId' => '3'));
var_dump($result->result);
```

</div>


