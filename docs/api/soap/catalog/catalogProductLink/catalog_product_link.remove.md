

## [Module: Mage_Catalog](#catalog_product_link.remove-Module:MageCatalog)

## [Resource: catalog_product_link](#catalog_product_link.remove-Resource:catalogproductlink)

**Aliases:**

-   product_link

## [Method:](#catalog_product_link.remove-Method:)

-   catalog_product_link.remove (SOAP V1)
-   catalogProductLinkRemove (SOAP V2)

Allows you to remove the product link from a specific product.

**Aliases:**

-   product_link.remove

**Arguments:**

---

Type Name Description

---

string sessionId\ Session ID

string\ type\ Type of the link
(cross_sell, up_sell,
related, or grouped)

string\ product\\productId\ Product ID or SKU

string\ linkedProduct\\linkedProductId\ Product ID or SKU for
the link

string identifierType Defines whether the
product ID or SKU is
passed in the
\'product\' parameter

---

**Returns**:

Type Description

---

boolean\\int True (1) if the link is removed from a product

### [Examples](#catalog_product_link.remove-Examples)

### [Request Example SOAP V1](#catalog_product_link.remove-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_product_link.remove', array('type' => 'related', 'product' => '1', 'linkedProduct' => '4'));
var_dump ($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#catalog_product_link.remove-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductLinkRemove($sessionId, 'related', '1', '4');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_product_link.remove-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');
$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductLinkRemove((object)array('sessionId' => $sessionId->result, 'type' => 'related', 'productId' => '1', 'linkedProductId' => '4'));

var_dump($result->result);
```

</div>


