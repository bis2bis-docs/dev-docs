

## [Module: Mage_Catalog](#catalog_product_link.types-Module:MageCatalog)

## [Resource: catalog_product_link](#catalog_product_link.types-Resource:catalogproductlink)

**Aliases:**

-   product_link

## [Method:](#catalog_product_link.types-Method:)

-   catalog_product_link.types (SOAP V1)
-   catalogProductLinkTypes (SOAP V2)

Allows you to retrieve the list of product link types.

**Aliases:**

-   product_link.types

**Arguments:**

Type Name Description

---

string sessionId Session ID

**Returns**:

Type Name Description

---

ArrayOfString result Array of link types

### [Examples](#catalog_product_link.types-Examples)

### [Request Example SOAP V1](#catalog_product_link.types-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'product_link.types');
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#catalog_product_link.types-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductLinkTypes($sessionId);
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_product_link.types-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');
$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductLinkTypes((object)array('sessionId' => $sessionId->result));

var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#catalog_product_link.types-ResponseExampleSOAPV1)

<div>

```
array
  0 => string 'related' (length=7)
  1 => string 'up_sell' (length=7)
  2 => string 'cross_sell' (length=10)
  3 => string 'grouped' (length=7)
```

</div>


