

## [Module: Mage_Catalog](#catalog_product_link.list-Module:MageCatalog)

## [Resource: catalog_product_link](#catalog_product_link.list-Resource:catalogproductlink)

**Aliases:**

-   product_link

## [Method:](#catalog_product_link.list-Method:)

-   catalog_product_link.list (SOAP V1)
-   catalogProductLinkList (SOAP V2)

Allows you to retrieve the list of linked products for a specific
product.

**Aliases:**

-   product_link.list

**Arguments:**

---

Type Name Description

---

string sessionId\ Session ID

string type\ Type of the link
(cross_sell, up_sell,
related, or grouped)

string product\\productId\ Product ID or SKU

string identifierType Defines whether the
product ID or SKU is
passed in the
\'product\' parameter

---

**Returns**:

Type Name Description

---

array result Array of catalogProductLinkEntity

The **catalogProductLinkEntity** content is as follows:

---

Type Name Description

---

string product_id\ Product ID

string\ type\ Type of the link

string\ set\ Product attribute set

string\ sku\ Product SKU

string\ position\ Position

string\ qty\ Quantity

---

### [Examples](#catalog_product_link.list-Examples)

### [Request Example SOAP V1](#catalog_product_link.list-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_product_link.list', array('type' => 'related', 'product' => '1'));
var_dump ($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#catalog_product_link.list-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductLinkList($sessionId, 'related', '1');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_product_link.list-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');
$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductLinkList((object)array('sessionId' => $sessionId->result, 'type' => 'related', 'productId' => '1'));

var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#catalog_product_link.list-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
    array
      'product_id' => string '3' (length=1)
      'type' => string 'simple' (length=6)
      'set' => string '4' (length=1)
      'sku' => string 'canonxt' (length=7)
      'position' => string '1' (length=1)
  1 =>
    array
      'product_id' => string '4' (length=1)
      'type' => string 'simple' (length=6)
      'set' => string '4' (length=1)
      'sku' => string 'canon_powershot' (length=15)
      'position' => string '0' (length=1)
```

</div>


