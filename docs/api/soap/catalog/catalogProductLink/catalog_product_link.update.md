

## [Module: Mage_Catalog](#catalog_product_link.update-Module:MageCatalog)

## [Resource: catalog_product_link](#catalog_product_link.update-Resource:catalogproductlink)

**Aliases:**

-   product_link

## [Method:](#catalog_product_link.update-Method:)

-   catalog_product_link.update (SOAP V1)
-   catalogProductLinkUpdate (SOAP V2)

Allows you to update the product link.

**Aliases:**

-   product_link.update

**Arguments:**

---

Type Name Description

---

string sessionId Session ID

string\ type\ Type of the link
(cross_sell, grouped,
related, or up_sell)

string\ product\\productId\ Product ID or SKU

string\ linkedProduct\\linkedProductId\ Product ID or SKU for the
link

array data\ Array of
catalogProductLinkEntity

string identifierType Defines whether the
product ID or SKU is
passed in the \'product\'
parameter

---

**Returns**:

Type Name Description

---

boolean\\int result True (1) if the link is updated

The **catalogProductLinkEntity** content is as follows:

---

Type Name Description

---

string product_id\ Product ID

string\ type\ Type of the link

string\ set\ Product attribute set

string\ sku\ Product SKU

string\ position\ Position

string\ qty\ Quantity

---

### [Examples](#catalog_product_link.update-Examples)

### [Request Example SOAP V1](#catalog_product_link.update-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$productId = '1';
$linkedProductId = '2';
$data = array(
    'position' => '50'
);

$result = $proxy->call(
    $session,
    'catalog_product_link.update',
    array(
        'cross_sell',
        $productId,
        $linkedProductId,
        $data
    )
);
```

</div>



### [Request Example SOAP V2](#catalog_product_link.update-RequestExampleSOAPV2)

<div>

```
<?php

$client = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$session = $client->login('apiUser', 'apiKey');

$data = array(
   "position" => 15
  );

$identifierType = "product_id";
$type = "related";
$product = "1";
$linkedProduct = "3";

$orders = $client->catalogProductLinkUpdate($session, $type, $product, $linkedProduct, $data, $identifierType);

echo 'Number of results: ' . count($orders) . '<br/>';
var_dump ($orders);
?>
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_product_link.update-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');
$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductLinkUpdate((object)array('sessionId' => $sessionId->result, 'type' => 'cross_sell', 'productId' => '1', 'linkedProductId' => '2', 'data' => ((object)array(
'position' => '1'
))));

var_dump($result->result);
```

</div>


