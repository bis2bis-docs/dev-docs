

## [Module: Mage_Catalog](#MAPI-ProductLinks-Module:MageCatalog)

The Mage_Catalog module allows you to manage categories and products.

### [Product Links](#MAPI-ProductLinks-ProductLinks)

Allows you to manage links for products, including related, cross-sells,
up-sells, and grouped.

**Resource Name**: catalog_product_link

**Aliases**:

-   product_link

**Methods**:

-   [catalog_product_link.list](catalog_product_link.list.html "catalog_product_link.list") -
    Retrieve products linked to the specified product
-   [catalog_product_link.assign](catalog_product_link.assign.html "catalog_product_link.assign") -
    Link a product to another product
-   [catalog_product_link.update](catalog_product_link.update.html "catalog_product_link.update") -
    Update a product link
-   [catalog_product_link.remove](catalog_product_link.remove.html "catalog_product_link.remove") -
    Remove a product link
-   [catalog_product_link.types](catalog_product_link.types.html "catalog_product_link.types") -
    Retrieve product link types
-   [catalog_product_link.attributes](catalog_product_link.attributes.html "catalog_product_link.attributes") -
    Retrieve product link type attributes

### [Faults](#MAPI-ProductLinks-Faults)

Fault Code Fault Message

---

100 Given invalid link type.
101 Product not exists.
102 Invalid data given. Details in error message.
104 Product link not removed.

### [Examples](#MAPI-ProductLinks-Examples)

### [Example 1. Working with product links](#MAPI-ProductLinks-Example1.Workingwithproductlinks)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

// Get list of related products
var_dump($proxy->call($sessionId, 'product_link.list', array('related', 'Sku')));

// Assign related product
$proxy->call($sessionId, 'product_link.assign', array('related', 'Sku', 'Sku2', array('position'=>0, 'qty'=>56)));

var_dump($proxy->call($sessionId, 'product_link.list', array('related', 'Sku')));

// Update related product
$proxy->call($sessionId, 'product_link.update', array('related', 'Sku', 'Sku2', array('position'=>2)));

var_dump($proxy->call($sessionId, 'product_link.list', array('related', 'Sku')));

// Remove related product
$proxy->call($sessionId, 'product_link.remove', array('related', 'Sku', 'Sku2'));

var_dump($proxy->call($sessionId, 'product_link.list', array('related', 'Sku')));
```

</div>



Create the Magento file system owner
