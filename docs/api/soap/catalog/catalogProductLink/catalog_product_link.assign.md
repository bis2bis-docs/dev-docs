

## [Module: Mage_Catalog](#catalog_product_link.assign-Module:MageCatalog)

## [Resource: catalog_product_link](#catalog_product_link.assign-Resource:catalogproductlink)

**Aliases:**

-   product_link

## [Method:](#catalog_product_link.assign-Method:)

-   catalog_product_link.assign (SOAP V1)
-   catalogProductLinkAssign (SOAP V2)

Allows you to assign a product link (cross_sell, grouped, related, or
up_sell) to another product.

**Aliases:**

-   product_link.assign

**Arguments:**

---

Type Name Description

---

string sessionId Session ID

string\ type\ Type of the link
(cross_sell, grouped,
related, or up_sell)

string\ product\\productId\ Product ID or SKU

string\ linkedProduct\\linkedProductId\ Product ID or SKU for the
link

array data\ Array of
catalogProductLinkEntity

string\ identifierType\ Defines whether the
product ID or SKU is
passed in the \'product\'
parameter

---

**Returns**:

Type Description

---

boolean True if the link is assigned to the product

The **catalogProductLinkEntity** content is as follows:

---

Type Name Description

---

string product_id\ Product ID

string\ type\ Type of the link
(cross_sell, grouped,
related, or up_sell)

string\ set\ Product attribute set

string\ sku\ Product SKU

string\ position\ Position of the product

string\ qty\ Quantity of products

---

### [Examples](#catalog_product_link.assign-Examples)

### [Request Example SOAP V1](#catalog_product_link.assign-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apikey');

$result = $client->call($session, 'catalog_product_link.assign', array('type' => 'related', 'product' => '1', 'linkedProduct' => '4'));
var_dump ($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#catalog_product_link.assign-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductLinkAssign($sessionId, 'related', '1', '4');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_product_link.assign-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');
$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductLinkAssign((object)array('sessionId' => $sessionId->result, 'type' => 'related', 'productId' => '1', 'linkedProductId' => '4'));

var_dump($result->result);
```

</div>


