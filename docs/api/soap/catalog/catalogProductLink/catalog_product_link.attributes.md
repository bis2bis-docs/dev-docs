

## [Module: Mage_Catalog](#catalog_product_link.attributes-Module:MageCatalog)

## [Resource: catalog_product_link](#catalog_product_link.attributes-Resource:catalogproductlink)

**Aliases:**

-   product_link

## [Method:](#catalog_product_link.attributes-Method:)

-   catalog_product_link.attributes (SOAP V1)
-   catalogProductLinkAttributes (SOAP V2)

Allows you to retrieve the product link type attributes.

**Aliases:**

-   product_link.attributes

**Arguments:**

---

Type Name Description

---

string sessionId\ Session ID

string type\ Type of the link
(cross_sell, up_sell,
related, or grouped)

---

**Returns**:

Type Name Description

---

array result Array of catalogProductLinkAttributeEntity

The **catalogProductLinkAttributeEntity** content is as follows:

Type Name Description

---

string code Attribute code
string type Attribute type

### [Examples](#catalog_product_link.attributes-Examples)

### [Request Example SOAP V1](#catalog_product_link.attributes-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_product_link.attributes', 'related');
var_dump ($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#catalog_product_link.attributes-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductLinkAttributes($sessionId, 'related');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_product_link.attributes-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');
$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductLinkAttributes((object)array('sessionId' => $sessionId->result, 'type' => 'related'));

var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#catalog_product_link.attributes-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
    array
      'code' => string 'position' (length=8)
      'type' => string 'int' (length=3)
```

</div>


