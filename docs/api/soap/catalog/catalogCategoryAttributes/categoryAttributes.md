

## [Module: Mage_Catalog](#MAPI-CategoryAttributes-Module:MageCatalog)

The Mage_Catalog module allows you to manage categories and products.

### [Category Attributes](#MAPI-CategoryAttributes-CategoryAttributes)

Allows you to retrieve the list of category attributes and options.

**Resource Name**: catalog_category_attribute

**Aliases**:

-   category_attribute

**Methods**:

-   [catalog_category_attribute.currentStore](catalog_category_attribute.currentStore.html "catalog_category_attribute.currentStore") -
    Set/Get the current store view
-   [catalog_category_attribute.list](catalog_category_attribute.list.html "catalog_category_attribute.list") -
    Retrieve the category attributes
-   [catalog_category_attribute.options](catalog_category_attribute.options.html "catalog_category_attribute.options") -
    Retrieve the attribute options

### [Faults](#MAPI-CategoryAttributes-Faults)

Fault Code Fault Message

---

100 Requested store view not found.
101 Requested attribute not found.

### [Examples](#MAPI-CategoryAttributes-Examples)

### [Example 1. Retrieving attributes and options](#MAPI-CategoryAttributes-Example1.Retrievingattributesandoptions)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');
$attributes = $proxy->call($sessionId, 'category_attribute.list');
foreach ($attributes as &$attribute) {
   if (isset($attribute['type'])
       && ($attribute['type'] == 'select' || $attribute['type'] == 'multiselect')) {
       $attribute['options'] = $proxy->call($sessionId, 'category_attribute.options', $attribute['code']);
   }
}
var_dump($attributes);
```

</div>


