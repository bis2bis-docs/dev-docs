

## [Module: Mage_Catalog](#catalog_category_attribute.list-Module:MageCatalog)

## [Resource: catalog_category_attribute](#catalog_category_attribute.list-Resource:catalogcategoryattribute)

**Aliases:**

-   category_attribute

## [Method:](#catalog_category_attribute.list-Method:)

-   catalog_category_attribute.list (SOAP V1)
-   catalogCategoryAttributeList (SOAP V2)

Allows you to retrieve the list of category attributes.

**Aliases:**

-   category_attribute.list

**Arguments:**

Type Name Description

---

string sessionId Session ID

**Returns**:

Type Name Description

---

array result Array of catalogAttributeEntity

The **catalogAttributeEntity** content is as follows:

---

Type Name Description

---

int attribute_id\ Attribute ID

string code\ Attribute code

string\ type\ Attribute type

string\ required\ Defines whether the
attribute is required

string\ scope\ Attribute scope:
global, website, or
store

---

### [Examples](#catalog_category_attribute.list-Examples)

### [Request Example SOAP V1](#catalog_category_attribute.list-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_category_attribute.list',);
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#catalog_category_attribute.list-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogCategoryAttributeList($sessionId);
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_category_attribute.list-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$client = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$session = $client->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $client->catalogCategoryAttributeList((object)array('sessionId' => $session->result));

var_dump ($result);
```

</div>



### [Response Example SOAP V1](#catalog_category_attribute.list-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
    array
      'attribute_id' => null
      'code' => string 'parent_id' (length=9)
      'type' => null
      'required' => null
      'scope' => string 'global' (length=6)
  1 =>
    array
      'attribute_id' => null
      'code' => string 'increment_id' (length=12)
      'type' => null
      'required' => null
      'scope' => string 'global' (length=6)
  2 =>
    array
      'attribute_id' => null
      'code' => string 'updated_at' (length=10)
      'type' => null
      'required' => null
      'scope' => string 'global' (length=6)
```

</div>


