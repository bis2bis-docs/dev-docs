

## [Module: Mage_Catalog](#catalog_category_attribute.options-Module:MageCatalog)

## [Resource: catalog_category_attribute](#catalog_category_attribute.options-Resource:catalogcategoryattribute)

**Aliases:**

-   category_attribute

## [Method:](#catalog_category_attribute.options-Method:)

-   catalog_category_attribute.options (SOAP V1)
-   catalogCategoryAttributeOptions (SOAP V2)

Allows you to retrieve the attribute options.

**Aliases:**

-   category_attribute.options

**Arguments:**

Type Name Description

---

string sessionId Session ID
string attributeId Attribute ID or code
string storeView Store view ID or code

**Returns**:

Type Name Description

---

array result Array of catalogAttributeOptionEntity

The **catalogAttributeOptionEntity** content is as follows:

Type Name Description

---

string label Option label
string value Option value

### [Examples](#catalog_category_attribute.options-Examples)

### [Request Example SOAP V1](#catalog_category_attribute.options-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_category_attribute.options', '65');
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#catalog_category_attribute.options-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogCategoryAttributeOptions($sessionId, '65');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_category_attribute.options-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$client = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$session = $client->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $client->catalogCategoryAttributeOptions((object)array('sessionId' => $session->result, 'attributeId' => '65'));

var_dump ($result);
```

</div>



### [Response Example SOAP V1](#catalog_category_attribute.options-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
    array
      'label' => string 'Yes' (length=3)
      'value' => int 1
  1 =>
    array
      'label' => string 'No' (length=2)
      'value' => int 0
```

</div>


