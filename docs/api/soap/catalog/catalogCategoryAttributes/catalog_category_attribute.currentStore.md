

## [Module: Mage_Catalog](#catalog_category_attribute.currentStore-Module:MageCatalog)

## [Resource: catalog_category_attribute](#catalog_category_attribute.currentStore-Resource:catalogcategoryattribute)

**Aliases:**

-   category_attribute

## [Method:](#catalog_category_attribute.currentStore-Method:)

-   catalog_category_attribute.currentStore (SOAP V1)
-   catalogCategoryAttributeCurrentStore (SOAP V2)

Allows you to set/get the current store view.

**Aliases:**

-   category_attribute.currentStore

**Arguments:**

Type Name Description

---

string sessionId Session ID
string storeView Store view ID or code

**Returns**:

Type Name Description

---

int storeView Store view ID

### [Examples](#catalog_category_attribute.currentStore-Examples)

### [Request Example SOAP V1](#catalog_category_attribute.currentStore-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_category_attribute.currentStore', 'english');
var_dump ($result);
```

</div>



### [Request Example SOAP V2](#catalog_category_attribute.currentStore-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogCategoryAttributeCurrentStore($sessionId, 'english');
var_dump($result);
```

</div>


