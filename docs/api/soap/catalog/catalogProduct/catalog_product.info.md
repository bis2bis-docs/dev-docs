

## [Module: Mage_Catalog](#catalog_product.info-Module:MageCatalog)

## [Resource: catalog_product](#catalog_product.info-Resource:catalogproduct)

**Aliases:**

-   product

## [Method:](#catalog_product.info-Method:)

-   catalog_product.info (SOAP V1)
-   catalogProductInfo (SOAP V2)

Allows you to retrieve information about the required product.

**Aliases:**

-   product.info

**Arguments:**

---

Type Name Description

---

string sessionId Session ID

string productId Product ID or SKU

string storeView Store view ID or code (optional)

array attributes Array of
catalogProductRequestAttributes
(optional)

string identifierType Defines whether the product ID or
SKU value is passed in the
\"productId\" parameter.
(optional)

---

**Returns**:

Type Name Description

---

array info Array of catalogProductReturnEntity

The **catalogProductRequestAttributes** content is as follows:

---

Type Name Description

---

ArrayOfString\ attributes\ Array of attributes

ArrayOfString\ additional_attributes\ Array of additional
attributes

---

The **catalogProductReturnEntity** content is as follows:

---

Type Name Description

---

string product_id\ Product ID

string\ sku\ Product SKU

string\ set\ Product set

string\ type\ Product type

ArrayOfString categories\ Array of categories

ArrayOfString websites\ Array of websites

string\ created_at\ Date when the product was
created

string\ updated_at\ Date when the product was last
updated

string\ type_id\ Type ID

string\ name\ Product name

string\ description\ Product description

string\ short_description\ Short description for a product

string\ weight\ Product weight

string\ status\ Status of a product

string\ url_key\ Relative URL path that can be
entered in place of a target
path

string\ url_path\ URL path

string\ visibility\ Product visibility on the
frontend

ArrayOfString\ category_ids\ Array of category IDs

ArrayOfString\ website_ids\ Array of website IDs

string\ has_options\ Defines whether the product has
options

string\ gift_message_available\ Defines whether the gift
message is available for the
product

string\ price\ Product price

string\ special_price\ Product special price

string\ special_from_date\ Date starting from which the
special price is applied to the
product

string\ special_to_date\ Date till which the special
price is applied to the product

string\ tax_class_id\ Tax class ID

array tier_price\ Array of
catalogProductTierPriceEntity

string\ meta_title\ Mate title

string\ meta_keyword\ Meta keyword

string\ meta_description\ Meta description

string\ custom_design\ Custom design

string\ custom_layout_update\ Custom layout update

string\ options_container\ Options container

associativeArray additional_attributes\ Array of additional attributes

string\ enable_googlecheckout\ Defines whether Google Checkout
is applied to the product

---

The **catalogProductTierPriceEntity** content is as follows:

---

Type Name Description

---

string customer_group_id\ ID of the customer
group

string website\ Website

int qty\ Quantity to which the
price will be applied

double price\ Price that each item
will cost

---

### [Examples](#catalog_product.info-Examples)

### [Request Example SOAP V1](#catalog_product.info-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_product.info', '4');
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#catalog_product.info-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductInfo($sessionId, '4');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_product.info-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductInfo((object)array('sessionId' => $sessionId->result, 'productId' => '4'));
var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#catalog_product.info-ResponseExampleSOAPV1)

<div>

```
array
  'product_id' => string '4' (length=1)
  'sku' => string 'canon_powershot' (length=15)
  'set' => string '4' (length=1)
  'type' => string 'simple' (length=6)
  'categories' =>
    array
      0 => string '3' (length=1)
      1 => string '5' (length=1)
  'websites' =>
    array
      0 => string '2' (length=1)
  'type_id' => string 'simple' (length=6)
  'name' => string 'Canon PowerShot A630 8MP Digital Camera with 4x Optical Zoom' (length=60)
  'description' => string 'Replacing the highly popular PowerShot A620, the PowerShot A630 features a rotating 2.5-inch vari-angle LCD, 4x optical zoom lens, and a vast array of creative shooting modes.

The PowerShot A630 packs a vast array of advanced features into a remarkably compact space' (length=267)
  'short_description' => string 'Replacing the highly popular PowerShot A620, the PowerShot A630 features a rotating 2.5-inch vari-angle LCD, 4x optical zoom lens, and a vast array of creative shooting modes.' (length=175)
  'weight' => string '1.0000' (length=6)
  'old_id' => null
  'news_from_date' => null
  'news_to_date' => null
  'status' => string '1' (length=1)
  'url_key' => string 'canon-powershot-a630-8mp-digital-camera-with-4x-optical-zoom' (length=60)
  'url_path' => string 'canon-powershot-a630-8mp-digital-camera-with-4x-optical-zoom.html' (length=65)
  'visibility' => string '4' (length=1)
  'category_ids' =>
    array
      0 => string '3' (length=1)
      1 => string '5' (length=1)
  'required_options' => string '0' (length=1)
  'has_options' => string '0' (length=1)
  'image_label' => null
  'small_image_label' => null
  'thumbnail_label' => null
  'created_at' => string '2012-03-29 12:47:56' (length=19)
  'updated_at' => string '2012-03-29 12:47:56' (length=19)
  'country_of_manufacture' => null
  'price' => string '329.9900' (length=8)
  'group_price' =>
    array
      empty
  'special_price' => null
  'special_from_date' => null
  'special_to_date' => null
  'tier_price' =>
    array
      empty
  'minimal_price' => null
  'msrp_enabled' => string '2' (length=1)
  'msrp_display_actual_price_type' => string '4' (length=1)
  'msrp' => null
  'enable_googlecheckout' => string '1' (length=1)
  'tax_class_id' => string '2' (length=1)
  'meta_title' => null
  'meta_keyword' => null
  'meta_description' => null
  'is_recurring' => string '0' (length=1)
  'recurring_profile' => null
  'custom_design' => null
  'custom_design_from' => null
  'custom_design_to' => null
  'custom_layout_update' => null
  'page_layout' => null
  'options_container' => string 'container2' (length=10)
  'gift_message_available' => null
```

</div>


