

## [Module: Mage_Catalog](#catalog_product.getSpecialPrice-Module:MageCatalog)

## [Resource: catalog_product](#catalog_product.getSpecialPrice-Resource:catalogproduct)

**Aliases:**

-   product

## [Method:](#catalog_product.getSpecialPrice-Method:)

-   catalog_product.getSpecialPrice (SOAP V1)
-   catalogProductGetSpecialPrice (SOAP V2)

Allows you to get the product special price data.

**Aliases:**

-   product.getSpecialPrice

**Arguments:**

---

Type Name Description

---

string sessionId Session ID

string\ product\\productId\ Product ID or SKU

string\ storeView\ Store view ID or code

string identifierType Defines whether the
product ID or SKU is
passed in the
\'product\' parameter

---

**Returns**:

Type Name Description

---

array result Array of catalogProductReturnEntity

The **catalogProductReturnEntity** content is as follows:

---

Type Name Description

---

string\ special_price\ Product special price

string\ special_from_date\ Date starting from
which the special price
is applied to the
product

string\ special_to_date\ Date till which the
special price is
applied to the product

---

### [Examples](#catalog_product.getSpecialPrice-Examples)

### [Request Example SOAP V1](#catalog_product.getSpecialPrice-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_product.getSpecialPrice', '1');
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#catalog_product.getSpecialPrice-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductGetSpecialPrice($sessionId, '1');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_product.getSpecialPrice-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductGetSpecialPrice((object)array('sessionId' => $sessionId->result, 'productId' => '1'));
var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#catalog_product.getSpecialPrice-ResponseExampleSOAPV1)

<div>

```
array
  'special_price' => string '139.9900' (length=8)
  'special_from_date' => string '2012-03-28 00:00:00' (length=19)
  'special_to_date' => string '2012-03-31 00:00:00' (length=19)
```

</div>


