

## [Module: Mage_Catalog](#catalog_product.update-Module:MageCatalog)

## [Resource: catalog_product](#catalog_product.update-Resource:catalogproduct)

**Aliases:**

-   product

## [Method:](#catalog_product.update-Method:)

-   catalog_product.update (SOAP V1)
-   catalogProductUpdate (SOAP V2)

Allows you to update the required product. Note that you should specify
only those parameters which you want to be updated.

**Aliases:**

-   product.update

**Note:**

Although the API accepts up to four digits of precision for all price
arguments, Magento strongly recommends you pass in two digits to reduce
inaccuracy in the tax calculation process. (That is, use a price like
12.35 and not 12.3487).

**Arguments:**

---

Type Name Description

---

string sessionId Session ID

string product\\productId\ Product ID

array productData\ Array of
catalogProductCreateEntity

string storeView\ Store view ID or code
(optional)

string identifierType\ Defines whether the product
ID or SKU is passed in the
\'product\' parameter

---

**Returns**:

Type Description

---

boolean True if the product is updated

The **catalogProductCreateEntity** content is as follows:

---

Type Name Description

---

ArrayOfString categories\ Array of categories

ArrayOfString\ websites\ Array of websites

string name\ Product name

string\ description\ Product description

string\ short_description\ Product short description

string\ weight\ Product weight

string\ status\ Product status

string\ url_key\ A relative URL path that can be entered in
place of the target path

string\ url_path\ URL path

string\ visibility\ Product visibility on the frontend

ArrayOfString\ category_ids\ Array of category IDs

ArrayOfString\ website_ids\ Array of website IDs

string\ has_options\ Defines whether the product has options

string\ gift_message_available\ Defines whether the gift message is
available for the product

string\ price\ Product price

string\ special_price\ Product special price

string\ special_from_date\ Date starting from which the special price
will be applied to the product

string\ special_to_date\ Date till which the special price will be
applied to the product

string\ tax_class_id\ Tax class ID

array tier_price\ Array of catalogProductTierPriceEntity

string\ meta_title\ Meta title

string\ meta_keyword\ Meta keyword

string\ meta_description\ Meta description

string\ custom_design\ Custom design

string\ custom_layout_update\ Custom layout update

string\ options_container\ Options container

associativeArray\ additional_attributes\ Array of
catalogProductAdditionalAttributesEntity

array stock_data\ Array of
catalogInventoryStockItemUpdateEntity

---

**Notes**: The \"websites\" and \"website_ids\" or \"categories\" and
\"category_ids\" parameters are interchangeable. In other words, you
can specify an array of website IDs (int) and then you don\'t need to
specify the array of website codes (string) and vice versa.

The **catalogProductTierPriceEntity** content is as follows:

---

Type Name Description

---

string customer_group_id\ Customer group ID

string website\ Website

int qty\ Quantity of items to
which the price will be
applied

double price\ Price that each item
will cost

---

The **catalogInventoryStockItemUpdateEntity** content is as follows:

---

Type Name Description

---

string qty\ Quantity of items

int is_in_stock\ Defines whether the
item is in stock

int\ manage_stock\ Manage stock

int\ use_config_manage_stock\ Use config manage stock

int\ min_qty\ Minimum quantity for
items to be in stock

int\ use_config_min_qty\ Use config settings
flag (value defined in
the Inventory System
Configuration)

int\ min_sale_qty\ Minimum quantity
allowed in the shopping
cart

int\ use_config_min_sale_qty\ Use config settings
flag

int\ max_sale_qty\ Maximum quantity
allowed in the shopping
cart

int\ use_config_max_sale_qty\ Use config settings
flag

int\ is_qty_decimal\ Defines whether the
quantity is decimal

int\ backorders\ Backorders status

int\ use_config_backorders\ Use config settings
flag (for backorders)

int\ notify_stock_qty\ Stock quantity below
which a notification
will appear

int\ use_config_notify_stock_qty\ Use config settings
flag (for stock
quantity)

---

The **catalogProductAdditionalAttributesEntity** content is as follows:

Type Name

---

associativeMultiArray multi_data
associativeArray single_data

Single Data: array of attributes with only single value
Multi Data: array of attributes which could contain several values

**Faults:**

Fault Code Fault Message

---

100 Requested store view not found.
101 Product not exists.
102 Invalid data given. Details in error message.

### [Examples](#catalog_product.update-Examples)

### [Request Example SOAP V1](#catalog_product.update-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If some stuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');
$result = $client->call($session, 'catalog_product.update', array('product_sku', array(
    'categories' => array(2),
    'websites' => array(1),
    'name' => 'Product name new 2',
    'description' => 'Product description',
    'short_description' => 'Product short description',
    'weight' => '10',
    'status' => '1',
    'url_key' => 'product-url-key',
    'url_path' => 'product-url-path',
    'visibility' => '4',
    'price' => '100',
    'tax_class_id' => 1,
    'meta_title' => 'Product meta title',
    'meta_keyword' => 'Product meta keyword',
    'meta_description' => 'Product meta description'
)));

var_dump ($result);
```

</div>



### [Request Example SOAP V2](#catalog_product.update-RequestExampleSOAPV2)

<div>

```
$client = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

// If some stuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');
$result = $client->catalogProductUpdate($session, 'product_sku', array(
    'categories' => array(2),
    'websites' => array(1),
    'name' => 'Product name new',
    'description' => 'Product description',
    'short_description' => 'Product short description',
    'weight' => '10',
    'status' => '1',
    'url_key' => 'product-url-key',
    'url_path' => 'product-url-path',
    'visibility' => '4',
    'price' => '100',
    'tax_class_id' => 1,
    'meta_title' => 'Product meta title',
    'meta_keyword' => 'Product meta keyword',
    'meta_description' => 'Product meta description'
));

var_dump ($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_product.update-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductUpdate((object)array('sessionId' => $sessionId->result, 'productId' => '1',
'productData' => ((object)array(
    'name' => 'Product name updated',
    'status' => '1',
))));

var_dump($result->result);
```

</div>


