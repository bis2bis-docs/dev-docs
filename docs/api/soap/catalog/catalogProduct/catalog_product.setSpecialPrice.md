

## [Module: Mage_Catalog](#catalog_product.setSpecialPrice-Module:MageCatalog)

## [Resource: catalog_product](#catalog_product.setSpecialPrice-Resource:catalogproduct)

**Aliases:**

-   product

## [Method:](#catalog_product.setSpecialPrice-Method:)

-   catalog_product.setSpecialPrice (SOAP V1)
-   catalogProductSetSpecialPrice (SOAP V2)

Allows you to set the product special price.

**Aliases:**

-   product.setSpecialPrice

**Note:**

Although the API accepts up to four digits of precision for all price
arguments, Magento strongly recommends you pass in two digits to reduce
inaccuracy in the tax calculation process. (That is, use a price like
12.35 and not 12.3487).

**Arguments:**

---

Type Name Description

---

string sessionId\ Session ID

string\ productId\ Product ID or SKU

string\ specialPrice\ Product special price

string\ fromDate\ Date starting from
which special price
will be applied

string\ toDate\ Date till which special
price will be applied

string\ storeView\ Store view ID or code
(optional)

string productIdentifierType Defines whether the
product ID or SKU is
passed in the
\'productId\' parameter
(optional)

---

**Returns**:

Type Name Description

---

boolean/int result True (1) if the special price is set for the product

### [Examples](#catalog_product.setSpecialPrice-Examples)

### [Request Example SOAP V1](#catalog_product.setSpecialPrice-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_product.setSpecialPrice', array('product' => 'testproduct', 'specialPrice' => '77.5', 'fromDate' => '2012-03-29 12:30:51', 'toDate' => '2012-04-29 12:30:51'));
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#catalog_product.setSpecialPrice-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductSetSpecialPrice($sessionId, '2', '77.5', '2012-03-29 12:30:51', '2012-04-29 12:30:51');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_product.setSpecialPrice-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductSetSpecialPrice((object)array('sessionId' => $sessionId->result, 'productId' => '2', 'specialPrice' => '77.5', 'fromDate' => '2012-03-29 12:30:51', 'toDate' => '2012-04-29 12:30:51'));
var_dump($result->result);
```

</div>


