

## [Module: Mage_Catalog](#catalog_product.currentStore-Module:MageCatalog)

## [Resource: catalog_product](#catalog_product.currentStore-Resource:catalogproduct)

**Aliases:**

-   product

## [Method:](#catalog_product.currentStore-Method:)

-   catalog_product.currentStore (SOAP V1)
-   catalogProductCurrentStore (SOAP V2)

Allows you to set/get the current store view.

**Aliases:**

-   product.currentStore

**Arguments:**

Type Name Description

---

string sessionId Session ID
string storeView Store view ID or code (optional)

**Returns**:

Type Name Description

---

int/string storeView Store view ID

### [Examples](#catalog_product.currentStore-Examples)

### [Request Example SOAP V1](#catalog_product.currentStore-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_product.currentStore', 'english');
var_dump ($result);
```

</div>



### [Request Example SOAP V2](#catalog_product.currentStore-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductCurrentStore($sessionId, 'english');
var_dump($result);
```

</div>



Create the Magento file system owner
