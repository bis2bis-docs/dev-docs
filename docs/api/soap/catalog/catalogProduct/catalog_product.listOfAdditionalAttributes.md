\-\-- layout: m1x_soap title: List of Additional Product Attributes
\-\--

### [Module: Product API](#product.listOfAdditionalAttributes-Module:ProductAPI)

## [Resource: product](#product.listOfAdditionalAttributes-Resource:product)

## [Method:](#product.listOfAdditionalAttributes-Method:)

-   product.listOfAdditionalAttributes (SOAP V1)
-   catalogProductListOfAdditionalAttributes (SOAP V2)

Get the list of additional attributes. Additional attributes are
attributes that are not in the default set of attributes.

**_Arguments:_**

Type Name Description

---

string sessionId Session ID
string productType Product type (e.g., simple)
string attributeSetId Attribute set ID

**_Returns:_**

---

Type Name Description

---

int attribute_id Attribute ID

string code Attribute code

string\ type Attribute type (e.g.,
text, select, date,
etc.)

string\ required Defines whether the
attribute is required

string\ scope Attribute scope
(global, website, or
store)

---

**_Faults:_**

Fault Code Fault Message

---

104 Product type is not in allowed types.
105 Product attribute set is not existed
106 Product attribute set is not belong catalog product entity type

### [Examples](#product.listOfAdditionalAttributes-Examples)

### [Request Example SOAP V1](#product.listOfAdditionalAttributes-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$listAttributes = $proxy->call(
    $sessionId,
    'product.listOfAdditionalAttributes',
    array(
        'simple',
        13
    )
);
```

</div>



### [Request Example SOAP V2](#product.listOfAdditionalAttributes-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductListOfAdditionalAttributes($sessionId, 'simple', '13');
var_dump($result);
```

</div>



### [Response Example SOAP V1](#product.listOfAdditionalAttributes-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
    array
      'attribute_id' => string '89' (length=2)
      'code' => string 'old_id' (length=6)
      'type' => string 'text' (length=4)
      'required' => string '0' (length=1)
      'scope' => string 'global' (length=6)
  1 =>
    array
      'attribute_id' => string '93' (length=2)
      'code' => string 'news_from_date' (length=14)
      'type' => string 'date' (length=4)
      'required' => string '0' (length=1)
      'scope' => string 'website' (length=7)
  2 =>
    array
      ...
```

</div>


