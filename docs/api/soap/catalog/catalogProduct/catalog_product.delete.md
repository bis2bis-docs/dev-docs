

## [Module: Mage_Catalog](#catalog_product.delete-Module:MageCatalog)

## [Resource: catalog_product](#catalog_product.delete-Resource:catalogproduct)

**Aliases:**

-   product

## [Method:](#catalog_product.delete-Method:)

-   catalog_product.delete (SOAP V1)
-   catalogProductDelete (SOAP V2)

Allows you to delete the required product.

**Aliases:**

-   product.delete

**Arguments:**

---

Type Name Description

---

string sessionId\ Session ID

string product\\productId\ Product ID or SKU

string identifierType Defines whether the
product ID or SKU is
passed in the
\'product\' parameter

---

**Returns**:

Type Name Description

---

boolean/int result True (1) if the product is deleted

### [Examples](#catalog_product.delete-Examples)

### [Request Example SOAP V1](#catalog_product.delete-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_product.delete', '6');
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#catalog_product.delete-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductDelete($sessionId, '6');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_product.delete-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductDelete((object)array('sessionId' => $sessionId->result, 'productId' => '21'));
var_dump($result->result);
```

</div>


