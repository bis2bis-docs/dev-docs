

## [Module: Mage_Catalog](#catalog_product.list-Module:MageCatalog)

## [Resource: catalog_product](#catalog_product.list-Resource:catalogproduct)

**Aliases:**

-   product

## [Method:](#catalog_product.list-Method:)

-   catalog_product.list (SOAP V1)
-   catalogProductList (SOAP V2)

Allows you to retrieve the list of products.

**Aliases:**

-   product.list

**Arguments:**

Type Name Description

---

string sessionId Session ID
array filters Array of filters by attributes (optional)
string storeView Store view ID or code (optional)

**Returns**:

Type Name Description

---

array storeView Array of catalogProductEntity

The **catalogProductEntity** content is as follows:

---

Type Name Description

---

string product_id\ Product ID

string\ sku\ Product SKU

string\ name\ Product name

string\ set\ Product attribute set

string\ type\ Type of the product

ArrayOfString category_ids\ Array of category IDs

ArrayOfString\ website_ids Array of website IDs

---

### [Examples](#catalog_product.list-Examples)

### [Request Example SOAP V1](#catalog_product.list-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_product.list');
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



## [Request Example SOAP V2 (List of All Products)](#catalog_product.list-RequestExampleSOAPV2%28ListofAllProducts%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductList($sessionId);
var_dump($result);
```

</div>



## [Request Example SOAP V2 (Complex Filter)](#catalog_product.list-RequestExampleSOAPV2%28ComplexFilter%29)

<div>

```
$client = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

// If some stuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');
$complexFilter = array(
    'complex_filter' => array(
        array(
            'key' => 'type',
            'value' => array('key' => 'in', 'value' => 'simple,configurable')
        )
    )
);
$result = $client->catalogProductList($session, $complexFilter);

var_dump ($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_product.list-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductList((object)array('sessionId' => $sessionId->result, 'filters' => null));
var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#catalog_product.list-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
    array
      'product_id' => string '1' (length=1)
      'sku' => string 'n2610' (length=5)
      'name' => string 'Nokia 2610 Phone' (length=16)
      'set' => string '4' (length=1)
      'type' => string 'simple' (length=6)
      'category_ids' =>
        array
          0 => string '4' (length=1)
  1 =>
    array
      'product_id' => string '2' (length=1)
      'sku' => string 'b8100' (length=5)
      'name' => string 'BlackBerry 8100 Pearl' (length=21)
      'set' => string '4' (length=1)
      'type' => string 'simple' (length=6)
      'category_ids' =>
        array
          0 => string '4' (length=1)
```

</div>


