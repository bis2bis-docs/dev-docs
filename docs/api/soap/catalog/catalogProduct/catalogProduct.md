

## [Module: Mage_Catalog](#MAPI-Product-Module:MageCatalog)

The Mage_Catalog module allows you to manage categories and products.

### [Product](#MAPI-Product-Product)

Allows you to manage products.

**Resource Name**: catalog_product

**Aliases**:

-   product

**Methods**:

-   [catalog_product.currentStore](catalog_product.currentStore.html "catalog_product.currentStore") -
    Set/Get the current store view
-   [catalog_product.list](catalog_product.list.html "catalog_product.list") -
    Retrieve the list of products using filters
-   [catalog_product.info](catalog_product.info.html "catalog_product.info") -
    Retrieve information about the required product
-   [catalog_product.create](catalog_product.create.html "catalog_product.create") -
    Create a new product
-   [catalog_product.update](catalog_product.update.html "catalog_product.update") -
    Update a required product
-   [catalog_product.setSpecialPrice](catalog_product.setSpecialPrice.html "catalog_product.setSpecialPrice") -
    Set special price for a product
-   [catalog_product.getSpecialPrice](catalog_product.getSpecialPrice.html "catalog_product.getSpecialPrice") -
    Get special price for a product
-   [catalog_product.delete](catalog_product.delete.html "catalog_product.delete") -
    Delete a required product
-   [catalog_product.listOfAdditionalAttributes](catalog_product.listOfAdditionalAttributes.html "catalog_product.listOfAdditionalAttributes") -
    Get the list of additional attributes

### [Faults](#MAPI-Product-Faults)

Fault Code Fault Message

---

100 Requested store view not found.
101 Product not exists.
102 Invalid data given. Details in error message.
103 Product not deleted. Details in error message.

### [Examples](#MAPI-Product-Examples)

## [Example 1](#MAPI-Product-Example1.Retrievingtheproductlist). Retrieving the ﻿product list

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$filters = array(
    'sku' => array('like'=>'zol%')
);

$products = $proxy->call($sessionId, 'product.list', array($filters));

var_dump($products);
```

</div>



## [Example 2](#MAPI-Product-Example2.Creating,viewing,updating,anddeletingaproduct). Creating, viewing, updating, and deleting a product

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$attributeSets = $proxy->call($sessionId, 'product_attribute_set.list');
$set = current($attributeSets);


$newProductData = array(
    'name'              => 'name of product',
     // websites - Array of website ids to which you want to assign a new product
    'websites'          => array(1), // array(1,2,3,...)
    'short_description' => 'short description',
    'description'       => 'description',
    'status'            => 1,
    'weight'            => 0,
    'tax_class_id'      => 1,
    'categories'    => array(3),    //3 is the category id
    'price'             => 12.05
);

// Create new product
$proxy->call($sessionId, 'product.create', array('simple', $set['set_id'], 'sku_of_product', $newProductData));
$proxy->call($sessionId, 'product_stock.update', array('sku_of_product', array('qty'=>50, 'is_in_stock'=>1)));

// Get info of created product
var_dump($proxy->call($sessionId, 'product.info', 'sku_of_product'));

// Update product name on german store view
$proxy->call($sessionId, 'product.update', array('sku_of_product', array('name'=>'new name of product'), 'german'));

// Get info for default values
var_dump($proxy->call($sessionId, 'product.info', 'sku_of_product'));
// Get info for german store view

var_dump($proxy->call($sessionId, 'product.info', array('sku_of_product', 'german')));

// Delete product
$proxy->call($sessionId, 'product.delete', 'sku_of_product');

try {
    // Ensure that product deleted
    var_dump($proxy->call($sessionId, 'product.info', 'sku_of_product'));
} catch (SoapFault $e) {
    echo "Product already deleted";
}
```

</div>



Create the Magento file system owner
