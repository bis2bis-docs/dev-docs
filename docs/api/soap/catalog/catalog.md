

## [Module: Mage_Catalog](#Mage_Catalog-Module:MageCatalog)

The Mage_Catalog module allows you to manage categories and products.

### [Category](#Mage_Catalog-Category)

Allows you to manage categories and how products are assigned to
categories.

**Resource Name**: catalog_category

**Aliases**:

-   category

**Methods**:

-   [catalog_category.currentStore](catalogCategory/catalog_category.currentStore.html "catalog_category.currentStore") -
    Set/Get the current store view
-   [catalog_category.tree](catalogCategory/catalog_category.tree.html "catalog_category.tree") -
    Retrieve the hierarchical category tree
-   [catalog_category.level](catalogCategory/catalog_category.level.html "catalog_category.level") -
    Retrieve one level of categories by a website, store view, or parent
    category
-   [catalog_category.info](catalogCategory/catalog_category.info.html "catalog_category.info") -
    Retrieve the category data
-   [catalog_category.create](catalogCategory/catalog_category.create.html "catalog_category.create") -
    Create a new category
-   [catalog_category.update](catalogCategory/catalog_category.update.html "catalog_category.update") -
    Update a category
-   [catalog_category.move](catalogCategory/catalog_category.move.html "catalog_category.move") -
    Move a category in its tree
-   [catalog_category.delete](catalogCategory/catalog_category.delete.html "catalog_category.delete") -
    Delete a category
-   [catalog_category.assignedProducts](catalogCategory/catalog_category.assignedProducts.html "catalog_category.assignedProducts") -
    Retrieve a list of products assigned to a category
-   [catalog_category.assignProduct](catalogCategory/catalog_category.assignProduct.html "catalog_category.assignProduct") -
    Assign product to a category
-   [catalog_category.updateProduct](catalogCategory/catalog_category.updateProduct.html "catalog_category.updateProduct") -
    Update an assigned product
-   [catalog_category.removeProduct](catalogCategory/catalog_category.removeProduct.html "catalog_category.removeProduct") -
    Remove a product assignment

### [Category Attributes](#Mage_Catalog-CategoryAttributes)

Allows you to retrieve the list of category attributes and options.

**Resource Name**: catalog_category_attribute

**Aliases**:

-   category_attribute

**Methods**:

-   [catalog_category_attribute.currentStore](catalogCategoryAttributes/catalog_category_attribute.currentStore.html "catalog_category_attribute.currentStore") -
    Set/Get the current store view
-   [catalog_category_attribute.list](catalogCategoryAttributes/catalog_category_attribute.list.html "catalog_category_attribute.list") -
    Retrieve the category attributes
-   [catalog_category_attribute.options](catalogCategoryAttributes/catalog_category_attribute.options.html "catalog_category_attribute.options") -
    Retrieve the attribute options

### [Product](#Mage_Catalog-Product)

Allows you to manage products.

**Resource Name**: catalog_product

**Aliases**:

-   product

**Methods**:

-   [catalog_product.currentStore](catalogProduct/catalog_product.currentStore.html "catalog_product.currentStore") -
    Set/Get the current store view
-   [catalog_product.list](catalogProduct/catalog_product.list.html "catalog_product.list") -
    Retrieve the list of products using filters
-   [catalog_product.info](catalogProduct/catalog_product.info.html "catalog_product.info") -
    Retrieve information about the required product
-   [catalog_product.create](catalogProduct/catalog_product.create.html "catalog_product.create") -
    Create a new product
-   [catalog_product.update](catalogProduct/catalog_product.update.html "catalog_product.update") -
    Update a required product
-   [catalog_product.setSpecialPrice](catalogProduct/catalog_product.setSpecialPrice.html "catalog_product.setSpecialPrice") -
    Set special price for a product
-   [catalog_product.getSpecialPrice](catalogProduct/catalog_product.getSpecialPrice.html "catalog_product.getSpecialPrice") -
    Get special price for a product
-   [catalog_product.delete](catalogProduct/catalog_product.delete.html "catalog_product.delete") -
    Delete a required product
-   [catalog_product.listOfAdditionalAttributes](catalogProduct/catalog_product.listOfAdditionalAttributes.html "catalog_product.listOfAdditionalAttributes") -
    Get the list of additional attributes

### [Product Attributes](#Mage_Catalog-ProductAttributes)

Allows you to retrieve product attributes and options.

**Resource Name**: catalog_product_attribute

**Aliases**:

-   product_attribute

**Methods**:

-   [product_attribute.currentStore](catalogProductAttribute/product_attribute.currentStore.html "product_attribute.currentStore") -
    Set/Get the current store view
-   [product_attribute.list](catalogProductAttribute/product_attribute.list.html "product_attribute.list") -
    Retrieve the attribute list
-   [product_attribute.options](catalogProductAttribute/product_attribute.options.html "product_attribute.options") -
    Retrieve the attribute options
-   [product_attribute.addOption](catalogProductAttribute/product_attribute.addOption.html "product_attribute.addOption") -
    Add a new option for attributes with selectable fields
-   [product_attribute.create](catalogProductAttribute/product_attribute.create.html "product_attribute.create") -
    Create a new attribute
-   [product_attribute.info](catalogProductAttribute/product_attribute.info.html "product_attribute.info") -
    Get full information about an attribute with the list of options
-   [product_attribute.remove](catalogProductAttribute/product_attribute.remove.html "product_attribute.remove") -
    Remove the required attribute
-   [product_attribute.removeOption](catalogProductAttribute/product_attribute.removeOption.html "product_attribute.removeOption") -
    Remove an option for attributes with selectable fields
-   [product_attribute.types](catalogProductAttribute/product_attribute.types.html "product_attribute.types") -
    Get the list of possible attribute types
-   [product_attribute.update](catalogProductAttribute/product_attribute.update.html "product_attribute.update") -
    Update the required attribute

### [Product Attribute Sets](#Mage_Catalog-ProductAttributeSets)

Allows you to retrieve product attribute sets.

**Resource Name**: catalog_product_attribute_set

**Aliases**:

-   product_attribute_set

**Methods**:

-   [product_attribute_set.list](catalogProductAttributeSet/product_attribute_set.list.html "product_attribute_set.list") -
    Retrieve the list of product attribute sets
-   [product_attribute_set.attributeAdd](catalogProductAttributeSet/product_attribute_set.attributeAdd.html "product_attribute_set.attributeAdd") -
    Add an attribute to the attribute set
-   [product_attribute_set.attributeRemove](catalogProductAttributeSet/product_attribute_set.attributeRemove.html "product_attribute_set.attributeRemove") -
    Remove an attribute from an attribute set
-   [product_attribute_set.create](catalogProductAttributeSet/product_attribute_set.create.html "product_attribute_set.create") -
    Create a new attribute set
-   [product_attribute_set.groupAdd](catalogProductAttributeSet/product_attribute_set.groupAdd.html "product_attribute_set.groupAdd") -
    Add a new group for attributes in the attribute set
-   [product_attribute_set.groupRemove](catalogProductAttributeSet/product_attribute_set.groupRemove.html "product_attribute_set.groupRemove") -
    Remove a group of attributes from an attribute set
-   [product_attribute_set.groupRename](catalogProductAttributeSet/product_attribute_set.groupRename.html "product_attribute_set.groupRename") -
    Rename a group of attributes in an attribute set
-   [product_attribute_set.remove](catalogProductAttributeSet/product_attribute_set.remove.html "product_attribute_set.remove") -
    Remove an attribute set

### [Product Types](#Mage_Catalog-ProductTypes)

Allows you to retrieve product types.

**Resource Name**: catalog_product_type

**Aliases**:

-   product_type

**Methods**:

-   [catalog_product_type.list](catalogProductTypes/catalog_product_type.list.html "catalog_product_type.list") -
    Retrieve the list of product types

### [Product Images](#Mage_Catalog-ProductImages)

Allows you to manage product images.

**Resource Name**: catalog_product_attribute_media

**Aliases**:

-   product_attribute_media
-   product_media

**Methods**:

-   [catalog_product_attribute_media.currentStore](catalogProductAttributeMedia/catalog_product_attribute_media.currentStore.html "catalog_product_attribute_media.currentStore") -
    Set/Get the current store view
-   [catalog_product_attribute_media.list](catalogProductAttributeMedia/catalog_product_attribute_media.list.html "catalog_product_attribute_media.list") -
    Retrieve the product images
-   [catalog_product_attribute_media.info](catalogProductAttributeMedia/catalog_product_attribute_media.info.html "catalog_product_attribute_media.info") -
    Retrieve the specified product image
-   [catalog_product_attribute_media.types](catalogProductAttributeMedia/catalog_product_attribute_media.types.html "catalog_product_attribute_media.types") -
    Retrieve product image types
-   [catalog_product_attribute_media.create](catalogProductAttributeMedia/catalog_product_attribute_media.create.html "catalog_product_attribute_media.create") -
    Upload a new image for a product
-   [catalog_product_attribute_media.update](catalogProductAttributeMedia/catalog_product_attribute_media.update.html "catalog_product_attribute_media.update") -
    Update an image for a product
-   [catalog_product_attribute_media.remove](catalogProductAttributeMedia/catalog_product_attribute_media.remove.html "catalog_product_attribute_media.remove") -
    Remove an image for a product

### [Product Tier Price](#Mage_Catalog-ProductTierPrice)

Allows you to retrieve and update product tier prices.

**Resource Name**: catalog_product_attribute_tier_price

**Aliases**:

-   product_attribute_tier_price
-   product_tier_price

**Methods**:

-   [catalog_product_attribute_tier_price.info](catalogProductTierPrice/catalog_product_attribute_tier_price.info.html "catalog_product_attribute_tier_price.info") -
    Retrieve information about product tier prices
-   [catalog_product_attribute_tier_price.update](catalogProductTierPrice/catalog_product_attribute_tier_price.update.html "catalog_product_attribute_tier_price.update") -
    Update the product tier prices

### [Product Links](#Mage_Catalog-ProductLinks)

Allows you to manage links for products, including related, cross-sells,
up-sells, and grouped.

**Resource Name**: catalog_product_link

**Aliases**:

-   product_link

**Methods**:

-   [catalog_product_link.list](catalogProductLink/catalog_product_link.list.html "catalog_product_link.list") -
    Retrieve products linked to the specified product
-   [catalog_product_link.assign](catalogProductLink/catalog_product_link.assign.html "catalog_product_link.assign") -
    Link a product to another product
-   [catalog_product_link.update](catalogProductLink/catalog_product_link.update.html "catalog_product_link.update") -
    Update a product link
-   [catalog_product_link.remove](catalogProductLink/catalog_product_link.remove.html "catalog_product_link.remove") -
    Remove a product link
-   [catalog_product_link.types](catalogProductLink/catalog_product_link.types.html "catalog_product_link.types") -
    Retrieve product link types
-   [catalog_product_link.attributes](catalogProductLink/catalog_product_link.attributes.html "catalog_product_link.attributes") -
    Retrieve product link type attributes

### [Product Downloadable Link](#Mage_Catalog-ProductDownloadableLink)

Allows you to add, remove, and retrieve a link to a downloadable
product.

**Resource Name**: product_downloadable_link

## [Methods](#Mage_Catalog-ProductDownloadableLink-Methods):

-   [product_downloadable_link.add](catalogProductDownloadableLink/product_downloadable_link.add.html "product_downloadable_link.add") -
    Add a new link to the downloadable product
-   [product_downloadable_link.list](catalogProductDownloadableLink/product_downloadable_link.list.html "product_downloadable_link.list") -
    Get the list of links for a downloadable product
-   [product_downloadable_link.remove](catalogProductDownloadableLink/product_downloadable_link.remove.html "product_downloadable_link.remove") -
    Remove a link from a downloadable product

### [Product Tag](#Mage_Catalog-ProductTag)

Allows you to add, update, remove, and retrieve product tags.

**Resource Name**: catalog_product_tag

**Aliases**:

-   product_tag

## [Methods](#Mage_Catalog-ProductTag-Methods):

-   [product_tag.add](catalogProductTag/product_tag.add.html "product_tag.add") -
    Retrieve the list of tags by the product ID
-   [product_tag.info](catalogProductTag/product_tag.info.html "product_tag.info") -
    Retrieve information about a product tag
-   [product_tag.add](catalogProductTag/product_tag.add.html "product_tag.add") -
    Add one or more tags to a product
-   [product_tag.update](catalogProductTag/product_tag.update.html "product_tag.update") -
    Update an existing product tag
-   [product_tag.remove](catalogProductTag/product_tag.remove.html "product_tag.remove") -
    Remove a product tag

### [Product Custom Option](#Mage_Catalog-ProductCustomOption)

Allows you to manage product custom options, including adding, updating,
removing, and retrieving information.

**Resource Name**: catalog_product_custom_option

**Aliases**:

-   product_custom_option

## [Methods](#Mage_Catalog-ProductCustomOption-Methods):

-   [product_custom_option.add](catalogProductCustomOption/product_custom_option.add.html "product_custom_option.add") -
    Add a new custom option to a product
-   [product_custom_option.update](catalogProductCustomOption/product_custom_option.update.html "product_custom_option.update") -
    Update the product custom option
-   [product_custom_option.types](catalogProductCustomOption/product_custom_option.types.html "product_custom_option.types") -
    Get the list of available custom option types
-   [product_custom_option.list](catalogProductCustomOption/product_custom_option.list.html "product_custom_option.list") -
    Retrieve the list of product custom options
-   [product_custom_option.info](catalogProductCustomOption/product_custom_option.info.html "product_custom_option.info") -
    Get full information about the custom option in a product
-   [product_custom_option.remove](catalogProductCustomOption/product_custom_option.remove.html "product_custom_option.remove") -
    Remove the custom option

### [Product Custom Option Value](#Mage_Catalog-ProductCustomOptionValue)

Allows you to manage product custom option values, including adding,
updating, removing, and retrieving information.

**Resource Name**: catalog_product_custom_option_value

**Aliases**:

-   product_custom_option_value

## [Methods](#Mage_Catalog-ProductCustomOptionValue-Methods):

-   [product_custom_option_value.add](catalogProductCustomOptionValue/product_custom_option_value.add.html "product_custom_option_value.add") -
    Add a new custom option value to a selectable custom option
-   [product_custom_option_value.list](catalogProductCustomOptionValue/product_custom_option_value.list.html "product_custom_option_value.list") -
    Retrieve the list of product custom option values
-   [product_custom_option_value.info](catalogProductCustomOptionValue/product_custom_option_value.info.html "product_custom_option_value.info") -
    Retrieve full information about the specified product custom option
    value
-   [product_custom_option_value.update](catalogProductCustomOptionValue/product_custom_option_value.update.html "product_custom_option_value.update") -
    Update the custom option value
-   [product_custom_option_value.remove](catalogProductCustomOptionValue/product_custom_option_value.remove.html "product_custom_option_value.remove") -
    Remove the custom option value
