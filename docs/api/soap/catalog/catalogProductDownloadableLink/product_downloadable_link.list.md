

### [Module: Complex Product API](#product_downloadable_link.list-Module:ComplexProductAPI)

## [Resource: product_downloadable_link](#product_downloadable_link.list-Resource:productdownloadablelink)

## [Method:](#product_downloadable_link.list-Method:)

-   product_downloadable_link.list (SOAP V1)
-   catalogProductDownloadableLinkList (SOAP V2)

Allows you to retrieve a list of links of a downloadable product.

**Arguments**:

Type Name Description

---

string sessionId Session ID
string productId Product ID or SKU
string store Store view ID or code (optional)
string identifierType Defines whether the product ID or SKU is passed in the request

**Return:**

Type Name Description

---

array result Array of catalogProductDownloadableLinkListEntity

The **catalogProductDownloadableLinkListEntity** content is as follows:

Type Name Description

---

array links Array of catalogProductDownloadableLinkEntity
array samples Array of catalogProductDownloadableLinkSampleEntity

The **catalogProductDownloadableLinkEntity** content is as follows:

---

Type Name Description

---

string link_id\ Link ID

string title\ Link title

string price\ Downloadable link price value 

int number_of_downloads\ Maximum number of possible downloads

int is_unlimited\ Defines whether the number of downloads is
unlimited

int is_shareable\ Defines whether the link is shareable

string link_url\ Link URL address

string link_type\ Type of the link data source. Can have one of
the following values: \"file\" or \"url\"

string sample_file\ Sample file name

string sample_url\ Sample URL

string sample_type\ Type of sample data source. Can have one of
the following values: \"file\" or \"url\"

int sort_order\ Link sort order

array file_save\ Array of
catalogProductDownloadableLinkFileInfoEntity

array sample_file_save\ Array of
catalogProductDownloadableLinkFileInfoEntity

---

The **catalogProductDownloadableLinkSampleEntity** content is as
follows:

---

Type Name Description

---

string sample_id\ Sample ID

string\ product_id\ Product ID

string\ sample_file\ Sample file name

string\ sample_url\ Sample URL

string\ sample_type\ Sample type. Can have
one of the following
values: \"file\" or
\"url\"

string\ sort_order\ Sort order

string\ default_title\ Default title

string\ store_title\ Store title

string\ title\ Sample title

---

The **catalogProductDownloadableLinkFileInfoEntity** content is as
follows:

---

Type Name Description

---

string file\ File

string name\ File name

int size\ File size

string status\ Status

---

**Faults:**
_No Faults_

### [Examples](#product_downloadable_link.list-Examples)

### [Request Example SOAP V1](#product_downloadable_link.list-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$downloadableProductId = '5';

$resultList = $proxy->call(
    $sessionId,
    'product_downloadable_link.list',
    array($downloadableProductId)
);
```

</div>



### [Request Example SOAP V2](#product_downloadable_link.list-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductDownloadableLinkList($sessionId, '5', null, 'sku');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#product_downloadable_link.list-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductDownloadableLinkList((object)array('sessionId' => $sessionId->result, 'productId' => '5'));

var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#product_downloadable_link.list-ResponseExampleSOAPV1)

<div>

```
array
  'links' =>
    array
      0 =>
        array
          'link_id' => string '1' (length=1)
          'title' => string 'link 1' (length=11)
          'price' => string '30.0000' (length=7)
          'number_of_downloads' => string '0' (length=1)
          'is_shareable' => string '1' (length=1)
          'link_url' => null
          'link_type' => string 'file' (length=4)
          'sample_file' => string '/s/o/software.jpg' (length=17)
          'sample_url' => null
          'sample_type' => string 'file' (length=4)
          'sort_order' => string '1' (length=1)
          'file_save' =>
            array
              ...
          'sample_file_save' =>
            array
              ...
          'is_unlimited' => int 1
  'samples' =>
    array
      0 =>
        array
          'sample_id' => string '1' (length=1)
          'product_id' => string '5' (length=1)
          'sample_url' => null
          'sample_file' => string '/s/o/software.jpg' (length=17)
          'sample_type' => string 'file' (length=4)
          'sort_order' => string '2' (length=1)
          'default_title' => string 'Sample 1' (length=8)
          'store_title' => string 'Sample 1' (length=8)
          'title' => string 'Sample 1' (length=8)
```

</div>


