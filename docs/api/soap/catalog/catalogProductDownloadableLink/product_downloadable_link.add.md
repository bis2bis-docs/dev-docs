

### [Module: Complex Product API](#product_downloadable_link.add-Module:ComplexProductAPI)

## [Resource: product_downloadable_link](#product_downloadable_link.add-Resource:productdownloadablelink)

## [Method:](#product_downloadable_link.add-Method:)

-   product_downloadable_link.add (SOAP V1)
-   catalogProductDownloadableLinkAdd (SOAP V2)

Allows you to add a new link to a downloadable product.

**Arguments**:

---

Type Name Description

---

string sessionId Session ID

string productId\ Product ID

array resource\ Array of
catalogProductDownloadableLinkAddEntity

string resourceType\ Resource type. Can have one of the
following values: \"sample\" or \"link\".

string store\ Store view ID or code (optional)

string identifierType\ Type of the product identifier. Can have
one of the following values: \"sku\" or
\"id\".

---

**Return**:

Type Name Description

---

int result Result of adding a link to the downloadable product

The **catalogProductDownloadableLinkAddEntity** content is as follows:

---

Type Name Description

---

string title\ Link title

string price\ Custom option value row price

int is_unlimited\ Defines whether the number of downloads is
unlimited

int number_of_downloads\ Maximum number of possible downloads

int is_shareable\ Defines whether the link is shareable

array sample\ Array of
catalogProductDownloadableLinkAddSampleEntity

string type\ Type of the data source. Can have one of the
following values: \"file\" or \"url\"

array file\ Array of
catalogProductDownloadableLinkFileEntity

string link_url\ Link URL address

string sample_url\ Sample URL address

int sort_order\ Link sort order

---

The **catalogProductDownloadableLinkAddSampleEntity** content is as
follows:

---

Type Name Description

---

string type Type of the data source. Can have one of
the following values: \"file\" or
\"url\" 

array file\ Array of
catalogProductDownloadableLinkFileEntity

string url\ URL to upload

---

The **catalogProductDownloadableLinkFileEntity** content is as follows:

---

Type Name Description

---

string name\ File name

string base64_content\ BASE64 encoded file

---

**Faults**:

---

Fault Code Fault Message

---

414 Unable to save action. Details in
error message.

415 Validation error has occurred.

---

### [Examples](#product_downloadable_link.add-Examples)

### [Request Example SOAP V1](#product_downloadable_link.add-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');
$filesPath = '/var/www/ws/tests/WebService/etc/Modules/Downloadable/Product/Link';
$downloadableProductId = 'downloadable_demo_product';

$items = array(
    'small' => array(
        'link' => array(
            'title' => 'Test file',
            'price' => '123',
            'is_unlimited' => '1',
            'number_of_downloads' => '111',
            'is_shareable' => '0',
            'sample' => array(
                'type' => 'file',
                'file' =>
                array(
                    'filename' => 'files/test.txt',
                ),
                'url' => 'http://www.magentocommerce.com/img/logo.gif',
            ),
            'type' => 'file',
            'file' =>
            array(
                'filename' => 'files/test.txt',
            ),
            'link_url' => 'http://www.magentocommerce.com/img/logo.gif',
        ),
        'sample' => array(
            'title' => 'Test sample file',
            'type' => 'file',
            'file' => array(
                'filename' => 'files/image.jpg',
            ),
            'sample_url' => 'http://www.magentocommerce.com/img/logo.gif',
            'sort_order' => '3',
        )
    ),
    'big' => array(
        'link' => array(
            'title' => 'Test url',
            'price' => '123',
            'is_unlimited' => '0',
            'number_of_downloads' => '111',
            'is_shareable' => '1',
            'sample' => array(
                'type' => 'url',
                'file' => array(
                    'filename' => 'files/book.pdf',
                ),
                'url' => 'http://www.magentocommerce.com/img/logo.gif',
            ),
            'type' => 'url',
            'file' => array(
                'filename' => 'files/song.mp3',
            ),
            'link_url' => 'http://www.magentocommerce.com/img/logo.gif',
        ),
        'sample' => array(
            'title' => 'Test sample url',
            'type' => 'url',
            'file' => array(
                'filename' => 'files/image.jpg',
            ),
            'sample_url' => 'http://www.magentocommerce.com/img/logo.gif',
            'sort_order' => '3',
        )
    )
);

$result = true;
foreach ($items as $item) {
    foreach ($item as $key => $value) {
        if ($value['type'] == 'file') {
            $filePath = $filesPath . '/' . $value['file']['filename'];
            $value['file'] = array('name' => str_replace('/', '_', $value['file']['filename']), 'base64_content' => base64_encode(file_get_contents($filePath)), 'type' => $value['type']);
        }
        if ($value['sample']['type'] == 'file') {
            $filePath = $filesPath . '/' . $value['sample']['file']['filename'];
            $value['sample']['file'] = array('name' => str_replace('/', '_', $value['sample']['file']['filename']), 'base64_content' => base64_encode(file_get_contents($filePath)));
        }
        if (!$proxy->call(
            $sessionId,
            'product_downloadable_link.add',
            array($downloadableProductId, $value, $key)
        )
        ) {
            $result = false;
        }
    }
}
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#product_downloadable_link.add-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductDownloadableLinkAdd((object)array('sessionId' => $sessionId->result, 'productId' => '3', 'resourceType' => 'link', 'resource' => ((object)array(
'title' => 'link',
'price' => '10.99',
'sample' => array(
'type' => 'url',
'url' => 'http://sometesturl.com')
))));

var_dump($result->result);
```

</div>



<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductDownloadableLinkAdd((object)array('sessionId' => $sessionId->result, 'productId' => '3', 'resourceType' => 'link', 'resource' => ((object)array(
'title' => 'link_2',
'price' => '11.99',
'type' => 'file',
'file' => array(
'name' => 'file_test',
'base64_content' => '/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAAXABcDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDLooor8XP4DCiiigAooooAKKKKAP/Z'
)
))));

var_dump($result->result);
```

</div>


