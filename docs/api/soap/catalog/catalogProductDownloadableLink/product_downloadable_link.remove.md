

### [Module: Complex Product API](#product_downloadable_link.remove-Module:ComplexProductAPI)

## [Resource: product_downloadable_link](#product_downloadable_link.remove-Resource:productdownloadablelink)

## [Method:](#product_downloadable_link.remove-Method:)

-   product_downloadable_link.remove (SOAP V1)
-   catalogProductDownloadableLinkRemove (SOAP V2)

Allows you to remove a link/sample from a downloadable product.

**Arguments:**

Type Name Description

---

string sessionId Session ID
string linkId/sampleId Link/sample ID
string resourceType Resource type. Can have one of the following values: \'sample\' or \'link\'

**Return:**

Type Description

---

boolean True if the link/sample is removed from a downloadable product

**Faults**:

---

Fault Code Fault Message

---

412 Link or sample with specified ID
was not found. 

415 Validation error has occurred.

416 Unable to remove link. Details in
error message.

---

### [Examples](#product_downloadable_link.remove-Examples)

### [Request Example SOAP V1](#product_downloadable_link.remove-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$sampleId = 7;
$linkId = 9;

$resultSample = $proxy->call(
    $sessionId,
    'product_downloadable_link.remove',
    array($sampleId, 'sample')
);

$resultLink = $proxy->call(
    $sessionId,
    'product_downloadable_link.remove',
    array($linkId, 'link')
);
```

</div>



### [Request Example SOAP V2](#product_downloadable_link.remove-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductDownloadableLinkRemove($sessionId, '7', 'sample');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#product_downloadable_link.remove-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductDownloadableLinkRemove((object)array('sessionId' => $sessionId->result, 'linkId' => '7', 'resourceType' => 'sample'));
var_dump($result->result);
```

</div>


