\-\-- layout: m1x\_soap title: Catalog Product Downloadable Link \-\--

[]{#MAPI-DownloadableProductLink-Module:ComplexProductAPI}Module: Complex Product API
---------------------------------------------------------------------------------------

## [Resource: product](#MAPI-DownloadableProductLink-Resource:productdownloadablelink)\_downloadable\_link

## [Methods:](#MAPI-DownloadableProductLink-Methods:)

-   [product\_downloadable\_link.add](product_downloadable_link.add.html "product_downloadable_link.add") -
    Add a new link to the downloadable product
-   [product\_downloadable\_link.list](product_downloadable_link.list.html "product_downloadable_link.list") -
    Get the list of links for a downloadable product
-   [product\_downloadable\_link.remove](product_downloadable_link.remove.html "product_downloadable_link.remove") -
    Remove a link from a downloadable product
