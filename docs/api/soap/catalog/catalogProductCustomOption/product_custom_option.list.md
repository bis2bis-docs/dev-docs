

### [Module: Complex Product API](#product_custom_option.list-Module:ComplexProductAPI)

## [Resource: product_custom_option](#product_custom_option.list-Resource:productcustomoption)

## [Method:](#product_custom_option.list-Method:)

-   product_custom_option.list (SOAP V1)
-   catalogProductCustomOptionList (SOAP V2)

Allows you to retrieve the list of custom options for a specific
product.

**Arguments:**

---

Type Name Description

---

string sessionId\ Session ID

string\ productId Product ID

string store Store view ID or code
(optional but required
for WS-I mode)

---

**Return:**

---

Type Name Description

---

array result Array of
catalogProductCustomOptionList

---

The **catalogProductCustomOptionList** content is as follows:

---

Type Name Description

---

string option_id Custom option ID

string\ title\ Custom option title

string\ type\ Custom option type

string\ sort_order\ Custom option sort
order

int is_require\ Defines whether the
custom option is
required

---

**Faults:**

Fault Code Fault Message

---

101 Product with requested id does not exist.
104 Store with requested code/id does not exist.

### [Examples](#product_custom_option.list-Examples)

### [Request Example SOAP V1](#product_custom_option.list-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'product_custom_option.list', '1');
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#product_custom_option.list-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductCustomOptionList($sessionId, '1');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#product_custom_option.list-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductCustomOptionList((object)array('sessionId' => $sessionId->result, 'productId' => '1', 'store' => '1'));
var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#product_custom_option.list-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
    array
      'option_id' => string '1' (length=1)
      'title' => string 'model' (length=5)
      'type' => string 'drop_down' (length=9)
      'is_require' => string '1' (length=1)
      'sort_order' => string '0' (length=1)
```

</div>


