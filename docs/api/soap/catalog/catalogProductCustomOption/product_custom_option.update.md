

### [Module: Complex Product API](#product_custom_option.update-Module:ComplexProductAPI)

## [Resource: product_custom_option](#product_custom_option.update-Resource:productcustomoption)

## [Method:](#product_custom_option.update-Method:)

-   product_custom_option.update (SOAP V1)
-   catalogProductCustomOptionUpdate (SOAP V2)

Allows you to update the required product custom option.

**Arguments:**

Type Name Description

---

string sessionId Session ID
string optionId Option ID
array data Array of catalogProductCustomOptionToUpdate
string store Store view ID or code (optional)

**Return:**

Type Description

---

boolean\\int True (1) if the custom option is updated

The **catalogProductCustomOptionToUpdate** content is as follows:

---

Type Name Description

---

string title\ Title of the custom option to be updated

string\ type\ Custom option type

string\ sort_order\ Custom option sort order

int is_require\ Defines whether the custom option is
required

array additional_fields\ Array of
catalogProductCustomOptionAdditionalFields

---

The **catalogProductCustomOptionAdditionalFields** content is as
follows:

---

Type Name Description

---

string title\ Custom option title

string\ price\ Custom option price

string\ price_type\ Price type. Possible
values are as follows:
\"fixed\" or
\"percent\"

string\ sku\ Custom option SKU

string\ max_characters\ Maximum number of
characters for the
customer input on the
frontend (optional)

string\ sort_order\ Custom option sort
order

string\ file_extension\ List of file extensions
allowed to upload by
the user on the
frontend (optional; for
the **File** input
type)

string\ image_size_x\ Width limit for
uploaded images
(optional; for the
**File** input type)

string\ image_size_y\ Height limit for
uploaded images
(optional; for the
**File** input type)

string\ value_id\ Value ID

---

**Faults:**

Fault Code Fault Message

---

101 Product with requested id does not exist.
102 Provided data is invalid.
103 Error while saving an option. Details are in the error message.
104 Store with requested code/id does not exist.
105 Option with requested id does not exist.
106 Invalid option type provided. Call \'types\' to get list of allowed option types.

### [Examples](#product_custom_option.update-Examples)

### [Request Example SOAP V1](#product_custom_option.update-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');
$selectOptionId = 1379;
$selectOptionValueId = 794;
$textOptionId = 1380;
$fileOptionId = 1381;

// Update custom option of Text Field type
$customTextFieldOption = array(
    "title" => "Custom Text Field Option Title Updated",
    "type" => "field",
    "is_require" => 1,
    "sort_order" => 20,
    "additional_fields" => array(
        array(
            "price" => 13.00,
            "price_type" => "fixed",
            "sku" => "custom_text_option_sku_updated",
            "max_characters" => 127
        )
    )
);
$resultCustomTextFieldOptionUpdate = $proxy->call(
    $sessionId,
    "product_custom_option.update",
    array(
         $textOptionId,
         $customTextFieldOption
    )
);

// Update custom option of File type
$customFileOption = array(
    "title" => "Custom File Option Title Updated",
    "additional_fields" => array(
        array(
            "image_size_x" => 800,
            "image_size_y" => 999
        )
    )
);
$resultCustomFileOptionUpdate = $proxy->call(
    $sessionId,
    "product_custom_option.update",
    array(
         $fileOptionId,
         $customFileOption
    )
);


// Update custom option of Dropdown type
$customDropdownOption = array(
    "title" => "Custom Dropdown Option Title Updated to Multiselect",
    "type" => "multiple",
    "additional_fields" => array(
        array(
            "value_id" => $selectOptionValueId,
            "price" => 14.00,
            "price_type" => 'percent',
            "sku" => "custom_select_option_sku_1 updated",
            "sort_order" => 26
        )
    )
);
$resultCustomDropdownOptionUpdate = $proxy->call(
    $sessionId,
    "product_custom_option.update",
    array(
         $selectOptionId,
         $customDropdownOption
    )
);
```

</div>



### [Request Example SOAP V2](#product_custom_option.update-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login('apiUser', 'apiKey');

$result = $proxy->catalogProductCustomOptionUpdate($sessionId, '1', array(
'title' => 'title_updated',
'is_require' => 0,
'sort_order' => '2'
));
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#product_custom_option.update-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductCustomOptionUpdate((object)array('sessionId' => $sessionId->result, 'optionId' => '1', 'data' => ((object)array(
'title' => 'title_updated',
'is_require' => 0,
'sort_order' => '2'
))));
var_dump($result->result);
```

</div>


