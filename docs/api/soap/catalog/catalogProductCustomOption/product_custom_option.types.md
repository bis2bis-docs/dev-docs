

### [Module: Complex Product API](#product_custom_option.types-Module:ComplexProductAPI)

## [Resource: product_custom_option](#product_custom_option.types-Resource:productcustomoption)

## [Method:](#product_custom_option.types-Method:)

-   product_custom_option.types (SOAP V1)
-   catalogProductCustomOptionTypes (SOAP V2)

Allows you to retrieve the list of available custom option types.

**Arguments:**

Type Name Description

---

string sessionId Session ID

**Return:**

Type Name Description

---

array result Array of catalogProductCustomOptionTypes

The **catalogProductCustomOptionTypesEntity** content is as follows:

---

Type Name Description

---

string label Custom option label

string value\ Custom option value

---

**Faults:**

_No faults_

### [Examples](#product_custom_option.types-Examples)

### [Request Example SOAP V1](#product_custom_option.types-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'product_custom_option.types');
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#product_custom_option.types-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductCustomOptionTypes($sessionId);
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#product_custom_option.types-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductCustomOptionTypes((object)array('sessionId' => $sessionId->result));
var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#product_custom_option.types-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
    array
      'label' => string 'Field' (length=5)
      'value' => string 'field' (length=5)
  1 =>
    array
      'label' => string 'Area' (length=4)
      'value' => string 'area' (length=4)
  2 =>
    array
      'label' => string 'File' (length=4)
      'value' => string 'file' (length=4)
  3 =>
    array
      'label' => string 'Drop-down' (length=9)
      'value' => string 'drop_down' (length=9)
  4 =>
    array
      'label' => string 'Radio Buttons' (length=13)
      'value' => string 'radio' (length=5)
  5 =>
    array
      'label' => string 'Checkbox' (length=8)
      'value' => string 'checkbox' (length=8)
  6 =>
    array
      'label' => string 'Multiple Select' (length=15)
      'value' => string 'multiple' (length=8)
  7 =>
    array
      'label' => string 'Date' (length=4)
      'value' => string 'date' (length=4)
  8 =>
    array
      'label' => string 'Date & Time' (length=11)
      'value' => string 'date_time' (length=9)
  9 =>
    array
      'label' => string 'Time' (length=4)
      'value' => string 'time' (length=4)
```

</div>


