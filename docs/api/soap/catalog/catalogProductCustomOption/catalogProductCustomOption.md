\-\-- layout: m1x\_soap title: Catalog Product Custom Option \-\--

[]{#MAPI-ProductCustomOption-Module:MageCatalog}Module: Mage\_Catalog
-----------------------------------------------------------------------

## [Resource: catalog](#MAPI-ProductCustomOption-Resource:catalogproductcustomoption)\_product\_custom\_option

**Aliases**: product\_custom\_option

## [Methods:](#MAPI-ProductCustomOption-Methods:)

-   [product\_custom\_option.add](product_custom_option.add.html "product_custom_option.add") -
    Add a new custom option to a product
-   [product\_custom\_option.update](product_custom_option.update.html "product_custom_option.update")
    **- ** Update the product custom option
-   [product\_custom\_option.types](product_custom_option.types.html "product_custom_option.types")
    **- ** Get the list of available custom option types
-   [product\_custom\_option.list](product_custom_option.list.html "product_custom_option.list")
    **- ** Retrieve the list of product custom options
-   [product\_custom\_option.info](product_custom_option.info.html "product_custom_option.info")
    **- ** Get full information about the custom option in a product
-   [product\_custom\_option.remove](product_custom_option.remove.html "product_custom_option.remove")
    **- ** Remove the custom option

## [Faults:](#MAPI-ProductCustomOption-Faults:)

  Fault Code   Fault Message
  ------------ -----------------------------------------------------------------------------------
  101          Product with requested id does not exist.
  102          Provided data is invalid.
  103          Error while saving an option. Details are in the error message.
  104          Store with requested code/id does not exist.
  105          Option with requested id does not exist.
  106          Invalid option type provided. Call \'types\' to get list of allowed option types.
  107          Error while deleting an option. Details are in the error message.

Create the Magento file system owner
