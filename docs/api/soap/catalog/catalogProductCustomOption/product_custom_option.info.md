

### [Module: Complex Product API](#product_custom_option.info-Module:ComplexProductAPI)

## [Resource: product_custom_option](#product_custom_option.info-Resource:productcustomoption)

## [Method:](#product_custom_option.info-Method:)

-   product_custom_option.info (SOAP V1)
-   catalogProductCustomOptionInfo (SOAP V2)

Allows you to retrieve full information about the custom option in a
product.

**Arguments:**

Type Name Description

---

string sessionId Session ID
string optionId Option ID
string store Store view ID or code (optional)

**Return:**

Type Name Description

---

array result Array of catalogProductCustomOptionInfoEntity

The **catalogProductCustomOptionInfoEntity** content is as follows:

---

Type Name Description

---

string title\ Custom option title

string\ type\ Custom option type. Can have one of the
following values: \"fixed\" or \"percent\"

string\ sort_order\ Custom option sort order

int is_require\ Defines whether the custom option is
required

array additional_fields\ Array of
catalogProductCustomOptionAdditionalFields

---

The **catalogProductCustomOptionAdditionalFields** content is as
follows:

---

Type Name Description

---

string title\ Custom option title

string\ price\ Custom option price

string\ price_type\ Price type. Possible
values are as follows:
\"fixed\" or
\"percent\"

string\ sku\ Custom option SKU

string\ max_characters\ Maximum number of
characters for the
customer input on the
frontend (optional)

string\ sort_order\ Custom option sort
order

string\ file_extension\ List of file extensions
allowed to upload by
the user on the
frontend (optional; for
the **File** input
type)

string\ image_size_x\ Width limit for
uploaded images
(optional; for the
**File** input type)

string\ image_size_y\ Height limit for
uploaded images
(optional; for the
**File** input type)

string\ value_id\ Value ID

---

**Faults:**

Fault Code Fault Message

---

101 Product with requested id does not exist.
104 Store with requested code/id does not exist.
105 Option with requested id does not exist.

### [Examples](#product_custom_option.info-Examples)

### [Request Example SOAP V1](#product_custom_option.info-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'product_custom_option.info', '1');
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#product_custom_option.info-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductCustomOptionInfo($sessionId, '1');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#product_custom_option.info-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductCustomOptionInfo((object)array('sessionId' => $sessionId->result, 'optionId' => '1'));
var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#product_custom_option.info-ResponseExampleSOAPV1)

<div>

```
array
  'title' => string 'model' (length=5)
  'type' => string 'drop_down' (length=9)
  'is_require' => string '1' (length=1)
  'sort_order' => string '0' (length=1)
  'additional_fields' =>
    array
      0 =>
        array
          'value_id' => string '1' (length=1)
          'title' => string 'monoblock' (length=9)
          'price' => string '139.9900' (length=8)
          'price_type' => string 'fixed' (length=5)
          'sku' => string 'monoblock' (length=9)
          'sort_order' => string '0' (length=1)
      1 =>
        array
          'value_id' => string '2' (length=1)
          'title' => string 'slider' (length=6)
          'price' => string '239.9900' (length=8)
          'price_type' => string 'fixed' (length=5)
          'sku' => string 'slider' (length=6)
          'sort_order' => string '0' (length=1)
```

</div>


