

### [Module: Complex Product API](#product_custom_option.add-Module:ComplexProductAPI)

## [Resource: product_custom_option](#product_custom_option.add-Resource:productcustomoption)

## [Method:](#product_custom_option.add-Method:)

-   product_custom_option.add (SOAP V1)
-   catalogProductCustomOptionAdd (SOAP V2)

Allows you to add a new custom option for a product.

**Arguments:**

---

Type Name Description

---

string sessionId Session ID

string\ productId Product ID

array data Array of
catalogProductCustomOptionToAdd

string store Store view ID or code (optional)

---

**Return:**

Type Description

---

boolean True if the custom option is added

The **catalogProductCustomOptionToAdd** content is as follows:

---

Type Name Description

---

string title\ Option title

string\ type\ Option type

string\ sort_order\ Option sort order

int is_require\ Defines whether the option is required

array additional_fields\ Array of
catalogProductCustomOptionAdditionalFields

---

The **catalogProductCustomOptionAdditionalFieldsEntity** content is as
follows:

---

Type Name Description

---

string title\ Custom option title

string\ price\ Custom option price

string\ price_type\ Price type. Possible
values are as follows:
\"fixed\" or
\"percent\"

string\ sku\ Custom option SKU

string\ max_characters\ Maximum number of
characters for the
customer input on the
frontend (optional)

string\ sort_order\ Custom option sort
order

string\ file_extension\ List of file extensions
allowed to upload by
the user on the
frontend (optional)

string\ image_size_x\ Width limit for
uploaded images
(optional)

string\ image_size_y\ Height limit for
uploaded images
(optional)

string\ value_id\ Value ID

---

**Faults:**

Fault Code Fault Message

---

101 Product with requested id does not exist.
102 Provided data is invalid.
103 Error while saving an option. Details are in the error message.
104 Store with requested code/id does not exist.
106 Invalid option type provided. Call \'types\' to get list of allowed option types.

### [Examples](#product_custom_option.add-Examples)

### [Request Example SOAP V1](#product_custom_option.add-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');
$productId = 1;// Existing product ID

// Add custom option of Text Field type
$customTextFieldOption = array(
    "title" => "Custom Text Field Option Title",
    "type" => "field",
    "is_require" => 1,
    "sort_order" => 0,
    "additional_fields" => array(
        array(
            "price" => 10.00,
            "price_type" => "fixed",
            "sku" => "custom_text_option_sku",
            "max_characters" => 255
        )
    )
);
$resultCustomTextFieldOptionAdd = $proxy->call(
    $sessionId,
    "product_custom_option.add",
    array(
        $productId,
        $customTextFieldOption
    )
);

// Add custom option of File type
$customFileOption = array(
    "title" => "Custom File Option Title",
    "type" => "file",
    "is_require" => 1,
    "sort_order" => 5,
    "additional_fields" => array(
        array(
            "price" => 10.00,
            "price_type" => "fixed",
            "sku" => "custom_file_option_sku",
            "file_extension" => "jpg",
            "image_size_x" => 800,
            "image_size_y" => 600
        )
    )
);
$resultCustomFileOptionAdd = $proxy->call(
    $sessionId,
    "product_custom_option.add",
    array(
        $productId,
        $customFileOption
    )
);

// Add custom option of Dropdown type
$customDropdownOption = array(
    "title" => "Custom Dropdown Option Title",
    "type" => "drop_down",
    "is_require" => 1,
    "sort_order" => 10,
    "additional_fields" => array(
        array(
            "title" => "Dropdown row #1",
            "price" => 10.00,
            "price_type" => "fixed",
            "sku" => "custom_select_option_sku_1",
            "sort_order" => 0
        ),
        array(
            "title" => "Dropdown row #2",
            "price" => 10.00,
            "price_type" => "fixed",
            "sku" => "custom_select_option_sku_2",
            "sort_order" => 5
        )
    )
);
$resultCustomDropdownOptionAdd = $proxy->call(
    $sessionId,
    "product_custom_option.add",
    array(
        $productId,
        $customDropdownOption
    )
);
```

</div>



### [Request Example SOAP V2](#product_custom_option.add-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login('apiUser', 'apiKey');

$result = $proxy->catalogProductCustomOptionAdd($sessionId, '1', array('title' => 'title',
'type' => 'field',
'sort_order' => '1',
'is_require' => 1,
'additional_fields' => array(array(
'price' => '15',
'price_type' => 'fixed',
'sku' => 'sku',
'max_characters' => '100'
))));
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Example)](#product_custom_option.add-RequestExampleSOAPV2%28WSIComplianceExample%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductCustomOptionAdd((object)array('sessionId' => $sessionId->result, 'productId' => '1', 'store' => '1', 'data' => ((object)array(
'title' => 'title',
'type' => 'field',
'sort_order' => '1',
'is_require' => 1,
'additional_fields' => array(array(
'price' => '15',
'price_type' => 'fixed',
'sku' => 'sku',
'max_characters' => '100'
))))));
var_dump($result->result);
```

</div>


