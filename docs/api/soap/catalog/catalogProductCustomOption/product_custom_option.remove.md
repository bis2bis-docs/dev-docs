

### [Module: Complex Product API](#product_custom_option.remove-Module:ComplexProductAPI)

## [Resource: product_custom_option](#product_custom_option.remove-Resource:productcustomoption)

## [Method:](#product_custom_option.remove-Method:)

-   product_custom_option.remove (SOAP V1)
-   catalogProductCustomOptionRemove (SOAP V2)

Allows you to remove a custom option from the product.

**Arguments:**

---

Type Name Description

---

string sessionId\ Session ID

string optionId Custom option ID

---

**Return:**

Type Description

---

boolean True if the custom option is removed

**Faults:**

Fault Code Fault Message

---

105 Option with requested id does not exist.
107 Error while deleting an option. Details are in the error message.

### [Examples](#product_custom_option.remove-Examples)

### [Request Example SOAP V1](#product_custom_option.remove-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');
$optionId  = 1;// Existing option ID

$result = $proxy->call(
    $sessionId,
    "product_custom_option.remove",
    array(
        $optionId
    )
);
```

</div>



### [Request Example SOAP V2](#product_custom_option.remove-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductCustomOptionRemove($sessionId, '1');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#product_custom_option.remove-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductCustomOptionRemove((object)array('sessionId' => $sessionId->result, 'optionId' => '1'));
var_dump($result->result);
```

</div>


