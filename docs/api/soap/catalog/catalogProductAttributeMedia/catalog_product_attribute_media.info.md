

## [Module: Mage_Catalog](#catalog_product_attribute_media.info-Module:MageCatalog)

## [Resource:catalog_product_attribute_media](#catalog_product_attribute_media.info-Resource:catalogproductattributemedia)

**Aliases:**

-   product_attribute_media
-   product_media

## [Method:](#catalog_product_attribute_media.info-Method:)

-   catalog_product_attribute_media.info (SOAP V1)
-   catalogProductAttributeMediaInfo (SOAP V2)

Allows you to retrieve information about the specified product image.

**Aliases:**

-   product_attribute_media.info
-   product_media.info

**Arguments:**

---

Type Name Description

---

string sessionId\ Session ID

string\ product\\productId\ Product ID or SKU

string\ file\ Name of the image file (e.g.,
/b/l/blackberry8100_2.jpg)

string\ storeView\ Store view ID or code
(optional)

string identifierType Defines whether the product
ID or SKU is passed in the
\'product\' parameter

---

**Returns**:

Type Name Description

---

array result Array of catalogProductImageEntity

The **catalogProductImageEntity** content is as follows:

---

Type Name Description

---

string file\ Image file name

string\ label\ Image file label

string\ position\ Image file position

string\ exclude\ Defines whether the
image will associate
only to one of three
image types

string\ url\ Image URL

ArrayOfString types\ Array of types

---

### [Examples](#catalog_product_attribute_media.info-Examples)

### [Request Example SOAP V1](#catalog_product_attribute_media.info-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_product_attribute_media.info', array('product' => '2', 'file' => '/b/l/blackberry8100_2.jpg'));
var_dump ($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#catalog_product_attribute_media.info-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductAttributeMediaInfo($sessionId, '2', '/b/l/blackberry8100_2.jpg');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_product_attribute_media.info-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductAttributeMediaInfo((object)array('sessionId' => $sessionId->result, 'productId' => '1', 'file' => '/i/m/image.png'));

var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#catalog_product_attribute_media.info-ResponseExampleSOAPV1)

<div>

```
array
  'file' => string '/b/l/blackberry8100_2.jpg' (length=25)
  'label' => string '' (length=0)
  'position' => string '1' (length=1)
  'exclude' => string '0' (length=1)
  'url' => string 'http://magentohost/media/catalog/product/b/l/blackberry8100_2.jpg' (length=71)
  'types' =>
    array
      0 => string 'image' (length=5)
      1 => string 'small_image' (length=11)
      2 => string 'thumbnail' (length=9)
```

</div>


