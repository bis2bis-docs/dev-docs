

## [Module: Mage_Catalog](#catalog_product_attribute_media.update-Module:MageCatalog)

## [Resource:catalog_product_attribute_media](#catalog_product_attribute_media.update-Resource:catalogproductattributemedia)

**Aliases:**

-   product_attribute_media
-   product_media

## [Method:](#catalog_product_attribute_media.update-Method:)

-   catalog_product_attribute_media.update (SOAP V1)
-   catalogProductAttributeMediaUpdate (SOAP V2)

Allows you to update the product image.

**Aliases:**

-   product_attribute_media.update
-   product_media.update

**Arguments:**

---

Type Name Description

---

string sessionId Session ID

string\ product\\productId\ Product ID or code

string\ file\ Image file name (e.g., /i/m/image.jpeg)

array data\ Array of
catalogProductAttributeMediaCreateEntity

string\ storeView\ Store view ID or code

string identifierType Defines whether the product ID or SKU is
passed in the \'product\' parameter

---

**Notes**: You should specify only those parameters which you want to be
updated. Parameters that were not specified in the request, will
preserve the previous values.

**Returns**:

Type Name Description

---

boolean result Result of product image updating

The **catalogProductAttributeMediaCreateEntity** content is as follows:

---

Type Name Description

---

array file\ Array of
catalogProductImageFileEntity

string label\ Product image label

string position\ Product image position

ArrayOfString types\ Array of types

string exclude\ Defines whether the image will
associate only to one of three
image types

string remove\ Image remove flag

---

The **catalogProductImageFileEntity** content is as follows:

Type Name Description

---

string content Product image content (base_64 encoded)
string mime Image mime type (e.g., image/jpeg)
string name Image name

### [Examples](#catalog_product_attribute_media.update-Examples)

### [Request Example SOAP V1](#catalog_product_attribute_media.update-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$productId = 1;
$file = '/i/m/image.jpg';

$newFile = array(
    'content' => '/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAAXABcDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDLooor8XP4DCiiigAooooAKKKKAP/Z',
    'mime' => 'image/jpeg'
);

$result = $client->call(
    $session,
    'catalog_product_attribute_media.update',
    array(
        $productId,
        $file,
        array('file' => $newFile, 'label' => 'New label', 'position' => '50', 'types' => array('image'), 'exclude' => 1)
    )
);
```

</div>



### [Request Example SOAP V2](#catalog_product_attribute_media.update-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$productId = 1;
$file = '/i/m/image.jpg';

$newFile = array(
'content' => '/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAAXABcDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDLooor8XP4DCiiigAooooAKKKKAP/Z',
'mime' => 'image/jpeg'
);

$result = $client->catalogProductAttributeMediaUpdate(
$session,
$productId,
$file,
array('file' => $newFile, 'label' => 'New label', 'position' => '50', 'types' => array('image'), 'exclude' => 1)
);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_product_attribute_media.update-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductAttributeMediaUpdate((object)array('sessionId' => $sessionId->result, 'productId' => '1', 'file' => '/t/u/tulips.jpg', 'data' => ((object)array(
'label' => 'tulips',
'position' => '1',
'remove' => '0',
'types' => array('small_image')
))));

var_dump($result->result);
```

</div>


