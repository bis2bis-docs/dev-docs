

## [Module: Mage_Catalog](#catalog_product_attribute_media.list-Module:MageCatalog)

## [Resource:catalog_product_attribute_media](#catalog_product_attribute_media.list-Resource:catalogproductattributemedia)

**Aliases:**

-   product_attribute_media
-   product_media

## [Method:](#catalog_product_attribute_media.list-Method:)

-   catalog_product_attribute_media.list (SOAP V1)
-   catalogProductAttributeMediaList (SOAP V2)

Allows you to retrieve the list of product images.

**Aliases:**

-   product_attribute_media.list
-   product_media.list

**Arguments:**

---

Type Name Description

---

string sessionId\ Session ID

string\ product\\productId\ Product ID or SKU

string\ storeView\ Store view ID or code
(optional)

string identifierType Defines whether the
product ID or sku is
passed in the
\'product\' parameter

---

**Returns**:

Type Name Description

---

array result Array of catalogProductImageEntity

The **catalogProductImageEntity** content is as follows:

---

Type Name Description

---

string file\ Image file name

string\ label\ Image label

string\ position\ Image position

string\ exclude\ Defines whether the
image will associate
only to one of three
image types

string\ url\ Image URL

ArrayOfString types\ Array of types

---

### [Examples](#catalog_product_attribute_media.list-Examples)

### [Request Example SOAP V1](#catalog_product_attribute_media.list-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_product_attribute_media.list', '2');
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#catalog_product_attribute_media.list-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductAttributeMediaList($sessionId, '2');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_product_attribute_media.list-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductAttributeMediaList((object)array('sessionId' => $sessionId->result, 'productId' => '2'));

var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#catalog_product_attribute_media.list-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
    array
      'file' => string '/b/l/blackberry8100_2.jpg' (length=25)
      'label' => string '' (length=0)
      'position' => string '1' (length=1)
      'exclude' => string '0' (length=1)
      'url' => string 'http://magentopath/blackberry8100_2.jpg' (length=71)
      'types' =>
        array
          0 => string 'image' (length=5)
          1 => string 'small_image' (length=11)
          2 => string 'thumbnail' (length=9)
```

</div>


