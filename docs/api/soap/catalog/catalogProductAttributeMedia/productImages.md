

## [Module: Mage_Catalog](#MAPI-ProductImages-Module:MageCatalog)

The Mage_Catalog module allows you to manage categories and products.

### [Product Images](#MAPI-ProductImages-ProductImages)

Allows you to manage product images.

**Resource Name**: catalog_product_attribute_media

**Aliases**:

-   product_attribute_media
-   product_media

**Methods**:

-   [catalog_product_attribute_media.currentStore](catalog_product_attribute_media.currentStore.html "catalog_product_attribute_media.currentStore") -
    Set/Get the current store view
-   [catalog_product_attribute_media.list](catalog_product_attribute_media.list.html "catalog_product_attribute_media.list") -
    Retrieve the product images
-   [catalog_product_attribute_media.info](catalog_product_attribute_media.info.html "catalog_product_attribute_media.info") -
    Retrieve the specified product image
-   [catalog_product_attribute_media.types](catalog_product_attribute_media.types.html "catalog_product_attribute_media.types") -
    Retrieve product image types
-   [catalog_product_attribute_media.create](catalog_product_attribute_media.create.html "catalog_product_attribute_media.create") -
    Upload a new image for a product
-   [catalog_product_attribute_media.update](catalog_product_attribute_media.update.html "catalog_product_attribute_media.update") -
    Update an image for a product
-   [catalog_product_attribute_media.remove](catalog_product_attribute_media.remove.html "catalog_product_attribute_media.remove") -
    Remove an image for a product

### [Faults](#MAPI-ProductImages-Faults)

Fault Code Fault Message

---

100 Requested store view not found.
101 Product not exists.
102 Invalid data given. Details in error message.
103 Requested image not exists in product images' gallery.
104 Image creation failed. Details in error message.
105 Image not updated. Details in error message.
106 Image not removed. Details in error message.
107 Requested product doesn't support images

### [Examples](#MAPI-ProductImages-Examples)

### [Example 1. Working with product images](#MAPI-ProductImages-Example1.Workingwithproductimages)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$newImage = array(
    'file' => array(
        'name' => 'file_name',
        'content' => base64_encode(file_get_contents('product.jpg')),
        'mime'    => 'image/jpeg'
    ),
    'label'    => 'Cool Image Through Soap',
    'position' => 2,
    'types'    => array('small_image'),
    'exclude'  => 0
);

$imageFilename = $proxy->call($sessionId, 'product_media.create', array('Sku', $newImage));


var_dump($imageFilename);

// Newly created image file
var_dump($proxy->call($sessionId, 'product_media.list', 'Sku'));

$proxy->call($sessionId, 'product_media.update', array(
    'Sku',
    $imageFilename,
    array('position' => 2, 'types' => array('image') /* Lets do it main image for product */)
));

// Updated image file
var_dump($proxy->call($sessionId, 'product_media.list', 'Sku'));

// Remove image file
$proxy->call($sessionId, 'product_media.remove', array('Sku', $imageFilename));

// Images without our file
var_dump($proxy->call($sessionId, 'product_media.list', 'Sku'));
```

</div>



Create the Magento file system owner
