

## [Module: Mage_Catalog](#catalog_product_attribute_media.types-Module:MageCatalog)

## [Resource:catalog_product_attribute_media](#catalog_product_attribute_media.types-Resource:catalogproductattributemedia)

**Aliases:**

-   product_attribute_media
-   product_media

## [Method:](#catalog_product_attribute_media.types-Method:)

-   catalog_product_attribute_media.types (SOAP V1)
-   catalogProductAttributeMediaTypes (SOAP V2)

Allows you to retrieve product image types including standard image,
small_image, thumbnail, etc. Note that if the product attribute set
contains attributes of the Media Image type (**Catalog Input Type for
Store Owner \> Media Image**), it will also be returned in the response.

**Aliases:**

-   product_attribute_media.types
-   product_media.types

**Arguments:**

Type Name Description

---

string sessionId Session ID
string setId ID of the product attribute set

**Returns**:

Type Name Description

---

array result Array of catalogProductAttributeMediaTypeEntity

The **catalogProductAttributeMediaTypeEntity** content is as follows:

Type Name Description

---

string code Image type code
string scope Image scope (store, website, or global)

### [Examples](#catalog_product_attribute_media.types-Examples)

### [Request Example SOAP V1](#catalog_product_attribute_media.types-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_product_attribute_media.types', '4');
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#catalog_product_attribute_media.types-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductAttributeMediaTypes($sessionId, '4');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_product_attribute_media.types-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductAttributeMediaTypes((object)array('sessionId' => $sessionId->result, 'setId' => '4'));

var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#catalog_product_attribute_media.types-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
    array
      'code' => string 'image' (length=5)
      'scope' => string 'store' (length=5)
  1 =>
    array
      'code' => string 'small_image' (length=11)
      'scope' => string 'store' (length=5)
  2 =>
    array
      'code' => string 'thumbnail' (length=9)
      'scope' => string 'store' (length=5)
```

</div>


