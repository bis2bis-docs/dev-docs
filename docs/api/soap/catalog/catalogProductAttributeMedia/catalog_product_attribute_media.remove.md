

## [Module: Mage_Catalog](#catalog_product_attribute_media.remove-Module:MageCatalog)

## [Resource:catalog_product_attribute_media](#catalog_product_attribute_media.remove-Resource:catalogproductattributemedia)

**Aliases:**

-   product_attribute_media
-   product_media

## [Method:](#catalog_product_attribute_media.remove-Method:)

-   catalog_product_attribute_media.remove (SOAP V1)
-   catalogProductAttributeMediaRemove (SOAP V2)

Allows you to remove the image from a product.

**Aliases:**

-   product_attribute_media.remove
-   product_media.remove

**Arguments:**

---

Type Name Description

---

string sessionId\ Session ID

string\ product\\productId\ Product ID or SKU

string\ file\ Image file name (e.g.,
/b/l/blackberry8100_2.jpg)

string identifierType Defines whether the product
ID or SKU is passed in the
\'product\' parameter

---

**Returns**:

Type Description

---

boolean\\int True (1) if the image is removed from a product

### [Examples](#catalog_product_attribute_media.remove-Examples)

### [Request Example SOAP V1](#catalog_product_attribute_media.remove-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_product_attribute_media.remove', array('product' => '3', 'file' => '/b/l/blackberry8100_2.jpg'));
var_dump ($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#catalog_product_attribute_media.remove-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductAttributeMediaRemove($sessionId, '3', '/b/l/blackberry8100_2.jpg');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_product_attribute_media.remove-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductAttributeMediaRemove((object)array('sessionId' => $sessionId->result, 'productId' => '3', 'file' => '/b/l/blackberry8100_2.jpg'));

var_dump($result->result);
```

</div>


