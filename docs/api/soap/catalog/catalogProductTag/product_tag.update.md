

### [Module: Tag API](#product_tag.update-Module:TagAPI)

## [Resource: catalog_product_tag](#product_tag.update-Resource:catalogproducttag)

**Aliases**: product_tag

## [Method:](#product_tag.update-Method:)

-   catalog_product_tag.update (SOAP V1)
-   catalogProductTagUpdate (SOAP V2)

Allows you to update information about an existing product tag.

**Arguments:**

Type Name Description

---

string sessionId Session ID
string tagId ID of the tag to be updated
array data Array of catalogProductTagUpdateEntity
string store Store view code or ID (optional; required for WS-I compliance mode)

**Return:**

Type Description

---

boolean True if the product tag is updated

The **catalogProductTagUpdateEntity** content is as follows:

---

Type Name Description

---

string name\ Tag name

string\ status\ Tag status. Can have
the following values:
-1 - Disabled, 0 -
Pending, 1- Approved

string\ base_popularity\ Tag base popularity

---

**Faults:**

Fault Code Fault Message

---

101 Requested store does not exist.
104 Requested tag does not exist.
105 Provided data is invalid.
106 Error while saving tag. Details in error message.

### [Examples](#product_tag.update-Examples)

### [Request Example SOAP V1](#product_tag.update-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_product_tag.update', array('tagId' => '4', 'data' => array('name' => 'digital_1')));
var_dump ($result);
```

</div>



### [Request Example SOAP V2](#product_tag.update-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); 

$sessionId = $proxy->login('apiUser', 'apiKey'); 
 
$result = $proxy->catalogProductTagUpdate($sessionId, '1', array(
'name' => 'tag',
'status' => '1'
));   
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#product_tag.update-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductTagUpdate((object)array('sessionId' => $sessionId->result, 'tagId' => '1', 'store' => '0', 'data' => ((object)array(
'name' => 'tag',
'status' => '1',
'base_popularity' => null
))));
var_dump($result->result);
```

</div>


