

### [Module: Tag API](#product_tag.list-Module:TagAPI)

## [Resource: catalog_product_tag](#product_tag.list-Resource:catalogproducttag)

**Aliases**: product_tag

## [Method:](#product_tag.list-Method:)

-   catalog_product_tag.list (SOAP V1)
-   catalogProductTagList (SOAP V2)

Allows you to retrieve the list of tags for a specific product.

**Arguments:**

Type Name Description

---

string sessionId Session ID
string productId Product ID
string store Store view code or ID

**Return:**

Type Name Description

---

array result Array of catalogProductTagListEntity

The **catalogProductTagListEntity** content is as follows:

---

Type Name Description

---

string tag_id\ Tag ID

string name\ Tag name

---

**Faults:**

Fault Code Fault Message

---

101 Requested store does not exist.
102 Requested product does not exist.

### [Examples](#product_tag.list-Examples)

### [Request Example SOAP V1](#product_tag.list-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_product_tag.list', array('productId' => '4', 'store' => '2'));
var_dump ($result);
```

</div>



### [Request Example SOAP V2](#product_tag.list-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductTagList($sessionId, '4', '2');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#product_tag.list-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductTagList((object)array('sessionId' => $sessionId->result, 'productId' => '4', 'store' => '2'));
var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#product_tag.list-ResponseExampleSOAPV1)

<div>

```
array
  3 =>
    array
      'tag_id' => string '3' (length=1)
      'name' => string 'canon' (length=5)
  4 =>
    array
      'tag_id' => string '4' (length=1)
      'name' => string 'digital' (length=7)
```

</div>


