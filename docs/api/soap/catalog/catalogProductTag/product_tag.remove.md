

### [Module: Tag API](#product_tag.remove-Module:TagAPI)

## [Resource: catalog_product_tag](#product_tag.remove-Resource:catalogproducttag)

**Aliases**: product_tag

## [Method:](#product_tag.remove-Method:)

-   catalog_product_tag.remove (SOAP V1)
-   catalogProductTagRemove (SOAP V2)

Allows you to remove an existing product tag.

**Arguments:**

Type Name Description

---

string sessionId Session ID
string tagId Tag ID

**Return:**

Type Description

---

boolean\\int True (1) if the product tag is removed

**Faults:**

Fault Code Fault Message

---

104 Requested tag does not exist.
107 Error while removing tag. Details in error message.

### [Examples](#product_tag.remove-Examples)

### [Request Example SOAP V1](#product_tag.remove-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_product_tag.remove', '3');
var_dump ($result);
```

</div>



### [Request Example SOAP V2](#product_tag.remove-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductTagRemove($sessionId, '3');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#product_tag.remove-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductTagRemove((object)array('sessionId' => $sessionId->result, 'tagId' => '3'));
var_dump($result->result);
```

</div>


