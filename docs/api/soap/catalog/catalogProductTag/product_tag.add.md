

### [Module: Tag Api](#product_tag.add-Module:TagApi)

## [Resource: catalog_product_tag](#product_tag.add-Resource:catalogproducttag)

**Aliases**: product_tag

## [Method:](#product_tag.add-Method:)

-   catalog_product_tag.add (SOAP V1)
-   catalogProductTagAdd (SOAP V2)

Allows you to add one or more tags to a product.

**Arguments:**

Type Name Description

---

string sessionId Session ID
array data Array of catalogProductTagAddEntity

**Return:**

Type Name Description

---

array result Associative array of added tags with the tag name as a key and the tag ID as a value

The **catalogProductTagAddEntity** content is as follows:

---

Type Name Description

---

string tag\ Tag to be added (can
contain several tags
separated with white
spaces). A tag that
contains several words
should be enclosed in
single quotes.

string\ product_id\ Product ID

string\ customer_id\ Customer ID

string\ store\ Store ID

---

**Faults:**

Fault Code Fault Message

---

101 Requested store does not exist.
102 Requested product does not exist.
103 Requested customer does not exist.
105 Provided data is invalid.
106 Error while saving tag. Details in error message.

### [Examples](#product_tag.add-Examples)

### [Request Example SOAP V1](#product_tag.add-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');
$data = array('product_id' => 2, 'store' => 'default', 'customer_id' => 10002, 'tag' => "First 'Second tag' Third");
echo "Adding Tag... ";
$addResult = $proxy->call(
    $sessionId,
    "product_tag.add",
    array($data)
);
echo ((count($addResult) == 3) ? "Done!" : "Fail!");
echo "<br />";
print_r($addResult);
```

</div>



### [Request Example SOAP V2](#product_tag.add-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login('apiUser', 'apiKey');

$result = $proxy->catalogProductTagAdd($sessionId, array(
'tag' => 'album',
'product_id' => '3',
'customer_id' => '1',
'store' => '0'
));
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#product_tag.add-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductTagAdd((object)array('sessionId' => $sessionId->result, 'data' => ((object)array(
'tag' => 'album',
'product_id' => '3',
'customer_id' => '1',
'store' => '0'
))));
var_dump($result->result);
```

</div>


