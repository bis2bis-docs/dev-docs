

## [Module: Tag](#MAPI-ProductTag-Module:Tag)

Allows you to operate with product tags.

## [Resource: catalog_product_tag](#MAPI-ProductTag-Resource:catalogproducttag)

**Aliases**: product_tag

## [Methods:](#MAPI-ProductTag-Methods:)

-   [product_tag.list](product_tag.list.html "product_tag.list") -
    Retrieve the list of tags by the product ID
-   [product_tag.info](product_tag.info.html "product_tag.info") -
    Retrieve information about a product tag
-   [product_tag.add](product_tag.add.html "product_tag.add") - Add one
    or more tags to a product
-   [product_tag.update](product_tag.update.html "product_tag.update") -
    Update an existing product tag
-   [product_tag.remove](product_tag.remove.html "product_tag.remove") -
    Remove a product tag

## [Faults:](#MAPI-ProductTag-Faults:)

Fault Code Fault Message

---

101 Requested store does not exist.
102 Requested product does not exist.
103 Requested customer does not exist.
104 Requested tag does not exist.
105 Provided data is invalid.
106 Error while saving tag. Details in error message.
107 Error while removing tag. Details in error message.

## [Example:](#MAPI-ProductTag-Example:)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');
$productId = 2;
$customerId = 10002;
$store = 'default';

// Add tags
$data = array('product_id' => $productId, 'store' => $store, 'customer_id' => $customerId, 'tag' => "First 'Second tag' Third");
echo "Adding Tag... ";
$addResult = $proxy->call(
    $sessionId,
    "product_tag.add",
    array($data)
);
echo ((count($addResult) == 3) ? "Done!" : "Fail!");
echo "<br />";
print_r($addResult);
$tagId = reset($addResult);

// Get tag info
echo "<br />Get Tag Info (id = $tagId)... ";
$infoResult = $proxy->call(
    $sessionId,
    "product_tag.info",
    array($tagId, $store)
);
echo "Done!<br />";
print_r($infoResult);

// Update tag data
$data = array('status' => -1, 'base_popularity' => 12, 'name' => 'Changed name');
echo "<br />Update Tag (id = $tagId)... ";
$updateResult = $proxy->call(
    $sessionId,
    "product_tag.update",
    array($tagId, $data, $store)
);
echo ($updateResult ? "Done!" : "Fail!");

// Retrieve list of tags by product
echo "<br />Tag list for product with id = $productId... ";
$listResult = $proxy->call(
    $sessionId,
    "product_tag.list",
    array($productId, $store)
);
echo (count($listResult) ? "Done!" : "Fail!");
echo "<br />";
print_r($listResult);

// Remove existing tag
echo "<br />Remove Tag (id = $tagId)... ";
$removeResult = $proxy->call(
    $sessionId,
    "product_tag.remove",
    array($tagId)
);
echo ($removeResult ? "Done!" : "Fail!");
```

</div>



Create the Magento file system owner
