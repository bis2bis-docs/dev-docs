

### [Module: Tag API](#product_tag.info-Module:TagAPI)

## [Resource: catalog_product_tag](#product_tag.info-Resource:catalogproducttag)

**Aliases**: product_tag

## [Method:](#product_tag.info-Method:)

-   catalog_product_tag.info (SOAP V1)
-   catalogProductTagInfo (SOAP V2)

Allows you to retrieve information about the required product tag.

**Arguments:**

Type Name Description

---

string sessionId Session ID
string tagId Tag ID
string store Store view code or ID

**Return:**

Type Name Description

---

array result Array of catalogProductTagInfoEntity

The **catalogProductTagInfoEntity** content is as follows:

Type Name Description

---

string name Tag name
string status Tag status
string base_popularity Tag base popularity for a specific store
associativeArray products Associative array of tagged products with related product ID as a key and popularity as a value

**Faults:**

Fault Code Fault Message

---

101 Requested store does not exist.
104 Requested tag does not exist.

### [Examples](#product_tag.info-Examples)

### [Request Example SOAP V1](#product_tag.info-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_product_tag.info', array('tagId' => '4', 'store' => '2'));
var_dump ($result);
```

</div>



### [Request Example SOAP V2](#product_tag.info-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductTagInfo($sessionId, '4', '2');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#product_tag.info-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductTagInfo((object)array('sessionId' => $sessionId->result, 'tagId' => '4', 'store' => '2'));
var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#product_tag.info-ResponseExampleSOAPV1)

<div>

```
array
  'status' => string '1' (length=1)
  'name' => string 'digital' (length=7)
  'base_popularity' => int 0
  'products' =>
    array
      1 => string '1' (length=1)
      3 => string '1' (length=1)
      4 => string '1' (length=1)
```

</div>


