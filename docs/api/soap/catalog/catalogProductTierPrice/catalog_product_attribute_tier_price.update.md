

## [Module: Mage_Catalog](#catalog_product_attribute_tier_price.update-Module:MageCatalog)

## [Resource: catalog_product_attribute_tier_price](#catalog_product_attribute_tier_price.update-Resource:catalogproductattributetierprice)

**Aliases:**

-   product_attribute_tier_price
-   product_tier_price

## [Method:](#catalog_product_attribute_tier_price.update-Method:)

-   catalog_product_attribute_tier_price.update (SOAP V1)
-   catalogProductAttributeTierPriceUpdate (SOAP V2)

Allows you to update the product tier prices.

**Aliases:**

-   product_attribute_tier_price.update
-   product_tier_price.update

**Arguments:**

---

Type Name Description

---

string sessionId Session ID

string product\\productId\ Product ID or SKU

array tierPrices\ Array of
catalogProductTierPriceEntity

string identifierType Defines whether the product ID
or SKU is passed in the
\'product\' parameter

---

**Returns**:

Type Name Description

---

boolean\\int result True (1) if the product tier price is updated

The **catalogProductTierPriceEntity** content is as follows:

---

Type Name Description

---

string customer_group_id\ Customer group ID

string website\ Website

int qty\ Quantity of items to
which the price will be
applied

double price\ Price that each item
will cost

---

### [Examples](#catalog_product_attribute_tier_price.update-Examples)

### [Request Example SOAP V1](#catalog_product_attribute_tier_price.update-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$productId = 1;
$tierPrices = array(
    array('customer_group_id' => '0', 'website' => '0', 'qty' => '50', 'price' => '9.90')
);

$result = $proxy->call(
    $sessionId,
    'product_attribute_tier_price.update',
    array(
        $productId,
        $tierPrices
    )
);

var_dump($result);
```

</div>



### [Request Example SOAP V2](#catalog_product_attribute_tier_price.update-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');


$productId = 1;
$tierPrices = array(
    array('customer_group_id' => '0', 'website' => '0', 'qty' => '50', 'price' => '9.90')
);

$result = $proxy->catalogProductAttributeTierPriceUpdate(
    $sessionId,
    $productId,
    $tierPrices
);

var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_product_attribute_tier_price.update-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');
$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));


$productId = 1;
$tierPrices = array(
    array('customer_group_id' => '0', 'website' => '0', 'qty' => '50', 'price' => '9.90')
);

$result = $proxy->catalogProductAttributeTierPriceUpdate((object)array(
'sessionId' => $sessionId->result,
'productId' => $productId,
'tierPrices' => $tierPrices
));

var_dump($result->result);
```

</div>


