

## [Module: Mage_Catalog](#catalog_product_attribute_tier_price.info-Module:MageCatalog)

## [Resource: catalog_product_attribute_tier_price](#catalog_product_attribute_tier_price.info-Resource:catalogproductattributetierprice)

**Aliases:**

-   product_attribute_tier_price
-   product_tier_price

## [Method:](#catalog_product_attribute_tier_price.info-Method:)

-   catalog_product_attribute_tier_price.info (SOAP V1)
-   catalogProductAttributeTierPriceInfo (SOAP V2)

Allows you to retrieve information about product tier prices.

**Aliases:**

-   product_attribute_tier_price.info
-   product_tier_price.info

**Arguments:**

---

Type Name Description

---

string sessionId\ Session ID

string product\\productId\ Product ID or SKU

string identifierType Defines whether the
product ID or SKU is
passed in the
\'product\' parameter

---

**Returns**:

Type Name Description

---

array result Array of catalogProductTierPriceEntity

The **catalogProductTierPriceEntity** content is as follows:

---

Type Name Description

---

string customer_group_id\ Customer group ID

string website\ Website

int qty\ Quantity of items to
which the price will be
applied

double price\ Price that each item
will cost

---

### [Examples](#catalog_product_attribute_tier_price.info-Examples)

### [Request Example SOAP V1](#catalog_product_attribute_tier_price.info-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_product_attribute_tier_price.info', 'productId');
var_dump($result);
```

</div>



### [Request Example SOAP V2](#catalog_product_attribute_tier_price.info-RequestExampleSOAPV2)

<div>

```
$client = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$productId = 1;

$result = $client->catalogProductAttributeTierPriceInfo(
    $session,
    $productId
);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_product_attribute_tier_price.info-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductAttributeTierPriceInfo((object)array('sessionId' => $sessionId->result, 'productId' => '1'));

var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#catalog_product_attribute_tier_price.info-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
    array
      'customer_group_id' => string '1' (length=1)
      'website' => string 'all' (length=3)
      'qty' => string '2.0000' (length=6)
      'price' => string '129.9900' (length=8)
```

</div>



### [Response Example SOAP V2](#catalog_product_attribute_tier_price.info-ResponseExampleSOAPV2)

<div>

```
array
  0 =>
    object(stdClass)[2]
      public 'customer_group_id' => string '0' (length=1)
      public 'website' => string 'all' (length=3)
      public 'qty' => int 5
      public 'price' => float 99
  1 =>
    object(stdClass)[3]
      public 'customer_group_id' => string '0' (length=1)
      public 'website' => string 'all' (length=3)
      public 'qty' => int 10
      public 'price' => float 98
```

</div>


