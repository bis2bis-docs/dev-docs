

## [Module: Mage_Catalog](#MAPI-ProductTierPrice-Module:MageCatalog)

The Mage_Catalog module allows you to manage categories and products.

### [Product Tier Price](#MAPI-ProductTierPrice-ProductTierPrice)

Allows you to retrieve and update product tier prices.

**Resource Name**: catalog_product_attribute_tier_price

**Aliases**:

-   product_attribute_tier_price
-   product_tier_price

**Methods**:

-   [catalog_product_attribute_tier_price.info](catalog_product_attribute_tier_price.info.html "catalog_product_attribute_tier_price.info") -
    Retrieve information about product tier prices
-   [catalog_product_attribute_tier_price.update](catalog_product_attribute_tier_price.update.html "catalog_product_attribute_tier_price.update") -
    Update the product tier prices

### [Faults](#MAPI-ProductTierPrice-Faults)

Fault Code Fault Message

---

100 Product not exists.
101 Invalid data given. Details in error message.
102 Tier prices not updated. Details in error message.

### [Examples](#MAPI-ProductTierPrice-Examples)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

// Get tier prices
$tierPrices = $proxy->call($sessionId, 'product_tier_price.info', 'Sku');

var_dump($tierPrices);

// Add new
$tierPrices[] = array(
    'website'           => 'all',
    'customer_group_id' => 'all',
    'qty'               => 68,
    'price'             => 18.20
);

// Update tier prices
$proxy->call($sessionId, 'product_tier_price.update', array('Sku', $tierPrices));

// Compare values
var_dump($proxy->call($sessionId, 'product_tier_price.info', 'Sku'));

var_dump($tierPrices);
```

</div>



Create the Magento file system owner
