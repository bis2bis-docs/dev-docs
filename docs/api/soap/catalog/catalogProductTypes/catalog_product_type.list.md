

## [Module: Mage_Catalog](#catalog_product_type.list-Module:MageCatalog)

## [Resource: catalog_product_type](#catalog_product_type.list-Resource:catalogproducttype)

**Aliases:**

-   product_type

## [Method:](#catalog_product_type.list-Method:)

-   catalog_product_type.list (SOAP V1)
-   catalogProductTypeList (SOAP V2)

Allows you to retrieve the list of product types.

**Aliases:**

-   product_type.list

**Arguments:**

Type Name Description

---

string sessionId Session ID

**Returns**:

Type Name Description

---

array result Array of catalogProductTypeEntity

The **catalogProductTypeEntity** content is as follows:

Type Name Description

---

string type Product type
string label Product label in the Admin Panel

### [Examples](#catalog_product_type.list-Examples)

### [Request Example SOAP V1](#catalog_product_type.list-RequestExampleSOAPV1)

<div>

```
$client = new SoapClient('http://magentohost/api/soap/?wsdl');

// If somestuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');

$result = $client->call($session, 'catalog_product_type.list');
var_dump($result);

// If you don't need the session anymore
//$client->endSession($session);
```

</div>



### [Request Example SOAP V2](#catalog_product_type.list-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->catalogProductTypeList($sessionId);
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#catalog_product_type.list-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->catalogProductTypeList((object)array('sessionId' => $sessionId->result));
var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#catalog_product_type.list-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
    array
      'type' => string 'simple' (length=6)
      'label' => string 'Simple Product' (length=14)
  1 =>
    array
      'type' => string 'grouped' (length=7)
      'label' => string 'Grouped Product' (length=15)
  2 =>
    array
      'type' => string 'configurable' (length=12)
      'label' => string 'Configurable Product' (length=20)
  3 =>
    array
      'type' => string 'virtual' (length=7)
      'label' => string 'Virtual Product' (length=15)
  4 =>
    array
      'type' => string 'bundle' (length=6)
      'label' => string 'Bundle Product' (length=14)
  5 =>
    array
      'type' => string 'downloadable' (length=12)
      'label' => string 'Downloadable Product' (length=20)
```

</div>


