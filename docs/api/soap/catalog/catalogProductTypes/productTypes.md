

## [Module: Mage_Catalog](#MAPI-ProductTypes-Module:MageCatalog)

The Mage_Catalog module allows you to manage categories and products.

### [Product types](#MAPI-ProductTypes-Producttypes)

Allows you to retrieve product types.

**Resource Name**: catalog_product_type

**Aliases**:

-   product_type

**Methods**:

-   [catalog_product_type.list](catalog_product_type.list.html "catalog_product_type.list") -
    Retrieve the list of product types

### [Examples](#MAPI-ProductTypes-Examples)

### [Example 1. Retrieving the product types](#MAPI-ProductTypes-Example1.Retrievingtheproducttypes)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');
$types = $proxy->call($sessionId, 'product_type.list');

var_dump($types);
```

</div>



Create the Magento file system owner
