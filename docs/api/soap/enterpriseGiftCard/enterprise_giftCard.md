

### [Customer Gift Card](#Enterprise_GiftCard-CustomerGiftCard)

Allows you to retrieve information on a gift card and redeem gift card
amount.

**Resource Name**: giftcard_customer

## [Methods](#Enterprise_GiftCard-CustomerGiftCard-Methods):

-   [giftcard_customer.info](giftCardCustomer/giftcard_customer.info.html "giftcard_customer.info") -
    Receive information about the gift card for a selected customer
-   [giftcard_customer.redeem](giftCardCustomer/giftcard_customer.redeem.html "giftcard_customer.redeem") -
    Redeem amount present on the gift card to the store credit

### [Shopping Cart Gift Card](#Enterprise_GiftCard-ShoppingCartGiftCard)

Allows you to retrieve, add, and remove a gift card from/to a shopping
cart.

**Resource Name**: cart_giftcard

## [Methods](#Enterprise_GiftCard-ShoppingCartGiftCar-Methods):

-   [cart_giftcard.list](cartGiftCard/cart_giftcard.list.html "cart_giftcard.list") -
    Retrieve the list of gift cards used in the shopping cart (quote).
-   [cart_giftcard.add](cartGiftCard/cart_giftcard.add.html "cart_giftcard.add") -
    Add a gift card to a shopping cart (quote).
-   [cart_giftcard.remove](cartGiftCard/cart_giftcard.remove.html "cart_giftcard.remove") -
    Remove a gift card from the shopping cart (quote).

### [Gift Card Account](#Enterprise_GiftCard-GiftCardAccount)

Allows you to create, update, remove, and retrieve information on a gift
card account.

**Resource Name**: giftcard_account

## [Methods](#Enterprise_GiftCard-GiftCardAccount-Methods):

-   [giftcard_account.create](giftCardAccount/giftcard_account.create.html "giftcard_account.create") -
    Create a new gift card
-   [giftcard_account.list](giftCardAccount/giftcard_account.list.html "giftcard_account.list") -
    Get list of available gift cards
-   [giftcard_account.update](giftCardAccount/giftcard_account.update.html "giftcard_account.update") -
    Update a gift card
-   [giftcard_account.info](giftCardAccount/giftcard_account.info.html "giftcard_account.info") -
    Receive full information about selected gift card
-   [giftcard_account.remove](giftCardAccount/giftcard_account.remove.html "giftcard_account.remove") -
    Remove unnecessary gift card
