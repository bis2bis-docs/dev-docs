

### [Module: GiftCard API](#giftcard_customer.redeem-Module:GiftCardAPI)

## [Resource: giftcard_customer](#giftcard_customer.redeem-Resource:giftcardcustomer)

## [Method:](#giftcard_customer.redeem-Method:)

-   giftcard_customer.redeem (SOAP V1)
-   giftcardCustomerRedeem (SOAP V2)

Allows you to redeem amount from a giftcard to the customer store
credit.

**Arguments:**

---

Type Name Description

---

string sessionId\ Session ID

string code Gift card code

string\ customer_id Customer ID

string\ store_id Store view ID

---

**Return:**

Type Description

---

boolean True if the amount is redeemed

**Faults:**

Fault Code Fault Message

---

100 Gift card does not exists.
101 Gift card is not valid.
103 Redemption functionality is disabled.
104 Unable to redeem gift card.

### [Examples](#giftcard_customer.redeem-Examples)

### [Request Example SOAP V1](#giftcard_customer.redeem-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$code = "giftcardcode";
$customerId = 1;
$storeId = 1;

$giftcardInfo = $proxy->call(
    $sessionId,
    "giftcard_customer.redeem",
    array(
         $code,
         $customerId,
         $storeId
    )
);
```

</div>



### [Request Example SOAP V2](#giftcard_customer.redeem-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->giftcardCustomerRedeem($sessionId, 'giftcardcode', '1', '1');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#giftcard_customer.redeem-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->giftcardCustomerRedeem((object)array('sessionId' => $sessionId->result, 'code' => 'giftcardcode', 'customerId' => '1', 'storeId' => '1'));

var_dump($result->result);
```

</div>


