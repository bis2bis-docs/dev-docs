\-\-- layout: m1x\_soap title: Gift Card Customer \-\--

[]{#MAPI-GiftCardCustomer-Module:EnterpriseGiftCard}Module: Enterprise\_GiftCard
----------------------------------------------------------------------------------

## [Resource: giftcard](#MAPI-GiftCardCustomer-Resource:giftcardcustomer)\_customer

## [Methods:](#MAPI-GiftCardCustomer-Methods:)

-   [giftcard\_customer.info](giftcard_customer.info.html "giftcard_customer.info") -
    Receive information about the giftcard for a selected customer
-   [giftcard\_customer.redeem](giftcard_customer.redeem.html "giftcard_customer.redeem") -
    Redeem amount present on the giftcard to the store credit

## [Faults:](#MAPI-GiftCardCustomer-Faults:)

  Fault Code   Fault Message
  ------------ ---------------------------------------
  100          Gift card does not exists.
  101          Gift card is not valid.
  103          Redemption functionality is disabled.
  104          Unable to redeem gift card.

Create the Magento file system owner
