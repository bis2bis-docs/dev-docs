

### [Module: GiftCard API](#giftcard_customer.info-Module:GiftCardAPI)

## [Resource: giftcard_customer](#giftcard_customer.info-Resource:giftcardcustomer)

## [Method:](#giftcard_customer.info-Method:)

-   giftcard_customer.info (SOAP V1)
-   giftcardCustomerInfo (SOAP V2)

Allows you to receive information about the gift card for a selected
customer.

**Arguments:**

Type Name Description

---

string sessionId Session ID
string code Gift card code

**Return:**

Type Name Description

---

array result Array of giftcardCustomerEntity

The **giftcardCustomerEntity** content is as follows:

---

Type Name Description

---

double balance\ Gift card balance

string expire_date\ Gift card expiration
date in the YYYY-MM-DD
format

---

**Faults:**

Fault Code Fault Message

---

100 Gift card does not exists.
101 Gift card is not valid.

### [Examples](#giftcard_customer.info-Examples)

### [Request Example SOAP V1](#giftcard_customer.info-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$code = "code";

$giftcardInfo = $proxy->call(
    $sessionId,
    "giftcard_customer.info",
    array(
         $code
    )
);
```

</div>



### [Request Example SOAP V2](#giftcard_customer.info-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->giftcardCustomerInfo($sessionId, 'code');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#giftcard_customer.info-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->giftcardCustomerInfo((object)array('sessionId' => $sessionId->result, 'code' => 'code'));

var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#giftcard_customer.info-ResponseExampleSOAPV1)

<div>

```
array
  'balance' => string '500.0000' (length=8)
  'expire_date' => string '2012-04-25' (length=10)
```

</div>


