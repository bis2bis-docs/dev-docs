

### [Module: GiftCard API](#giftcard_account.info-Module:GiftCardAPI)

## [Resource: giftcard_account](#giftcard_account.info-Resource:giftcardaccount)

## [Method:](#giftcard_account.info-Method:)

-   giftcard_account.info (SOAP V1)
-   giftcardAccountInfo (SOAP V2)

Allows you to receive full information about the selected gift card
account.

**Arguments:**

Type Name Description

---

string sessionId Session ID
string giftcardAccountId Gift card account ID

**Return:**

Type Name Description

---

array result Array of giftcardAccountEntity

The **giftcardAccountEntity** content is as follows:

---

Type Name Description

---

int giftcard_id Gift card account ID

string code\ Gift card account code

int store_id\ Gift card account store ID

string date_created\ Creation date in the YYYY-MM-DD
format

string expire_date\ Expiration date in the YYYY-MM-DD
format

int is_active\ Defines whether the gift card
account is active

int is_redeemable\ Defines whether the gift card
account is redeemable

string status\ Gift card account status

double balance\ Gift card account initial balance

array history\ Array
of giftcardAccountEntityHistory

---

The **giftcardAccountEntityHistory** content is as follows:

---

Type Name Description

---

int record_id\ Record ID

string date\ Date

string action\ History action

double balance_delta\ Balance delta

double balance\ Balance

string info\ Record comment

---

**Faults:**

Fault Code Fault Message

---

100 Gift card does not exists.

### [Examples](#giftcard_account.info-Examples)

### [Request Example SOAP V1](#giftcard_account.info-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$giftcardId = 1;
$giftcardInfo = $proxy->call(
    $sessionId,
    "giftcard_account.info",
    array(
         $giftcardId
    )
);
```

</div>



### [Request Example SOAP V2](#giftcard_account.info-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->giftcardAccountInfo($sessionId, '1');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#giftcard_account.info-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->giftcardAccountInfo((object)array('sessionId' => $sessionId->result, 'giftcardAccountId' => '1'));

var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#giftcard_account.info-ResponseExampleSOAPV1)

<div>

```
array
  'giftcard_id' => string '1' (length=1)
  'code' => string '02DQ9T1K0Y0V' (length=12)
  'store_id' => string '0' (length=1)
  'date_created' => string '2012-04-06' (length=10)
  'expire_date' => null
  'is_active' => string '0' (length=1)
  'status' => string 'Available' (length=9)
  'balance' => string '0.0000' (length=6)
  'is_redeemable' => string '1' (length=1)
  'history' =>
    array
      0 =>
        array
          'record_id' => string '1' (length=1)
          'date' => string '2012-04-06 14:31:57' (length=19)
          'action' => string 'Created' (length=7)
          'balance_delta' => string '0.0000' (length=6)
          'balance' => string '0.0000' (length=6)
          'info' => null
```

</div>


