

### [Module: GiftCard API](#giftcard_account.update-Module:GiftCardAPI)

## [Resource: giftcard_account](#giftcard_account.update-Resource:giftcardaccount)

## [Method:](#giftcard_account.update-Method:)

-   giftcard_account.update (SOAP V1)
-   giftcardAccountUpdate (SOAP V2)

Allows you to update the required gift card account.

**Arguments:**

Type Name Description

---

string sessionId Session ID
string giftcardAccountId Gift card account ID
array giftcardData Array of giftcardAccountEntityToUpdate

**Return:**

Type Description

---

boolean True is the gift card account is updated

The **giftcardAccountEntityToUpdate** content is as follows:

---

Type Name Description

---

boolean is_active\ Defines whether the
gift card account is
active

boolean is_redeemable\ Defines whether the
gift card account is
redeemable

int store_id\ Store view ID

double balance\ Gift card account
initial balance

string date_expires\ Expiration date in the
YYYY-MM-DD format

---

**Faults:**

Fault Code Fault Message

---

102 Unable to save data.

### [Examples](#giftcard_account.update-Examples)

### [Request Example SOAP V1](#giftcard_account.update-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$giftcardId = 1;
$giftcardToUpdate = array(
    "balance" => 700
);

$result = $proxy->call(
    $sessionId,
    "giftcard_account.update",
    array(
         $giftcardId,
         $giftcardToUpdate
    )
);
```

</div>



### [Request Example SOAP V2](#giftcard_account.update-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); 

$sessionId = $proxy->login('apiUser', 'apiKey'); 
 
$result = $proxy->giftcardAccountUpdate($sessionId, '3', array(
'date_expires' => null, 
'store_id' => '2', 
'balance' => '700', 
'is_active' => TRUE, 
'is_redeemable' => TRUE));   

var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#giftcard_account.update-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->giftcardAccountUpdate((object)array('sessionId' => $sessionId->result, 'giftcardAccountId' => '3', 'giftcardData' => array(
'balance' => '700',
'is_active' => TRUE,
'is_redeemable' => TRUE,
'store_id' => '2',
'date_expires' => null)));

var_dump($result->result);
```

</div>


