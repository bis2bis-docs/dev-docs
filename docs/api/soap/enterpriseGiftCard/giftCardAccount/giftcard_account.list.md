

### [Module: GiftCard API](#giftcard_account.list-Module:GiftCardAPI)

## [Resource: giftcard_account](#giftcard_account.list-Resource:giftcardaccount)

## [Method:](#giftcard_account.list-Method:)

-   giftcard_account.list (SOAP V1)
-   giftcardAccountList (SOAP V2)

Allows you to retrieve the list of available gift card accounts.

**Arguments:**

Type Name Description

---

string sessionId Session ID
array filters Array of filters (optional)

**Return:**

Type Name Description

---

array result Array of giftcardAccountEntity

The **giftcardAccountEntity** content is as follows:

---

Type Name Description

---

int giftcard_id\ Gift card ID

string code\ Gift card code

int store_id\ Gift card store ID

string date_created\ Creation date in the
YYYY-MM-DD format

string expire_date\ Expiration date in the
YYYY-MM-DD format

int is_active\ Defines whether the
gift card is active

string status\ Gift card status

double balance\ Gift card balance

---

**Faults:**

Fault Code Fault Message

---

101 Invalid filters specified. Details in error message.

### [Examples](#giftcard_account.list-Examples)

### [Request Example SOAP V1](#giftcard_account.list-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$giftcardList = $proxy->call(
    $sessionId,
    "giftcard_account.list"
);
```

</div>



## [Request Example SOAP V2 (List of All Gift Card Accounts)](#giftcard_account.list-RequestExampleSOAPV2%28ListofAllGiftCardAccounts%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->giftcardAccountList($sessionId);
var_dump($result);
```

</div>



## [Request Example SOAP V2 (Complex Filter)](#giftcard_account.list-RequestExampleSOAPV2%28ComplexFilter%29)

<div>

```
$client = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

// If some stuff requires API authentication,
// then get a session token
$session = $client->login('apiUser', 'apiKey');
$complexFilter = array(
    'complex_filter' => array(
        array(
            'key' => 'state',
            'value' => array('key' => 'in', 'value' => '2,1')
        )
    )
);
$result = $client->giftcardAccountList($session, $complexFilter);

var_dump ($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#giftcard_account.list-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->giftcardAccountList((object)array('sessionId' => $sessionId->result, 'filters' => null));

var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#giftcard_account.list-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
    array
      'giftcard_id' => string '1' (length=1)
      'code' => string 'code' (length=12)
      'store_id' => string '2' (length=1)
      'date_created' => string '2012-04-06' (length=10)
      'expire_date' => string '2012-04-22' (length=10)
      'is_active' => string '0' (length=1)
      'status' => string 'Available' (length=9)
      'balance' => string '300.0000' (length=8)
  1 =>
    array
      'giftcard_id' => string '2' (length=1)
      'code' => string 'code' (length=12)
      'store_id' => string '2' (length=1)
      'date_created' => string '2012-04-09' (length=10)
      'expire_date' => string '2012-04-25' (length=10)
      'is_active' => string '1' (length=1)
      'status' => string 'Available' (length=9)
      'balance' => string '500.0000' (length=8)
```

</div>


