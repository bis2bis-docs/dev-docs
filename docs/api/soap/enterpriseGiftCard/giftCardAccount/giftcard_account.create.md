

### [Module: GiftCard API](#giftcard_account.create-Module:GiftCardAPI)

## [Resource: giftcard_account](#giftcard_account.create-Resource:giftcardaccount)

## [Method:](#giftcard_account.create-Method:)

-   giftcard_account.create (SOAP V1)
-   giftcardAccountCreate (SOAP V2)

Allows you to create a new gift card account.

**Arguments:**

Type Name Description

---

string sessionId Session ID
array giftcardAccountData Array of giftcardAccountCreateGiftcardAccountData
array notificationData Array of giftcardAccountCreateNotificationData (optional)

**Return:**

Type Description

---

string ID of the created gift card account

The **giftcardAccountCreateGiftcardAccountData** content is as follows:

---

Type Name Description

---

string status\ Gift card status:
available, used,
redeemed, expired

string\ date_expires\ Gift card expiration
date in the YYYY-MM-DD
format

string\ website_id\ Gift card website ID

string\ balance\ Initial gift card
balance

string\ state\ State: active or not
active

string\ is_redeemable\ Defines whether the
gift card is redeemable

---

The **giftcardAccountCreateNotificationData** content is as follows:

---

Type Name Description

---

string recipient_name\ Recipient name

string recipient_email\ Recipient email address

string recipient_store\ Recipient store

---

**Faults:**

Fault Code Fault Message

---

105 Provided gift card account data is invalid.
104 Provided email notification data is invalid

### [Examples](#giftcard_account.create-Examples)

### [Request Example SOAP V1](#giftcard_account.create-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$giftcardToCreate = array(
    "status" => 'available',
    "is_redeemable" => 1,
    "balance" => 200,
    "website_id" => 2,
    "date_expires" => null
);

$giftcardId = $proxy->call(
    $sessionId,
    "giftcard_account.create",
    array(
         $giftcardToCreate
    )
);
```

</div>



### [Request Example SOAP V2](#giftcard_account.create-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login('apiUser', 'apiKey');

$result = $proxy->giftcardAccountCreate($sessionId, array(
'status' => 'available',
'date_expires' => null,
'website_id' => '2',
'balance' => '200',
'state' => '1',
'is_redeemable' => '1'),
array(
'recipient_name' => 'name',
'recipient_email' => 'email',
'recipient_store' => null)
);

var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#giftcard_account.create-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->giftcardAccountCreate((object)array('sessionId' => $sessionId->result, 'giftcardAccountData' => array(
'status' => 'available',
'date_expires' => null,
'website_id' => '2',
'balance' => '200',
'state' => '1',
'is_redeemable' => '1'),
'notificationData' => array(
'recipient_name' => 'name',
'recipient_email' => 'email',
'recipient_store' => null)
));

var_dump($result->result);
```

</div>


