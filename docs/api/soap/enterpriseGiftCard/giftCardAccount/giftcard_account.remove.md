

### [Module: GiftCard API](#giftcard_account.remove-Module:GiftCardAPI)

## [Resource: giftcard_account](#giftcard_account.remove-Resource:giftcardaccount)

## [Method:](#giftcard_account.remove-Method:)

-   giftcard_account.remove (SOAP V1)
-   giftcardAccountRemove (SOAP V2)

Allows you to remove the specified gift card account.

**Arguments:**

---

Type Name Description

---

string sessionId\ Session ID

string giftcardAccountId Gift card account ID

---

**Return:**

Type Description

---

boolean True if the gift card account is removed

**Faults:**

Fault Code Fault Message

---

106 Gift card account with requested id does not exist
107 Error occurs while deleting gift card

### [Examples](#giftcard_account.remove-Examples)

### [Request Example SOAP V1](#giftcard_account.remove-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$giftcardId = 2;
$result = $proxy->call(
    $sessionId,
    "giftcard_account.remove",
    array(
         $giftcardId
    )
);
```

</div>



### [Request Example SOAP V2](#giftcard_account.remove-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->giftcardAccountRemove($sessionId, '2');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#giftcard_account.remove-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->giftcardAccountRemove((object)array('sessionId' => $sessionId->result, 'giftcardAccountId' => '2'));

var_dump($result->result);
```

</div>


