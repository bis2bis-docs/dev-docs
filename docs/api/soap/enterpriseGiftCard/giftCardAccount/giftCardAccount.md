\-\-- layout: m1x\_soap title: Gift Card Account \-\--

[]{#MAPI-GiftCardAccount-Module:EnterpriseGiftCardAccount}Module: Enterprise\_GiftCardAccount
-----------------------------------------------------------------------------------------------

Allows you to operate with giftcards.

## [Resource: giftcard](#MAPI-GiftCardAccount-Resource:giftcardaccount)\_account

## [Methods:](#MAPI-GiftCardAccount-Methods:)

-   [giftcard\_account.create](giftcard_account.create.html "giftcard_account.create") -
    Create a new giftcard
-   [giftcard\_account.list](giftcard_account.list.html "giftcard_account.list") -
    Get list of available giftcards
-   [giftcard\_account.update](giftcard_account.update.html "giftcard_account.update") -
    Update a giftcard
-   [giftcard\_account.info](giftcard_account.info.html "giftcard_account.info") -
    Receive full information about selected giftcard
-   [giftcard\_account.remove](giftcard_account.remove.html "giftcard_account.remove") -
    Remove unnecessary giftcard

## [Faults:](#MAPI-GiftCardAccount-Faults:)

  Fault Code   Fault Message
  ------------ ------------------------------------------------------
  100          Gift card does not exists.
  101          Invalid filters specified. Details in error message.
  102          Unable to save data.
  104          Provided email notification data is invalid
  105          Provided gift card account data is invalid
  106          Gift card account with requested id does not exist
  107          Error occurs while deleting gift card

Create the Magento file system owner
