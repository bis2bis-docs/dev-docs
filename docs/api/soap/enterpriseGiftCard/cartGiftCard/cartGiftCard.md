\-\-- layout: m1x\_soap title: Cart Gift Card \-\--

[]{#MAPI-ShoppingCartGiftCard-Module:EnterpriseGiftCard}Module: Enterprise\_GiftCard
--------------------------------------------------------------------------------------

## [Resource: cart](#MAPI-ShoppingCartGiftCard-Resource:cartgiftcard)\_giftcard

## [Methods:](#MAPI-ShoppingCartGiftCard-Methods:)

-   [cart\_giftcard.list](cart_giftcard.list.html "cart_giftcard.list") -
    Retrieve the list of giftcards used in the shopping cart (quote).
-   [cart\_giftcard.add](cart_giftcard.add.html "cart_giftcard.add") -
    Add a giftcard to a shopping cart (quote).
-   [cart\_giftcard.remove](cart_giftcard.remove.html "cart_giftcard.remove") -
    Remove a giftcard from the shopping cart (quote).

## [Faults:](#MAPI-ShoppingCartGiftCard-Faults:)

  Fault Code   Fault Message
  ------------ ------------------------------------------------------
  1001         Can not make operation because store is not exists
  1002         Can not make operation because quote is not exists
  1003         Provided data is invalid
  1004         Gift card account with requested code does not exist
  1005         Error happened while adding gift card to quote

Create the Magento file system owner
