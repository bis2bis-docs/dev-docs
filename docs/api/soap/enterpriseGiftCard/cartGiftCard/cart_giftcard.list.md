

### [Module: GiftCard API](#cart_giftcard.list-Module:GiftCardAPI)

## [Resource: cart_giftcard](#cart_giftcard.list-Resource:cartgiftcard)

## [Method:](#cart_giftcard.list-Method:)

-   cart_giftcard.list (SOAP V1)
-   shoppingCartGiftcardList (SOAP V2)

Allows you to retrieve the list of gift cards used in the shopping cart
(quote).

**Arguments:**

Type Name Description

---

string sessionId Session ID
string quoteId Shopping cart ID (quote ID)
string storeId Store ID (optional)

**Return:**

Type Name Description

---

array result Array of enterpriseGiftcardaccountListEntity

The **enterpriseGiftcardaccountListEntity** content is as follows:

---

Type Name Description

---

string giftcardaccount_id Gift card account ID

string\ code\ Gift card code

string\ used_amount\ Used amount (in store
currency)

string\ base_amount\ Base amount (in base
currency)

---

**Faults:**

Fault Code Fault Message

---

1001 Can not make operation because store is not exists
1002 Can not make operation because quote is not exists

### [Examples](#cart_giftcard.list-Examples)

### [Request Example SOAP V1](#cart_giftcard.list-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$quoteId = 15;

$giftcardList = $proxy->call(
    $sessionId,
    "cart_giftcard.list",
    array(
         $quoteId
    )
);
```

</div>



### [Request Example SOAP V2](#cart_giftcard.list-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->shoppingCartGiftcardList($sessionId, '15');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#cart_giftcard.list-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->shoppingCartGiftcardList((object)array('sessionId' => $sessionId->result, 'quoteId' => '15', 'storeId' => '3'));

var_dump($result->result);
```

</div>



### [Response Example SOAP V1](#cart_giftcard.list-ResponseExampleSOAPV1)

<div>

```
array
  0 =>
    array
      'giftcardaccount_id' => string '5' (length=1)
      'code' => string 'code' (length=12)
      'used_amount' => float 141.34
      'base_amount' => string '200.0000' (length=8)
  1 =>
    array
      'giftcardaccount_id' => string '6' (length=1)
      'code' => string 'code' (length=12)
      'used_amount' => float 35.34
      'base_amount' => string '50.0000' (length=7)
```

</div>


