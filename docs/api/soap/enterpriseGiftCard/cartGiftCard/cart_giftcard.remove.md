

### [Module: GiftCard API](#cart_giftcard.remove-Module:GiftCardAPI)

## [Resource: cart_giftcard](#cart_giftcard.remove-Resource:cartgiftcard)

## [Method:](#cart_giftcard.remove-Method:)

-   cart_giftcard.remove (SOAP V1)
-   shoppingCartGiftcardRemove (SOAP V2)

Allows you to remove a gift card from the shopping cart (quote).

**Arguments:**

---

Type Name Description

---

string sessionId Session ID

string giftcardAccountCode Gift card account code

string\ quoteId Shopping cart ID (quote
ID)

string\ storeId Store ID (optional)

---

**Return:**

Type Description

---

boolean True if the gift card is removed from the shopping cart

**Faults:**

Fault Code Fault Message

---

1001 Can not make operation because store is not exists
1002 Can not make operation because quote is not exists
1004 Gift card account with requested code does not exist
1005 Error happened while adding gift card to quote

### [Examples](#cart_giftcard.remove-Examples)

### [Request Example SOAP V1](#cart_giftcard.remove-RequestExampleSOAPV1)

<div>

```
$proxy = new SoapClient('http://magentohost/api/soap/?wsdl');
$sessionId = $proxy->login('apiUser', 'apiKey');

$code = "giftcardAccountCode";
$quoteId = 15;

$giftcardList = $proxy->call(
    $sessionId,
    "cart_giftcard.remove",
    array(
         $code,
         $quoteId
    )
);
```

</div>



### [Request Example SOAP V2](#cart_giftcard.remove-RequestExampleSOAPV2)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

$result = $proxy->shoppingCartGiftcardRemove($sessionId, 'giftcardAccountCode', '15');
var_dump($result);
```

</div>



## [Request Example SOAP V2 (WS-I Compliance Mode)](#cart_giftcard.remove-RequestExampleSOAPV2%28WSIComplianceMode%29)

<div>

```
$proxy = new SoapClient('http://magentohost/api/v2_soap/?wsdl');

$sessionId = $proxy->login((object)array('username' => 'apiUser', 'apiKey' => 'apiKey'));

$result = $proxy->shoppingCartGiftcardRemove((object)array('sessionId' => $sessionId->result, 'giftcardAccountCode' => 'giftcardAccountCode', 'quoteId' => '15', 'storeId' => '3'));

var_dump($result->result);
```

</div>


