

### WS-I Compliance Mode

Magento provides you with the ability to use two modes for SOAP API V2.
These are with WS-I compliance mode enabled and WS-I compliance mode
disabled. The first one was introduced to make the system flexible,
namely, to increase compatibility with .NET and Java programming
languages.

To enable/disable the WS-I compliance mode, perform the following steps:

1.  In the Magento Admin Panel, go to **System \> Configuration \>
    Magento Core API**.
2.  In the WS-I Compliance drop-down, select **Yes** to enable the WS-I
    compliance mode and **No** to disable the WS-I compliance mode,
    correspondingly.
    ![](~@assets/soap_wsi-config.png)

The WS-I compliant mode uses the same WSDL endpoint as SOAP API V2 does.
The key difference is that XML namespaces are used in WS-I compliance
mode.

WSDL file with disabled WS-I compliance mode:
(~@assets/soap_wsdl.png)

WSDL file with enabled WS-I compliance mode:
![](~@assets/soap_wsdl-wsi.png)
