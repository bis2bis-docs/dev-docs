const path = require("path");
const glob = require("glob");
const sidebar = require("vuepress-auto-sidebar");

module.exports = {
    configureWebpack: {
        resolve: {
            alias: {
                "@assets": path.resolve(__dirname, "../images")
            }
        }
    },
    dest: path.resolve(__dirname, "../../public"),
    base: '/dev-docs/',
    theme: "yuu",
    themeConfig: {
        yuu: {
            defaultColorTheme: "red",
            colorThemes: ["red"],
            disableDarkTheme: false,
            defaultDarkTheme: false,
            disableThemeIgnore: true
        },
        searchPlaceholder: "Search...",
        nav: [
            {
                text: "Home",
                link: "/"
            },

            {
                text: "Contact",
                link: "https://gitlab.com/macoaureb2b/dev-docs/-/issues"
            }
        ]
    }
};
